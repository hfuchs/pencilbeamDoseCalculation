reset
a=838.3207
b=5.53017
c=0.6


#150.00000 0.30000 83.83207 5.53017


set xrange [0:200]
#set yrange [0:2]

f(x)=1.0+c/(1+exp(a-b*x))

#plot f(x)
#,\
#LUTProton_1_5MeV_10102013/OneDim_Edep_150.dat u 1:2


#plot \
'LUTProton_1_5MeV_10102013/OneDim_Edep_150.dat' u 1:($2*f($1)) w lp,\
'LUTProton_1_5MeV_10102013/OneDim_Edep_150.dat' u 1:2 w lp,\

plot 'LUTProton_1_5MeV_10102013/OneDim_Edep_150.dat' u 1:(f($1)) w lp
