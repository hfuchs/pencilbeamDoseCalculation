reset

#set yrange[0:1.3]
set yrange[0:1.5]
set ylabel "Normalized Energy Depositition [a.u.]"
#set xrange[0:400]
set xlabel "Penetration Depth [cm]"
unset logscale x
unset logscale y
set mxtics
set mytics
set grid xtics ytics

set key top left

plot './EdepProton.dat' using ($1/10):($2) with line lw 3 linecolor rgb "red"  title 'Proton Phys. Dose'

maxProt=GPVAL_DATA_Y_MAX

plot './EdepHelium.dat' using ($1/10):($2) with line lw 3 linecolor rgb "red"  title 'Helium Phys. Dose'

maxHel=GPVAL_DATA_Y_MAX


#set term x11
set terminal postscript eps enhanced color solid "Helvetica,20"
set output "ReportRBE.eps"

#set terminal png
#set output "ReportRBE.png"



set style line 1 lt 0 lw 3 pt 3 linecolor rgb "red"
set style line 2 lt 1 lw 3 pt 3 linecolor rgb "blue"
set style line 3 lt 0 lw 3 pt 3 linecolor rgb "green"
set style line 4 lt 1 lw 3 pt 3 linecolor rgb "brown"


plot './EdepProton.dat' using ($1/10):($2/maxProt) w l ls 1  title 'Proton Phys. Dose',\
     './EdepProton.dat' using ($1/10):($3/maxProt) w l ls 2  title 'Proton Biol. Dose',\
     './EdepHelium.dat' using ($1/10):($2/maxHel)  w l ls 3  title 'Helium Phys. Dose',\
     './EdepHelium.dat' using ($1/10):($3/maxHel)  w l ls 4  title 'Helium Biol. Dose'

