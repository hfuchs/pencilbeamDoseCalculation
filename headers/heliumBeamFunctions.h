/* 
 * File:   pencilBeamHeaders.h
 * Author: main
 *
 * Created on July 1, 2011, 10:25 AM
 */

#ifndef HELIUMBEAMFUNCTIONSHEADERS_H
#define	HELIUMBEAMFUNCTIONSHEADERS_H

#include <systemAndRootHeaders.h>
#include <LUT_class.h>
#include <nucLUT_class.h>
#include <input_class.h>
#include <beamDecomposition_class.h>

using namespace std;

//------------------------------------------------------------------------------
//---------FUNCTION DEFINITIONS
//------------------------------------------------------------------------------
Double_t myPower(Double_t x, Double_t y);
Double_t getScatteringAngleSquare(Double_t stepSize, Double_t energy2, Double_t energy, Double_t radiationLength);
void createFluenceArray(nucLUT &nucCorrTable, Double_t **fluenceArray, Double_t x0, Double_t y0, Double_t beamSigmaX,Double_t beamSigmaY, 
        Int_t dimensionX, Int_t dimensionY,Int_t offsetX,Int_t offsetY,Double_t stepSize, 
        Double_t initFluence, Double_t initbeamSigmaX, Double_t initbeamSigmaY, Double_t currentDepth);
Double_t getInDepthDoseDepositionFactorWithCorrections(Double_t meanRange, Double_t currRange, Double_t density, Double_t parabolicZylinderFunction1, Double_t parabolicZylinderFunction2);
Double_t getSummedScattAngle(Double_t *scatAngleSquaredArray,Double_t stepSize,Int_t maxIndex);
Double_t relativisticBeta(Double_t energy, Double_t mass);
Double_t calculateStoppingPower(Double_t energy,Double_t atomicZARatio, Double_t excitationEnergy, Double_t particleMass);
Double_t getStoppingPowerRatioToWater(Double_t stoppingPowerWater,Double_t stoppingPowerMaterial);
void singlePencilBeamAlgorithm(LUT &lookUpTable,nucLUT &nucCorrTable, inputClass &myInputClass, Double_t **outputArray,Double_t **initFluenceArray,
        Double_t &integratedDepth,Double_t *correctedStepSizeArray, Double_t *scatAngleSquaredArray,Double_t densityMaterial,
        Double_t radiationLengthMaterial,Double_t excitationEnergyMaterial, Double_t materialAtomZARatio, Double_t initEnergy, 
        Double_t stepSize, Double_t beamInitSigmaX,Double_t beamInitSigmaY,Double_t initFluence,Int_t currDepthStep, 
        Double_t x0, Double_t y0, Int_t dimensionX, Int_t dimensionY,Int_t offsetX,Int_t offsetY);

////decomposes the initial gauss distributed beam in subBeams
////spread is the maximum distance from the original beam axis in times of sigma 
//Int_t beamDecomposition(Double_t **beamDataArray, Double_t initSigmaX,Double_t initSigmaY,Double_t initFluence,
//        Double_t x0, Double_t y0, Int_t numOfSubBeamsOneDim,Double_t initEnergy);
Int_t beamDecomposition(Double_t **beamDataArray, Double_t **beamShapeDataArray, Int_t NumDataPointsInArray, Double_t stepSizeOfDataPoints, Double_t initFWHMX,Double_t initFWHMY,Double_t initFluence,
        Double_t x0, Double_t y0, Int_t numOfSubBeamsOneDim,Double_t initEnergy);

void createBeamShapeArray(Double_t* initialBeamShapeDataArray,Int_t NumDataPointsInArray,Double_t stepSizeOfDataPoints,Double_t FWHM, Double_t Center);




//------------------------------------------------------------------------------
//---------END OF FUNCTION DEFINITIONS
//------------------------------------------------------------------------------


#endif	/* HELIUMBEAMFUNCTIONSHEADERS_H */

