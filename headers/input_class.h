/* 
 * File:   pencilBeamHeaders.h
 * Author: main
 *
 * Created on July 1, 2011, 10:25 AM
 */

#ifndef LUTHEADERS_H
#define	LUTHEADERS_H

#include <interpol.h>
#include <systemAndRootHeaders.h>

#include <string.h>

using namespace std;

//------------------------------------------------------------------------------------------------
//------------------------CLASS DEFINITIONS
//------------------------------------------------------------------------------------------------ 
//inputClass is intended to read in and provide geometry data
//later on perhaps CT images
class inputClass
{
private:
    Int_t numberOfHUDataElements;
    
    //Arrays
    Char_t dataFilename[1000];
    Double_t **huDataArray;
    Double_t *huOnlyArray;
    interpol* densityLUT;
    

public:
    inputClass();
    ~inputClass();
    inputClass(const inputClass& orig);

    void setDataFileName(char* filename);
    Int_t readInDataFromFile(char* filename,Double_t** dataArray,Int_t numbOfLinesToRead);
    Int_t readInDataArrayReturnSorted(char* filename,Int_t huDataElements, Double_t** dataArray);
    Int_t countRowsInFile(char* filename);
    void readInDataFile();
    void getOneMaterialPropertiesFromHU(Double_t dHUnit, Double_t &densityMaterial, Double_t &radiationLengthMaterial, 
                                          Double_t &excitationEnergyMaterial, Double_t &materialZARatio);
    void getMaterialDensityFromHU(Double_t dHUnit, Double_t &densityMaterial);  
    int getNearestElementIndex(int numberOfElements,double* dataArray, double searchValue);

};//end of class




//------------------------------------------------------------------------------
//---------END OF CLASS DEFINITIONS
//------------------------------------------------------------------------------


#endif	/* LUTHEADERS_H */

