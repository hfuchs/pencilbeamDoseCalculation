/* 
 * File:   optimizerRBERBE_class.h
 * Author: hfuchs
 *
 * Created on July 1, 2013, 9:08 AM
 */

#ifndef OPTIMIZERRBE_CLASS_H
#define	OPTIMIZERRBE_CLASS_H

#include <systemAndRootHeaders.h>

#include <string>
#include <sstream>
#include <vector>
#include <algorithm>

#include "TMinuit.h"
#include "Math/Minimizer.h"
#include "Math/Factory.h"
#include "Math/Functor.h"
#include "TRandom2.h"
#include "TError.h"

using namespace std;

class optimizerRBE {
public:
    Double_t initialBeamEnergy;
    LUT* lookUpTable;
    std::vector<Double_t> positionVector;
    std::vector<Double_t> rbeTargetValuesVector;
    
    optimizerRBE();
    optimizerRBE(LUT* look);
    virtual ~optimizerRBE();
    void optimizeSOBP(std::vector<Double_t>& output);
    Double_t costFunction(const Double_t *par);

//    lookUpTable= new LUT;
    Double_t setBeamEnergy(Double_t beamEnergy);

};

#endif	/* OPTIMIZERRBE_CLASS_H */
