/* 
 * File:   heliumBeam.h
 * Author: main
 *
 * Created on 14 February 2012, 09:06
 */

#ifndef HELIUMBEAM_H
#define	HELIUMBEAM_H

#include <systemAndRootHeaders.h>
#include <heliumBeamFunctions.h>
int heliumBeam();

#endif	/* HELIUMBEAM_H */

