/* 
 * File:   LUTclass.h
 * Author: main
 *
 * Created on 13 February 2012, 15:09
 */

#ifndef LUTCLASS_H
#define	LUTCLASS_H

#include <systemAndRootHeaders.h>
#include <string.h>
using namespace std;

//LUT class is responsible for reading in, processing and providing look-up table data
class LUT 
{
private:    
    const static Int_t maxNumberOfCharsToRead=10000;//maximum chars per LUT table line
    const static Int_t energyArraySize=100;//defines the size of the energy array
    const static Int_t filenameTotalSize=3000;//defines the size of the total filename including path
    const static Int_t filenameComponentSize=1000;//defines the size of the filename components
    
    Double_t targetEnergy;//initial beam energy
    Double_t stepSize;//step size of the data files in mm
    Double_t stepSizeCalculation;//Step size of the requesting calculation in cm
    Double_t totalDepositedEnergy;//energy deposited up to now
    Double_t maxDepositedEnergyAtCurrentEnergy;//contains the maximum energy deposited in current LUT
    Double_t correctionFactorToCM;//factor by which the values have to be multiplied to get cm
    Double_t rangeOfEnergy;//the total range of this energy
                                //All values sent to and read from class are in cm
    Int_t iDimensionOfArray;//number of z entries per energy
    Int_t numbOfParticlesInSource;//number of beam particles in LUT files
    Int_t numbOfBeamParticles;//number of particles in current beam
    
    Double_t **lutArray;//contains depth and energy deposition in water
    Double_t **remainingEnergyArray;//contains depth and remaining energy of particle
    Double_t **penetrationDepthArray;//contains energy and penetration depth in water    
    Double_t energyAvailArray[energyArraySize];
    Double_t ***rawDataArray;
    Char_t folder[filenameComponentSize],configFileName[filenameComponentSize], dataFilePrefix[filenameComponentSize];
    
    //values necessary for RBE calculation
    Double_t percent40EdepPosition;
    Double_t percent50EdepPosition;
    Double_t percent60EdepPosition;  
    Double_t percent70EdepPosition; 
    Double_t percent100EdepPosition;
    Double_t diff40to50PercentEdep;
    Double_t diff50to60PercentEdep;
    Double_t diff60to70PercentEdep;
    Double_t edepAt40Pos;
    Double_t edepAt50Pos;
    Double_t edepAt60Pos;
    Int_t whichParticle; //proton=0, helium=1
public:    
    ROOT::Math::Interpolator *interpolEdep;
    
public:
        LUT();
        LUT(Double_t targetEnergyToSet,Double_t stepSizeToSet,Int_t numbOfBeamParticlesToSet);
        ~LUT();
        LUT(const LUT& orig);
//        LUT& operator= (const LUT &orig);

        Int_t setNumbOfParticlesInSource(Int_t particles);
        Int_t setNumbOfParticlesInBeam(Int_t particles);
        Double_t setCorrectionFactorToCM(Double_t correction);
        Int_t setDimension(Int_t dimension);
        Double_t setStepSizeData(Double_t step);
        Double_t setStepSizeDataInCM(Double_t step);
        Double_t setStepSizeCalculation(Double_t step);
        void setConfigFileName(char* filename);
        void setFolderName(char* foldername);
        void setDataFilePrefix(char* filePrefix);
        void setTargetEnergy(Double_t energyToSet);      
        Double_t getTargetEnergy();
        Double_t getEdep(Double_t position);
        Double_t getInDepthEnergy(Double_t position); 
        Double_t calculateTotalDepositedEnergy();
        Double_t getEnergyRequiredToPenetrate(Double_t dWaterEqDepth);
        Double_t getPeakRangeOfEnergy(Double_t dEnergy);
        Double_t getMaxDepositedEnergy();
        Double_t* getEnergyAvailArray(Int_t& sizeOfArray);
        Int_t numberOfAvailableEnergies;
        void fillPenetrationDepthArray();
        Double_t getRangeOfEnergy();
        Int_t setParticleType(Int_t particle);
        Double_t getRBEEdep(Double_t position);
        void returnEdepPositions();
        Double_t getPositionAtEdep(Double_t edep);
        Double_t findMaxEdep();


private:        
        void initializeLutArray();
        void initializeRemainingEnergyArray();
        void initializeRawDataArray();
        void initializePenetrationDepthArray();
        void deletePenetrationDepthArray();
        void deleteRemainingEnergyArray();
        void deleteRawDataArray();
        void deleteLutArray();
        void readInAllDataFilesIntoRawDataArray();
        Int_t readInEnergies();
        Int_t countRowsInFile(char* filename);
        Int_t readInDataFromFile(char* filename,Double_t** dataArray);
        
        void readOutLUT(char* filename,Double_t** dataArray);
        void interpolateEnergy(Double_t** interpolDataArray, Double_t energy);
        void interpolateArray(Double_t ** dataArray,Int_t arraySize, Double_t** interpolDataArray);
        void calculateLUT();
        void calculateRemainingEnergyArray();
        void setInterpolEdepData();
        void retrieveData(Int_t targetEnergyIdx,Double_t** dataArray);
        void getLUTarraySplittedUp(Int_t targetEnergyIdx, Double_t *positionArray,Double_t *valueArray);
        void calcPeakRangeOfEnergy();
        void determinePositionsOnCurve();

        //function reads in data file with multiple columns
//number of columns to read has to be passed to function
//function returns number of created entries
//dataArray[entriesPerLine][numberOfEntries]
//ATTENTION: Does only read iDimensionArray values from file, if more, data will be ignored
        Int_t readInDataColFromFile(char* filename,Double_t** dataArray, Int_t numColumns, Int_t numLinesToStore);
           
};//end of class LUT



#endif	/* LUTCLASS_H */

