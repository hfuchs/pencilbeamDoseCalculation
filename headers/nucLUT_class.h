/* 
 * File:   nucLUT.h
 * Author: main
 *
 * Created on 29 November 2011, 12:59
 */

#ifndef NUCLUT_H
#define	NUCLUT_H

#include <systemAndRootHeaders.h>
#include <string.h>
using namespace std;

class nucLUT
{
    private:    
    const static Int_t maxNumberOfCharsToRead=10000;//maximum chars per LUT table line
    const static Int_t energyArraySize=100;//defines the size of the energy array
    const static Int_t filenameTotalSize=3000;//defines the size of the total filename including path
    const static Int_t filenameComponentSize=1000;//defines the size of the filename components
         
    const static Int_t lutArrayEntries=5;//Number of elements in lutArray

    Double_t targetSigma;//initial beam sigma, necessary for nuclear lookup table
    Double_t lutSigma;//initial beam sigma of the look-up table
    Double_t beamSigma;//dimension of current beam
    
    Double_t targetEnergy;//initial beam energy
    Double_t stepSize;//step size of the data files in mm
    Double_t stepSizeCalculation;//Step size of the requesting calculation in cm
    Double_t correctionFactorToCM;//factor by which the values have to be multiplied to get cm
                                //All values sent to and read from class are in cm
    Int_t iDimensionOfArray;//number of z entries per energy
    Int_t numbOfParticlesInSource;//number of beam particles in LUT files
    
    Double_t **lutArray;
    Double_t ***rawDataArray;
    Double_t energyAvailArray[energyArraySize];
    Char_t folder[filenameComponentSize],configFileName[filenameComponentSize], dataFilePrefix[filenameComponentSize];
public:    
    ROOT::Math::Interpolator **interpolLocalValues;
    
public:
        nucLUT();
        nucLUT(Double_t targetEnergyToSet,Double_t targetSigmaToSet, Double_t stepSizeToSet);
        ~nucLUT();
        nucLUT(const nucLUT& orig);
        Double_t setCorrectionFactorToCM(Double_t correction);
        Int_t setDimension(Int_t dimension);
        Double_t setStepSizeData(Double_t step);
        Double_t setStepSizeDataInCM(Double_t step);
        Double_t setStepSizeCalculation(Double_t step);
        void setConfigFileName(char* filename);
        void setFolderName(char* foldername);
        void setDataFilePrefix(char* filePrefix);
        void setTargetEnergy(Double_t energy);   
        Double_t setTargetSigma(Int_t targetSigmaToSet);
        Double_t setLutSigma(Double_t lutSigmaToSet);//set initial beam sigma of LUT data in cm
        Double_t setBeamSigma(Double_t beamSigmaToSet);//set beam sigma of current beam in cm
        Double_t getTargetEnergy();
        Double_t getGaussVoigtRelation(Double_t position);
        Double_t getGaussSigma(Double_t position);
        Double_t getVoigtSigma(Double_t position);
        Double_t getVoigtLg(Double_t position);
        Int_t readInEnergies();
        void getAllParameters(Double_t position, Double_t& voigtGaussRelation, Double_t& gaussSigma, Double_t& voigtSigma, Double_t& voigtLg);
        void getVoigtParameters(Double_t position, Double_t& voigtSigma, Double_t& voigtLg);
       
private:        
        void initializeLutArray();
        void initializeLocalInterpolArray();
        void initializeRawDataArray();
        void deleteLocalInterpolArray();
        void deleteRawDataArray();
        void deleteLutArray();

//        void getLUTarraySplittedUp(char* filename,Double_t *positionArray,Double_t **valueArray);
        void getLUTarraySplittedUp(Int_t targetEnergyIdx, Double_t *positionArray,Double_t **valueArray);
        Int_t countRowsInFile(char* filename);
        Int_t readInDataFromFile(char* filename,Double_t** dataArray);
        void readOutLUT(char* filename,Double_t** dataArray);
        void interpolateLUTArray( Double_t energy,Double_t** interpolDataArray);
        void calculateLUT();
        void setinterpolLocalValuesData();
        Int_t readInDataFromFileToLutArrayFormat(char* filename,Double_t** dataArray);
        //function reads in data file with multiple columns
//number of columns to read has to be passed to function
//function returns number of created entries
//dataArray[entriesPerLine][numberOfEntries]
//ATTENTION: Does only read iDimensionArray values from file, if more, data will be ignored
        Int_t readInDataColFromFile(char* filename,Double_t** dataArray, Int_t numColumns, Int_t numLinesToStore);
        void retrieveData(Int_t targetEnergyIdx,Double_t** dataArray);
        void readInAllDataFilesIntoRawDataArray();

           
};

#endif	/* NUCLUT_H */

