/* 
 * File:   pencilBeamHeaders.h
 * Author: main
 *
 * Created on July 1, 2011, 10:25 AM
 */

#ifndef BEAMDECOMPOSITIONHEADERS_H
#define	BEAMDECOMPOSITIONHEADERS_H

#include <systemAndRootHeaders.h>

#include <vector>
#include <algorithm>
#include <sstream>

#include "TMinuit.h"
#include "Math/Minimizer.h"
#include "Math/Factory.h"
#include "Math/Functor.h"
#include "TRandom2.h"
#include "TError.h"


//------------------------------------------------------------------------------
//compile
//        g++ `root-config --cflags --glibs` -lMinuit fitmultiGaussV14.cpp
//------------------------------------------------------------------------------


using namespace std;

class GaussFitClass
{
public:
    //Double_t* realData;
    std::vector<Double_t> realData;
    std::vector<Double_t> fixedMu;
    std::vector<Double_t> correlationCoeffForC;
    
    Int_t numDataPoints;
    Double_t stepSize,startVal;
    Int_t numOfSubGaussians,numOfCalcGaussians;
    Int_t counter;
    Int_t halfBeamNum;

    //PI
    Double_t constPI;
    
    //constructor
    GaussFitClass(); 

    //returns the value of the sum multiple Gaussians at a specific point
    Double_t myMultiGauss(Double_t x,std::vector<Double_t> &mus,std::vector<Double_t> &sigmas,std::vector<Double_t> &constants);


    //Returns the chisquared value of the difference between realfkt and fitted functions
    Double_t myfcn(const Double_t *par);

    //sets the data of initial beam shape from function to realData array
    //mu of initial Gauss has to be at position 0
    void setBeamShapeFromGaussFktToRealData(Double_t FWHM,Double_t stepSizeToSet);

    Double_t setStepSize(Double_t stepSizeToSet);
    
    //sets the real data array
    //set the initial beam shape data taken from an array to realData array
    void setBeamShapeFromDataArrayToRealData(Double_t *initialBeamShapeDataArray,Int_t NumDataPointsInArray,Double_t stepSizeOfDataPoints);

    void numericalFit(Int_t numOfSubGaussiansToSet,Double_t FWHM,std::vector<Double_t>& output);
    
};//end of Class GaussFitClass


#endif	/* BEAMDECOMPOSITIONHEADERS_H */