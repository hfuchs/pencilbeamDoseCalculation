/* 
 * File:   interpol.h
 * Author: hfuchs
 *
 * Created on August 28, 2012, 12:25 PM
 */

#ifndef INTERPOL_H
#define	INTERPOL_H

class interpol {
public:
    interpol();
    virtual ~interpol();
    interpol(const interpol& orig);

    
    ROOT::Math::Interpolator *interpolator;
    double getData(double x);
    double getDataClosestMatch(double x);
    void setInterpolEdepData(const std::vector< double > &x, const std::vector< double > &y);    
    void setInterpolEdepDataArray(int iDimensionOfArray,const double *x, const double *y);
    

private:

    std::vector< double > xVector;
    std::vector< double > yVector;
};

#endif	/* INTERPOL_H */

