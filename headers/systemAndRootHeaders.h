/* 
 * File:   systemAndRootHeaders.h
 * Author: main
 *
 * Created on 13 February 2012, 14:39
 */

#ifndef SYSTEMANDROOTHEADERS_H
#define	SYSTEMANDROOTHEADERS_H

#define USEPROTONBEAM
//#define USEHELIUMBEAM
//USEPROTONBEAM switches to proton beam
//USEHELIUMBEAM switches to helium beam

#define PREPROCESSOR_PRECISION 1

//#define USERBE
//RBE enables RBE calculation for both! particles
//eg.g for protons *1.1
//for helium 1.1 or 1.4 if within 40 percent of maximum

#include <iostream>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <fstream>
#include <sstream>
#include <vector>
#include <algorithm>
#include <sstream>


#include "TROOT.h"
#include "TMath.h"
#include "TApplication.h"
#include "TSystem.h"
#include "TGFrame.h"
#include "TGClient.h"
#include "TGButton.h"
#include "TGLabel.h"
#include "TGMenu.h"
#include "TGFileDialog.h"
#include "TBrowser.h"
#include "TRootEmbeddedCanvas.h"
#include "TRootHelpDialog.h"
#include "TCanvas.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TH2.h"
#include "TGraph.h"
#include "TImage.h"
#include "TRandom.h"
#include "TGMsgBox.h"
#include "TGPicture.h"
#include "TGListTree.h"
#include "TObjString.h"
#include "TMessage.h"
#include "TTimer.h"
#include "TGDNDManager.h"
#include "TPad.h"
#include "TColor.h"
#include "TStyle.h"
#include "TGaxis.h"
#include "TLegend.h"
#include "TMultiGraph.h"
#include "TTree.h"
#include "TFile.h"
#include "TBranch.h"
#include "TObject.h"
#include "TPaletteAxis.h"
#include "Math/Interpolator.h"
#include "TF1.h"
#include "Math/WrappedTF1.h"
#include "Math/GaussIntegrator.h"
#include "Math/GSLIntegrator.h"
#include "Math/GaussLegendreIntegrator.h"
 #include "Math/IFunction.h"


#include <omp.h>
/*
#include "TMinuit.h"
#include "Math/Minimizer.h"
#include "Math/Factory.h"
#include "Math/Functor.h"
#include "TRandom2.h"
#include "TError.h"
*/

#endif	/* SYSTEMANDROOTHEADERS_H */

