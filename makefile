# I am a comment, and I want to say that the variable CC will be
# the compiler to use.
CC=g++
# Hey!, I am comment number 2. I want to say that CFLAGS will be the
# options I'll pass to the compiler.
#-Wall ...complain a lot
# -O ... optimize code, -O0 -01 -O2 -O3 are also available, -O3 is best
#CFLAGS=-O3 -Wall -DNDEBUG -DPHANTOM -DALL_INLINE -DNOXSDIR -DDCM_MOSAIQ -DUSE_BUILD_INFO
CFLAGS=-g -Wall -DNDEBUG -DPHANTOM -DALL_INLINE -DNOXSDIR -DDCM_MOSAIQ -DUSE_BUILD_INFO
#includes and libraries
LIBS= -I/usr/X11R6/include -I. -L/usr/X11R6/lib -L/lib -L/usr/lib/x86_64-linux-gnu/ -L/usr/lib -I/usr/include -lXm -lXt -lXpm -lX11 -ldl -lboost_thread -pthread -lstdc++ -fopenmp
LINKER=-lXm -lXt -lXpm -lX11 -ldl -lboost_thread -pthread -lstdc++ -lGui -lCore -lCint -lRIO -lNet -lHist -lGraf -lGraf3d -lGpad -lTree -lRint -lPostscript -lMatrix -lPhysics -lMathCore -lThread -pthread -lm -ldl -rdynamic -lMathMore
# -fopenmp
RCONFIG=-pthread -m64 -I/opt/simulation/root_5_32/include -L/opt/simulation/root_5_32/lib
#`root-config --cflags --glibs` -lMathMore
PBINC=-I headers/

#ALL_SRCS := $(wildcard *.c*)
#SRCS     := $(filter-out ProtonSpotPBold.cpp, $(ALL_SRCS))
SRCS := testbenchStoreRBEfit.cpp
#SRCS := testbenchRBE.cpp


all: compile link

test:
	echo $(SRCS)
	echo $(SRCS)
	

compile:
	$(CC) $(CFLAGS) $(RCONFIG) $(PBINC) $(LIBS) -c $(SRCS) $(LINKER)
#source hyperion.sh

link:
#	$(CC) $(CFLAGS) $(RCONFIG) $(LIBS) -o testbenchRBE *.o $(LINKER)
	$(CC) $(CFLAGS) $(RCONFIG) $(LIBS) -o testbenchStoreRBEfit *.o $(LINKER)

#source hyperion.sh

clean:
	rm -rf testbenchRBE
#		rm -rf testbenchStoreRBEfit


#Do not forget to type export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/opt/boost/lib:/usr/local/lib64/"
#by typing source hyperion.sh


#g++ -O3 -Wall -DNDEBUG -DPHANTOM -DALL_INLINE -DNOXSDIR -DDCM_MOSAIQ -DUSE_BUILD_INFO -pthread -m64 -I/opt/simulation/root_5_32/include -L/opt/simulation/root_5_32/lib -I/usr/X11R6/include -I. -L/usr/X11R6/lib -L/lib -L/usr/lib/x86_64-linux-gnu/ -L/usr/lib -I/usr/include -o hyperion *.o -lXm -lXt -lXpm -lX11 -ldl -lboost_thread -pthread -lstdc++ -lGui -lCore -lCint -lRIO -lNet -lHist -lGraf -lGraf3d -lGpad -lTree -lRint -lPostscript -lMatrix -lPhysics -lMathCore -lThread -pthread -lm -ldl -rdynamic -lMathMore
