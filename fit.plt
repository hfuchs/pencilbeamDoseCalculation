a=100
b=0.7
c=0.3


f(x)=1.1+c/(1+exp(a-b*x))

#fit f(x) 'EdepHeliumMod.dat' using 1:4 via a, b
#fit f(x) 'rbedata.dat' using 1:3 via a, b
#1.5MeV Fit
fit f(x) 'rbedata150.dat' using 2:3 via a, b

#fit f(x) 'rbedata250.dat' using 2:3 via a, b

#a= center of exponential function / b



#a               = 23.7672          +/- 0.1851       (0.779%)
#b               = 0.186832         +/- 0.001458     (0.7806%)
#c               = 0.201046         +/- 0.0001258    (0.06256%)

#a               = 24.2627          +/- 3.306        (13.63%)
#b               = 0.182502         +/- 0.02456      (13.46%)
#c		= 0.3


#long_50_up =  106.80
#long_60_up =  128.58
#long_70_up =  137.84
#long_80_up =  142.99
#long_90_up =  146.95
#long_100_up =  153
#brag_peakPosition =  159.95

#c=0.3

set key top left
set yrange[1.05:1.45]

#plot './EdepHelium.dat' using 1:($2*f($1)) w l, 'EdepHelium.dat' using 1:3 w l, 'EdepHelium.dat' using 1:2 w l
plot 'EdepHelium.dat' using 1:4 w l, f(x), 'rbedata150.dat' using 2:3 w p

