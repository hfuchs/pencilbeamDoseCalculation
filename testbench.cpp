/* 
 * File:   testbench.cpp
 * Author: hfuchs
 *
 * Created on December 28, 2012, 8:53 AM
 */

#include "testbench.h"

#include <cstdlib>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <fstream>

#include <math.h>
#include <string>
#include <vector>
#include <assert.h>

#if defined(WIN32)
#include <erfQ.h>
#endif


//#include <XiPSData.h>
//#include <DensityDoseCompGrid.h>
//#include <ProtonSpotGeometry.h>
//#include <DoseMeta.h>
//
//#include <PropertyGrid3D.h>

//#define CMSXIO

//#ifndef CMSXIO
//#include <DoseAtomSpec.h>
//#else
//#include <DoseAtomSpec_XIO.h>
//#endif

using namespace std;


/*
 * 
 */
int main(int argc, char** argv) 
{
    cout<<"casting Dim ProtonSpotPB"<<endl;  
  XDim = 20;//in cm
  YDim = 20;
  ZDim = 20;
  cout<<"setting voxelSize ProtonSpotPB"<<endl; 
  gridStepSize = 2.;//in mm
  
  cout<<"initializing LUT classes ProtonSpotPB"<<endl;
    lookUpTable= new LUT;
    nucCorrTable= new nucLUT;
    myInputClass= new inputClass;
cout<<"setting LUT classes config ProtonSpotPB"<<endl;
    Double_t stepSizeInterpolData=0.05;//step size of the interpolation data files in cm
    lookUpTable->setDimension(2000);//set dimension of read data file

#ifdef USEHELIUMBEAM
    lookUpTable->setFolderName((char *)"LUTHelium_INCLXX/"); 
    //lookUpTable->setFolderName((char *)"calibrationHeliumEnergGaussSmeared3MeV/"); 
#endif	/* USEHELIUMBEAM */
#ifdef USEPROTONBEAM
    lookUpTable->setFolderName((char *)"LUTProton_INCLXX/"); 
    //lookUpTable->setFolderName((char *)"calibrationProtonEnergGaussSmeared3MeV/"); 
#endif	/* USEPROTONBEAM */
    //lookUpTable->setFolderName((char *)"/home/hfuchs/testdevel/calibrationHeliumcalibrationHelium/"); 
    lookUpTable->setDataFilePrefix((char *)"OneDim_Edep_");
    lookUpTable->setConfigFileName((char *)"dataAvailable.dat");
    lookUpTable->setStepSizeDataInCM(stepSizeInterpolData);//step size of the interpolation data files in cm
    lookUpTable->setStepSizeCalculation(gridStepSize*0.1);//in cm
    lookUpTable->setNumbOfParticlesInBeam(1);//set to one due to scaling with multiple beams only necessary with LUT
    lookUpTable->calculateTotalDepositedEnergy();
    lookUpTable->fillPenetrationDepthArray();
cout<<"setting nucCorr classes config ProtonSpotPB"<<endl;
    nucCorrTable->setDimension(2000);//set dimension of read data file

#ifdef USEHELIUMBEAM
    nucCorrTable->setFolderName((char *)"LUTHelium_INCLXX/");
#endif	/* USEHELIUMBEAM */
#ifdef USEPROTONBEAM
    nucCorrTable->setFolderName((char *)"LUTProton_INCLXX/"); 
#endif	/* USEPROTONBEAM */
//    nucCorrTable->setFolderName((char *)"calibrationHelium/"); 
//nucCorrTable->setFolderName((char *)"calibrationHelium/"); 
    nucCorrTable->setDataFilePrefix((char *)"nucCorrLatFit_GATE_Water_3mm_");
    nucCorrTable->setConfigFileName((char *)"dataAvailable.dat");
    nucCorrTable->setLutSigma(0.3);////set initial beam sigma of LUT data in cm
    nucCorrTable->setStepSizeDataInCM(stepSizeInterpolData);//step size of the interpolation data files in cm
    nucCorrTable->setStepSizeCalculation(gridStepSize*0.1);//in cm
    nucCorrTable->readInEnergies();//read in available energies
    

 cout<<"setting input classes config ProtonSpotPB"<<endl;   
    myInputClass->setDataFileName((char*)"calibrationHelium/myHUDataFile.dat");
    myInputClass->readInDataFile();   
  
    lookUpTable->setTargetEnergy(50.);    
    nucCorrTable->setTargetEnergy(50.);
        
    //ATTENTION: PB used cm not mm!!!
    ProtonSpotPB_stepSize=gridStepSize/10.; //set step size to same step size as hyperion     

//    ProtonSpotPB_initFluence = 1000000000000; //set initial beam fluence
        ProtonSpotPB_initFluence=10000000;//set initial beam fluence

  
    //number of lateral voxels to calculate
    ProtonSpotPB_lateralVoxelsToCalculate=10000;//8;//from position plus and minus number of Voxels
        
    conversionMMtoVoxel=1.0/gridStepSize;
    
//##############################################################################    
    //start testbench
//##############################################################################    
    
    double beamX0                 = 0.;
    double beamY0                 = 0.;
//    double beamInitSigmaX     = beamDataArray[2];
//    double beamInitSigmaY     = beamDataArray[3];
//    double thisBeamFluence    = beamDataArray[4];
    double initEnergy         = 139.0;
    
        
    std::vector<double> HUArray;
    HUArray.clear();
    int maxIterations =2000;
    for (int i=0;i<maxIterations;i++)
    {
        HUArray.push_back(0.);
    }

    std::vector<double> realDepthVector;
    std::vector<double> WEQDepthVector;
    std::vector<double> MScurrentSquaredVarianceVector;
    std::vector<double> stoppingPowerWaterVector;
    std::vector<double> voigtCorrectionFactorVector;
    double realBPdepth, maxRealDepth;
    lookUpTable->setTargetEnergy(initEnergy);
    nucCorrTable->setTargetEnergy(initEnergy);
    double distanceSourceToContour=100.;//distance from beam source to contour
    double distanceToGrid=0;
    bool reachedBraggPeak;
    
    cout<<"HUArray.size(): "<<HUArray.size()<<endl;

    calcuateWEQandMSarrays(HUArray, realDepthVector, WEQDepthVector, MScurrentSquaredVarianceVector,
                               stoppingPowerWaterVector,voigtCorrectionFactorVector,
                               realBPdepth, maxRealDepth, distanceSourceToContour,
                               initEnergy, distanceToGrid, beamX0, beamY0);
    cout<<"realDepth.size(): "<<realDepthVector.size()<<endl;
    cout<<"WEQDepth.size(): "<<WEQDepthVector.size()<<endl;
    cout<<"MScurrentSquaredVariance.size(): "<<MScurrentSquaredVarianceVector.size()<<endl;
//    for(unsigned int i=0;i<realDepth.size();i++)
//    {
//        std::cout<<"i: "<<i<<" HU: "<<HUArray.at(i)<<" realDepth: "<<realDepth.at(i)
//                <<" WEQDepth: "<<WEQDepth.at(i)<<" MScurrentSquaredVariance: "<<MScurrentSquaredVariance.at(i)<<endl;
//    }
    
    
    //initialize class
    interpol* weqLUT;
    weqLUT = new interpol;
    weqLUT->setInterpolEdepData(realDepthVector,WEQDepthVector);
    interpol* MSsquaredVarianceLUT;
    MSsquaredVarianceLUT = new interpol;
//    MSsquaredVarianceLUT->setInterpolEdepData(realDepthVector,MScurrentSquaredVarianceVector);
    
        interpol* stoppingPowerWaterLUT;    
    stoppingPowerWaterLUT = new interpol;
    stoppingPowerWaterLUT->setInterpolEdepData(realDepthVector,stoppingPowerWaterVector); 
    
    interpol* voigtCorrectionLUT;    
    voigtCorrectionLUT = new interpol;
    voigtCorrectionLUT->setInterpolEdepData(realDepthVector,voigtCorrectionFactorVector); 
    

    double dHUnit=0.;
    double distanceFromBeamX,  distanceFromBeamY, distanceFromBeamZ;
    distanceFromBeamX=distanceFromBeamY=0.;
    distanceFromBeamZ=1.;
    
    double doseToWrite;
    double *beamDataArray=new double[8];
          beamDataArray[0]=beamX0;//PosX0
          beamDataArray[1]=beamY0;//PosY0
          
          cout<<"beamX0 "<<beamX0<<" beamY0 "<<beamY0<<endl;
          
          beamDataArray[2]=0.3;//SigmaX in cm
          beamDataArray[3]=0.3;//SigmaY

          beamDataArray[4]=ProtonSpotPB_initFluence;
          beamDataArray[5]=initEnergy;
          
          beamDataArray[6]=0.;
          beamDataArray[7]=0.;
            
          
//   string filename;
//   filename = "beamDataOutput.dat";      
//      ofstream datafile(filename.c_str());
//   if(!datafile.good()) {
//     std::cerr << "Could not open beam data output file "<<filename<<"\n";
//     return 0;
//   }
      distanceFromBeamY=0.;
      double range=200.;//in mm
      double stepsize=gridStepSize;//in mm
      double sum;
      int lateralIntegrate=ProtonSpotPB_lateralVoxelsToCalculate;
      
    dHUnit=0.;  
    
//   for(int i=0; i<(range/stepsize);i++)
//   {   
//    int i= 20./stepsize;
    int i=0;
    sum = 0.;
        
    distanceFromBeamZ=i*stepsize;//in mm
    
//    distanceFromBeamX=distanceFromBeamY=0;
//    cout<<distanceFromBeamZ<<endl;
//    for(int j=-lateralIntegrate; j<=lateralIntegrate;j++)
//    {
////        cout<<"j: "<<j<<endl;
//        if( (j % 100)==0 )cout<<"j: "<<j<<endl;
//        
//     distanceFromBeamX=j*stepsize;//in mm
////     cout<<"distanceFromBeamX "<<distanceFromBeamX<<endl;
//     for(int k=-lateralIntegrate; k<=lateralIntegrate;k++)
//     {  
////         cout<<"j: "<<j<<endl;
//        distanceFromBeamY=k*stepsize;//in mm
//        ////cout<<"distanceFromBeamY "<<distanceFromBeamY<<endl;
//        ////everything passed in cm
//        doseToWrite=  getDoseAtVoxel(dHUnit, weqLUT, MSsquaredVarianceLUT,stoppingPowerWaterLUT, voigtCorrectionLUT, 
//            beamDataArray, distanceFromBeamX,  distanceFromBeamY, distanceFromBeamZ);
//    
////            datafile<<distanceFromBeamX<<" "<<doseToWrite<<endl;
//
//        sum+=doseToWrite;
//     }
//     
//    }
    
    double tmp=stepsize*stepsize* 1. / (3.*sqrt(2.*TMath::Pi()));

    //for time measurements
struct timeval start, end;
long mtime, seconds, useconds;    

//set starting time for calculation time measurement
gettimeofday(&start, NULL);
    

//copy class
// LUT* lookUpTable;
//lookUpTable= new LUT;
//    nucLUT* nucCorrTable;

//LUT* testLUT;
//testLUT = new LUT(*lookUpTable);
//
//cout<<"lookUpTable->getTargetEnergy() "<<lookUpTable->getTargetEnergy()<<endl;
//cout<<"testLUT->getTargetEnergy() "<<testLUT->getTargetEnergy()<<endl;
//testLUT->setTargetEnergy(123);
//cout<<"lookUpTable->getTargetEnergy() "<<lookUpTable->getTargetEnergy()<<endl;
//cout<<"testLUT->getTargetEnergy() "<<testLUT->getTargetEnergy()<<endl;
//
//cout<<
//testLUT->getEdep(10)<<" "<<
//testLUT->getInDepthEnergy(10)<<" "<<
//testLUT->calculateTotalDepositedEnergy()<<" "<<
//testLUT->getEnergyRequiredToPenetrate(10)<<" "<<
//testLUT->getPeakRangeOfEnergy(123)<<" "<<
//testLUT->getMaxDepositedEnergy()<<" "<<
//testLUT-> getRangeOfEnergy()<<" "<<
//testLUT->getRBEEdep(10)<<endl;


    interpol* testSTPW;    
    testSTPW = new interpol(*stoppingPowerWaterLUT);
    cout<<"testSTPW->getData(10) "<<testSTPW->getData(10)<<endl;
    cout<<"stoppingPowerWaterLUT->getData(10) "<<stoppingPowerWaterLUT->getData(10)<<endl;
    stoppingPowerWaterLUT->setInterpolEdepData(realDepthVector,voigtCorrectionFactorVector); 
     cout<<"testSTPW->getData(10) "<<testSTPW->getData(10)<<endl;
    cout<<"stoppingPowerWaterLUT->getData(10) "<<stoppingPowerWaterLUT->getData(10)<<endl;


    inputClass* cpClass;
    cpClass= new inputClass(*myInputClass);


double mat,matCP;
    myInputClass->getMaterialDensityFromHU(10, mat);  
    cpClass->getMaterialDensityFromHU(10, matCP);
    cout<<"mat: "<<mat<<" matCP: "<<matCP<<endl;







//    for(int j=-lateralIntegrate; j<=lateralIntegrate;j++)
//    {
//        
//     distanceFromBeamX=j*stepsize;//in mm
////     cout<<"distanceFromBeamX "<<distanceFromBeamX<<endl;
//     for(int k=-lateralIntegrate; k<=lateralIntegrate;k++)
//     {  
//         distanceFromBeamY=k*stepsize;//in mm
//             //Gauss distribution due to MS
//            double yg= TMath::Gaus(distanceFromBeamY, 0.0, 3., true)*stepsize;//*spacingBetweenPoints to normalize to one again  
//            double xg= TMath::Gaus(distanceFromBeamX, 0.0, 3., true)*stepsize;
////            double radius=sqrt(distanceFromBeamX*distanceFromBeamX+distanceFromBeamY*distanceFromBeamY);
//        
////                     doseToWrite=1;
//
//            doseToWrite=yg*xg;
////            double doseToWrite=TMath::Gaus(radius, 0.0, 3., true)*tmp;
//            sum+=doseToWrite;
//    }
//}
    //        //set end time
gettimeofday(&end, NULL);

seconds  = end.tv_sec  - start.tv_sec;
useconds = end.tv_usec - start.tv_usec;
mtime = ((seconds) * 1000. + useconds/1000.0);
printf("Elapsed time: %ld milliseconds\n", mtime);
    
    
    cout<<sum<<endl;
//    cout<<"depth: "<<distanceFromBeamZ  <<" sum "<<sum<<endl;
//    datafile<<distanceFromBeamZ<<" "<<sum<<endl;
//   }
//       datafile.flush();
//       datafile.close();  
       
//    sum=0;
//    double temp;
//    for (int i=0;i<1000;i++)
//    {
//       calcuateWEQandMSarrays(HUArray, realDepthVector, WEQDepthVector, MScurrentSquaredVarianceVector,
//                               stoppingPowerWaterVector,voigtCorrectionFactorVector,
//                               realBPdepth, maxRealDepth, distanceSourceToContour,
//                               initEnergy, distanceToGrid, beamX0, beamY0);     
//    }
//    cout<<sum<<endl;
    return 0;
}




//calculate the relativistic beta
//energy and Mass in MeV required
 Double_t  relativisticBeta(Double_t energy, Double_t mass) 
{
    //protonRestMass = 938.272013;////in MeV
    if (energy<0) energy=0;
    if (mass<0) mass=0;
    return sqrt(1 - pow( mass/(mass+energy) ,2) );
    //alternative version, same result
    //return  sqrt( 1 - 1/pow(1+ energy/(protonRestMass) ,2) );
}


////calculate the scattering angle
//// everything in SI units
////See Geant4 physics documentation
 Double_t  getScatteringAngleSquare(Double_t currentStepSize, Double_t energy, Double_t radiationLength)
{
        //Highland-Lynch-Dahl formula
	Double_t constC1, constC2, relatBeta, scatAngle,momentum;

	constC1=13.6; ////in MeV
	constC2=0.038;//// in MeV

        energy=energy*ProtonSpotPB_constParticleMassNumber;//passed Energy is per nucleon, formula works for whole energy
	relatBeta=relativisticBeta(energy, ProtonSpotPB_constParticleMass);
        momentum=ProtonSpotPB_constParticleMass*relatBeta/ProtonSpotPB_constLightSpeed;
       
	if (relatBeta<=0 || currentStepSize<=0) scatAngle=0;
	else scatAngle = constC1/(relatBeta*momentum*ProtonSpotPB_constLightSpeed) * ProtonSpotPB_constParticleCharge *  sqrt( currentStepSize / radiationLength ) * (  1 + constC2 * log(currentStepSize / radiationLength)   );
        ////log() computes natural logarithm in C++
	return pow(scatAngle,2);//necessary to return squared scattering angle	
}


 
inline Double_t getBeamShapeAtDistanceFromCenter(Double_t distanceX, Double_t distanceY,
                                                        Double_t currentBeamSigmaX, Double_t currentBeamSigmaY,
                                                        Double_t initialBeamSigmaX, Double_t initialBeamSigmaY,
                                                        Double_t GaussVoigtRelation, Double_t VoigtSigma,
                                                        Double_t VoigtLg, Double_t spacingBetweenPoints, Double_t voigtCorrectionFactor)
{
    //Gauss and Voigt function are centered around zero.
    //using +/- distance from beam center, normalized beam shape value at this position is returned
    
    Double_t xg,yg,xvoigt,yvoigt,finalResult;
    xg=yg=xvoigt=yvoigt=finalResult=0.0;
    Double_t voigtArea,gaussArea; 
//    Double_t xResult=0.0;
//    Double_t yResult=0.0;
    
           //include correction
            //how much goes to Gauss, how much goes to Voigt
            //in total, area of both curves should be one due to normalization
            voigtArea=1./(1.+GaussVoigtRelation);
            gaussArea=1.-voigtArea;
//    cout<<"GaussVoigtRelation: "<<GaussVoigtRelation<<" voigtArea: "<<voigtArea<<" gaussArea: "<<gaussArea<<endl;

     //Gauss distribution due to MS
//            yg= TMath::Gaus(distanceY, 0.0, currentBeamSigmaY, true)*spacingBetweenPoints;//*spacingBetweenPoints to normalize to one again  
//currentBeamSigmaY
     //Gauss distribution due to MS
//            xg= TMath::Gaus(distanceX, 0.0, currentBeamSigmaX, true)*spacingBetweenPoints;//*spacingBetweenPoints to normalize to one again  
//            if ( isnan(xg) ||  isinf(xg) || xg<0. ) {cerr<<"ERROR: Aborting, xg inf or nan or negative :"<< xg<<endl; exit(-1);}


            //Create overlap between curves of both dimensions -> final weight factor for this spot, does only need fluence
//            double radius;
//            radius=sqrt(distanceX*distanceX+distanceY*distanceY);
//            double rVoigt;
//            rVoigt=pow(TMath::Voigt(radius, VoigtSigma,VoigtLg , 4)*spacingBetweenPoints,2);
            
//            finalResult=xg*yg*gaussArea+2.*voigtArea*pow(TMath::Voigt(radius, VoigtSigma,VoigtLg , 4)*spacingBetweenPoints,2);
            
            
//            //Voigt function is unlimited, we calculate only limited range to normalize to one

//            double integralx=NumericalIntegration(0., VoigtSigma,VoigtLg,  -17.,17.);
//            double integralx=NumericalIntegration(0., VoigtSigma,VoigtLg,  -34.,34.);
//            double integralx=NumericalIntegration(0., VoigtSigma,VoigtLg,  -(ProtonSpotPB_lateralVoxelsToCalculate+0.5)*spacingBetweenPoints,(ProtonSpotPB_lateralVoxelsToCalculate+0.5)*spacingBetweenPoints);

//            double integralGx=NumericalIntegrationGaus(0., currentBeamSigmaX, -12.,12.);
//            cout<<"integralx "<<integralx<<" VoigtSigma "<<VoigtSigma<<   endl;
            
//            finalResult=1./integralx * TMath::Voigt(distanceX, VoigtSigma, VoigtLg, 4)*spacingBetweenPoints*
//                     1./integralx * TMath::Voigt(distanceY, VoigtSigma, VoigtLg, 4)*spacingBetweenPoints;
            
//            finalResult=1./voigtCorrectionFactor * TMath::Voigt(distanceX, VoigtSigma, VoigtLg, 4)*spacingBetweenPoints*
//                     1./voigtCorrectionFactor * TMath::Voigt(distanceY, VoigtSigma, VoigtLg, 4)*spacingBetweenPoints;
            
            
            // 1./1.1=0.90909 is a correction factor to account for changes in normalization due to number of voxels taken into account
            
            double radius=sqrt(distanceX*distanceX+distanceY*distanceY);
//            cout<<"spacingBetweenPoints "<<spacingBetweenPoints<<endl;
            finalResult= TMath::Voigt(radius, VoigtSigma, VoigtLg, 4);
//                    TMath::Gaus(radius, 0.0, currentBeamSigmaY, false);//TMath::Gaus(distanceY, 0.0, currentBeamSigmaY, false) *TMath::Gaus(distanceX, 0.0, currentBeamSigmaX, false);
                    
                    
                    // TMath::Gaus(radius, 0.0, currentBeamSigmaY, true);//TMath::Voigt(radius, VoigtSigma, VoigtLg, 4);
//            cout<<"x "<<distanceX<<" y "<<distanceY<<" radius: "<<radius<<endl;
                    //TMath::Voigt(radius, VoigtSigma, VoigtLg, 4);
                    
//                    TMath::Voigt(distanceX, VoigtSigma, VoigtLg, 4)*
//                     TMath::Voigt(distanceY, VoigtSigma, VoigtLg, 4);
//                    TMath::Voigt(radius, VoigtSigma, VoigtLg, 4)*TMath::Voigt(radius, VoigtSigma, VoigtLg, 4);
                    
//                     *TMath::Voigt(distanceX, VoigtSigma, VoigtLg, 4)*
//                     *TMath::Voigt(distanceY, VoigtSigma, VoigtLg, 4);
                    
                    
//xg*yg  *gaussArea + voigtArea*   ( 1./voigtCorrectionFactor *TMath::Voigt(distanceX, VoigtSigma, VoigtLg, 4)*spacingBetweenPoints*
//                           1./voigtCorrectionFactor *TMath::Voigt(distanceY, VoigtSigma, VoigtLg, 4)*spacingBetweenPoints);
            

            if ( isnan(finalResult) ||  isinf(finalResult) || finalResult<0.) {cerr<<"ERROR: Aborting, Weight at given spot inf or nan or negative:"<< finalResult<<endl; exit(-1);}    
 
            return   finalResult;       
}


//loop through the scatAngleSquaredArray and quadratically sum up all contents with distance from current point
//defined through stepSize*maxIndex
//see Soukup Dissertation
 double  getSummedScattAngle(std::vector<double>& scatAngleSquaredVector,double stepSize)
{
	int i,currentSize;
        double depthSum=0;
	double quadrSum=0;
        
        currentSize=scatAngleSquaredVector.size();
        
        depthSum=stepSize*currentSize;
            for (i=0;i<currentSize;i++)
            {
             //first (first voxel) entry for generated angle is now farthest away.
             //quadratically sums up squared angles * depth   
             //scatAngleSquaredArray contains already squared values
             quadrSum=quadrSum + scatAngleSquaredVector.at(i)*pow(depthSum,2);
             depthSum=depthSum-stepSize;
            }
	return quadrSum;
}



//calculate the stoppingPowerRatioToWater in order to rescale the real depth to a depth in water
 Double_t  getStoppingPowerRatioToWater(Double_t stoppingPowerWater,Double_t stoppingPowerMaterial) 
{
	if (stoppingPowerMaterial<=0||stoppingPowerWater<=0) return 1.0;
	else return stoppingPowerMaterial/stoppingPowerWater;
}


//Bethe-Bloch formula
//see Ziegler 1999
//energy, excitationEnergy and particle mass in MeV
 Double_t  calculateStoppingPower(Double_t energy,Double_t atomicZARatio, Double_t excitationEnergy) 
{
	Double_t constK,relatBeta,deltaEmax,stoppingPower;

        if(energy<=0.) return 0.0; //if energy is below or zero, return 0
        relatBeta=relativisticBeta(energy,ProtonSpotPB_constParticleMass);
        
        //formula to calculate stopping power from PDG
        constK=0.307075;//in MeV * 1/g * cm^2
	deltaEmax=(2*ProtonSpotPB_constMassElectron*relatBeta*relatBeta)/(1-(relatBeta*relatBeta));
        
	stoppingPower=constK*ProtonSpotPB_constParticleCharge*ProtonSpotPB_constParticleCharge*atomicZARatio*1/(relatBeta*relatBeta) *
	(       0.5 * log ( (2*ProtonSpotPB_constMassElectron*relatBeta*relatBeta*deltaEmax)/(1-(relatBeta*relatBeta)) )
	- (relatBeta*relatBeta) - log(excitationEnergy)         );
        
        //alternative from Leo Techniques for Nuclear and Particle Physics Experiments
        //both give the same results 
//        constK=0.1535;//in MeV * 1/g * cm^2
//        Double_t gamma;
//        gamma=1 / sqrt( 1 - relatBeta*relatBeta );      
//        deltaEmax=( 2*constMassElectron*relatBeta*relatBeta*gamma*gamma*ProtonSpotPB_constLightSpeed*ProtonSpotPB_constLightSpeed )/
//                (1 + 2*constMassElectron/ProtonSpotPB_constParticleMass*sqrt( 1+relatBeta*relatBeta*gamma*gamma ) + pow(constMassElectron/ProtonSpotPB_constParticleMass,2) );
//        
//        stoppingPower=constK*1*1*atomicZARatio*1/(relatBeta*relatBeta) *
//	( log ( (2*constMassElectron*gamma*gamma*ProtonSpotPB_constLightSpeed*ProtonSpotPB_constLightSpeed*relatBeta*relatBeta*deltaEmax)/(excitationEnergy*excitationEnergy) )
//	- 2*(relatBeta*relatBeta));
if (isinf(stoppingPower)!=0 || stoppingPower<0.) stoppingPower=0.0;
	return stoppingPower;
}


////decomposes the initial gauss distributed beam in subBeams
////spread is the maximum distance from the original beam axis in times of sigma 
Int_t  beamDecomposition(Double_t **beamDataArray, Int_t numOfSubBeamsOneDim)
{

    Double_t beamInitX0=round(XDim/2.); //set initial beam coordinates in beams eye view coordinate system
    Double_t beamInitY0=round(YDim/2.);
    Double_t initEnergy=150.; //set initial beam energy   
//    if(initEnergy>80.)cout<<"WARNING: energy higher than 80 MeV/A: "<<initEnergy<<endl;
    Double_t beamInitSigmaX= 0.2;//in cm
    Double_t beamInitSigmaY= 0.2;//in cm
    Double_t beamInitSigmaXWidening = 0.;
    Double_t beamInitSigmaYWidening = 0.;
    
    assert((initEnergy>=0.));//beam energy may not be negative
    assert((beamInitSigmaX>0.)&&(beamInitSigmaY>0.));//beam sigma may not be negative or zero
    assert((beamInitSigmaXWidening>=0.)&&(beamInitSigmaYWidening>=0.));//widening may not be negative
    
    if (numOfSubBeamsOneDim<=1) //in case no splitting is required, if only one beam
    {
          beamDataArray[0][0]=beamInitX0;//PosX0
          beamDataArray[0][1]=beamInitY0;//PosY0
          
          beamDataArray[0][2]=beamInitSigmaX;//SigmaX
          beamDataArray[0][3]=beamInitSigmaY;//SigmaY

          beamDataArray[0][4]=ProtonSpotPB_initFluence;
          beamDataArray[0][5]=initEnergy;
          
          beamDataArray[0][6]=beamInitSigmaXWidening;
          beamDataArray[0][7]=beamInitSigmaYWidening;
          
//          printf("k: %i weight: %f x0: %f y0: %f sigx: %f sigy: %f fluence: %f energy: %f\n",
//                        0, (Double_t)1,beamDataArray[0][0],beamDataArray[0][1],
//                        beamDataArray[0][2], beamDataArray[0][3],
//                        beamDataArray[0][4],beamDataArray[0][5]);   
     return 1;   
    }    
    
  Int_t i,j,k;
  std::vector<Double_t> xGauss,yGauss;
  GaussFitClass *fTest;
        
  //calculate one Dim fit for x      
  fTest = new GaussFitClass();      
  fTest->numericalFit(numOfSubBeamsOneDim,beamInitSigmaX,xGauss);
  
//  cout<<"size of xGauss vector: "<<xGauss.size()<<std::endl;
  for( i=0;i<(int)xGauss.size();i=i+3)//all three dimensions in one array
  {
      //correct for initial Position x0
      xGauss.at(i)=xGauss.at(i)+beamInitX0;
//      cout<<"Mu: "<<xGauss.at(i);
//      cout<<" Sigma: "<<xGauss.at(i+1); 
//      cout<<" C: "<<xGauss.at(i+2)<<std::endl;
  }
  delete fTest;
  
  //calculate one Dim fit for y   
  fTest = new GaussFitClass();  
  fTest->numericalFit(numOfSubBeamsOneDim,beamInitSigmaY,yGauss);
  
  //cout<<"size of yGauss vector: "<<yGauss.size()<<std::endl;
  for(i=0;i<(int)yGauss.size();i=i+3)//all three dimensions in one array
  {
      //correct for initial Position y0
      yGauss.at(i)=yGauss.at(i)+beamInitY0;
//      cout<<"Mu: "<<yGauss.at(i);
//      cout<<" Sigma: "<<yGauss.at(i+1); 
//      cout<<" C: "<<yGauss.at(i+2)<<std::endl;
  }
  delete fTest;
  
  Double_t weight=0;  
  Double_t weightTotal=0;
  k=0;
  //combine dimensions and 
  for(i=0;i<numOfSubBeamsOneDim;i++)//yloop
  {
      for(j=0;j<numOfSubBeamsOneDim;j++)//xloop
      {
          //cout<<"i:"<<i<<"j:"<<j<<std::endl;
          beamDataArray[k][0]=xGauss.at(j*3);//PosX0
          beamDataArray[k][1]=yGauss.at(i*3);//PosY0
          
          beamDataArray[k][2]=xGauss.at((j*3)+1);//SigmaX
          beamDataArray[k][3]=yGauss.at((i*3)+1);//SigmaY
          
          weight=xGauss.at((j*3)+2)* yGauss.at((i*3)+2);//Re-Normalization required, done afterwards
          weightTotal=weightTotal+weight;
//          cout<<"gauss:"<<TMath::Gaus(beamInitX0, beamInitY0, initSigmaX, true)<<std::endl;
//          cout<<"cx:"<<xGauss.at((j*3)+2)<<std::endl;
//          cout<<"cy:"<<yGauss.at((i*3)+2)<<std::endl;
          beamDataArray[k][4]= weight;
          beamDataArray[k][5]=initEnergy;
          
          beamDataArray[k][6]=beamInitSigmaXWidening;
          beamDataArray[k][7]=beamInitSigmaYWidening;
          k++;
      }
  }
  
  //weight needs to be renormalized to one 
  weight=0;
//Loop over all sub beams k redistribute weight
  for (k=0;k<numOfSubBeamsOneDim*numOfSubBeamsOneDim;k++)
  {
    weight=1/weightTotal*beamDataArray[k][4];
    //cout<<"total weight: "<<weightTotal<<" old weight: "<<beamDataArray[k][4]<<" new weight: "<<weight<<" initFluence: "<<initFluence<<endl;
    beamDataArray[k][4]= weight*ProtonSpotPB_initFluence;
    
//    printf("k: %i weight: %.2f x0: %.2f y0: %.2f sigx: %.2f sigy: %.2f fluence: %.2f energy: %.2f\n",
//                        k, weight,beamDataArray[k][0],beamDataArray[k][1],
//                        beamDataArray[k][2], beamDataArray[k][3],
//                        beamDataArray[k][4],beamDataArray[k][5]);
  }
    return k;
}

 void  setIsoCenter(double isoX, double isoY, double isoZ)
{
    //isocenter is passed in voxel therefore necessary to recalculate
  m_IsoX = isoX*gridStepSize;
  m_IsoY = isoY*gridStepSize;
  m_IsoZ = isoZ*gridStepSize;

  return;
}


//bool  calculateWEQtoTarget(const DoseAtomSpec& bx,double BeamX0,double BeamY0,double initEnergy,
//        double& distanceToDensGrid, double& distanceSourceToTargetStartWEQ,double& distanceSourceToTargetStopWEQ) const
//{
//    double startX,startY,startZ;
//    double xTemp2,yTemp2,zTemp2;
//    
//    lookUpTable->setTargetEnergy(initEnergy);
//    
//    //get start point of beam
//    transformBeamCoordIntoCubeCoord(bx,BeamX0,BeamY0,0.0,startX,startY,startZ);
//    //get second point to create a direction vector
//    transformBeamCoordIntoCubeCoord(bx,BeamX0,BeamY0,1.0,xTemp2,yTemp2,zTemp2);
//
//    //calculate normalized direction vector
//    //in hyperion coordinates
//    double nRvX,nRvY,nRvZ,norm;
//    nRvX=xTemp2-startX;
//    nRvY=yTemp2-startY;
//    nRvZ=zTemp2-startZ;
//    norm=1./sqrt(nRvX*nRvX + nRvY*nRvY + nRvZ*nRvZ);
//    nRvX*=norm;
//    nRvY*=norm;
//    nRvZ*=norm;
//    
//    //calc distance to grid
//    double distanceToGrid;
//    getDistanceToDensGridIfHit(bx,BeamX0,BeamY0,0.0, distanceToGrid);
//
//    double energyProbNextStep=0.0;
//    double pointX,pointY,pointZ;
//    double steplength=gridStepSize/3.;
//    double dHUnit,densityMaterial, radiationLengthMaterial,excitationEnergyMaterial, materialAtomZARatio;
//    double stoppingPowerWater,stoppingPowerMaterial,rescaleRatio;
//    //start with offset to grid, density is air
//    double currentIntegratedDepthWEQ=distanceToGrid*1.20E-03;
//    //start at Beam entry in density grid, therefore distance source-Grid needed
//    startX+=distanceToGrid*nRvX;
//    startY+=distanceToGrid*nRvY;
//    startZ+=distanceToGrid*nRvZ;
//    
////    cout<<"WEQ XDim: "<<XDim<<" YDim "<<YDim<<" ZDim "<<ZDim<<endl;
//    int iHypX,iHypY,iHypZ;
//     bool bReachedTarget,bIsInTarget,bNotYetLeftTarget;
//     bReachedTarget=false;
//     bIsInTarget=false;
//     bNotYetLeftTarget=false;
//    int i;
//    for(i=1;;i++)
//    {
//        //start at Beam entry in density grid, therefore distance source-Grid needed
//        //proceed with scale increments and offset to density grid
//        //offset included in startX,Y and Z
//        pointX=startX + i*steplength*nRvX;
//        pointY=startY + i*steplength*nRvY;
//        pointZ=startZ + i*steplength*nRvZ;
//        convertHyperionToVoxelCoordinates(pointX, pointY, pointZ, iHypX, iHypY, iHypZ);
//       //check if coordinates are within bounds 
//       if( iHypX<0 || iHypX>=XDim || 
//           iHypY<0 || iHypY>=YDim ||
//           iHypZ<0 || iHypZ>=ZDim )
//       {
//           break;
//       }
//        
//        //if outside body, use air
////        if( m_pDoseCompGrid->isInContour(iHypX, iHypY, iHypZ)==false ) 
////            {
////              dHUnit=-1000; //Houndsfield unit of air is -1000
////              densityMaterial=1.20E-03;
////              radiationLengthMaterial=36.62;
////              excitationEnergyMaterial=85.7;
////              materialAtomZARatio=0.49919;
////              //HoundsfieldUnit density[g/cm3] radiationLength[g*cm_2] Z/A excitation energy[eV] Name
////              //-1000 1.20E-03 36.62 0.49919 85.7 Air 
////            }
////        else 
////        {
//         dHUnit = convertCTtoHU(m_pDoseCompGrid->operator()(iHypX,iHypY,iHypZ));        //query Material
//         //convert Hounsfield Unit to material properties
//         myInputClass->getOneMaterialPropertiesFromHU(dHUnit, densityMaterial, radiationLengthMaterial, 
//                                                     excitationEnergyMaterial, materialAtomZARatio);
////        }
//
//        //calculate stepSize due to different material
//         if (i<=0) energyProbNextStep=initEnergy;
//         else 
//         {
//           energyProbNextStep=lookUpTable->getInDepthEnergy( (currentIntegratedDepthWEQ+steplength)/10. );
//         }
//         //calculate rescale ratio for water equivalent path scaling       
//         stoppingPowerWater= calculateStoppingPower(energyProbNextStep, ProtonSpotPB_constZARatioWater, ProtonSpotPB_constExcitationEnergyWater);
//         stoppingPowerMaterial= calculateStoppingPower(energyProbNextStep,materialAtomZARatio, excitationEnergyMaterial);
//         rescaleRatio=  getStoppingPowerRatioToWater(stoppingPowerWater, stoppingPowerMaterial)*densityMaterial;
//         currentIntegratedDepthWEQ+=steplength*rescaleRatio;
//
//         //check whether the voxel is in the target
//           bIsInTarget=m_pTargetGrid->operator()( iHypX, iHypY , iHypZ );
//           //add up density sum from contour to target
//           if(bReachedTarget==false)
//           {
//            if (bIsInTarget==true) 
//            {
//               distanceSourceToTargetStartWEQ=currentIntegratedDepthWEQ;
//               distanceSourceToTargetStopWEQ=currentIntegratedDepthWEQ;
//                //avoid calling this statement again
//                bReachedTarget=true;
//            }
//           }
//           //add up density sum from start to end of target
//           if(bNotYetLeftTarget==false && bReachedTarget==true)
//           {
//            if (bIsInTarget==false) 
//            {
//                distanceSourceToTargetStopWEQ=currentIntegratedDepthWEQ;
//                 //avoid calling this statement again
//                bNotYetLeftTarget=true;
//            }
//           }
//    }
//
//   return bReachedTarget; 
//}

 inline void calcuateWEQandMSarrays(std::vector<double>& HUArray,
        std::vector<double>& realDepth,std::vector<double>& WEQDepth,std::vector<double>& MScurrentSquaredVariance,
        std::vector<double>& stoppingPowerWaterVector,std::vector<double>& voigtCorrectionFactorVector,
        double& braggPeakDepthHyperion,double& maxDepthHyperion, double& distanceToContour,
        double initEnergy, double distanceToGrid,double BeamX0, double BeamY0)
{      
    realDepth.clear();
    realDepth.reserve(500);
    WEQDepth.clear();
    WEQDepth.reserve(500);
//    voigtCorrectionFactorVector.clear();
//    voigtCorrectionFactorVector.reserve(500);
//    MScurrentSquaredVariance.clear();
//    MScurrentSquaredVariance.reserve(500);//sigmaHERE
    stoppingPowerWaterVector.clear();
    stoppingPowerWaterVector.reserve(500);
    
    double startX,startY,startZ;
    double xTemp2,yTemp2,zTemp2;
    
//    //get start point of beam
//    transformBeamCoordIntoCubeCoord(bx,BeamX0,BeamY0,0.0,startX,startY,startZ);
//    //get second point to create a direction vector
//    transformBeamCoordIntoCubeCoord(bx,BeamX0,BeamY0,1.0,xTemp2,yTemp2,zTemp2);

    //calculate normalized direction vector
    //in hyperion coordinates
    double nRvX,nRvY,nRvZ,norm;
    nRvX=0.;
    nRvY=0.;
    nRvZ=1.;
    startX=startY=startZ=0.;

    
    double scatteringAngleSquared,currSquaredVarianceDueToMS;

    double energyProbNextStep=0.0;
    double pointX,pointY,pointZ;
    double steplength=gridStepSize/2.;//in mm
    double dHUnit,densityMaterial, radiationLengthMaterial,excitationEnergyMaterial, materialAtomZARatio;
    double GaussVoigtRelation,currentSigmaX,VoigtSigma,VoigtLg;
    double stoppingPowerWater,stoppingPowerMaterial,rescaleRatio;
    //start with offset to grid, density is air
    double currentIntegratedDepthWEQ=distanceToGrid*1.20E-03;
    double voigtCorrectionFactor=1.;
    //start at Beam entry in density grid, therefore distance source-Grid needed
    startX+=distanceToGrid*nRvX;
    startY+=distanceToGrid*nRvY;
    startZ+=distanceToGrid*nRvZ;
    
//    cout<<" nRvY "<<nRvY<<" startY "<<startY<<endl;
//    nucCorrTable->getAllParameters(0.,  GaussVoigtRelation,  currentSigmaX,  VoigtSigma, VoigtLg);//in cm
//         voigtCorrectionFactor=NumericalIntegration(0., VoigtSigma*10., VoigtLg*10.,  -(ProtonSpotPB_lateralVoxelsToCalculate+0.5)*ProtonSpotPB_stepSize*10.,(ProtonSpotPB_lateralVoxelsToCalculate+0.5)*ProtonSpotPB_stepSize*10.);
////                     double integralx=NumericalIntegration(0., VoigtSigma,VoigtLg,  -(ProtonSpotPB_lateralVoxelsToCalculate+0.5)*spacingBetweenPoints,(ProtonSpotPB_lateralVoxelsToCalculate+0.5)*spacingBetweenPoints);
//
//         voigtCorrectionFactorVector.push_back(voigtCorrectionFactor);

    rescaleRatio=1.0;
    //first depth step
    realDepth.push_back(0.);
    WEQDepth.push_back(0.);
//    voigtCorrectionFactorVector.push_back(1.);

//    MScurrentSquaredVariance.push_back(0.);
    stoppingPowerWaterVector.push_back(calculateStoppingPower(initEnergy, ProtonSpotPB_constZARatioWater, ProtonSpotPB_constExcitationEnergyWater) );
    
    //get WEQ Bragg Peak Depth
    double braggPeakDepthWaterEQ=lookUpTable->getRangeOfEnergy()*10.;
//    lookUpTable->getPeakRangeOfEnergy(initEnergy)*10.; 
    
    braggPeakDepthHyperion=-1.0;//initialize
    maxDepthHyperion=-1.0;
    
    distanceToContour=-1.0;//initialize
    
    int iHypX,iHypY,iHypZ;
    int i;
    double currRealLength=0.0;
    double scattSum=0.;
    currSquaredVarianceDueToMS=0.0;
        for(i=1;i<HUArray.size();i++)
    {
        currRealLength=i*steplength+distanceToGrid;
        //start at Beam entry in density grid, therefore distance source-Grid needed
        //proceed with scale increments and offset to density grid
        //offset included in startX,Y and Z

        //start at Beam entry in density grid, therefore distance source-Grid needed
        //proceed with scale increments and offset to density grid
        //offset included in startX,Y and Z
        pointX=startX + i*steplength*nRvX;
        pointY=startY + i*steplength*nRvY;
        pointZ=startZ + i*steplength*nRvZ;
        convertHyperionToVoxelCoordinates(pointX, pointY, pointZ, iHypX, iHypY, iHypZ);
        
        //determine distance from beam source to start of contour
        if( HUArray.size()==(i-1) )  
        {
            distanceToContour=currRealLength;

          
           maxDepthHyperion=currRealLength;//set maximum depth which was calculated
           
           //if bragg peak is behind patient braggPeakDepthHyperion=-1
           
           //add additional buffer, with last settings, just to avoid rounding errors
           realDepth.push_back(currRealLength);
           WEQDepth.push_back(currentIntegratedDepthWEQ+steplength*rescaleRatio);
           stoppingPowerWaterVector.push_back(stoppingPowerWater);
//           voigtCorrectionFactorVector.push_back(voigtCorrectionFactor);
//           MScurrentSquaredVariance.push_back(currSquaredVarianceDueToMS);
           
           realDepth.push_back(currRealLength+steplength);
           WEQDepth.push_back(currentIntegratedDepthWEQ+steplength*rescaleRatio+steplength*rescaleRatio);
           stoppingPowerWaterVector.push_back(stoppingPowerWater);
//           voigtCorrectionFactorVector.push_back(voigtCorrectionFactor);
//           MScurrentSquaredVariance.push_back(currSquaredVarianceDueToMS);
//                      MScurrentSquaredVariance.push_back(currSquaredVarianceDueToMS);

           //MScurrentSquaredVariance.push_back(currSquaredVarianceDueToMS);  

           break;
       } 
        
        //query Material
        dHUnit = HUArray.at(i);

        //convert Hounsfield Unit to material properties
        myInputClass->getOneMaterialPropertiesFromHU(dHUnit, densityMaterial, radiationLengthMaterial, 
                                                     excitationEnergyMaterial, materialAtomZARatio);

        //calculate stepSize due to different material
         if (i<=0) energyProbNextStep=initEnergy;
         else 
         {
           energyProbNextStep=lookUpTable->getInDepthEnergy( (currentIntegratedDepthWEQ+steplength)/10. );
         }
         //calculate rescale ratio for water equivalent path scaling       
         stoppingPowerWater=calculateStoppingPower(energyProbNextStep, ProtonSpotPB_constZARatioWater, ProtonSpotPB_constExcitationEnergyWater);
         stoppingPowerMaterial=calculateStoppingPower(energyProbNextStep,materialAtomZARatio, excitationEnergyMaterial);
         rescaleRatio= getStoppingPowerRatioToWater(stoppingPowerWater, stoppingPowerMaterial)*densityMaterial;

         currentIntegratedDepthWEQ+=steplength*rescaleRatio;

         
         if ( isnan(currentIntegratedDepthWEQ) ||  isinf(currentIntegratedDepthWEQ) || currentIntegratedDepthWEQ<0.0) 
                                 {cerr<<"ERROR currentIntegratedDepthWEQ inf or nan or negative:"<< currentIntegratedDepthWEQ<<endl; exit(-1);}
         if ( isnan(currRealLength) ||  isinf(currRealLength) || currRealLength<0.0) 
                                 {cerr<<"ERROR currRealLength inf or nan or negative:"<< currRealLength<<endl; exit(-1);}
                                 
         realDepth.push_back(currRealLength);
         WEQDepth.push_back(currentIntegratedDepthWEQ);
         stoppingPowerWaterVector.push_back(stoppingPowerWater);       
         
//         nucCorrTable->getAllParameters(currentIntegratedDepthWEQ/10.,  GaussVoigtRelation,  currentSigmaX,  VoigtSigma, VoigtLg);//in cm
//
//         if(i % 2 ==0)
//         {
//         voigtCorrectionFactor=NumericalIntegration(0., VoigtSigma*10., VoigtLg*10.,  -(ProtonSpotPB_lateralVoxelsToCalculate+0.5)*ProtonSpotPB_stepSize*10.,(ProtonSpotPB_lateralVoxelsToCalculate+0.5)*ProtonSpotPB_stepSize*10.);
//         }
//         voigtCorrectionFactorVector.push_back(voigtCorrectionFactor);
//         cout<<"currDepthWEQ "<<currentIntegratedDepthWEQ<< " voigtCorrectionFactor "<<voigtCorrectionFactor<<" Vsig: "<<VoigtSigma*10.<<" borderleft: "<<-ProtonSpotPB_lateralVoxelsToCalculate*ProtonSpotPB_stepSize<<endl;
         
//         
//         cout<<"initEnergy: "<<initEnergy<<" currentDepthWEQ: "<<currentIntegratedDepthWEQ<<" voigtCorrectionFactor "<<voigtCorrectionFactor<<endl;
//         cout<<"VSig: "<<VoigtSigma*10.<<" VLg: "<<VoigtLg*10.
//                 <<" leftArea: "<<-ProtonSpotPB_lateralVoxelsToCalculate*ProtonSpotPB_stepSize
//                 <<" rightArea: "<<ProtonSpotPB_lateralVoxelsToCalculate*ProtonSpotPB_stepSize
//                 <<" voigtCorrectionFactor "<<voigtCorrectionFactor
//                 <<endl;
         
//         cout        <<" ProtonSpotPB_lateralVoxelsToCalculate "<<ProtonSpotPB_lateralVoxelsToCalculate<<" ProtonSpotPB_stepSize "<<ProtonSpotPB_stepSize
//                 <<" voigtCorrectionFactor "<<voigtCorrectionFactor<<
//                 endl;
         //find Bragg Peak
         if(braggPeakDepthHyperion==-1.0)
         if( currentIntegratedDepthWEQ>=braggPeakDepthWaterEQ ) 
            {
                braggPeakDepthHyperion=currRealLength;
            }
         
//             if(initEnergy>163. && initEnergy<164.)
//                 cout<<"realDepth: "<<currRealLength-distanceToGrid<<" weq: "<<currentIntegratedDepthWEQ<<endl;
         
        
        
    }
     calculateVoigtCorrection(WEQDepth,voigtCorrectionFactorVector);

//    exit(0);
   return; 
}

inline void calculateVoigtCorrection(std::vector<double>& WEQDepth,std::vector<double>& voigtCorrectionFactorVector)
{
    voigtCorrectionFactorVector.clear();
    voigtCorrectionFactorVector.resize(WEQDepth.size());
    
#pragma omp parallel shared(voigtCorrectionFactorVector,WEQDepth)
  {
            
//  #pragma omp for schedule(static) nowait
//  #pragma omp for schedule(runtime) nowait
  #pragma omp for schedule(guided) nowait
#   
    for(int i=0;i<(int)WEQDepth.size();i++)
    {
//        cout<<"depth: "<<WEQDepth.at(i)<<" size: "<<WEQDepth.size()<<endl;
        double GaussVoigtRelation,  currentSigmaX,  VoigtSigma, VoigtLg,voigtCorrectionFactor;
         nucCorrTable->getAllParameters(WEQDepth.at(i)/10.,  GaussVoigtRelation,  currentSigmaX,  VoigtSigma, VoigtLg);//in cm

         if(i % 2 ==0)
         {
         voigtCorrectionFactor=NumericalIntegration(0., VoigtSigma*10., VoigtLg*10.,  -(ProtonSpotPB_lateralVoxelsToCalculate+0.5)*ProtonSpotPB_stepSize*10.,(ProtonSpotPB_lateralVoxelsToCalculate+0.5)*ProtonSpotPB_stepSize*10.);
//         voigtCorrectionFactor=NumericalIntegration(0., VoigtSigma*10., VoigtLg*10.,  -24.04,24.04);

         }
         voigtCorrectionFactorVector.at(i)=voigtCorrectionFactor;  
//         cout<<"i "<<i<<" corr "<<voigtCorrectionFactor<<endl;
//         cout<<"area: "<<-(ProtonSpotPB_lateralVoxelsToCalculate+0.5)*ProtonSpotPB_stepSize*10.<<endl;
    }
  }
   
  return;   
}
 
 
 
 


 
 
//necessary for correction of the voigt functin
//voigt function is unlimited but normalized, calculating limited range changes normalization-> needs to be corrected
inline double NumericalIntegration(double mu, double Vsig, double VLg, double startInterval,double endInterval)
{
   // Create the function and wrap it
//   TF1 f("Voigt Function", " TMath::Voigt(x-[0],[1],[2],4)", startInterval, endInterval);
//   f.SetParameter(0,mu);
//   f.SetParameter(1,Vsig);
//   f.SetParameter(2,VLg);
//   ROOT::Math::WrappedTF1 wf1(f);
   
   voigtFkt f;
   f.setParameters(mu, Vsig, VLg);
   
   // Create the Integrator
//   ROOT::Math::GSLIntegrator ig(ROOT::Math::IntegrationOneDim::kADAPTIVE);
      ROOT::Math::GaussLegendreIntegrator ig;
//      ROOT::Math::GaussIntegrator ig;
//    ig.SetNumberPoints(30);
   // Set parameters of the integration
   ig.SetFunction(f);
      ig.SetRelTolerance(0.0001);
//      ig.SetRelTolerance(1000.);

//   ig.SetRelTolerance(1000.);
  
   return ig.Integral(startInterval, endInterval);
//   return 1.;
}


////necessary for correction of the voigt functin
////voigt function is unlimited but normalized, calculating limited range changes normalization-> needs to be corrected
//inline double NumericalIntegration(double mu, double Vsig, double VLg, double startInterval,double endInterval)
//{
//   // Create the function and wrap it
//   TF1 f("Voigt Function", " TMath::Voigt(x-[0],[1],[2],4)", startInterval, endInterval);
//   f.SetParameter(0,mu);
//   f.SetParameter(1,Vsig);
//   f.SetParameter(2,VLg);
//   ROOT::Math::WrappedTF1 wf1(f);
// 
//   // Create the Integrator
////   ROOT::Math::GSLIntegrator ig(ROOT::Math::IntegrationOneDim::kADAPTIVE);
//      ROOT::Math::GaussLegendreIntegrator ig;
////      ROOT::Math::GaussIntegrator ig;
////    ig.SetNumberPoints(30);
//   // Set parameters of the integration
//   ig.SetFunction(wf1);
//   ig.SetRelTolerance(0.0001);
////      ig.SetRelTolerance(1000.);
//
////   ig.SetRelTolerance(1000.);
//  
//   return ig.Integral(startInterval, endInterval);
//}

//necessary for correction of the voigt functin
//voigt function is unlimited but normalized, calculating limited range changes normalization-> needs to be corrected
inline double NumericalIntegrationGaus(double mu, double sig, double startInterval,double endInterval)
{
   // Create the function and wrap it
   TF1 f("Voigt Function", " TMath::Gaus(x, [0], [1], 1)", startInterval, endInterval);
   f.SetParameter(0,mu);
   f.SetParameter(1,sig);
   ROOT::Math::WrappedTF1 wf1(f);
 
   // Create the Integrator
//   ROOT::Math::GSLIntegrator ig(ROOT::Math::IntegrationOneDim::kADAPTIVE);
      ROOT::Math::GaussIntegrator ig;
 
   // Set parameters of the integration
   ig.SetFunction(wf1);
   ig.SetRelTolerance(0.0001000);
  
   return ig.Integral(startInterval, endInterval);
}

 
// void  calcuateWEQandMSarrays(std::vector<double>& HUArray, 
//        std::vector<double>& realDepth,std::vector<double>& WEQDepth,std::vector<double>& MScurrentSquaredVariance,
//        bool& reachedBraggPeak, double& braggPeakDepthHyperion,double& maxDepthHyperion, double& distanceToContour,
//        double initEnergy,
//        double distanceToGrid,double BeamX0, double BeamY0)
//{   
//
////    std::vector<double> MSScatAngleSquared;  
////    MSScatAngleSquared.clear();
////    MSScatAngleSquared.reserve(1000);
//    
//    realDepth.clear();
//    realDepth.reserve(1000);
//    WEQDepth.clear();
//    WEQDepth.reserve(1000);
//    MScurrentSquaredVariance.clear();
//    MScurrentSquaredVariance.reserve(1000);
//    
//    double startX,startY,startZ;
//    double xTemp2,yTemp2,zTemp2;
//    
//    double nRvX,nRvY,nRvZ;
//    nRvX=0.;
//    nRvY=0.;
//    nRvZ=1.;
//    startX=startY=startZ=0.;
//    
//    double scatteringAngleSquared,currSquaredVarianceDueToMS;
//
//    double energyProbNextStep=0.0;
//    double pointX,pointY,pointZ;
//    double steplength=gridStepSize/3.;
//    double dHUnit,densityMaterial, radiationLengthMaterial,excitationEnergyMaterial, materialAtomZARatio;
//    double stoppingPowerWater,stoppingPowerMaterial,rescaleRatio;
//    //start with offset to grid, density is air
//    double currentIntegratedDepthWEQ=distanceToGrid*1.20E-03;
//    //start at Beam entry in density grid, therefore distance source-Grid needed
//    startX+=distanceToGrid*nRvX;
//    startY+=distanceToGrid*nRvY;
//    startZ+=distanceToGrid*nRvZ;
////    cout<<" offset to grid: "<<distanceToGrid<<endl;
////               double distanceFromBeamX;
////                double distanceFromBeamY;
////                double distanceFromBeamZ;
////    getDistanceFromBeamAndDepthAlongBeamAxis(bx,BeamX0,BeamY0, startX,  startY,  startZ, distanceFromBeamX,  distanceFromBeamY, distanceFromBeamZ);
////    cout<<"function says: startPointOffset distanceFromBeamX "<<distanceFromBeamX<<" distanceFromBeamY "<<distanceFromBeamY<<" distanceFromBeamZ "<<distanceFromBeamZ<<endl;
//
//    rescaleRatio=1.0;
//    //first depth step
//    realDepth.push_back(0.);
//    WEQDepth.push_back(0.);
//    MScurrentSquaredVariance.push_back(0.);
//    //get WEQ bragg Peak Depth
//    double braggPeakDepthWaterEQ=lookUpTable->getPeakRangeOfEnergy(initEnergy)*10.; 
//    braggPeakDepthHyperion=-1.0;//initialize
//    maxDepthHyperion=-1.0;
//    
//    distanceToContour=-1.0;//initialize
//    
////    cout<<"WEQ XDim: "<<XDim<<" YDim "<<YDim<<" ZDim "<<ZDim<<endl;
//    int iHypX,iHypY,iHypZ;
//    int i;
//    double currRealLength=0.0;
//    double scattSum=0.;
//    currSquaredVarianceDueToMS=0.0;
//    for(i=1;i<HUArray.size();i++)
//    {
//        currRealLength=i*steplength+distanceToGrid;
//        //start at Beam entry in density grid, therefore distance source-Grid needed
//        //proceed with scale increments and offset to density grid
//        //offset included in startX,Y and Z
//       
//        //determine distance from beam source to start of contour
//        if( HUArray.size()==(i-1) )
//        {
//            distanceToContour=currRealLength;
//           if (currentIntegratedDepthWEQ>=braggPeakDepthWaterEQ) reachedBraggPeak=true;
//           else reachedBraggPeak=false;
////           cout<<"Reached end of grid."<<endl;
////               double distanceFromBeamX;
////                double distanceFromBeamY;
////                double distanceFromBeamZ;
////           cout<<"at iHypX: "<<iHypX<<" iHypY "<<iHypY<<" iHypZ "<<iHypZ<<endl;
////           cout<<"MaxDepthreal "<<(i-1)*steplength+distanceToGrid<<" max WEQ depth "<<currentIntegratedDepthWEQ<<endl;
////           getDistanceFromBeamAndDepthAlongBeamAxis(bx,BeamX0,BeamY0, pointX,  pointY,  pointZ, distanceFromBeamX,  distanceFromBeamY, distanceFromBeamZ);
////           cout<<"function says: distanceFromBeamX "<<distanceFromBeamX<<" distanceFromBeamY "<<distanceFromBeamY<<" distanceFromBeamZ "<<distanceFromBeamZ<<endl;
////           cout<<"steps: "<<i<<" steplength "<<steplength<<endl;
//           
//           maxDepthHyperion=currRealLength;//set maximum depth which was calculated
//           
//           //add additional buffer, with last settings, just to avoid rounding errors
//           realDepth.push_back(currRealLength);
//           WEQDepth.push_back(currentIntegratedDepthWEQ+steplength*rescaleRatio);
//           realDepth.push_back(currRealLength+steplength);
//           WEQDepth.push_back(currentIntegratedDepthWEQ+steplength*rescaleRatio);
//           break;
//       } 
//        
//        //query Material
//        dHUnit = HUArray.at(i);
//
//        //convert Hounsfield Unit to material properties
//        myInputClass->getOneMaterialPropertiesFromHU(dHUnit, densityMaterial, radiationLengthMaterial, 
//                                                     excitationEnergyMaterial, materialAtomZARatio);
//
//        //calculate stepSize due to different material
//         if (i<=0) energyProbNextStep=initEnergy;
//         else 
//         {
//           energyProbNextStep=lookUpTable->getInDepthEnergy( (currentIntegratedDepthWEQ+steplength)/10. );
//         }
//         //calculate rescale ratio for water equivalent path scaling       
//         stoppingPowerWater= calculateStoppingPower(energyProbNextStep, ProtonSpotPB_constZARatioWater, ProtonSpotPB_constExcitationEnergyWater);
//         stoppingPowerMaterial= calculateStoppingPower(energyProbNextStep,materialAtomZARatio, excitationEnergyMaterial);
//         rescaleRatio=  getStoppingPowerRatioToWater(stoppingPowerWater, stoppingPowerMaterial)*densityMaterial;
//         currentIntegratedDepthWEQ+=steplength*rescaleRatio;
//         
//         realDepth.push_back(currRealLength);
//         WEQDepth.push_back(currentIntegratedDepthWEQ);
//         
//         if( currentIntegratedDepthWEQ>=braggPeakDepthWaterEQ && braggPeakDepthHyperion==-1.0 ) 
//         {
//             braggPeakDepthHyperion=currRealLength;
////             else if ( (currentIntegratedDepthWEQ>=(braggPeakDepthWaterEQ*1.5) ) && maxDepthHyperion==-1.0)  maxDepthHyperion=currRealLength;
//         }
//         
//         //calculate scattering angle
//         
//         //recalculate the scattering Angle every z step, due to multiple scattering corrections
//         scatteringAngleSquared= getScatteringAngleSquare(steplength*rescaleRatio,
//                                                         lookUpTable->getInDepthEnergy( currentIntegratedDepthWEQ/10.), radiationLengthMaterial);
//         //store in array
//         //MSScatAngleSquared.push_back(scatteringAngleSquared*(0.9*0.9));//reduce calculated angle by ten percent
//
//         //calculate current squared variance by looping over all previous angles and steps
//    
//           currSquaredVarianceDueToMS=currSquaredVarianceDueToMS+ scattSum * steplength*steplength;
//           scattSum=scattSum+scatteringAngleSquared*(0.9*0.9) ;//reduce calculated angle by ten percent
//         
//         //currSquaredVarianceDueToMS = getSummedScattAngle(MSScatAngleSquared,steplength);
//         if ( isnan(currSquaredVarianceDueToMS) ||  isinf(currSquaredVarianceDueToMS) || currSquaredVarianceDueToMS<0.0) 
//                                 {cerr<<"ERROR currSquaredVarianceDueToMS inf or nan or negative:"<< currSquaredVarianceDueToMS<<endl; exit(-1);}
////         MScurrentSquaredVariance.push_back(currSquaredVarianceDueToMS); 
//                  MScurrentSquaredVariance.push_back(0.);  
//
//         
////         cout<<"initEnergy: "<<initEnergy<<" currEnergy: "<<energyProbNextStep<<" HU: "<<dHUnit<<" WEQdepth: "<< currentIntegratedDepthWEQ<<" realDepth: "<<currRealLength<<endl;
//    }
//
//   return; 
//}

//gets everything in cm
inline double getDoseAtVoxel(double dHUnit, interpol* weqLUT,interpol* MSsquaredVarianceLUT,interpol* stoppingPowerWaterLUT,interpol* voigtCorrectionLUT,
                                                Double_t *beamDataArray, 
                                                double distanceFromBeamX,double distanceFromBeamY,double distanceFromBeamZ)
{
//      double beamX0             = beamDataArray[0];
//      double beamY0             = beamDataArray[1];      
      double beamInitSigmaX     = beamDataArray[2];
      double beamInitSigmaY     = beamDataArray[3];
      double thisBeamFluence    = beamDataArray[4];
      double initEnergy         = beamDataArray[5];
//      double beamInitSigmaXWidening = beamDataArray[6];
//      double beamInitSigmaYWidening = beamDataArray[7];

          double currentSigmaX,currentSigmaY;        

//    //get distances , now queried outside of this function
//    double distanceFromBeamX,distanceFromBeamY, beamDepth;
//    getDistanceFromBeamAndDepthAlongBeamAxis(bx, beamX0, beamY0,(double) iHypX*gridStepSize,(double) iHypY*gridStepSize,(double) iHypZ*gridStepSize,
//                                             distanceFromBeamX, distanceFromBeamY, beamDepth);    

    //get WEQ depth
    double weqDepth=weqLUT->getData(distanceFromBeamZ);

    //determine material properties
    //get Hounsfield unit from Hyperion and convert to HU as Hyperion gives
    //raw scanner value, converted to Hounsfield unit
//    double dHUnit = convertCTtoHU( m_pDoseCompGrid->operator()(iHypX,iHypY,iHypZ) );
        
    double densityMaterial, radiationLengthMaterial, excitationEnergyMaterial, materialAtomZARatio;
    //convert Hounsfield Unit to material properties
//    myInputClass->getOneMaterialPropertiesFromHU(dHUnit, densityMaterial, radiationLengthMaterial, 
//                                                 excitationEnergyMaterial, materialAtomZARatio);
    
    //get rescale ratio
//    double currEnergy=lookUpTable->getInDepthEnergy( weqDepth/10.);//LUT in cm
    
//    //double stoppingPowerWater=ProtonSpotPB::calculateStoppingPower(currEnergy, ProtonSpotPB_constZARatioWater, ProtonSpotPB_constExcitationEnergyWater);
//    double stoppingPowerWater= stoppingPowerWaterLUT->getDataClosestMatch(distanceFromBeamZ);
//    double stoppingPowerWater= stoppingPowerWaterLUT->getData(distanceFromBeamZ);
//    double stoppingPowerWater=calculateStoppingPower(currEnergy, ProtonSpotPB_constZARatioWater, ProtonSpotPB_constExcitationEnergyWater);

//    double stoppingPowerMaterial=calculateStoppingPower(currEnergy,materialAtomZARatio, excitationEnergyMaterial);
    double rescaleRatio= 1;//getStoppingPowerRatioToWater(stoppingPowerWater, stoppingPowerMaterial)*densityMaterial;
    
    //get BeamShape

//    double MSsquaredVariance=MSsquaredVarianceLUT->getData(weqDepth);//newLUTs in mm
//    double currentSigmaX=sqrt(MSsquaredVariance/100.+pow((beamInitSigmaX+ (weqDepth/10.)*beamInitSigmaXWidening),2) );//in cm
//    double currentSigmaY=sqrt(MSsquaredVariance/100.+pow((beamInitSigmaY+ (weqDepth/10.)*beamInitSigmaYWidening),2) );//in cm
    
    
//    currentSigmaX=currentSigmaY=nucCorrTable->getGaussSigma(weqDepth/10.);//in cm
//
//    //nuclear correction is calculated with function
//    double GaussVoigtRelationO=nucCorrTable->getGaussVoigtRelation(weqDepth/10.);
//    double VoigtSigmaO=nucCorrTable->getVoigtSigma(weqDepth/10.);
//    double VoigtLgO=nucCorrTable->getVoigtLg(weqDepth/10.);
//    double GaussSigmaO=nucCorrTable->getGaussSigma(weqDepth/10.);//in cm
    
    //get data from LUT
    double GaussVoigtRelation,VoigtSigma,VoigtLg;
      nucCorrTable->getAllParameters(weqDepth/10.,  GaussVoigtRelation,  currentSigmaX,  VoigtSigma, VoigtLg);//in cm
      currentSigmaY=currentSigmaX;
//      double voigtCorrectionFactor=voigtCorrectionLUT->getData(distanceFromBeamZ);
      double voigtCorrectionFactor=voigtCorrectionLUT->getDataClosestMatch(distanceFromBeamZ);

      
//      cout<<"diffGaussVoigtRelation "<<GaussVoigtRelation-GaussVoigtRelationO<<
//            " diffSigma "<<currentSigmaX-GaussSigmaO<<
//            " diffVoigtSigma "<<VoigtSigma-VoigtSigmaO<<
//              " diffVoigtLG "<<VoigtLg-VoigtLgO<<endl;
//      
//     cout<<"GVR "<< GaussVoigtRelation<<" GVRO "<<GaussVoigtRelationO<<
//            " GSsig "<< currentSigmaX<<" GSsigO "<<GaussSigmaO<<
//             " VSig "<< VoigtSigma<<" VSigO "<<VoigtSigmaO<<
//             " VLg "<< VoigtLg<<" VLgO "<<VoigtLgO<<endl;
      

     if ( isnan(GaussVoigtRelation) ||  isinf(GaussVoigtRelation) || GaussVoigtRelation<0. ) {cerr<<"ERROR: Aborting, GaussVoigtRelation inf or nan or negative :"<< GaussVoigtRelation<<endl;cout<<" weqDepth "<<weqDepth<<endl; exit(-1);}
     if ( isnan(VoigtSigma) ||  isinf(VoigtSigma) || VoigtSigma<0. ) {cerr<<"ERROR: Aborting, VoigtSigma inf or nan or negative :"<< VoigtSigma<<endl;cout<<" weqDepth "<<weqDepth<<endl; exit(-1);}
     if ( isnan(VoigtLg) ||  isinf(VoigtLg) || VoigtLg<0. ) {cerr<<"ERROR: Aborting, VoigtLg inf or nan or negative :"<< VoigtLg<<endl;cout<<" weqDepth "<<weqDepth<<endl; exit(-1);}
    
    //get weightFactor
    double weightFactorNoFluence=getBeamShapeAtDistanceFromCenter(distanceFromBeamX,distanceFromBeamY,
                                                          currentSigmaX*10., currentSigmaY*10.,
                                                          beamInitSigmaX*10.,beamInitSigmaY*10.,
                                                          GaussVoigtRelation,VoigtSigma*10.,
                                                          VoigtLg*10., ProtonSpotPB_stepSize*10., voigtCorrectionFactor);//everything in mm 
//    cout<<"distanceFromBeamX "<<distanceFromBeamX<<" currentSigmaXinmm "<<currentSigmaX*10.
//            <<" beamInitSigmaXinmm "<<beamInitSigmaX*10.<<" VoigtSigmainmm "<<VoigtSigma*10.<<endl;
    //calc Dose
    //calculate dose deposition factor e.g. fraction of dose deposited at this depth   
     //LUT contains edep for water, should be the same as dose    
     double inDepthEdepDepositionFactor=lookUpTable->getEdep(weqDepth/10.);
     
     double rbeMuliplikator=1.;
     
//##############################################################################
//###########RBE Code
//##############################################################################     
#ifdef USERBE
//evaluate RBE
     
    //Protons
 #ifdef USEPROTONBEAM
     rbeMuliplikator=1.1;
 #endif	/* USEPROTONBEAM */
    //Helium
 #ifdef USEHELIUMBEAM
     double maxEdep=lookUpTable->getMaxDepositedEnergy();    

     rbeMuliplikator=1.1;
     if( inDepthEdepDepositionFactor > (0.5*maxEdep) )
     {
         rbeMuliplikator=1.2;
     }
     if( inDepthEdepDepositionFactor > (0.6*maxEdep) )
     {
       rbeMuliplikator=1.3;
     }     
     
     double braggPeakDepthWaterEQ=lookUpTable->getPeakRangeOfEnergy(initEnergy)*10.; 
     //determine if position is behind Bragg Peak
     //if so, apply maximum rbe Multiplikator
     if (weqDepth>braggPeakDepthWaterEQ) rbeMuliplikator=1.3;
     
 #endif	/* USEHELIUMBEAM */ 

#endif	/* USERBE */
     
     
//##############################################################################
//###########End of RBE Code
//##############################################################################

    
    
     double inDepthReleasedEnergy=inDepthEdepDepositionFactor*weightFactorNoFluence*thisBeamFluence*rescaleRatio;
double doseAtVoxel=weightFactorNoFluence;
       
//   cout<<"HU: "<<dHUnit <<" dosedepFactor: "<<inDepthDoseDepositionFactor<<" weightFactor "<<weightFactorNoFluence<<" thisBeamFluence "<<thisBeamFluence<<" rescaleRatio "<<rescaleRatio<<endl;
     //------------------------------------------------------------------------------                    
     //dose calculation
                         //ATTENTION !!!
                         //inDepthReleasedDose is not correct dose, needs to be scaled according to voxelsize
                         //May not be correct
                         //1 Gy = J/kg
                         //mass = density * Volume
                         //Gray= MeVtoJoule / mass *1000 (1000 due to density in gram)
                         //=>edep*ProtonSpotPB_constMeVToJoule / rescaleRatio
    double massOfVoxelInGram=densityMaterial* pow((ProtonSpotPB_stepSize),3);  
//    double doseAtVoxel=inDepthReleasedEnergy* pow((rescaleRatio),3)*ProtonSpotPB_constMeVToJoule / (massOfVoxelInGram/1000.);
//        double doseAtVoxel=inDepthReleasedEnergy * rbeMuliplikator * ProtonSpotPB_constMeVToJoule / (massOfVoxelInGram/1000.);
    if ( isnan(doseAtVoxel) ||  isinf(doseAtVoxel) || doseAtVoxel<0. ) {cerr<<"ERROR: Aborting, doseAtVoxel inf or nan or negative :"<< doseAtVoxel<<endl; exit(-1);}

//    if(m_pDoseCompGrid->isInContour(iHypX,iHypY,iHypZ)==false)
//    {
//    if(dHUnit>0.)
//    {
//    cout<<"distanceFromBeamSourceZ: "<< distanceFromBeamZ<<" weqDepth: "<<weqDepth<<" distanceFromBeamX: "<<distanceFromBeamX<<" distanceFromBeamY: "<<distanceFromBeamY<<endl;
//    cout<<" inDepthEdepDepositionFactor "<<inDepthEdepDepositionFactor<<" weightFactorNoFluence: "<<weightFactorNoFluence<<endl;
//    cout<<" HU: "<<dHUnit<<" densityMaterial "<<densityMaterial<<endl;
//    cout<<"  ix: "<<iHypX<<" iy: "<<iHypY<<" iz: "<<iHypZ<<endl;
////    cout<<"HU: "<<dHUnit<<" densityMaterial "<<densityMaterial<<" inDepthEdepDepositionFactor "<<inDepthEdepDepositionFactor<<endl;
////    cout<<" doseAtVoxelNoWeight "<<doseAtVoxel/weightFactorNoFluence<<" massOfVoxelInGram "<<massOfVoxelInGram<<endl;
////    }
//    }
 return doseAtVoxel; 
}

//singlePencilBeamAlgorithm(bx, Trafo, beamDataArray[k], dimensionZ,
//                                 distanceToGrid, reachedBraggPeaktemp, iPBXtemp,iPBYtemp,iPBZtemp);



 void  getNormVectorsForVector(double vecX,double vecY, double vecZ,
                                        double& nv1x,double& nv1y,double& nv1z,
                                        double& nv2x,double& nv2y,double& nv2z)
{
    
    nv1x=nv1y=nv1z=nv2x=nv2y=nv2z=0.0;
        //determine which element is the smallest
        //replace with 1 and other elements with 0
        //do crossproduct of both vectors, the result is an orthogonal vector
        if (vecX<= vecY && vecX<=vecZ)   vectorCrossProduct(vecX, vecY, vecZ,
                                            1.,0., 0.,
                                            nv1x,nv1y,nv1z);   
        else if (vecY<= vecX && vecY<=vecZ)   vectorCrossProduct(vecX, vecY, vecZ,
                                            0.,1., 0.,
                                            nv1x,nv1y,nv1z); 
        else if (vecZ<= vecX && vecZ<=vecY)   vectorCrossProduct(vecX, vecY, vecZ,
                                            0.,0., 1.,
                                            nv1x,nv1y,nv1z); 
        
        //calculate second orthogonal vector
        vectorCrossProduct(vecX, vecY, vecZ,
                            nv1x,nv1y, nv1z,
                            nv2x,nv2y,nv2z);
        
     //normalize vectors   
        double norm;
        norm=1./sqrt(nv1x*nv1x + nv1y*nv1y + nv1z*nv1z);
        nv1x*=norm;
        nv1y*=norm;
        nv1z*=norm;
        norm=1./sqrt(nv2x*nv2x + nv2y*nv2y + nv2z*nv2z);
        nv2x*=norm;
        nv2y*=norm;
        nv2z*=norm;

    return; 
}


 void  vectorCrossProduct(double ax, double ay, double az,
                                        double bx, double by, double bz,
                                        double& resX,double& resY,double& resZ)
{
    resX=ay*bz - az*by;
    resY=az*bx - ax*bz;
    resZ=ax*by - ay*bx;        
    return;
}


 void  convertHyperionToVoxelCoordinates(double dX, double dY, double dZ,
                                int& iX, int& iY, int& iZ) 
{
 if(dX>0.)    iX = static_cast<int>( floor(dX/gridStepSize) );
 else iX = static_cast<int>( ceil(dX/gridStepSize) );
 if(dY>0.)    iY = static_cast<int>( floor(dY/gridStepSize) );
 else iY = static_cast<int>( ceil(dY/gridStepSize) );
 if(dZ>0.)    iZ = static_cast<int>( floor(dZ/gridStepSize) );
 else iZ = static_cast<int>( ceil(dZ/gridStepSize) );

 return;
}


//############################################################################################################

//converts CT number to Hounsfield units
//By default, Rescale Intercept (Offset) is -1024
//and Rescale Slope (slope) is 1
//both defined in DICOM header (not read here, just default assumed)
 double convertCTtoHU(double CTnumber) 
{
   return CTnumber*1. - 1024.;
//   if (CTnumber<500.) return -1024.;
//   else return 0;
}
 
 Double_t cauchyBivariate(Double_t x,Double_t y, Double_t g)
 {
     Double_t ret;
     ret=g / ( TMath::Pi() * pow( ( g*g + x*x +y*y),1.5) );
   return ret;
 }
  
 
//------------------------------------------------------------------------------------------------
//------------------------INCLUDE FUNCTIONS AND CLASS FUNCTIONS
//------------------------------------------------------------------------------------------------
#include "includes/LUT_class.cpp"
#include "includes/beamDecomposition_class.cpp"
#include "includes/input_class.cpp"
#include "includes/nucLUT_class.cpp"
#include "includes/interpol.cpp"
 
