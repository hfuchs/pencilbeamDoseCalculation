/* 
 * File:   protonbeam_writeSpotsClass.cpp
 * Author: hfuchs
 * 
 * Created on December 17, 2012, 12:29 PM
 */

#include "protonbeam_writeSpotsClass.h"

writeSpotsClass::writeSpotsClass(const double& initCouchAngle, const double& initGantryAngle, const double& initSpotEnergy, const double& initSpotWeight,
                    const double& initSpotPosX, const double& initSpotPosY, const double& initSpotPosZ ):
                    couchAngle(initCouchAngle),gantryAngle(initGantryAngle), spotEnergy(initSpotEnergy), spotWeight(initSpotWeight),
                    spotPosX(initSpotPosX), spotPosY(initSpotPosY), spotPosZ(initSpotPosZ){}

writeSpotsClass::writeSpotsClass()
{
   couchAngle= gantryAngle=spotEnergy=spotWeight=spotPosX=spotPosY=spotPosZ=0.;
}

//writeSpotsClass::~protonbeam_writeSpotsClass() {
//}


double writeSpotsClass::setCouchAngle(double& initCouchAngle){ couchAngle=initCouchAngle; return couchAngle;}
double writeSpotsClass::setGantryAngle(double& initGantryAngle){ gantryAngle=initGantryAngle; return gantryAngle;}

double writeSpotsClass::setSpotEnergy(double& initSpotEnergy){ spotEnergy=initSpotEnergy; return spotEnergy;}
double writeSpotsClass::setSpotWeight(double& initSpotWeight){ spotWeight=initSpotWeight; return spotWeight;}

double writeSpotsClass::setPosX(double& initPosX){ spotPosX=initPosX; return spotPosX;}
double writeSpotsClass::setPosY(double& initPosY){ spotPosY=initPosY; return spotPosY;}
double writeSpotsClass::setPosZ(double& initPosZ){ spotPosZ=initPosZ; return spotPosZ;}


double writeSpotsClass::getCouchAngle(){ return couchAngle;}
double writeSpotsClass::getGantryAngle(){ return gantryAngle;}

double writeSpotsClass::getSpotEnergy( ){ return spotEnergy;}
double writeSpotsClass::getSpotWeight( ){ return spotWeight;}

double writeSpotsClass::getPosX( ){ return spotPosX;}
double writeSpotsClass::getPosY( ){ return spotPosY;}
double writeSpotsClass::getPosZ( ){ return spotPosZ;}






int CompareByCouchGantryAndEnergy (const writeSpotsClass& lhs, const writeSpotsClass& rhs)
{
    
return (lhs.couchAngle < rhs.couchAngle)
|| ( !(rhs.couchAngle < lhs.couchAngle) && (lhs.gantryAngle < rhs.gantryAngle) )
|| ( !(rhs.couchAngle < lhs.couchAngle) && !(rhs.gantryAngle < lhs.gantryAngle) && (lhs.spotEnergy < rhs.spotEnergy));

}

int countGantryAngles(std::vector<writeSpotsClass>& beamSpots)
{
   double tempAngle=0.;
   int counter=1;
   //initialize counter
   tempAngle=beamSpots.at(0).getGantryAngle();
   for(unsigned int i=0;i<beamSpots.size();i++)
   {
       if ( tempAngle != beamSpots.at(i).getGantryAngle() ) {
           tempAngle = beamSpots.at(i).getGantryAngle();
           counter++;
       }
   }
   return counter;
}

int countCouchAngles(std::vector<writeSpotsClass>& beamSpots)
{
   double tempAngle=0.;
   int counter=1;
   //initialize counter
   tempAngle=beamSpots.at(0).getCouchAngle();
   for(unsigned int i=0;i<beamSpots.size();i++)
   {
       if ( tempAngle != beamSpots.at(i).getCouchAngle() ) {
           tempAngle = beamSpots.at(i).getCouchAngle();
           counter++;
       }
   }
   return counter;
}

int countFields(std::vector<writeSpotsClass>& beamSpots)
{
   double tempGantry,tempCouch;
   int counter=1;
   //initialize counter
   tempGantry=beamSpots.at(0).getGantryAngle();
   tempCouch=beamSpots.at(0).getCouchAngle();

   for(unsigned int i=0;i<beamSpots.size();i++)
   {
       double curGantry,curCouch;
       curGantry=beamSpots.at(i).getGantryAngle();
       curCouch=beamSpots.at(i).getCouchAngle();
       
       if ( tempGantry != curGantry ) {
           tempGantry = curGantry;
           tempCouch = curCouch;
           counter++;
       }
       if ( tempCouch != curCouch ) {
           tempCouch = curCouch;
           counter++;
       }
   }
   return counter;
}

double countTotalBeamWeight(std::vector<writeSpotsClass>& beamSpots)
{
   double weight=0.;

   for(unsigned int i=0;i<beamSpots.size();i++)
   {
       weight+=beamSpots.at(i).getSpotWeight();       
   }
   return weight;
}

double countFieldWeight(std::vector<writeSpotsClass>& beamSpots, double gantryAngle, double couchAngle)
{
   double weight=0.;
   
   for(unsigned int i=0;i<beamSpots.size();i++)
   {
       if(beamSpots.at(i).getGantryAngle()==gantryAngle && beamSpots.at(i).getCouchAngle()==couchAngle)
                weight+=beamSpots.at(i).getSpotWeight();       
   }
   return weight;
}


double countFieldWeightPerEnergy(std::vector<writeSpotsClass>& beamSpots, double gantryAngle, double couchAngle, double energy)
{
   double weight=0.;
   
   for(unsigned int i=0;i<beamSpots.size();i++)
   {
       if(beamSpots.at(i).getGantryAngle()==gantryAngle && beamSpots.at(i).getCouchAngle()==couchAngle && beamSpots.at(i).getSpotEnergy()==energy)
                weight+=beamSpots.at(i).getSpotWeight();       
   }
   return weight;
}



int countEnergiesPerField(std::vector<writeSpotsClass>& beamSpots, double gantryAngle, double couchAngle)
{
   int counter=0;
   double temp=0.0;
   for(unsigned int i=0;i<beamSpots.size();i++)
   {
       if(beamSpots.at(i).getGantryAngle()==gantryAngle && beamSpots.at(i).getCouchAngle()==couchAngle)
       {
           if(temp!=beamSpots.at(i).getSpotEnergy() ) 
           {
               counter++; 
               temp=beamSpots.at(i).getSpotEnergy();
           }
       }
   }
   return counter;
}

int countSpotsPerEnergyPerField(std::vector<writeSpotsClass>& beamSpots, double gantryAngle, double couchAngle, double energy)
{
   int counter=0;
   for(unsigned int i=0;i<beamSpots.size();i++)
   {
       if(beamSpots.at(i).getGantryAngle()==gantryAngle && beamSpots.at(i).getCouchAngle()==couchAngle && beamSpots.at(i).getSpotEnergy()==energy)
       {
               counter++; 
       }
   }
   return counter;
}

double convertRadToDeg(double rad)
{
    //PI
    static const double constPI=3.141592653589793238;
    
    double deg;
    deg=rad / (constPI/180.);
    return deg;
}

bool fileExists(const std::string& filename)
{
    struct stat buf;
    if (stat(filename.c_str(), &buf) != -1)
    {
        return true;
    }
    return false;
}
//
//bool fileExists(const char *fileName)
//{
//    ifstream infile(fileName);
//    return infile.good();
//}

