 /* File:   ProtonSpotPB.h
 * Author: Hermann Fuchs
 *
 * Created on 07 February 2012, 14:33
 */
#include "ProtonSpotPB.h"

#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <fstream>

#include <math.h>
#include <string>
#include <vector>
#include <assert.h>

#if defined(WIN32)
#include <erfQ.h>
#endif


#include <XiPSData.h>
#include <DensityDoseCompGrid.h>
#include <ProtonSpotGeometry.h>
#include <DoseMeta.h>

#include <PropertyGrid3D.h>

//#define CMSXIO

#ifndef CMSXIO
#include <DoseAtomSpec.h>
#else
#include <DoseAtomSpec_XIO.h>
#endif

using namespace std;

namespace Ukt
{

ProtonSpotPB::ProtonSpotPB(std::auto_ptr<DensityDoseCompGrid> pDDCG, 
			   std::auto_ptr<Grid3D<bool> > pTG, 
			   std::auto_ptr<XiPSData> pDNA) :
  SpotPB(),                         
  m_pDoseCompGrid(pDDCG.release()), 
  m_pTargetGrid(pTG.release()),
  m_pData( pDNA.release() )
{
cout<<"initializing constructur ProtonSpotPB"<<endl;
  m_pDoseGrid = new Grid3D<float>(*m_pDoseCompGrid);
  //m_pDoseGrid = new Grid3D<float>( 100, 100, 100 );

cout<<"casting Dim ProtonSpotPB"<<endl;  
  XDim = static_cast<int>(m_pDoseCompGrid->getWidth());
  YDim = static_cast<int>(m_pDoseCompGrid->getHeight());
  ZDim = static_cast<int>(m_pDoseCompGrid->getDepth());
cout<<"setting voxelSize ProtonSpotPB"<<endl; 
  gridStepSize = m_pDoseCompGrid->getVoxelSize();
  
  cout<<"initializing LUT classes ProtonSpotPB"<<endl;
    lookUpTable= new LUT;
    nucCorrTable= new nucLUT;
    myInputClass= new inputClass;
cout<<"setting LUT classes config ProtonSpotPB"<<endl;
    Double_t stepSizeInterpolData=0.05;//step size of the interpolation data files in cm
    lookUpTable->setDimension(2000);//set dimension of read data file

#ifdef USEHELIUMBEAM
    lookUpTable->setFolderName((char *)"calibrationHelium/"); 
    //lookUpTable->setFolderName((char *)"calibrationHeliumEnergGaussSmeared3MeV/"); 
#endif	/* USEHELIUMBEAM */
#ifdef USEPROTONBEAM
    lookUpTable->setFolderName((char *)"calibrationProton/"); 
    //lookUpTable->setFolderName((char *)"calibrationProtonEnergGaussSmeared3MeV/"); 
#endif	/* USEPROTONBEAM */
    //lookUpTable->setFolderName((char *)"/home/hfuchs/testdevel/calibrationHeliumcalibrationHelium/"); 
    lookUpTable->setDataFilePrefix((char *)"OneDim_Edep_");
    lookUpTable->setConfigFileName((char *)"dataAvailable.dat");
    lookUpTable->setStepSizeDataInCM(stepSizeInterpolData);//step size of the interpolation data files in cm
    lookUpTable->setStepSizeCalculation(gridStepSize*0.1);//in cm
    lookUpTable->setNumbOfParticlesInBeam(1);//set to one due to scaling with multiple beams only necessary with LUT
    lookUpTable->calculateTotalDepositedEnergy();
    lookUpTable->fillPenetrationDepthArray();
cout<<"setting nucCorr classes config ProtonSpotPB"<<endl;
    nucCorrTable->setDimension(2000);//set dimension of read data file

#ifdef USEHELIUMBEAM
    nucCorrTable->setFolderName((char *)"calibrationHelium/");
#endif	/* USEHELIUMBEAM */
#ifdef USEPROTONBEAM
    nucCorrTable->setFolderName((char *)"calibrationProton/"); 
#endif	/* USEPROTONBEAM */
//    nucCorrTable->setFolderName((char *)"calibrationHelium/"); 
//nucCorrTable->setFolderName((char *)"calibrationHelium/"); 
    nucCorrTable->setDataFilePrefix((char *)"nucCorrLatFit_GATE_Water_2mm_");
    nucCorrTable->setConfigFileName((char *)"dataAvailable.dat");
    nucCorrTable->setLutSigma(0.2);////set initial beam sigma of LUT data in cm
    nucCorrTable->setStepSizeDataInCM(stepSizeInterpolData);//step size of the interpolation data files in cm
    nucCorrTable->setStepSizeCalculation(gridStepSize*0.1);//in cm
    nucCorrTable->readInEnergies();//read in available energies
    

 cout<<"setting input classes config ProtonSpotPB"<<endl;   
    myInputClass->setDataFileName((char*)"calibrationHelium/myHUDataFile.dat");
    myInputClass->readInDataFile();   
  
    lookUpTable->setTargetEnergy(50.);    
    nucCorrTable->setTargetEnergy(50.);
        
    //ATTENTION: PB used cm not mm!!!
    ProtonSpotPB_stepSize=gridStepSize/10.; //set step size to same step size as hyperion     

//    ProtonSpotPB_initFluence = 1000000000000; //set initial beam fluence
        ProtonSpotPB_initFluence=1000000;//set initial beam fluence

  
    //number of lateral voxels to calculate
    ProtonSpotPB_lateralVoxelsToCalculate=5;//from position plus and minus number of Voxels
        
    conversionMMtoVoxel=1.0/gridStepSize;
    
    //uncommenting disables roots internal seg fault debugging
//    gSystem->IgnoreSignal(kSigSegmentationViolation,true); 


cout<<"finished constructor ProtonSpotPB"<<endl;
}

//#######################################################################################################

ProtonSpotPB::~ProtonSpotPB()
{
  delete m_pDoseCompGrid;
  delete m_pDoseGrid;
  delete m_pData;
  
  delete lookUpTable;
  delete nucCorrTable;
  delete myInputClass;
}


DoseMeta* ProtonSpotPB::calcDose(const DoseAtomSpec& bx) 
  // the central part of the dose calculation. The orientation of bx is used to
  // calculate the SSD first, then a raytracing is performed that maps radiological
  // depth onto geometrical depth. Then for all voxel that intersect with the beam,
  // the rdacs are stored in m_e. Finally, a new rdac array is created and m_e copied into it.
  // Failure to allocate or during dose calcs causes exit 
  // A check is performed if the ray hits the target, (commented out)
  // important notice: the cube spans from -0.5 through X_D - 0.5 / Z_D - 0.5,
  // whereas the coordinates run from 0 through X_D - 1 / Z_D - 1. The coords describe
  // the centre of the spherical sampling volume.
 {
//cout<<"starting ProtonSpotPB::calcDose"<<endl;
//for time measurements
//struct timeval start, end;
//long mtime, seconds, useconds;    

//set starting time for calculation time measurement
//gettimeofday(&start, NULL);

//initialize some kind of geometry object
//     const ProtonSpotGeometry* const pGeo = static_cast<const ProtonSpotGeometry* const>(bx.getRayGeometry());
//     if(pGeo == NULL) 
//      return NULL;

//exit(0);
m_pDoseGrid->clear();

     //create coordinate transformation object
     UktBase::BeamPatientTrafo Trafo( bx );
     
    //go to beams-eye view coordinate system
    
//   double distanceToGrid;
//   bool hitTarget=getDistanceToDensGridIfHit(bx,BeamX0,BeamY0,0.0,distanceToGrid);
   bool reachedBraggPeak,reachedBraggPeaktemp;
//   Int_t iPBX,iPBY,iPBZ, iPBXtemp,iPBYtemp,iPBZtemp;
//   iPBX=iPBY=iPBZ=iPBXtemp=iPBYtemp=iPBZtemp=0;//initialize to relatively save value
   reachedBraggPeak=reachedBraggPeaktemp=false;

    //split beam into sub-Beams
    //beamDataArray entries are x0,y0,sigmaX,sigmaY,initFluence,initEnergy
    //per beam
    //precision should be number of beams in one dimension
    Int_t precision = 1;//pGeo->getPrecision();
    //allocate memory
    double **beamDataArray = new double*[precision*precision];
    for (Int_t i=0;i<precision*precision;i++)
    {
        beamDataArray[i]=new double[8];
    } 
    //Calculate all sub-beams initial positions and configurations
    beamDecomposition(bx,beamDataArray, precision);

    //Calculate all sub-beams
    //One after the other  
    double dBPX,dBPY,dBPZ;
    double tempx,tempy,tempz;
    for (Int_t k=0;k<precision*precision;k++)
    {
//        cout<<"--------------------------------------------"<<std::endl;
//        cout<<"Current Beam: "<<k+1<<" of "<<precision*precision <<std::endl;
        //Display sub-beams parameters
//        printf("Beam parameters: x0: %.2f y0: %.2f sigx: %.2f sigy: %.2f fluence: %.2f energ: %f.2\n",
//            beamDataArray[k][0],beamDataArray[k][1],beamDataArray[k][2],
//            beamDataArray[k][3],beamDataArray[k][4],beamDataArray[k][5]);           

        doseCalcLoop(bx,Trafo,beamDataArray[k], reachedBraggPeaktemp, tempx, tempy, tempz);
        if(k==0)//store data for central beam
        {
             reachedBraggPeak=reachedBraggPeaktemp;
             dBPX=tempx/gridStepSize;
             dBPY=tempy/gridStepSize;
             dBPZ=tempz/gridStepSize;
        }
          
    }//end sub beam loop       


    //delete beamDataArray
    for (Int_t i=0;i<precision*precision;i++)
    {
            delete[] beamDataArray[i];
    }
    delete[] beamDataArray;
    
//    delete pGeo;
    
        //creating back transfer DoseMeta object
        DoseMeta* tmprd = new DoseMeta(reachedBraggPeak);  
//        tmprd->m_braggXposition =  static_cast<double>(iPBX);//in voxel coordinates 
//        tmprd->m_braggYposition =  static_cast<double>(iPBY);
//        tmprd->m_braggZposition =  static_cast<double>(iPBZ);
        tmprd->m_braggXposition =  dBPX;//in voxel coordinates 
        tmprd->m_braggYposition =  dBPY;
        tmprd->m_braggZposition =  dBPZ;
//        if (reachedBraggPeak==true) cout<<"###Reached Bragg Peak position cubeVoxel: "<<dBPX<<" y "<<dBPY<<" z "<<dBPZ<<endl;
//        else cout<<"###Missed Bragg Peak position: "<<dBPX<<" y "<<dBPY<<" z "<<dBPZ<<endl;
//        cout<<"############"<<endl;
          
//        exit(0);
        //set end time
//gettimeofday(&end, NULL);
//
//seconds  = end.tv_sec  - start.tv_sec;
//useconds = end.tv_usec - start.tv_usec;
//mtime = ((seconds) * 1000. + useconds/1000.0);
//printf("Elapsed time: %ld milliseconds\n", mtime);

      return tmprd;
 }

  //#######################################################################################################

DoseMeta* ProtonSpotPB::hitsTarget(const DoseAtomSpec& bx) const
{
//    cout<<"hitsTarget"<<endl;
    bool bHitTarget=false;//true if ever hit contour, else false
    Double_t dSourceSurfaceDistance=0;//in mm
    Double_t minWaterEQDepthToContour,maxWaterEQDepthToContour;
     minWaterEQDepthToContour=maxWaterEQDepthToContour=0;
    //define transformation class
    UktBase::BeamPatientTrafo Trafo( bx );
    
    //set beam position
    const  ProtonSpotGeometry* const pGeo = static_cast<const ProtonSpotGeometry* const>(bx.getRayGeometry());
    double BeamX0=pGeo->getXPosition(); //set initial beam coordinates in beams eye view coordinate system in mm
    double BeamY0=pGeo->getYPosition(); //set initial beam coordinates in beams eye view coordinate system in mm
    
    //Source isocenter distance
    double sidcm = bx.getSID() * 0.1;
    
    double distanceToDensGrid, distanceSourceToTargetStartWEQ, distanceSourceToTargetStopWEQ,distanceSourceToContour;
    //ATTENTION Function returns values in mm
    //returns distanceSurfaceToTargetStart and distanceSurfaceToTargetStop in WaterEQ
    bHitTarget=getDistancesToDensAndTargetGridIfHit(bx,BeamX0,BeamY0,0.0, 
            distanceToDensGrid, distanceSourceToTargetStartWEQ, distanceSourceToTargetStopWEQ, distanceSourceToContour);
//    cout<<"old distanceSourceToTargetStartWEQ "<<distanceSourceToTargetStartWEQ<<" distanceSourceToTargetStopWEQ "<<distanceSourceToTargetStopWEQ;
//    bHitTarget=calculateWEQtoTarget(bx, BeamX0, BeamY0, 150.0,
//         distanceToDensGrid,  distanceSourceToTargetStartWEQ, distanceSourceToTargetStopWEQ);
//    cout<<"new distanceSourceToTargetStartWEQ "<<distanceSourceToTargetStartWEQ<<" distanceSourceToTargetStopWEQ "<<distanceSourceToTargetStopWEQ<<endl;
    //exit(0);
    //convert to cm
    distanceToDensGrid*=0.1;
    distanceSourceToTargetStartWEQ*=0.1;
    distanceSourceToTargetStopWEQ*=0.1;
    
    if (bHitTarget==false)
    {
      DoseMeta* pshape = new DoseMeta(bHitTarget);
      pshape->m_maxAvailableEnergy = 0.0; 
      pshape->m_mindepth = 0.0; 
      pshape->m_maxdepth = 0.0;
      pshape->m_minEnergy = 0.0;
      pshape->m_maxEnergy = 0.0;
      pshape->m_surfaceDistance = sidcm*10.-distanceToDensGrid;
      return pshape;
    }
               
    //number of energies available from source
    Int_t energyArraySize,minIdx;
    Double_t* availEnergyArray;
    //max index of array contains maximum
    availEnergyArray=lookUpTable->getEnergyAvailArray(energyArraySize);
    
    minIdx=0;
    for(int i=0;i<energyArraySize;i++)//find minimum
    {
        if(availEnergyArray[i]>0) 
        {
           minIdx=i;
           break;
        }
    }
    
    //min Energy available from source
    Double_t minEnergyAvailable=availEnergyArray[minIdx];
    //max Energy available from source
    Double_t maxEnergyAvailable=availEnergyArray[energyArraySize-1];
//    cout<<" minEnergyAvailable "<<minEnergyAvailable<<" maxEnergyAvailable "<<maxEnergyAvailable<<endl;

    //determine min energy required to reach contour
    Double_t minEnergyNeeded=lookUpTable->getEnergyRequiredToPenetrate(distanceSourceToTargetStartWEQ);
    //should be corrected for air
    minWaterEQDepthToContour=distanceSourceToTargetStartWEQ;
    
    //determine max energy required to reach contour
    Double_t maxEnergyNeeded=lookUpTable->getEnergyRequiredToPenetrate(distanceSourceToTargetStopWEQ);
//        cout<<"MaxEnergy distance: "<< tempDist<<" needed MeV: "<<maxEnergyNeeded<<endl;
    //should be corrected for air
    maxWaterEQDepthToContour=distanceSourceToTargetStopWEQ;
    
    dSourceSurfaceDistance=distanceToDensGrid;

    minEnergyNeeded-=10.;//3;
    maxEnergyNeeded+=10.;//-15.;//3.;
    //Create Output
       //in case max energy was not enough to pass contour (returns -1) set to max energy available
       if (maxEnergyNeeded<=0.) maxEnergyNeeded=maxEnergyAvailable;
       //in case min energy was not enough to reach contour set to min energy available
       if (minEnergyNeeded<=0.) minEnergyNeeded=minEnergyAvailable;
//if (bHitTarget) cout<<"Min energy needed: "<< minEnergyNeeded<<" Max energy needed: "<< maxEnergyNeeded<<endl;
    
    
//    //round to available energies
//    Int_t tempIdx=0;
//     tempIdx=TMath::BinarySearch(energyArraySize,availEnergyArray, minEnergyNeeded);
//    if (tempIdx>=energyArraySize) tempIdx=energyArraySize-1;//ensure index is within bounds of array
//    //go one energy step below  
////     tempIdx-=1;
//    if (tempIdx<0) tempIdx=0;//ensure index is within bounds of array
//    minEnergyNeeded = availEnergyArray[tempIdx];
//    
//    tempIdx=0;
//    tempIdx=TMath::BinarySearch(energyArraySize,availEnergyArray, maxEnergyNeeded);
//    //go one energy step above  
////    tempIdx+=1;
//    if (tempIdx>=energyArraySize) tempIdx=energyArraySize-1;//ensure index is within bounds of array
//    if (tempIdx<0) tempIdx=0;//ensure index is within bounds of array
//    if ( (tempIdx+1)<energyArraySize ) tempIdx=tempIdx+1;//as returned index is always below requested energy, add one index
//    maxEnergyNeeded = availEnergyArray[tempIdx];
    
    //convert units back to mm
    minWaterEQDepthToContour*=10.;
    maxWaterEQDepthToContour*=10.;
    dSourceSurfaceDistance*=10.;
    
    double isoCenterSurfaceDistance=sidcm*10.-distanceSourceToContour;
//    cout<<"isoCenterSurfaceDistance "<<isoCenterSurfaceDistance<<" sidcm "<<sidcm<<" distanceSourceToContour "<<distanceSourceToContour<<endl;
//    dIsoCenterSurfaceDistance=;
//            
//cout<<"hitsTarget returning m_maxAvailableEnergy "<<maxEnergyAvailable
//        <<" minWaterEQDepthToContour "<<minWaterEQDepthToContour
//        <<" maxWaterEQDepthToContour "<<maxWaterEQDepthToContour
//        <<" minEnergyNeeded "<<minEnergyNeeded
//        <<" maxEnergyNeeded "<<maxEnergyNeeded
//        <<" isoCenterSurfaceDistance "<<isoCenterSurfaceDistance
//        <<" hitting: "<<bHitTarget<<endl;
//    if( maxEnergyNeeded>80.)
//        cout<<"hitsTarget returning m_maxAvailableEnergy "<<maxEnergyAvailable
//        <<" minWaterEQDepthToContour "<<minWaterEQDepthToContour
//        <<" maxWaterEQDepthToContour "<<maxWaterEQDepthToContour
//        <<" minEnergyNeeded "<<minEnergyNeeded
//        <<" maxEnergyNeeded "<<maxEnergyNeeded
//        <<" isoCenterSurfaceDistance "<<isoCenterSurfaceDistance
//        <<" hitting: "<<bHitTarget<<endl;
    
//    cout<<" minEnergyNeeded "<<minEnergyNeeded
//        <<" maxEnergyNeeded "<<maxEnergyNeeded<<endl;   
    
    
      DoseMeta* pshape = new DoseMeta(bHitTarget);
      pshape->m_maxAvailableEnergy = maxEnergyAvailable; 
      pshape->m_mindepth = minWaterEQDepthToContour; 
      pshape->m_maxdepth = maxWaterEQDepthToContour;
      pshape->m_minEnergy = minEnergyNeeded;
      pshape->m_maxEnergy = maxEnergyNeeded;
      pshape->m_surfaceDistance = isoCenterSurfaceDistance;//dSourceSurfaceDistance;

//exit(0);
      return pshape;   
}


//calculate the relativistic beta
//energy and Mass in MeV required
inline Double_t ProtonSpotPB::relativisticBeta(Double_t energy, Double_t mass) const
{
    //protonRestMass = 938.272013;////in MeV
    if (energy<0) energy=0;
    if (mass<0) mass=0;
    return sqrt(1 - pow( mass/(mass+energy) ,2) );
    //alternative version, same result
    //return  sqrt( 1 - 1/pow(1+ energy/(protonRestMass) ,2) );
}


////calculate the scattering angle
//// everything in SI units
////See Geant4 physics documentation
inline Double_t ProtonSpotPB::getScatteringAngleSquare(Double_t currentStepSize, Double_t energy, Double_t radiationLength)
{
        //Highland-Lynch-Dahl formula
	Double_t constC1, constC2, relatBeta, scatAngle,momentum;

	constC1=13.6; ////in MeV
	constC2=0.038;//// in MeV

        energy=energy*ProtonSpotPB_constParticleMassNumber;//passed Energy is per nucleon, formula works for whole energy
	relatBeta=relativisticBeta(energy, ProtonSpotPB_constParticleMass);
        momentum=ProtonSpotPB_constParticleMass*relatBeta/ProtonSpotPB_constLightSpeed;
       
	if (relatBeta<=0 || currentStepSize<=0) scatAngle=0;
	else scatAngle = constC1/(relatBeta*momentum*ProtonSpotPB_constLightSpeed) * ProtonSpotPB_constParticleCharge *  sqrt( currentStepSize / radiationLength ) * (  1 + constC2 * log(currentStepSize / radiationLength)   );
        ////log() computes natural logarithm in C++
	return pow(scatAngle,2);//necessary to return squared scattering angle	
}


inline Double_t ProtonSpotPB::getBeamShapeAtDistanceFromCenter(Double_t distanceX,Double_t distanceY,
                                                        Double_t currentBeamSigmaX, Double_t currentBeamSigmaY,
                                                        Double_t initialBeamSigmaX, Double_t initialBeamSigmaY,
                                                        Double_t GaussVoigtRelation,Double_t VoigtSigma,
                                                        Double_t VoigtLg,Double_t spacingBetweenPoints)
{
    //Gauss and Voigt function are centered around zero.
    //using +/- distance from beam center, normalized beam shape value at this position is returned
    
//    Double_t GaussVoigtRelation=nucCorrTable->getGaussVoigtRelation(currentDepth);
//    Double_t VoigtSigma=nucCorrTable->getVoigtSigma(currentDepth);
//    Double_t VoigtLg=nucCorrTable->getVoigtLg(currentDepth);
//    Double_t spacingBetweenPoints=ProtonSpotPB_stepSize;
//    //Adapting nuclear correction LUT beam diameter to current diameter
//            nucCorrTable->setBeamSigma(ProtonSpotPB_beamInitSigmaY);
    
    Double_t xg,yg,xvoigt,yvoigt,xResult,yResult,finalResult;
    xg=yg=xvoigt=yvoigt=xResult=yResult=finalResult=0.0;
    Double_t voigtArea,gaussArea; 
    
//    cout<<"distanceX "<<distanceX<<" distanceY "<<distanceY<<" currentBeamSigmaX "<<currentBeamSigmaX<<" spacingBetweenPoints "<<spacingBetweenPoints<<endl;
    
           //include correction
            //how much goes to Gauss, how much goes to Voigt
            //in total, area of both curves should be one due to normalization
            voigtArea=1/(1+GaussVoigtRelation);
            gaussArea=1-voigtArea;
//            if ( isnan(voigtArea) ||  isinf(voigtArea) || voigtArea<0. ) {cerr<<"ERROR: Aborting, voigtArea inf or nan or negative :"<< voigtArea<<endl; exit(-1);}          

//            cout<<"voigtArea "<<voigtArea;
     //Gauss distribution due to MS
            yg= TMath::Gaus(distanceY, 0.0, currentBeamSigmaY, true)*spacingBetweenPoints;//*spacingBetweenPoints to normalize to one again  
//            cout<<"currentBeamSigmaY "<<currentBeamSigmaY<<" spacingBetweenPoints "<<spacingBetweenPoints<<" distanceY "<<distanceY<<endl;
//            if ( isnan(yg) ||  isinf(yg) || yg<0. ) {cerr<<"ERROR: Aborting, yg inf or nan or negative :"<< yg<<endl; exit(-1);}
//            cout<<" yg "<<yg<<endl;
            //create Voigt curve due to nuclear correction
            //Adapting nuclear correction LUT beam diameter to current diameter
            nucCorrTable->setBeamSigma(initialBeamSigmaY);
            yvoigt=TMath::Voigt(distanceY, VoigtSigma,VoigtLg , 4)*spacingBetweenPoints;//*spacingBetweenPoints to normalize to one again  
//            if ( isnan(yvoigt) ||  isinf(yvoigt) ) {cerr<<"ERROR: Aborting, yvoigt inf or nan or negative :"<< yvoigt<<endl; exit(-1);}    

            //Add up contributions of both curves          
            yResult=yg*gaussArea+yvoigt*voigtArea;
//            cout<<" yResult "<<yResult;
     //Gauss distribution due to MS
            xg= TMath::Gaus(distanceX, 0.0, currentBeamSigmaX, true)*spacingBetweenPoints;//*spacingBetweenPoints to normalize to one again  
//            if ( isnan(xg) ||  isinf(xg) || xg<0. ) {cerr<<"ERROR: Aborting, xg inf or nan or negative :"<< xg<<endl; exit(-1);}

            //create Voigt curve due to nuclear correction
            //Adapting nuclear correction LUT beam diameter to current diameter
            nucCorrTable->setBeamSigma(initialBeamSigmaX);
            xvoigt=TMath::Voigt(distanceX, VoigtSigma,VoigtLg , 4)*spacingBetweenPoints;//*spacingBetweenPoints to normalize to one again  
//            if ( isnan(xvoigt) ||  isinf(xvoigt) || xvoigt<0. ) {cerr<<"ERROR: Aborting, xvoigt inf or nan or negative :"<< xvoigt<<endl; exit(-1);}    

            //Add up contributions of both curves          
            xResult=xg*gaussArea+xvoigt*voigtArea;            
//            cout<<" xResult "<<xResult<<endl;
            //Create overlap between curves of both dimensions -> final weight factor for this spot, does only need fluence
            finalResult=xg * yg;
            if ( isnan(finalResult) ||  isinf(finalResult) || finalResult<0.) {cerr<<"ERROR: Aborting, Weight at given spot inf or nan or negative:"<< finalResult<<endl; exit(-1);}    
 
//            cout<<" finalResult "<<finalResult<<" without spacing result: "<<finalResult/spacingBetweenPoints<<endl;
            return   finalResult;       
}



//loop through the scatAngleSquaredArray and quadratically sum up all contents with distance from current point
//defined through stepSize*maxIndex
//see Soukup Dissertation
inline double ProtonSpotPB::getSummedScattAngle(std::vector<double>& scatAngleSquaredVector,double stepSize)
{
	int i,currentSize;
        double depthSum=0;
	double quadrSum=0;
        
        currentSize=scatAngleSquaredVector.size();
        
        depthSum=stepSize*currentSize;
            for (i=0;i<currentSize;i++)
            {
             //first (first voxel) entry for generated angle is now farthest away.
             //quadratically sums up squared angles * depth   
             //scatAngleSquaredArray contains already squared values
             quadrSum=quadrSum + scatAngleSquaredVector.at(i)*pow(depthSum,2);
             depthSum=depthSum-stepSize;
            }
	return quadrSum;
}



//calculate the stoppingPowerRatioToWater in order to rescale the real depth to a depth in water
inline Double_t ProtonSpotPB::getStoppingPowerRatioToWater(Double_t stoppingPowerWater,Double_t stoppingPowerMaterial) const
{
	if (stoppingPowerMaterial<=0||stoppingPowerWater<=0) return 1.0;
	else return stoppingPowerMaterial/stoppingPowerWater;
}


//Bethe-Bloch formula
//see Ziegler 1999
//energy, excitationEnergy and particle mass in MeV
inline Double_t ProtonSpotPB::calculateStoppingPower(Double_t energy,Double_t atomicZARatio, Double_t excitationEnergy) const
{
	Double_t constK,relatBeta,deltaEmax,stoppingPower;

        if(energy<=0.) return 0.0; //if energy is below or zero, return 0
        relatBeta=relativisticBeta(energy,ProtonSpotPB_constParticleMass);
        
        //formula to calculate stopping power from PDG
        constK=0.307075;//in MeV * 1/g * cm^2
	deltaEmax=(2*ProtonSpotPB_constMassElectron*relatBeta*relatBeta)/(1-(relatBeta*relatBeta));
        
	stoppingPower=constK*ProtonSpotPB_constParticleCharge*ProtonSpotPB_constParticleCharge*atomicZARatio*1/(relatBeta*relatBeta) *
	(       0.5 * log ( (2*ProtonSpotPB_constMassElectron*relatBeta*relatBeta*deltaEmax)/(1-(relatBeta*relatBeta)) )
	- (relatBeta*relatBeta) - log(excitationEnergy)         );
        
        //alternative from Leo Techniques for Nuclear and Particle Physics Experiments
        //both give the same results 
//        constK=0.1535;//in MeV * 1/g * cm^2
//        Double_t gamma;
//        gamma=1 / sqrt( 1 - relatBeta*relatBeta );      
//        deltaEmax=( 2*constMassElectron*relatBeta*relatBeta*gamma*gamma*ProtonSpotPB_constLightSpeed*ProtonSpotPB_constLightSpeed )/
//                (1 + 2*constMassElectron/ProtonSpotPB_constParticleMass*sqrt( 1+relatBeta*relatBeta*gamma*gamma ) + pow(constMassElectron/ProtonSpotPB_constParticleMass,2) );
//        
//        stoppingPower=constK*1*1*atomicZARatio*1/(relatBeta*relatBeta) *
//	( log ( (2*constMassElectron*gamma*gamma*ProtonSpotPB_constLightSpeed*ProtonSpotPB_constLightSpeed*relatBeta*relatBeta*deltaEmax)/(excitationEnergy*excitationEnergy) )
//	- 2*(relatBeta*relatBeta));
if (isinf(stoppingPower)!=0 || stoppingPower<0.) stoppingPower=0.0;
	return stoppingPower;
}


////decomposes the initial gauss distributed beam in subBeams
////spread is the maximum distance from the original beam axis in times of sigma 
Int_t ProtonSpotPB::beamDecomposition(const DoseAtomSpec& bx,Double_t **beamDataArray, Int_t numOfSubBeamsOneDim)
{
     const ProtonSpotGeometry* const pGeo = static_cast<const ProtonSpotGeometry* const>(bx.getRayGeometry());
     if(pGeo == NULL) 
      return 0;
    Double_t beamInitX0=pGeo->getXPosition(); //set initial beam coordinates in beams eye view coordinate system
    Double_t beamInitY0=pGeo->getYPosition(); 
    Double_t initEnergy=pGeo->getEnergy(); //set initial beam energy   
//    if(initEnergy>80.)cout<<"WARNING: energy higher than 80 MeV/A: "<<initEnergy<<endl;
    Double_t beamInitSigmaX= pGeo->getSigmaX()/10.;//in cm
    Double_t beamInitSigmaY= pGeo->getSigmaY()/10.;//in cm
    Double_t beamInitSigmaXWidening = pGeo->getSigmaXWidening()/10.;
    Double_t beamInitSigmaYWidening = pGeo->getSigmaYWidening()/10.;
    
    assert((initEnergy>=0.));//beam energy may not be negative
    assert((beamInitSigmaX>0.)&&(beamInitSigmaY>0.));//beam sigma may not be negative or zero
    assert((beamInitSigmaXWidening>=0.)&&(beamInitSigmaYWidening>=0.));//widening may not be negative
    
    if (numOfSubBeamsOneDim<=1) //in case no splitting is required, if only one beam
    {
          beamDataArray[0][0]=beamInitX0;//PosX0
          beamDataArray[0][1]=beamInitY0;//PosY0
          
          beamDataArray[0][2]=beamInitSigmaX;//SigmaX
          beamDataArray[0][3]=beamInitSigmaY;//SigmaY

          beamDataArray[0][4]=ProtonSpotPB_initFluence;
          beamDataArray[0][5]=initEnergy;
          
          beamDataArray[0][6]=beamInitSigmaXWidening;
          beamDataArray[0][7]=beamInitSigmaYWidening;
          
//          printf("k: %i weight: %f x0: %f y0: %f sigx: %f sigy: %f fluence: %f energy: %f\n",
//                        0, (Double_t)1,beamDataArray[0][0],beamDataArray[0][1],
//                        beamDataArray[0][2], beamDataArray[0][3],
//                        beamDataArray[0][4],beamDataArray[0][5]);   
     return 1;   
    }    
    
  Int_t i,j,k;
  std::vector<Double_t> xGauss,yGauss;
  GaussFitClass *fTest;
        
  //calculate one Dim fit for x      
  fTest = new GaussFitClass();      
  fTest->numericalFit(numOfSubBeamsOneDim,beamInitSigmaX,xGauss);
  
//  cout<<"size of xGauss vector: "<<xGauss.size()<<std::endl;
  for( i=0;i<(int)xGauss.size();i=i+3)//all three dimensions in one array
  {
      //correct for initial Position x0
      xGauss.at(i)=xGauss.at(i)+beamInitX0;
//      cout<<"Mu: "<<xGauss.at(i);
//      cout<<" Sigma: "<<xGauss.at(i+1); 
//      cout<<" C: "<<xGauss.at(i+2)<<std::endl;
  }
  delete fTest;
  
  //calculate one Dim fit for y   
  fTest = new GaussFitClass();  
  fTest->numericalFit(numOfSubBeamsOneDim,beamInitSigmaY,yGauss);
  
  //cout<<"size of yGauss vector: "<<yGauss.size()<<std::endl;
  for(i=0;i<(int)yGauss.size();i=i+3)//all three dimensions in one array
  {
      //correct for initial Position y0
      yGauss.at(i)=yGauss.at(i)+beamInitY0;
//      cout<<"Mu: "<<yGauss.at(i);
//      cout<<" Sigma: "<<yGauss.at(i+1); 
//      cout<<" C: "<<yGauss.at(i+2)<<std::endl;
  }
  delete fTest;
  
  Double_t weight=0;  
  Double_t weightTotal=0;
  k=0;
  //combine dimensions and 
  for(i=0;i<numOfSubBeamsOneDim;i++)//yloop
  {
      for(j=0;j<numOfSubBeamsOneDim;j++)//xloop
      {
          //cout<<"i:"<<i<<"j:"<<j<<std::endl;
          beamDataArray[k][0]=xGauss.at(j*3);//PosX0
          beamDataArray[k][1]=yGauss.at(i*3);//PosY0
          
          beamDataArray[k][2]=xGauss.at((j*3)+1);//SigmaX
          beamDataArray[k][3]=yGauss.at((i*3)+1);//SigmaY
          
          weight=xGauss.at((j*3)+2)* yGauss.at((i*3)+2);//Re-Normalization required, done afterwards
          weightTotal=weightTotal+weight;
//          cout<<"gauss:"<<TMath::Gaus(beamInitX0, beamInitY0, initSigmaX, true)<<std::endl;
//          cout<<"cx:"<<xGauss.at((j*3)+2)<<std::endl;
//          cout<<"cy:"<<yGauss.at((i*3)+2)<<std::endl;
          beamDataArray[k][4]= weight;
          beamDataArray[k][5]=initEnergy;
          
          beamDataArray[k][6]=beamInitSigmaXWidening;
          beamDataArray[k][7]=beamInitSigmaYWidening;
          k++;
      }
  }
  
  //weight needs to be renormalized to one 
  weight=0;
//Loop over all sub beams k redistribute weight
  for (k=0;k<numOfSubBeamsOneDim*numOfSubBeamsOneDim;k++)
  {
    weight=1/weightTotal*beamDataArray[k][4];
    //cout<<"total weight: "<<weightTotal<<" old weight: "<<beamDataArray[k][4]<<" new weight: "<<weight<<" initFluence: "<<initFluence<<endl;
    beamDataArray[k][4]= weight*ProtonSpotPB_initFluence;
    
//    printf("k: %i weight: %.2f x0: %.2f y0: %.2f sigx: %.2f sigy: %.2f fluence: %.2f energy: %.2f\n",
//                        k, weight,beamDataArray[k][0],beamDataArray[k][1],
//                        beamDataArray[k][2], beamDataArray[k][3],
//                        beamDataArray[k][4],beamDataArray[k][5]);
  }
    return k;
}

inline void ProtonSpotPB::setIsoCenter(double isoX, double isoY, double isoZ)
{
    //isocenter is passed in voxel therefore necessary to recalculate
  m_IsoX = isoX*gridStepSize;
  m_IsoY = isoY*gridStepSize;
  m_IsoZ = isoZ*gridStepSize;

  return;
}


bool ProtonSpotPB::calculateWEQtoTarget(const DoseAtomSpec& bx,double BeamX0,double BeamY0,double initEnergy,
        double& distanceToDensGrid, double& distanceSourceToTargetStartWEQ,double& distanceSourceToTargetStopWEQ) const
{
    double startX,startY,startZ;
    double xTemp2,yTemp2,zTemp2;
    
    lookUpTable->setTargetEnergy(initEnergy);
    
    //get start point of beam
    transformBeamCoordIntoCubeCoord(bx,BeamX0,BeamY0,0.0,startX,startY,startZ);
    //get second point to create a direction vector
    transformBeamCoordIntoCubeCoord(bx,BeamX0,BeamY0,1.0,xTemp2,yTemp2,zTemp2);

    //calculate normalized direction vector
    //in hyperion coordinates
    double nRvX,nRvY,nRvZ,norm;
    nRvX=xTemp2-startX;
    nRvY=yTemp2-startY;
    nRvZ=zTemp2-startZ;
    norm=1./sqrt(nRvX*nRvX + nRvY*nRvY + nRvZ*nRvZ);
    nRvX*=norm;
    nRvY*=norm;
    nRvZ*=norm;
    
    //calc distance to grid
    double distanceToGrid;
    getDistanceToDensGridIfHit(bx,BeamX0,BeamY0,0.0, distanceToGrid);

    double energyProbNextStep=0.0;
    double pointX,pointY,pointZ;
    double steplength=gridStepSize/3.;
    double dHUnit,densityMaterial, radiationLengthMaterial,excitationEnergyMaterial, materialAtomZARatio;
    double stoppingPowerWater,stoppingPowerMaterial,rescaleRatio;
    //start with offset to grid, density is air
    double currentIntegratedDepthWEQ=distanceToGrid*1.20E-03;
    //start at Beam entry in density grid, therefore distance source-Grid needed
    startX+=distanceToGrid*nRvX;
    startY+=distanceToGrid*nRvY;
    startZ+=distanceToGrid*nRvZ;
    
//    cout<<"WEQ XDim: "<<XDim<<" YDim "<<YDim<<" ZDim "<<ZDim<<endl;
    int iHypX,iHypY,iHypZ;
     bool bReachedTarget,bIsInTarget,bNotYetLeftTarget;
     bReachedTarget=false;
     bIsInTarget=false;
     bNotYetLeftTarget=false;
    int i;
    for(i=1;;i++)
    {
        //start at Beam entry in density grid, therefore distance source-Grid needed
        //proceed with scale increments and offset to density grid
        //offset included in startX,Y and Z
        pointX=startX + i*steplength*nRvX;
        pointY=startY + i*steplength*nRvY;
        pointZ=startZ + i*steplength*nRvZ;
        convertHyperionToVoxelCoordinates(pointX, pointY, pointZ, iHypX, iHypY, iHypZ);
       //check if coordinates are within bounds 
       if( iHypX<0 || iHypX>=XDim || 
           iHypY<0 || iHypY>=YDim ||
           iHypZ<0 || iHypZ>=ZDim )
       {
           break;
       }
        
        //if outside body, use air
//        if( m_pDoseCompGrid->isInContour(iHypX, iHypY, iHypZ)==false ) 
//            {
//              dHUnit=-1000; //Houndsfield unit of air is -1000
//              densityMaterial=1.20E-03;
//              radiationLengthMaterial=36.62;
//              excitationEnergyMaterial=85.7;
//              materialAtomZARatio=0.49919;
//              //HoundsfieldUnit density[g/cm3] radiationLength[g*cm_2] Z/A excitation energy[eV] Name
//              //-1000 1.20E-03 36.62 0.49919 85.7 Air 
//            }
//        else 
//        {
         dHUnit = convertCTtoHU(m_pDoseCompGrid->operator()(iHypX,iHypY,iHypZ));        //query Material
         //convert Hounsfield Unit to material properties
         myInputClass->getOneMaterialPropertiesFromHU(dHUnit, densityMaterial, radiationLengthMaterial, 
                                                     excitationEnergyMaterial, materialAtomZARatio);
//        }

        //calculate stepSize due to different material
         if (i<=0) energyProbNextStep=initEnergy;
         else 
         {
           energyProbNextStep=lookUpTable->getInDepthEnergy( (currentIntegratedDepthWEQ+steplength)/10. );
         }
         //calculate rescale ratio for water equivalent path scaling       
         stoppingPowerWater=ProtonSpotPB::calculateStoppingPower(energyProbNextStep, ProtonSpotPB_constZARatioWater, ProtonSpotPB_constExcitationEnergyWater);
         stoppingPowerMaterial=ProtonSpotPB::calculateStoppingPower(energyProbNextStep,materialAtomZARatio, excitationEnergyMaterial);
         rescaleRatio= ProtonSpotPB::getStoppingPowerRatioToWater(stoppingPowerWater, stoppingPowerMaterial)*densityMaterial;
         currentIntegratedDepthWEQ+=steplength*rescaleRatio;

         //check whether the voxel is in the target
           bIsInTarget=m_pTargetGrid->operator()( iHypX, iHypY , iHypZ );
           //add up density sum from contour to target
           if(bReachedTarget==false)
           {
            if (bIsInTarget==true) 
            {
               distanceSourceToTargetStartWEQ=currentIntegratedDepthWEQ;
               distanceSourceToTargetStopWEQ=currentIntegratedDepthWEQ;
                //avoid calling this statement again
                bReachedTarget=true;
            }
           }
           //add up density sum from start to end of target
           if(bNotYetLeftTarget==false && bReachedTarget==true)
           {
            if (bIsInTarget==false) 
            {
                distanceSourceToTargetStopWEQ=currentIntegratedDepthWEQ;
                 //avoid calling this statement again
                bNotYetLeftTarget=true;
            }
           }
    }

   return bReachedTarget; 
}

inline void ProtonSpotPB::calcuateWEQandMSarrays(const DoseAtomSpec& bx,
        std::vector<double>& realDepth,std::vector<double>& WEQDepth,std::vector<double>& MScurrentSquaredVariance,
        bool& reachedBraggPeak, double& braggPeakDepthHyperion,double& maxDepthHyperion, double& distanceToContour,
        double initEnergy,
        double distanceToGrid,double BeamX0, double BeamY0)
{   

//    std::vector<double> MSScatAngleSquared;  
//    MSScatAngleSquared.clear();
//    MSScatAngleSquared.reserve(1000);
    
    realDepth.clear();
    realDepth.reserve(1000);
    WEQDepth.clear();
    WEQDepth.reserve(1000);
    MScurrentSquaredVariance.clear();
    MScurrentSquaredVariance.reserve(1000);
    
    double startX,startY,startZ;
    double xTemp2,yTemp2,zTemp2;
    
    //get start point of beam
    transformBeamCoordIntoCubeCoord(bx,BeamX0,BeamY0,0.0,startX,startY,startZ);
    //get second point to create a direction vector
    transformBeamCoordIntoCubeCoord(bx,BeamX0,BeamY0,1.0,xTemp2,yTemp2,zTemp2);

    //calculate normalized direction vector
    //in hyperion coordinates
    double nRvX,nRvY,nRvZ,norm;
    nRvX=xTemp2-startX;
    nRvY=yTemp2-startY;
    nRvZ=zTemp2-startZ;
    norm=1./sqrt(nRvX*nRvX + nRvY*nRvY + nRvZ*nRvZ);
    nRvX*=norm;
    nRvY*=norm;
    nRvZ*=norm;
    
    double scatteringAngleSquared,currSquaredVarianceDueToMS;

    double energyProbNextStep=0.0;
    double pointX,pointY,pointZ;
    double steplength=gridStepSize/3.;
    double dHUnit,densityMaterial, radiationLengthMaterial,excitationEnergyMaterial, materialAtomZARatio;
    double stoppingPowerWater,stoppingPowerMaterial,rescaleRatio;
    //start with offset to grid, density is air
    double currentIntegratedDepthWEQ=distanceToGrid*1.20E-03;
    //start at Beam entry in density grid, therefore distance source-Grid needed
    startX+=distanceToGrid*nRvX;
    startY+=distanceToGrid*nRvY;
    startZ+=distanceToGrid*nRvZ;
//    cout<<" offset to grid: "<<distanceToGrid<<endl;
//               double distanceFromBeamX;
//                double distanceFromBeamY;
//                double distanceFromBeamZ;
//    getDistanceFromBeamAndDepthAlongBeamAxis(bx,BeamX0,BeamY0, startX,  startY,  startZ, distanceFromBeamX,  distanceFromBeamY, distanceFromBeamZ);
//    cout<<"function says: startPointOffset distanceFromBeamX "<<distanceFromBeamX<<" distanceFromBeamY "<<distanceFromBeamY<<" distanceFromBeamZ "<<distanceFromBeamZ<<endl;

    rescaleRatio=1.0;
    //first depth step
    realDepth.push_back(0.);
    WEQDepth.push_back(0.);
    MScurrentSquaredVariance.push_back(0.);
    //get WEQ bragg Peak Depth
    double braggPeakDepthWaterEQ=lookUpTable->getPeakRangeOfEnergy(initEnergy)*10.; 
    braggPeakDepthHyperion=-1.0;//initialize
    maxDepthHyperion=-1.0;
    
    distanceToContour=-1.0;//initialize
    
//    cout<<"WEQ XDim: "<<XDim<<" YDim "<<YDim<<" ZDim "<<ZDim<<endl;
    int iHypX,iHypY,iHypZ;
    int i;
    double currRealLength=0.0;
    double scattSum=0.;
    currSquaredVarianceDueToMS=0.0;
    for(i=1;;i++)
    {
        currRealLength=i*steplength+distanceToGrid;
        //start at Beam entry in density grid, therefore distance source-Grid needed
        //proceed with scale increments and offset to density grid
        //offset included in startX,Y and Z
        pointX=startX + i*steplength*nRvX;
        pointY=startY + i*steplength*nRvY;
        pointZ=startZ + i*steplength*nRvZ;
        convertHyperionToVoxelCoordinates(pointX, pointY, pointZ, iHypX, iHypY, iHypZ);
        
        //determine distance from beam source to start of contour
        if( m_pDoseCompGrid->isInContour(iHypX, iHypY, iHypZ)==true && distanceToContour==-1.0 )
        {
            distanceToContour=currRealLength;
        }
                
        
       //check if coordinates are within bounds 
        //or maximum depth to calculate was reached.      
        //if so calculate some maximal values
       if(iHypX<0 || iHypX>=XDim || 
          iHypY<0 || iHypY>=YDim ||
          iHypZ<0 || iHypZ>=ZDim ||
          (currentIntegratedDepthWEQ>=(braggPeakDepthWaterEQ*1.5) )     )
       {
           if (currentIntegratedDepthWEQ>=braggPeakDepthWaterEQ) reachedBraggPeak=true;
           else reachedBraggPeak=false;
//           cout<<"Reached end of grid."<<endl;
//               double distanceFromBeamX;
//                double distanceFromBeamY;
//                double distanceFromBeamZ;
//           cout<<"at iHypX: "<<iHypX<<" iHypY "<<iHypY<<" iHypZ "<<iHypZ<<endl;
//           cout<<"MaxDepthreal "<<(i-1)*steplength+distanceToGrid<<" max WEQ depth "<<currentIntegratedDepthWEQ<<endl;
//           getDistanceFromBeamAndDepthAlongBeamAxis(bx,BeamX0,BeamY0, pointX,  pointY,  pointZ, distanceFromBeamX,  distanceFromBeamY, distanceFromBeamZ);
//           cout<<"function says: distanceFromBeamX "<<distanceFromBeamX<<" distanceFromBeamY "<<distanceFromBeamY<<" distanceFromBeamZ "<<distanceFromBeamZ<<endl;
//           cout<<"steps: "<<i<<" steplength "<<steplength<<endl;
           
           maxDepthHyperion=currRealLength;//set maximum depth which was calculated
           
           //add additional buffer, with last settings, just to avoid rounding errors
           realDepth.push_back(currRealLength);
           WEQDepth.push_back(currentIntegratedDepthWEQ+steplength*rescaleRatio);
           realDepth.push_back(currRealLength+steplength);
           WEQDepth.push_back(currentIntegratedDepthWEQ+steplength*rescaleRatio);
           break;
       } 
        
        //query Material
        dHUnit = convertCTtoHU(m_pDoseCompGrid->operator()(iHypX,iHypY,iHypZ));

        //convert Hounsfield Unit to material properties
        myInputClass->getOneMaterialPropertiesFromHU(dHUnit, densityMaterial, radiationLengthMaterial, 
                                                     excitationEnergyMaterial, materialAtomZARatio);

        //calculate stepSize due to different material
         if (i<=0) energyProbNextStep=initEnergy;
         else 
         {
           energyProbNextStep=lookUpTable->getInDepthEnergy( (currentIntegratedDepthWEQ+steplength)/10. );
         }
         //calculate rescale ratio for water equivalent path scaling       
         stoppingPowerWater=ProtonSpotPB::calculateStoppingPower(energyProbNextStep, ProtonSpotPB_constZARatioWater, ProtonSpotPB_constExcitationEnergyWater);
         stoppingPowerMaterial=ProtonSpotPB::calculateStoppingPower(energyProbNextStep,materialAtomZARatio, excitationEnergyMaterial);
         rescaleRatio= ProtonSpotPB::getStoppingPowerRatioToWater(stoppingPowerWater, stoppingPowerMaterial)*densityMaterial;
         currentIntegratedDepthWEQ+=steplength*rescaleRatio;
         
         realDepth.push_back(currRealLength);
         WEQDepth.push_back(currentIntegratedDepthWEQ);
         
         if( currentIntegratedDepthWEQ>=braggPeakDepthWaterEQ && braggPeakDepthHyperion==-1.0 ) 
         {
             braggPeakDepthHyperion=currRealLength;
//             else if ( (currentIntegratedDepthWEQ>=(braggPeakDepthWaterEQ*1.5) ) && maxDepthHyperion==-1.0)  maxDepthHyperion=currRealLength;
         }
         
         //calculate scattering angle
         
         //recalculate the scattering Angle every z step, due to multiple scattering corrections
         scatteringAngleSquared= getScatteringAngleSquare(steplength*rescaleRatio,
                                                         lookUpTable->getInDepthEnergy( currentIntegratedDepthWEQ/10.), radiationLengthMaterial);
         //store in array
         //MSScatAngleSquared.push_back(scatteringAngleSquared*(0.9*0.9));//reduce calculated angle by ten percent

         //calculate current squared variance by looping over all previous angles and steps
    
           currSquaredVarianceDueToMS=currSquaredVarianceDueToMS+ scattSum * steplength*steplength;
           scattSum=scattSum+scatteringAngleSquared*(0.9*0.9) ;//reduce calculated angle by ten percent
         
         //currSquaredVarianceDueToMS = getSummedScattAngle(MSScatAngleSquared,steplength);
         if ( isnan(currSquaredVarianceDueToMS) ||  isinf(currSquaredVarianceDueToMS) || currSquaredVarianceDueToMS<0.0) 
                                 {cerr<<"ERROR currSquaredVarianceDueToMS inf or nan or negative:"<< currSquaredVarianceDueToMS<<endl; exit(-1);}
         MScurrentSquaredVariance.push_back(currSquaredVarianceDueToMS);  
         
//         cout<<"initEnergy: "<<initEnergy<<" currEnergy: "<<energyProbNextStep<<" HU: "<<dHUnit<<" WEQdepth: "<< currentIntegratedDepthWEQ<<" realDepth: "<<currRealLength<<endl;
    }

   return; 
}

inline double ProtonSpotPB::getDoseAtVoxel(const DoseAtomSpec& bx, interpol* weqLUT,interpol* MSsquaredVarianceLUT,Double_t *beamDataArray, 
                                                int iHypX,int iHypY, int iHypZ,
                                                double distanceFromBeamX,double distanceFromBeamY,double distanceFromBeamZ)
{
//      double beamX0             = beamDataArray[0];
//      double beamY0             = beamDataArray[1];      
      double beamInitSigmaX     = beamDataArray[2];
      double beamInitSigmaY     = beamDataArray[3];
      double thisBeamFluence    = beamDataArray[4];
      double beamInitSigmaXWidening = beamDataArray[6];
      double beamInitSigmaYWidening = beamDataArray[7];

//      double initEnergy         = beamDataArray[5];
      
//    //get distances  
//    double distanceFromBeamX,distanceFromBeamY, beamDepth;
//    getDistanceFromBeamAndDepthAlongBeamAxis(bx, beamX0, beamY0,(double) iHypX*gridStepSize,(double) iHypY*gridStepSize,(double) iHypZ*gridStepSize,
//                                             distanceFromBeamX, distanceFromBeamY, beamDepth);    

    //get WEQ depth
    double weqDepth=weqLUT->getData(distanceFromBeamZ);
//    cout<<" realDepth "<<distanceFromBeamZ<<" weqDepth "<<weqDepth<<endl;
//    cout<<" beamDepth "<<distanceFromBeamZ<<" distanceFromBeamX "<<distanceFromBeamX<<" distanceFromBeamY "<<distanceFromBeamY<<endl;

    //determine material properties
    //get Hounsfield unit from Hyperion and convert to HU as Hyperion gives
    //raw scanner value, converted to Hounsfield unit
    double dHUnit = convertCTtoHU( m_pDoseCompGrid->operator()(iHypX,iHypY,iHypZ) );
    double densityMaterial, radiationLengthMaterial, excitationEnergyMaterial, materialAtomZARatio;
    //convert Hounsfield Unit to material properties
    myInputClass->getOneMaterialPropertiesFromHU(dHUnit, densityMaterial, radiationLengthMaterial, 
                                                 excitationEnergyMaterial, materialAtomZARatio);
    
    //get rescaling
    double currEnergy=lookUpTable->getInDepthEnergy( weqDepth/10.);//LUT in cm
    double stoppingPowerWater=ProtonSpotPB::calculateStoppingPower(currEnergy, ProtonSpotPB_constZARatioWater, ProtonSpotPB_constExcitationEnergyWater);
    double stoppingPowerMaterial=ProtonSpotPB::calculateStoppingPower(currEnergy,materialAtomZARatio, excitationEnergyMaterial);
    double rescaleRatio= ProtonSpotPB::getStoppingPowerRatioToWater(stoppingPowerWater, stoppingPowerMaterial)*densityMaterial;
    
    //get BeamShape
    double MSsquaredVariance=MSsquaredVarianceLUT->getData(weqDepth);//newLUTs in mm
    double currentSigmaX=sqrt(MSsquaredVariance+pow(beamInitSigmaX+ (weqDepth/10.)*beamInitSigmaXWidening,2) );//in cm
    double currentSigmaY=sqrt(MSsquaredVariance+pow(beamInitSigmaY+ (weqDepth/10.)*beamInitSigmaYWidening,2) );//in cm
//nuclear correction is calculated with function
    double GaussVoigtRelation=nucCorrTable->getGaussVoigtRelation(weqDepth/10.);
    double VoigtSigma=nucCorrTable->getVoigtSigma(weqDepth/10.);
    double VoigtLg=nucCorrTable->getVoigtLg(weqDepth/10.);
    
//    cout<<"currEnergy "<<currEnergy<<" rescaleRatio "<<rescaleRatio<<" MSsquaredVariance "<<MSsquaredVariance<<
//          " GaussVoigtRelation "<<  GaussVoigtRelation<<" VoigtSigma "<<VoigtSigma<<" VoigtLg "<<VoigtLg<<endl;
    
     if ( isnan(GaussVoigtRelation) ||  isinf(GaussVoigtRelation) || GaussVoigtRelation<0. ) {cerr<<"ERROR: Aborting, GaussVoigtRelation inf or nan or negative :"<< GaussVoigtRelation<<endl;cout<<" weqDepth "<<weqDepth<<endl; exit(-1);}
     if ( isnan(VoigtSigma) ||  isinf(VoigtSigma) || VoigtSigma<0. ) {cerr<<"ERROR: Aborting, VoigtSigma inf or nan or negative :"<< VoigtSigma<<endl;cout<<" weqDepth "<<weqDepth<<endl; exit(-1);}
     if ( isnan(VoigtLg) ||  isinf(VoigtLg) || VoigtLg<0. ) {cerr<<"ERROR: Aborting, VoigtLg inf or nan or negative :"<< VoigtLg<<endl;cout<<" weqDepth "<<weqDepth<<endl; exit(-1);}
    
    //get weightFactor
    double weightFactorNoFluence=getBeamShapeAtDistanceFromCenter(distanceFromBeamX,distanceFromBeamY,
                                                          currentSigmaX*10., currentSigmaY*10.,
                                                          beamInitSigmaX*10.,beamInitSigmaY*10.,
                                                          GaussVoigtRelation,VoigtSigma*10.,
                                                          VoigtLg*10., ProtonSpotPB_stepSize*10.);//everything in mm 
    //calc Dose
    //calculate dose deposition factor e.g. fraction of dose deposited at this depth   
     //LUT contains edep for water, should be the same as dose    
     double inDepthEdepDepositionFactor=lookUpTable->getEdep(weqDepth/10.);
     double inDepthReleasedEnergy=inDepthEdepDepositionFactor*weightFactorNoFluence*thisBeamFluence*rescaleRatio;
//   cout<<"HU: "<<dHUnit <<" dosedepFactor: "<<inDepthDoseDepositionFactor<<" weightFactor "<<weightFactorNoFluence<<" thisBeamFluence "<<thisBeamFluence<<" rescaleRatio "<<rescaleRatio<<endl;
     //------------------------------------------------------------------------------                    
     //dose calculation
                         //ATTENTION !!!
                         //inDepthReleasedDose is not correct dose, needs to be scaled according to voxelsize
                         //May not be correct
                         //1 Gy = J/kg
                         //mass = density * Volume
                         //Gray= MeVtoJoule / mass *1000 (1000 due to density in gram)
                         //=>edep*ProtonSpotPB_constMeVToJoule / rescaleRatio
    double massOfVoxelInGram=densityMaterial* pow((ProtonSpotPB_stepSize),3);  
//    double doseAtVoxel=inDepthReleasedEnergy* pow((rescaleRatio),3)*ProtonSpotPB_constMeVToJoule / (massOfVoxelInGram/1000.);
    double doseAtVoxel=inDepthReleasedEnergy* ProtonSpotPB_constMeVToJoule / (massOfVoxelInGram/1000.);
    if ( isnan(doseAtVoxel) ||  isinf(doseAtVoxel) || doseAtVoxel<0. ) {cerr<<"ERROR: Aborting, doseAtVoxel inf or nan or negative :"<< doseAtVoxel<<endl; exit(-1);}

//    if(m_pDoseCompGrid->isInContour(iHypX,iHypY,iHypZ)==false)
//    {
//    if(dHUnit>0.)
//    {
//    cout<<"distanceFromBeamSourceZ: "<< distanceFromBeamZ<<" weqDepth: "<<weqDepth<<" distanceFromBeamX: "<<distanceFromBeamX<<" distanceFromBeamY: "<<distanceFromBeamY<<endl;
//    cout<<" inDepthEdepDepositionFactor "<<inDepthEdepDepositionFactor<<" weightFactorNoFluence: "<<weightFactorNoFluence<<endl;
//    cout<<" HU: "<<dHUnit<<" densityMaterial "<<densityMaterial<<endl;
//    cout<<"  ix: "<<iHypX<<" iy: "<<iHypY<<" iz: "<<iHypZ<<endl;
////    cout<<"HU: "<<dHUnit<<" densityMaterial "<<densityMaterial<<" inDepthEdepDepositionFactor "<<inDepthEdepDepositionFactor<<endl;
////    cout<<" doseAtVoxelNoWeight "<<doseAtVoxel/weightFactorNoFluence<<" massOfVoxelInGram "<<massOfVoxelInGram<<endl;
////    }
//    }
 return doseAtVoxel; 
}

//singlePencilBeamAlgorithm(bx, Trafo, beamDataArray[k], dimensionZ,
//                                 distanceToGrid, reachedBraggPeaktemp, iPBXtemp,iPBYtemp,iPBZtemp);
void ProtonSpotPB::doseCalcLoop(const DoseAtomSpec& bx,UktBase::BeamPatientTrafo& Trafo, 
        Double_t *beamDataArray,bool& reachedBraggPeak, double& dBPX,double& dBPY,double& dBPZ)
{
    double beamX0                 = beamDataArray[0];
    double beamY0                 = beamDataArray[1];      
//    double beamInitSigmaX     = beamDataArray[2];
//    double beamInitSigmaY     = beamDataArray[3];
//    double thisBeamFluence    = beamDataArray[4];
    double initEnergy         = beamDataArray[5];
    
    
    double distanceToGrid;
    getDistanceToDensGridIfHit(bx, beamX0,beamY0, 0.0, distanceToGrid);
    
    std::vector<double> realDepth;
    std::vector<double> WEQDepth;
    std::vector<double> MScurrentSquaredVariance;
    double realPBdepth,maxRealDepth;
    lookUpTable->setTargetEnergy(initEnergy);
    nucCorrTable->setTargetEnergy(initEnergy);
    double distanceSourceToContour;//distance from beam source to contour
    calcuateWEQandMSarrays(bx, realDepth,WEQDepth,MScurrentSquaredVariance,
                               reachedBraggPeak,realPBdepth,maxRealDepth,distanceSourceToContour,
                               initEnergy,distanceToGrid,beamX0, beamY0);
    
    //initialize class
    interpol* weqLUT;
    weqLUT = new interpol;
    weqLUT->setInterpolEdepData(realDepth,WEQDepth);
    interpol* MSsquaredVarianceLUT;
    MSsquaredVarianceLUT = new interpol;
    MSsquaredVarianceLUT->setInterpolEdepData(realDepth,MScurrentSquaredVariance);    
    
//    cout<<"energy "<<initEnergy <<" realPBdepth "<<realPBdepth<<" WEQPBdepth "<<weqLUT->getData(realPBdepth)
//            <<" maxRealDepth "<<maxRealDepth<<endl;
    

    //get start point
    double entryX,entryY,entryZ;

    transformBeamCoordIntoCubeCoord(bx,beamX0,beamY0,distanceToGrid, entryX, entryY, entryZ);
    double nRvX,nRvY,nRvZ,nxDirVecX, nxDirVecY, nxDirVecZ,nyDirVecX, nyDirVecY, nyDirVecZ;
    getNormVectorsOfBeamInHyperion(bx, nRvX, nRvY, nRvZ ,
                                     nxDirVecX, nxDirVecY, nxDirVecZ,
                                     nyDirVecX, nyDirVecY, nyDirVecZ );
    
    //calc BraggPeak location
//    cout<<"realPBdepth "<<realPBdepth<<" distanceToGrid "<<distanceToGrid<<endl;
    dBPX=entryX+(realPBdepth-distanceToGrid)*nRvX;
    dBPY=entryY+(realPBdepth-distanceToGrid)*nRvY;
    dBPZ=entryZ+(realPBdepth-distanceToGrid)*nRvZ; 

    int xMin,xMax,yMin,yMax,zMin,zMax;

//    if ( (realPBdepth-distanceToGrid)>160.) cout<<"PB depth "<<(realPBdepth-distanceToGrid)<<endl;

    //calculate start point of contour
    double startContourX,startContourY,startContourZ;
    int istartContourX,istartContourY,istartContourZ;

    startContourX=entryX+ (distanceSourceToContour-distanceToGrid)*nRvX;
    startContourY=entryY+ (distanceSourceToContour-distanceToGrid)*nRvY;
    startContourZ=entryZ+ (distanceSourceToContour-distanceToGrid)*nRvZ;

//    getGridEntryAndExitPoints(bx,beamX0,beamY0,distanceToGrid,
//                                    entryX, entryY, entryZ,
//                                    exitX, exitY, exitZ);
//    cout<<"old exitX "<<exitX<<" exitY "<<exitY<<" exitZ "<<exitZ<<endl;
    convertHyperionToVoxelCoordinates(startContourX, startContourY, startContourZ,
                                      istartContourX,istartContourY,istartContourZ);
    //calculate point along axis which is 1.5 times 
    //the distance behind the Bragg Peak
    double exitX,exitY,exitZ;
    int iexitX,iexitZ,iexitY;
    exitX=entryX+(maxRealDepth-distanceToGrid)*nRvX;
    exitY=entryY+(maxRealDepth-distanceToGrid)*nRvY;
    exitZ=entryZ+(maxRealDepth-distanceToGrid)*nRvZ;
    
    convertHyperionToVoxelCoordinates(exitX, exitY, exitZ,
                                iexitX,iexitY,iexitZ);
    
//    cout<<"ientryX "<<ientryX<<" ientryY "<<ientryY<<" ientryZ "<<ientryZ;
//    cout<<" iexitX "<<iexitX<<" iexitY "<<iexitY<<" iexitZ "<<iexitZ<<endl;
    
    if(istartContourX>=iexitX) {xMax=istartContourX; xMin=iexitX;}
    else {xMax=iexitX; xMin=istartContourX;}
    if(istartContourY>=iexitY) {yMax=istartContourY; yMin=iexitY;}
    else {yMax=iexitY; yMin=istartContourY;}
    if(istartContourZ>=iexitZ) {zMax=istartContourZ; zMin=iexitZ;}
    else {zMax=iexitZ; zMin=istartContourZ;}
    //add safety offset of 5 voxel in each direction
    xMin-=ProtonSpotPB_lateralVoxelsToCalculate;
    xMax+=ProtonSpotPB_lateralVoxelsToCalculate;
    yMin-=ProtonSpotPB_lateralVoxelsToCalculate;
    yMax+=ProtonSpotPB_lateralVoxelsToCalculate;
    zMin-=ProtonSpotPB_lateralVoxelsToCalculate;
    zMax+=ProtonSpotPB_lateralVoxelsToCalculate;
    
    if(xMin<0 )xMin=0;
    if(xMax>XDim) xMax=XDim;
    if(yMin<0 )yMin=0;
    if(yMax>YDim) yMax=YDim;
    if(zMin<0 )zMin=0;
    if(zMax>ZDim) zMax=ZDim;
    int temp;
    double x,y;
    
    xMin=yMin=zMin=0;
    xMax=XDim;yMax=YDim;zMax=ZDim;
    
    double sourceX,sourceY,sourceZ;
    sourceX=entryX-distanceToGrid*nRvX;
    sourceY=entryY-distanceToGrid*nRvY;
    sourceZ=entryZ-distanceToGrid*nRvZ;
//    cout<<"entryX "<<entryX<<" entryY "<<entryY<<" entryZ "<<entryZ<<"sourceX "<<sourceX<<" sourceY "<<sourceY<<" sourceZ "<<sourceZ<<endl;
    //transformBeamCoordIntoCubeCoord(bx,beamX0,beamY0,0.0,sourceX,sourceY,sourceZ);
       
//        cout<<"xMin "<<xMin<<" xMax "<<xMax<<" yMin "<<yMin<<" yMax "<<yMax<<" zMin "<<zMin<<" zMax "<<zMax<<endl;
    
//        int counter=0;
        //deposit dose in this area
        double doseToWrite=0.0;
        int i,j,k;
        i=j=k=0;
        double distanceFromBeamX,distanceFromBeamY,distanceFromBeamZ;
        distanceFromBeamX=distanceFromBeamY=distanceFromBeamZ=0;
        for(i=zMin;i<zMax;i++)
        {          
            if( round(nRvZ*1000)!=0 )
               {
//                   cout<<"adjusted range from yMin: "<<yMin<<" yMax "<<yMax;     
                   y=entryY+ (i*gridStepSize-entryZ)/nRvZ   *nRvY;
                   temp=y/gridStepSize-ProtonSpotPB_lateralVoxelsToCalculate;
                   if(temp>=0 && temp<YDim) yMin=temp;
                   temp=y/gridStepSize+ProtonSpotPB_lateralVoxelsToCalculate;
                   if(temp>=0 && temp<YDim) yMax=temp;
//                   cout<<"to yMin: "<<yMin<<" yMax "<<yMax<<endl;     
               }
            else if( round(nRvX*1000)!=0 )
               {
//                   cout<<"adjusted range from yMin: "<<yMin<<" yMax "<<yMax;     
                   y=entryY+ (k*gridStepSize-entryX)/nRvX   *nRvY;
                   temp=y/gridStepSize-ProtonSpotPB_lateralVoxelsToCalculate;
                   if(temp>=0 && temp<YDim) yMin=temp;
                   temp=y/gridStepSize+ProtonSpotPB_lateralVoxelsToCalculate;
                   if(temp>=0 && temp<YDim) yMax=temp;
//                   cout<<"to yMin: "<<yMin<<" yMax "<<yMax<<endl;     
               }
            xMin=yMin=zMin=0;
    xMax=XDim;yMax=YDim;zMax=ZDim;
           for(j=yMin;j<yMax;j++)
           { 
               if( round(nRvY*1000)!=0 )
               {
//                   cout<<"adjusted range from xMin: "<<xMin<<" xMax "<<xMax;     
                   x=entryX+ (j*gridStepSize-entryY)/nRvY   *nRvX;
                   temp=x/gridStepSize-ProtonSpotPB_lateralVoxelsToCalculate;
                   if(temp>=0 && temp<XDim) xMin=temp;
                   temp=x/gridStepSize+ProtonSpotPB_lateralVoxelsToCalculate;
                   if(temp>=0 && temp<XDim) xMax=temp;
//                cout<<"to xMin: "<<xMin<<" xMax "<<xMax<<endl;     
               }
               else if( round(nRvZ*1000)!=0 )
               {
//                   cout<<"adjusted range from xMin: "<<xMin<<" xMax "<<xMax;     
                   x=entryX+ (i*gridStepSize-entryZ)/nRvZ   *nRvX;
                   temp=x/gridStepSize-ProtonSpotPB_lateralVoxelsToCalculate;
                   if(temp>=0 && temp<XDim) xMin=temp;
                   temp=x/gridStepSize+ProtonSpotPB_lateralVoxelsToCalculate;
                   if(temp>=0 && temp<XDim) xMax=temp;
//                cout<<"to xMin: "<<xMin<<" xMax "<<xMax<<endl;     
               }
               xMin=yMin=zMin=0;
    xMax=XDim;yMax=YDim;zMax=ZDim;
                for(k=xMin;k<xMax;k++)
                {           
                  getDistanceFromBeamAndDepthAlongBeamAxis(bx,beamX0,beamY0, k*gridStepSize,  j*gridStepSize,  i*gridStepSize,
                          sourceX,sourceY,sourceZ,
                          nRvX, nRvY, nRvZ ,
                          nxDirVecX, nxDirVecY, nxDirVecZ,
                          nyDirVecX, nyDirVecY, nyDirVecZ,  
                          distanceFromBeamX,  distanceFromBeamY, distanceFromBeamZ);
////                  cout<<"distanceFromBeamX "<<distanceFromBeamX<<" distanceFromBeamY "<<distanceFromBeamY<<" distanceFromBeamZ "<<distanceFromBeamZ<<endl;
//                  
//                  if(distanceFromBeamZ>maxRealDepth) continue;// {cout<<"distanceFromBeamZ "<<distanceFromBeamZ<<" skipping due to Z: ix "<<k<<" iy "<<j<<" iz "<<i<<endl; continue;}
//                  if(distanceFromBeamY>ProtonSpotPB_lateralVoxelsToCalculate*gridStepSize) continue;//{cout<<"skipping due to Y: ix "<<k<<" iy "<<j<<" iz "<<i<<endl; continue;}
//                  if(distanceFromBeamX>ProtonSpotPB_lateralVoxelsToCalculate*gridStepSize) continue;//{cout<<"skipping due to X: ix "<<k<<" iy "<<j<<" iz "<<i<<endl; continue;}
//                  //                  cout<<"at voxel# "<<counter<<"ix "<<i<<" iy "<<j<<" iz "<<k;
//                                    //check whether voxel is in body contour
                  if(m_pDoseCompGrid->isInContour(k, j, i)==false) continue;//{cout<<"skipping out of body: ix "<<k<<" iy "<<j<<" iz "<<i<<endl; continue;}

                    doseToWrite= getDoseAtVoxel(bx, weqLUT, MSsquaredVarianceLUT, beamDataArray, k, j, i,
                                                distanceFromBeamX,  distanceFromBeamY, distanceFromBeamZ);
                    m_pDoseGrid->operator()( k, j, i) += static_cast<float>(doseToWrite);
//                    counter++;
//                   cout<<"written dose "<<doseToWrite<<endl;
                }
           }
        }//end dose deposit
//        cout<<counter<<" voxels calculated"<<endl;
    delete weqLUT;
    delete MSsquaredVarianceLUT;
 return;   
}


inline void ProtonSpotPB::getDistanceFromBeamAndDepthAlongBeamAxis(const DoseAtomSpec& bx,
        double beamX0,double beamY0,double pointX, double pointY, double pointZ,
        double startX,double startY, double startZ,
        double nRvX,double nRvY,double nRvZ ,
        double nxDirVecX,double nxDirVecY,double nxDirVecZ,
        double nyDirVecX,double nyDirVecY,double nyDirVecZ,        
        double& distanceFromBeamX, double& distanceFromBeamY,double& distanceFromBeamZ)
{
//    //get norm vectors
//    double nRvX, nRvY, nRvZ;
//    double nxDirVecX, nxDirVecY, nxDirVecZ, nyDirVecX, nyDirVecY, nyDirVecZ;
//    getNormVectorsOfBeamInHyperion(bx, nRvX, nRvY, nRvZ ,
//                                     nxDirVecX, nxDirVecY, nxDirVecZ,
//                                     nyDirVecX, nyDirVecY, nyDirVecZ );
//            
//    //get start point of beam
//    double startX,startY,startZ;
//    transformBeamCoordIntoCubeCoord(bx,beamX0,beamY0,0.0,startX,startY,startZ);   
    
    double distanceXBetweenPoints=pointX-startX;
    double distanceYBetweenPoints=pointY-startY;
    double distanceZBetweenPoints=pointZ-startZ;

    //Use Hesse distance formula
    double tempX,tempY,tempZ;
    double t;
    
    t= (distanceXBetweenPoints*nRvX+ distanceYBetweenPoints*nRvY + distanceZBetweenPoints*nRvZ);//division through length of nRv not necessary, because normalized
    //nearest point on vector to given point
    tempX=startX+t*nRvX;
    tempY=startY+t*nRvY;
    tempZ=startZ+t*nRvZ;
    //vector from start to closest point on vector
    distanceXBetweenPoints=startX-tempX;
    distanceYBetweenPoints=startY-tempY;
    distanceZBetweenPoints=startZ-tempZ;
//    cout<<"StartToClosestPoint Distance: distanceXBetweenPoints "<<distanceXBetweenPoints<<" distanceYBetweenPoints "<<distanceYBetweenPoints<<" distanceZBetweenPoints "<<distanceZBetweenPoints<<endl;

    
    distanceFromBeamZ=sqrt( distanceXBetweenPoints*distanceXBetweenPoints + distanceYBetweenPoints*distanceYBetweenPoints + distanceZBetweenPoints*distanceZBetweenPoints );
//    cout<<"distanceInVectorDirection test "<<distanceInVectorDirection<<endl;
    //vector from point to closest point on vector
    distanceXBetweenPoints=pointX-tempX;
    distanceYBetweenPoints=pointY-tempY;
    distanceZBetweenPoints=pointZ-tempZ;
//    cout<<"ClosestPoint Distance: distanceXBetweenPoints "<<distanceXBetweenPoints<<" distanceYBetweenPoints "<<distanceYBetweenPoints<<" distanceZBetweenPoints "<<distanceZBetweenPoints<<endl;

    
    double absnxDirVecX=fabs(nxDirVecX);
    double absnxDirVecY=fabs(nxDirVecY);
    double absnxDirVecZ=fabs(nxDirVecZ);
    double absnyDirVecX=fabs(nyDirVecX);
    double absnyDirVecY=fabs(nyDirVecY);
    double absnyDirVecZ=fabs(nyDirVecZ);
    //distance in X
    //find which component is maximal, use this component for calculation
//    cout<<"nxDirVecX "<<nxDirVecX<<" nxDirVecY "<<nxDirVecY<<" nxDirVecZ "<<nxDirVecZ<<endl;
    if(absnxDirVecX>=absnxDirVecY && absnxDirVecX>=absnxDirVecZ) distanceFromBeamX=distanceXBetweenPoints/nxDirVecX; //use x component
    else if(absnxDirVecY>=absnxDirVecX && absnxDirVecY>=absnxDirVecZ) distanceFromBeamX=distanceYBetweenPoints/nxDirVecY; //use y component
    else if(absnxDirVecZ>=absnxDirVecX && absnxDirVecZ>=absnxDirVecY) distanceFromBeamX=distanceZBetweenPoints/nxDirVecZ; //use z component
    
    //distance in Y
//    cout<<"nyDirVecX "<<nyDirVecX<<" nyDirVecY "<<nyDirVecY<<" nyDirVecZ "<<nyDirVecZ<<endl;
    //find which component is maximal, use this component for calculation
    if(absnyDirVecX>=absnyDirVecY && absnyDirVecX>=absnyDirVecZ) distanceFromBeamY=distanceXBetweenPoints/nyDirVecX; //use x component
    else if(absnyDirVecY>=absnyDirVecX && absnyDirVecY>=absnyDirVecZ) distanceFromBeamY=distanceYBetweenPoints/nyDirVecY; //use y component
    else if(absnyDirVecZ>=absnyDirVecX && absnyDirVecZ>=absnyDirVecY) distanceFromBeamY=distanceZBetweenPoints/nyDirVecZ; //use z component
    
    return;
}


inline void ProtonSpotPB::getNormVectorsOfBeamInHyperion(const DoseAtomSpec& bx,
                                    double& nRvX,double& nRvY,double& nRvZ ,
                                    double& nxDirVecX,double& nxDirVecY,double& nxDirVecZ,
                                    double& nyDirVecX,double& nyDirVecY,double& nyDirVecZ )
{
    double startX,startY,startZ;
    double xTemp,yTemp,zTemp;
    //get start point of beam
    transformBeamCoordIntoCubeCoord(bx,0.0,0.0,0.0,startX,startY,startZ);
    //get second point to create a direction vector
    transformBeamCoordIntoCubeCoord(bx,0.0,0.0,1.0,xTemp,yTemp,zTemp);
    
    //calculate normalized direction vector
    //in hyperion coordinates
    double norm;
    nRvX=xTemp-startX;
    nRvY=yTemp-startY;
    nRvZ=zTemp-startZ;
    norm=1./sqrt(nRvX*nRvX + nRvY*nRvY + nRvZ*nRvZ);
    nRvX*=norm;
    nRvY*=norm;
    nRvZ*=norm;
    
    //calculate Vector in BeamX direction
    //get second point to create a direction vector
    transformBeamCoordIntoCubeCoord(bx,1.,0.0,0.0,xTemp,yTemp,zTemp);
    //calculate normalized vector
    //in hyperion coordinates
    nxDirVecX=xTemp-startX;
    nxDirVecY=yTemp-startY;
    nxDirVecZ=zTemp-startZ;
    norm=1./sqrt(nxDirVecX*nxDirVecX + nxDirVecY*nxDirVecY + nxDirVecZ*nxDirVecZ);
    nxDirVecX*=norm;
    nxDirVecY*=norm;
    nxDirVecZ*=norm;
    
     //calculate Vector in BeamY direction
    //get second point to create a direction vector
    transformBeamCoordIntoCubeCoord(bx,0.0,1.0,0.0,xTemp,yTemp,zTemp);
    //calculate normalized vector
    //in hyperion coordinates
    nyDirVecX=xTemp-startX;
    nyDirVecY=yTemp-startY;
    nyDirVecZ=zTemp-startZ;
    norm=1./sqrt(nyDirVecX*nyDirVecX + nyDirVecY*nyDirVecY + nyDirVecZ*nyDirVecZ);
    nyDirVecX*=norm;
    nyDirVecY*=norm;
    nyDirVecZ*=norm;
 return;   
}


inline void ProtonSpotPB::transformBeamCoordIntoCubeCoord(const DoseAtomSpec& bx,double beamX,double beamY,double beamZ,double& cubeX,double& cubeY,double& cubeZ) const
{
	//create coordinate transformation object
	 UktBase::BeamPatientTrafo Trafo( bx );
	//initialize some kind of geometry object
	     //const ProtonSpotGeometry* const pGeo = static_cast<const ProtonSpotGeometry* const>(bx.getRayGeometry());

	//Source isocenter distance
	double sidcm = bx.getSID();

	// position of source in cube coordinate system in cm units
          // vector from isocentre to source
          // central axis direction is (0,0,1) in linac coord system
          double xs, ys, zs;
          Trafo.transform(0.0, 0.0, sidcm, xs, ys, zs);   
          // vector from origin of cube coord. system to source 
          xs = m_IsoX-xs;
          ys = m_IsoY-ys;
          zs = m_IsoZ-zs;
//	// set particle direction in cube coord system 
//	double xDir, yDir, zDir;
//	//Trafo.transform( particle.dir.x, particle.dir.y, particle.dir.z, xb, yb, zb);
//	//in helium case easier
//	//as positive z axis of beam plane coord system IS the beam direction
//        Trafo.transform( 0.0, 0.0, 1.0, xDir, yDir, zDir);

	//transform origin of beam into cube coord system
	//get origin using ->getXPosition()
	double xSource, ySource, zSource;
	Trafo.transform( beamX, beamY, beamZ, xSource, ySource, zSource);
	cubeX=xs+xSource;
	cubeY=ys+ySource;
	cubeZ=zs+zSource;

	//particle.pos.y = static_cast<real>(yp + ys);
	//particle.pos.z = static_cast<real>(zp + zs);  

//cout<<"BeamCoordX "<< beamX<<" BeamCoordY "<< beamY<<" BeamCoordZ "<< beamZ<<endl;
//cout<<"CubeCoordX "<< cubeX<<" CubeCoordY "<< cubeY<<" CubeCoordZ "<< cubeZ<<endl; 

return;
}

//beam in beam coord
inline bool ProtonSpotPB::getDistanceToDensGridIfHit(const DoseAtomSpec& bx,double beamX,double beamY,double beamZ, double& distance) const
{
    //calculate everything in millimeter
	//create coordinate transformation object
	     UktBase::BeamPatientTrafo Trafo( bx );
	//initialize some kind of geometry object
	     //const ProtonSpotGeometry* const pGeo = static_cast<const ProtonSpotGeometry* const>(bx.getRayGeometry());
             
	//Source isocenter distance
	double sidcm = bx.getSID();//* 0.1;

	// position of source in cube coordinate system in cm units
	      // vector from isocentre to source
	      // central axis direction is (0,0,1) in linac coord system
	      double xs, ys, zs;
	      Trafo.transform(0.0, 0.0, sidcm, xs, ys, zs);   
	      // vector from origin of cube coord. system to source 
	      xs = m_IsoX-xs;
	      ys = m_IsoY-ys;
	      zs = m_IsoZ-zs;

	// set particle direction in cube coord system 
	double xDir, yDir, zDir;
	//Trafo.transform( particle.dir.x, particle.dir.y, particle.dir.z, xb, yb, zb);
	//in helium case easier
	//as positive z axis of beam plane coord system IS the beam direction
        Trafo.transform( 0.0, 0.0, 1.0, xDir, yDir, zDir);

	//transform origin of beam into cube coord system
	//get origin using ->getXPosition()
	double xSource, ySource, zSource;
	double xParticlePosCube,yParticlePosCube,zParticlePosCube;
	Trafo.transform( beamX, beamY, beamZ, xSource, ySource, zSource);
	xParticlePosCube=xs+xSource;
	yParticlePosCube=ys+ySource;
	zParticlePosCube=zs+zSource;
//        double initPartx=xParticlePosCube;
//        double initParty=yParticlePosCube;
//        double initPartz=zParticlePosCube;

        
        double initParticlePosCubeX=xParticlePosCube;
        double initParticlePosCubeY=yParticlePosCube;
        double initParticlePosCubeZ=zParticlePosCube;
	//particle.pos.x = static_cast<real>(xp + xs);
	//particle.pos.y = static_cast<real>(yp + ys);
	//particle.pos.z = static_cast<real>(zp + zs);  

       
//cout<<"xDir "<<xDir<<" yDir "<<yDir<<" zDir"<<zDir<<endl;
	// trace to cube !!
	// find penetration point of central ray axis
	// transport particle to cube: rotate and trace
	//loop ends when first point in density grid was found
	//necessary, because density grid does not fill the cube coordinate system,
	//only parts of it
	int ix,iy,iz;
	ix=iy=iz=0;
	double minTransX,minTransY,minTransZ;
	minTransX=minTransY=minTransZ=ProtonSpotPB_stepSize*0.49*10.;
	double maxTransX = ProtonSpotPB_stepSize * (XDim-0.51)*10.;
  	double maxTransY = ProtonSpotPB_stepSize * (YDim-0.51)*10.;
  	double maxTransZ = ProtonSpotPB_stepSize * (ZDim-0.51)*10.;
//cout<<"DoseCompGridVoxel Size"<<m_pDoseCompGrid->getVoxelSize()<<endl;        
//cout<<"BEFORE xParticlePosCube "<<xParticlePosCube<<" yParticlePosCube "<<yParticlePosCube<<" zParticlePosCube"<<zParticlePosCube<<endl;
	bool bMaxDirNonNil = false;
	double fDirX = fabs( xDir );
	double fDirY = fabs( yDir );
	double fDirZ = fabs( zDir );

	do {  
	  double trans = 0.0;
	  if((fDirX >= fDirY) && (fDirX >= fDirZ) && (fDirX != 0.0)) {
	    if(xParticlePosCube < minTransX) 
	      trans = (xParticlePosCube + minTransX) / xDir;
	    else if ( xParticlePosCube > maxTransX )
	      trans = (xParticlePosCube - maxTransX) / xDir;
	    bMaxDirNonNil = true;
	    fDirX = 0.0;
//            cout<<"in xloop"<<endl;
//            cout<<"xParticlePosCube "<<xParticlePosCube<<" minTransX "<<minTransX<<" maxTransX "<<maxTransX<<endl;
	  }
	  else if((fDirY >= fDirZ) && (fDirY >= fDirX) && (fDirY != 0.0)) {
	    if(yParticlePosCube < minTransY) 
	      trans = (yParticlePosCube + minTransY) / yDir;
	    else if ( yParticlePosCube > maxTransY )
	      trans = (yParticlePosCube - maxTransY) / yDir;
	    bMaxDirNonNil = true;
	    fDirY = 0.0;
	  }
	  else if((fDirZ >= fDirX) && (fDirZ >= fDirY) && (fDirZ != 0.0)) {
	    if(zParticlePosCube < minTransZ) 
	      trans = (zParticlePosCube + minTransZ) / zDir;
	    else if ( zParticlePosCube > maxTransZ )
	      trans = (zParticlePosCube - maxTransZ) / zDir;
	    bMaxDirNonNil = true;
	    fDirZ = 0.0;
	  }
	  else {
	    bMaxDirNonNil = false;
	    break;
	  }
//          cout<<" xDir "<<xDir<<" yDir "<<yDir<<" zDir "<<zDir<<endl;
//          cout<<" fDirX "<<fDirX<<" fDirY "<<fDirY<<" fDirZ "<<fDirZ<<endl;
//          cout<<" trans "<<trans<<endl;
//          cout<< "xParticlePosCube "<<xParticlePosCube<<endl;
	  // if endpoint is in the cube break.
	   ix = static_cast<int>( round( (xParticlePosCube -= static_cast<double>( trans*xDir ))*conversionMMtoVoxel) );
	   iy = static_cast<int>( round( (yParticlePosCube -= static_cast<double>( trans*yDir ))*conversionMMtoVoxel) );
	   iz = static_cast<int>( round( (zParticlePosCube -= static_cast<double>( trans*zDir ))*conversionMMtoVoxel) );
//cout<<" xParticlePosCube "<<xParticlePosCube<<" yParticlePosCube "<<yParticlePosCube<<" zParticlePosCube"<<zParticlePosCube<<endl;
//cout<<"ix "<<ix<<" iy "<<iy<<" iz "<<iz<<endl;
	  if((ix < XDim) && (ix >= 0) &&
	     (iy < YDim) && (iy >= 0) &&
	     (iz < ZDim) && (iz >= 0))
	    break;
	} while(bMaxDirNonNil == true); 
        

//        if(bMaxDirNonNil == false) cout<<"NOT HIT density grid"<<endl;
//        if(bMaxDirNonNil == true) cout<<"Jipee HIT density grid"<<endl;
        
 //if(bMaxDirNonNil == true) exit(0);
        
        //abort here if we did not hit the density grid
        if (bMaxDirNonNil == false)
        {
        distance = -1.;          
        return false;
        }
    
//        cout<<"initial Particle Position x: "<<initPartx<<" y: "<<initParty<<" z: "<<initPartz<<endl;
////        cout<<"at dens grid border x: "<<xParticlePosCube<<" y: "<<yParticlePosCube<<" z: "<<zParticlePosCube<<endl;
//        cout<<"at dens grid border voxel ix: "<<ix<<" iy: "<<iy<<" iz: "<<iz<<endl; 


	double tempx,tempy,tempz;
	tempx=fabs( xParticlePosCube-initParticlePosCubeX );
	tempy=fabs( yParticlePosCube-initParticlePosCubeY );
	tempz=fabs( zParticlePosCube-initParticlePosCubeZ );
//        cout<<"DensGrid Vector x: "<<tempx <<" y: "<<tempy <<" z: "<<tempz << endl;
	distance=sqrt(tempx*tempx+tempy*tempy+tempz*tempz);
//        double distanceFromBeamX,  distanceFromBeamY, distanceFromBeamZ;
//        getDistanceFromBeamAndDepthAlongBeamAxis(bx,beamX,beamY, xParticlePosCube,  yParticlePosCube,  zParticlePosCube, distanceFromBeamX,  distanceFromBeamY, distanceFromBeamZ);
//           cout<<"distance function says: distanceFromBeamX "<<distanceFromBeamX<<" distanceFromBeamY "<<distanceFromBeamY<<" distanceFromBeamZ "<<distanceFromBeamZ<<endl;
//           cout<<" distance function says this: "<<distance<<endl;
	//calculate distance from beam source until cube hit
//	double tempx,tempy,tempz;
//	tempx=fabs( xParticlePosCube-xs );
//	tempy=fabs( yParticlePosCube-ys );
//	tempz=fabs( zParticlePosCube-zs );

//cout<<"xs "<<xs<<"ys "<<ys<<"zs "<<zs<<endl;
//cout<<"distance in fct "<<distance<<endl;
return bMaxDirNonNil;
}

inline bool ProtonSpotPB::getDistancesToDensAndTargetGridIfHit(const DoseAtomSpec& bx,double beamX,double beamY,double beamZ,
        double& distanceToDensGrid,double& distanceSourceToTargetStartWEQ,double& distanceSourceToTargetStopWEQ,double& distanceSourceToContour) const
{
    //calculate everything in millimeter
    bool bHitTarget=false;
	//create coordinate transformation object
	UktBase::BeamPatientTrafo Trafo( bx );

	//Source isocenter distance
	double sidcm = bx.getSID();//* 0.1;

	// position of source in cube coordinate system in cm units
	      // vector from isocentre to source
	      // central axis direction is (0,0,1) in linac coord system
          double xs, ys, zs;
          Trafo.transform(0.0, 0.0, sidcm, xs, ys, zs);   
          // vector from origin of cube coord. system to source 
          xs = m_IsoX-xs;
          ys = m_IsoY-ys;
          zs = m_IsoZ-zs;             

	// set particle direction in cube coord system 
	double xDir, yDir, zDir;
	//in helium case easier
	//as positive z axis of beam plane coord system IS the beam direction
        Trafo.transform( 0.0, 0.0, 1.0, xDir, yDir, zDir);

	//transform origin of beam into cube coord system
	double xSource, ySource, zSource;
	double xParticlePosCube,yParticlePosCube,zParticlePosCube;
	Trafo.transform( beamX, beamY, beamZ, xSource, ySource, zSource);
	xParticlePosCube=xs+xSource;
	yParticlePosCube=ys+ySource;
	zParticlePosCube=zs+zSource;
        
        double initParticlePosCubeX=xParticlePosCube;
        double initParticlePosCubeY=yParticlePosCube;
        double initParticlePosCubeZ=zParticlePosCube;
       
//cout<<"xDir "<<xDir<<" yDir "<<yDir<<" zDir"<<zDir<<endl;
	// trace to cube !!
	// find penetration point of central ray axis
	// transport particle to cube: rotate and trace
	//loop ends when first point in density grid was found
	//necessary, because density grid does not fill the cube coordinate system,
	//only parts of it
	int ix,iy,iz;
	ix=iy=iz=0;
	double minTransX,minTransY,minTransZ;
	minTransX=minTransY=minTransZ=ProtonSpotPB_stepSize*0.49*10.;
	double maxTransX = ProtonSpotPB_stepSize * (XDim-0.51)*10.;
  	double maxTransY = ProtonSpotPB_stepSize * (YDim-0.51)*10.;
  	double maxTransZ = ProtonSpotPB_stepSize * (ZDim-0.51)*10.;
//cout<<"DoseCompGridVoxel Size"<<m_pDoseCompGrid->getVoxelSize()<<endl;        
//cout<<"BEFORE xParticlePosCube "<<xParticlePosCube<<" yParticlePosCube "<<yParticlePosCube<<" zParticlePosCube"<<zParticlePosCube<<endl;
	bool bMaxDirNonNil = false;
	double fDirX = fabs( xDir );
	double fDirY = fabs( yDir );
	double fDirZ = fabs( zDir );

	do {  
	  double trans = 0.0;
	  if((fDirX >= fDirY) && (fDirX >= fDirZ) && (fDirX != 0.0)) {
	    if(xParticlePosCube < minTransX) 
	      trans = (xParticlePosCube + minTransX) / xDir;
	    else if ( xParticlePosCube > maxTransX )
	      trans = (xParticlePosCube - maxTransX) / xDir;
	    bMaxDirNonNil = true;
	    fDirX = 0.0;
	  }
	  else if((fDirY >= fDirZ) && (fDirY >= fDirX) && (fDirY != 0.0)) {
	    if(yParticlePosCube < minTransY) 
	      trans = (yParticlePosCube + minTransY) / yDir;
	    else if ( yParticlePosCube > maxTransY )
	      trans = (yParticlePosCube - maxTransY) / yDir;
	    bMaxDirNonNil = true;
	    fDirY = 0.0;
	  }
	  else if((fDirZ >= fDirX) && (fDirZ >= fDirY) && (fDirZ != 0.0)) {
	    if(zParticlePosCube < minTransZ) 
	      trans = (zParticlePosCube + minTransZ) / zDir;
	    else if ( zParticlePosCube > maxTransZ )
	      trans = (zParticlePosCube - maxTransZ) / zDir;
	    bMaxDirNonNil = true;
	    fDirZ = 0.0;
	  }
	  else {
	    bMaxDirNonNil = false;
	    break;
	  }
//          cout<<" xDir "<<xDir<<" yDir "<<yDir<<" zDir "<<zDir<<endl;
//          cout<<" fDirX "<<fDirX<<" fDirY "<<fDirY<<" fDirZ "<<fDirZ<<endl;
//          cout<<" trans "<<trans<<endl;
//          cout<< "xParticlePosCube "<<xParticlePosCube<<endl;
	  // if endpoint is in the cube break.
	   ix = static_cast<int>( round( (xParticlePosCube -= static_cast<double>( trans*xDir ))*conversionMMtoVoxel) );
	   iy = static_cast<int>( round( (yParticlePosCube -= static_cast<double>( trans*yDir ))*conversionMMtoVoxel) );
	   iz = static_cast<int>( round( (zParticlePosCube -= static_cast<double>( trans*zDir ))*conversionMMtoVoxel) );
//cout<<" xParticlePosCube "<<xParticlePosCube<<" yParticlePosCube "<<yParticlePosCube<<" zParticlePosCube"<<zParticlePosCube<<endl;
//cout<<"ix "<<ix<<" iy "<<iy<<" iz "<<iz<<endl;
	  if((ix < XDim) && (ix >= 0) &&
	     (iy < YDim) && (iy >= 0) &&
	     (iz < ZDim) && (iz >= 0))
	    break;
	} while(bMaxDirNonNil == true); 
        

//        if(bMaxDirNonNil == false) cout<<"NOT HIT density grid"<<endl;
//        if(bMaxDirNonNil == true) cout<<"Jipee HIT density grid"<<endl;
                
        //abort here if we did not hit the density grid
        if (bMaxDirNonNil == false)
        {
        distanceToDensGrid = distanceSourceToTargetStartWEQ = distanceSourceToTargetStopWEQ = distanceSourceToContour =-1.;          
        return false;
        }
 
                
        //save begin of density grid
        double dHUnit=0.;
        double densityMaterial=0.;
        double xStartDensGrid,yStartDensGrid,zStartDensGrid;
        double xStartTarget,yStartTarget,zStartTarget;
        double xStartContour,yStartContour,zStartContour;
        double xEndTarget,yEndTarget,zEndTarget;
        
        
        double densityDensGridToContour, densityContourToTarget, densityInTarget;
        densityDensGridToContour=densityContourToTarget=densityInTarget=0.0;
        int numbVoxelDensGridToContour,numbVoxelContourToTarget,numbVoxelInTarget;
        numbVoxelDensGridToContour=numbVoxelContourToTarget=numbVoxelInTarget=0;
        bool bReachedContour,bReachedTarget,bNotYetLeftTarget;
        bReachedContour=bReachedTarget=bNotYetLeftTarget=false;
             
        xStartTarget=yStartTarget=zStartTarget=xEndTarget=yEndTarget=zEndTarget=0.0;
        xStartContour=yStartContour=zStartContour=0.0;
        //save begin of density grid
        xStartDensGrid=xParticlePosCube;
        yStartDensGrid=yParticlePosCube;
        zStartDensGrid=zParticlePosCube;
        
        bool bIsInTarget=false;
        
        //check whether voxel is in contour (target)
        bool bIsInContour;//=m_pDoseCompGrid->isInContour(ix, iy, iz);
        
//        cout<<"Start at density grid border: ix: "<<ix<<" iy "<<iy<<" iz "<<iz<<endl;
//        cout<<"Start at density grid border (cube coord)x: "<<xParticlePosCube<<" y "<<yParticlePosCube<<" z "<<zParticlePosCube<<endl;
        
        
      bool runAgain=true;
      int iterator=1;
      int numberOfVoxelsAroundBeam=3;//how many voxels around target are viewed as target as well
      minTransX= ProtonSpotPB_stepSize*10.;//in mm a tiny bit smaller than grid size

while(runAgain==true)
{
   //continue to find start and end of target
  do {
            //continue one step            
           ix = static_cast<int>( (xParticlePosCube +=  minTransX*xDir )*conversionMMtoVoxel);
	   iy = static_cast<int>( (yParticlePosCube +=  minTransX*yDir )*conversionMMtoVoxel);
	   iz = static_cast<int>( (zParticlePosCube +=  minTransX*zDir )*conversionMMtoVoxel);
//         cout<<"Currently at position: ix: "<<ix<<" iy "<<iy<<" iz "<<iz<<endl;
           
           //if point is outside density grid break    
           if( (ix < 0) || (ix >= XDim) ||
	     (iy < 0) || (iy >= YDim) ||
	     (iz < 0) || (iz >= ZDim) )
           break;
 
//        //check whether voxel is in contour
        bIsInContour=m_pDoseCompGrid->isInContour(ix, iy, iz);
        
        //add up density sum form density grid until start of contour
           if(bReachedContour==false)
           {
                if (bIsInContour==true) 
                {
                    //also set start of contour
                    xStartContour=xParticlePosCube;
                    yStartContour=yParticlePosCube;
                    zStartContour=zParticlePosCube; 
                    //avoids calling this statement again
                    bReachedContour=true;
                }
            densityDensGridToContour+=densityMaterial;
            numbVoxelDensGridToContour++;
           }
        
        if (bIsInContour==true)
        {
            //get Hounsfield unit from Hyperion and convert to HU as Hyperion gives
           //raw scanner value
           dHUnit = convertCTtoHU(m_pDoseCompGrid->operator()(ix,iy,iz));  
            //query material properties for this Hounsfield Unit
           myInputClass->getMaterialDensityFromHU(dHUnit, densityMaterial);
//           myInputClass->getOneMaterialPropertiesFromHU(dHUnit, densityMaterial, radiationLengthMaterial, 
//                                                       excitationEnergyMaterial, materialAtomZARatio);
   //        cout<<"hu: "<<dHUnit<<" density: "<<densityMaterial<<endl;
           if( isnan(densityMaterial)||isinf(densityMaterial) ) densityMaterial=0.;//sanity check
           
            //check whether the voxel is in the target
           bIsInTarget=m_pTargetGrid->operator()( ix, iy , iz );
             
           //add up density sum from contour to target
           if(bReachedTarget==false && bReachedContour==true)
           {
            if (bIsInTarget==true) 
            {
                //set begin of target coordinates
                xStartTarget=xParticlePosCube;
                yStartTarget=yParticlePosCube;
                zStartTarget=zParticlePosCube;
                //avoid calling this statement again
                bReachedTarget=true;
            }
            densityContourToTarget+=densityMaterial;
            numbVoxelContourToTarget++;
           }
           //add up density sum from start to end of target
           if(bNotYetLeftTarget==false && bReachedTarget==true)
           {
            if (bIsInTarget==false) 
            {
                //set end of target coordinates
                 xEndTarget=xParticlePosCube;
                 yEndTarget=yParticlePosCube;
                 zEndTarget=zParticlePosCube;
                 //avoid calling this statement again
                bNotYetLeftTarget=true;
            }
            densityInTarget+=densityMaterial;
            numbVoxelInTarget++;
           }
        }
        
        if (bIsInTarget==true) bHitTarget=true;

        if( (ix > XDim) || (ix < 0) ||
	     (iy > YDim) || (iy < 0) ||
	     (iz > ZDim) || (iz < 0) )
	    break;    
   } while(true); 
   
   //if hit the target, abort loop
   if(bHitTarget==true)runAgain=false;
//runAgain=false;
   //adjust beam position slightly and look again
   if(bHitTarget==false)
   {
       if(iterator==1) {yParticlePosCube=yStartDensGrid;
                        zParticlePosCube=zStartDensGrid;
                        xParticlePosCube=xStartDensGrid+ gridStepSize*numberOfVoxelsAroundBeam;
                       }
       if(iterator==2) {yParticlePosCube=yStartDensGrid;
                        zParticlePosCube=zStartDensGrid;
                        xParticlePosCube=xStartDensGrid- gridStepSize*numberOfVoxelsAroundBeam;
                       }
       if(iterator==3) {xParticlePosCube=xStartDensGrid;
                        zParticlePosCube=zStartDensGrid;
                        yParticlePosCube=yStartDensGrid+ gridStepSize*numberOfVoxelsAroundBeam;
                       }
       if(iterator==4) {xParticlePosCube=xStartDensGrid;
                        zParticlePosCube=zStartDensGrid;
                        yParticlePosCube=yStartDensGrid- gridStepSize*numberOfVoxelsAroundBeam;
                       }
       
       if(iterator==5) {xParticlePosCube=xStartDensGrid;
                        yParticlePosCube=yStartDensGrid;
                        zParticlePosCube=zStartDensGrid+ gridStepSize*numberOfVoxelsAroundBeam;
                       }
       if(iterator==6) {xParticlePosCube=xStartDensGrid;
                        yParticlePosCube=yStartDensGrid;
                        zParticlePosCube=zStartDensGrid- gridStepSize*numberOfVoxelsAroundBeam;
                       }
    iterator++;
   }
  if (iterator>7)runAgain=false;//abort loop after modifications
   
}//end runAgain
        
// if (iterator!=1 && bHitTarget==true) cout<<"Hit target with modified beam position." <<endl;    
// if (iterator==1 && bHitTarget==true) cout<<"Hit target with standard beam position." <<endl;      
        
//        //display infos if contour (target) was hit
//        if (bHitTarget == true)
//        {
////        cout<<"isocentershiftX "<<bx.getIsocentreShift()[0]<< "isocentershiftY "<<bx.getIsocentreShift()[1]<<  "isocentershiftZ "<<bx.getIsocentreShift()[2]<<endl;           
////        cout<<"m_IsoX "<<m_IsoX<<"m_IsoY "<<m_IsoY<<"m_IsoZ "<<m_IsoZ<<endl;
////        cout<<"SID "<<sidcm<<endl;
////        cout<<"Source: xs "<<xs<<"ys "<<ys<<"zs "<<zs<<endl;
////        cout<<"DimX "<<XDim<<" DimY "<<YDim<<" DimZ "<<ZDim<<endl;
////        cout<<"DensGridDimX "<<XDim<<" DensGridDimY "<<YDim<<" DensGridDimZ "<<ZDim<<endl;
////            
//        cout<<"BeamCoordX "<< beamX<<" BeamCoordY "<< beamY<<" BeamCoordZ "<< beamZ<<" BeamCubeCord: init x "<<initPartx<<" y "<<initParty <<" z "<<initPartz <<endl;
//        cout<<"initialParticleVoxel ix: "<< static_cast<int>(initPartx *conversionMMtoVoxel ) <<" iy "<< static_cast<int>(initParty *conversionMMtoVoxel ) <<" iz "<< static_cast<int>(initPartz *conversionMMtoVoxel ) <<endl; 
////        cout<<"maxTransX "<<maxTransX<<" minTransX "<< minTransX<<" ProtonSpotPB_stepSize "<<ProtonSpotPB_stepSize<<" dimensionX "<<XDim<<endl;
//    
////        cout<<"Final Evaluating Spot at(cube coords): x: "<<xs+xSource<<" y: "<<ys+ySource<<" z: "<<zs+zSource<<endl;
////        cout<<"Final DensGridHit CubeCoords x "<<xParticlePosCube<<" y "<<yParticlePosCube<<" z "<<zParticlePosCube<<endl;
////        cout<<"Final coords at: ix "<<ix<<" iy "<<iy<<" iz "<<iz<<endl;
//        
//        cout<<"End at density grid border: ix: "<<ix<<" iy "<<iy<<" iz "<<iz<<endl;
//        
////        cout<<"Start at density grid border (cube coord)x: "<<xParticlePosCube<<" y "<<yParticlePosCube<<" z "<<zParticlePosCube<<endl;
////        cout<<"Start at Contour grid border (cube coord)x: "<<xStartTarget<<" y "<<yStartTarget<<" z "<<zStartTarget<<endl;
////        cout<<"End at Contour grid border (cube coord)x: "<<xEndTarget<<" y "<<yEndTarget<<" z "<<zEndTarget<<endl;
//        }
                      
        //calculate normalized density distances
        densityDensGridToContour/=numbVoxelDensGridToContour;
        densityContourToTarget/=numbVoxelContourToTarget;
        densityInTarget/=numbVoxelInTarget;
        
        double densityAir=1.20E-03;
        
//        cout<<"densitySum " <<densitySum<<" numbOfVoxels "<<numbOfDensVoxels<<endl;
//        densitySum=1.;
        //calculate distance from beam source until density cube hit
	double tempx,tempy,tempz,tempDistance;
	tempx=fabs( xStartDensGrid-initParticlePosCubeX );
	tempy=fabs( yStartDensGrid-initParticlePosCubeY );
	tempz=fabs( zStartDensGrid-initParticlePosCubeZ );
//        cout<<"DensGrid Vector x: "<<tempx <<" y: "<<tempy <<" z: "<<tempz << endl;
        //not rescaled, as absolute distance source to surface is needed
	distanceToDensGrid=sqrt(tempx*tempx+tempy*tempy+tempz*tempz);
        
        //calculate distance from beam source until contour
        tempx=fabs( xStartContour-initParticlePosCubeX );
	tempy=fabs( yStartContour-initParticlePosCubeY );
	tempz=fabs( zStartContour-initParticlePosCubeZ );
//        cout<<"DensGrid Vector x: "<<tempx <<" y: "<<tempy <<" z: "<<tempz << endl;
        tempDistance=sqrt(tempx*tempx+tempy*tempy+tempz*tempz);
        //calculate distance and rescale to water equivalent depth using depth
	double distanceSourceToContourWEQ=(tempDistance-distanceToDensGrid)*densityDensGridToContour + distanceToDensGrid*densityAir;
        distanceSourceToContour=tempDistance;
        
        
        //calculate distance from beam source until start of target
	tempx=fabs( xStartTarget-initParticlePosCubeX );
	tempy=fabs( yStartTarget-initParticlePosCubeY );
	tempz=fabs( zStartTarget-initParticlePosCubeZ );
        tempDistance=sqrt(tempx*tempx+tempy*tempy+tempz*tempz);
        //apply simple water equivalent path length conversion
        distanceSourceToTargetStartWEQ = (tempDistance-distanceSourceToContour)*densityContourToTarget + distanceSourceToContourWEQ; 
        double distanceSourceToTargetStart=tempDistance;
        
        
        //calculate distance from beam source until start of target
	tempx=fabs( xEndTarget-initParticlePosCubeX );
	tempy=fabs( yEndTarget-initParticlePosCubeY );
	tempz=fabs( zEndTarget-initParticlePosCubeZ );
        tempDistance=sqrt(tempx*tempx+tempy*tempy+tempz*tempz);
//        cout<<"XEnd Vector x: "<<tempx <<" y: "<<tempy <<" z: "<<tempz << endl;

	distanceSourceToTargetStopWEQ=(tempDistance-distanceSourceToTargetStart)*densityInTarget + distanceSourceToTargetStartWEQ;  
//        double distanceSourceToTargetStop = tempDistance;
       
//        if(bHitTarget==true)
//        {
//        cout<<"DensGrid Vector x: "<<tempx <<" y: "<<tempy <<" z: "<<tempz << endl;
//          cout<<"Source-DensGrid distance: "<<distanceToDensGrid <<" DensGrid-Contour distance: "<<distanceSourceToContour-distanceToDensGrid <<endl;
//          cout<<"Contour-Target distance: "<<distanceSourceToTargetStart-distanceSourceToContour<<"TargetStart-TargetEnd distance: "<<distanceSourceToTargetStop-distanceSourceToTargetStart <<endl;
//          cout<<"Source-TargetStartWEQ distance: "<<distanceSourceToTargetStartWEQ <<"Source-TargetStopWEQ distance: "<<distanceSourceToTargetStopWEQ <<endl;
//          cout<<"Target size: "<<distanceSourceToTargetStopWEQ-distanceSourceToTargetStartWEQ<<" Target size WEQ: "<< distanceSourceToTargetStop-distanceSourceToTargetStart<<endl;

//        cout<<"distance to density grid: "<<distanceToDensGrid <<" distance surface to target start: "<<distanceSurfaceToTargetStart <<" distance surface to target end: "<<distanceSurfaceToTargetStop <<" density correction to WaterEQ: "<<densitySum<<endl;
//        cout<<"XStart Vector x: "<<initParticlePosCubeX <<" y: "<<initParticlePosCubeY <<" z: "<<initParticlePosCubeZ << endl;
//        cout<<"XEnd Vector x: "<<tempx <<" y: "<<tempy <<" z: "<<tempz << endl;
//        cout<<"**size of contour: "<<distanceSurfaceToTargetStop-distanceSurfaceToTargetStart<<endl;
//        }

//        if( bHitTarget==false )
//            cout<<"WARNING: Target not found!!!"<<endl;
//        else {
//            cout<<"GOOD: Target found!!!"<<endl;}

return bHitTarget;
}

inline void ProtonSpotPB::getNormVectorsForVector(double vecX,double vecY, double vecZ,
                                        double& nv1x,double& nv1y,double& nv1z,
                                        double& nv2x,double& nv2y,double& nv2z)
{
    
    nv1x=nv1y=nv1z=nv2x=nv2y=nv2z=0.0;
        //determine which element is the smallest
        //replace with 1 and other elements with 0
        //do crossproduct of both vectors, the result is an orthogonal vector
        if (vecX<= vecY && vecX<=vecZ)   vectorCrossProduct(vecX, vecY, vecZ,
                                            1.,0., 0.,
                                            nv1x,nv1y,nv1z);   
        else if (vecY<= vecX && vecY<=vecZ)   vectorCrossProduct(vecX, vecY, vecZ,
                                            0.,1., 0.,
                                            nv1x,nv1y,nv1z); 
        else if (vecZ<= vecX && vecZ<=vecY)   vectorCrossProduct(vecX, vecY, vecZ,
                                            0.,0., 1.,
                                            nv1x,nv1y,nv1z); 
        
        //calculate second orthogonal vector
        vectorCrossProduct(vecX, vecY, vecZ,
                            nv1x,nv1y, nv1z,
                            nv2x,nv2y,nv2z);
        
     //normalize vectors   
        double norm;
        norm=1./sqrt(nv1x*nv1x + nv1y*nv1y + nv1z*nv1z);
        nv1x*=norm;
        nv1y*=norm;
        nv1z*=norm;
        norm=1./sqrt(nv2x*nv2x + nv2y*nv2y + nv2z*nv2z);
        nv2x*=norm;
        nv2y*=norm;
        nv2z*=norm;

    return; 
}


inline void ProtonSpotPB::vectorCrossProduct(double ax, double ay, double az,
                                        double bx, double by, double bz,
                                        double& resX,double& resY,double& resZ)
{
    resX=ay*bz - az*by;
    resY=az*bx - ax*bz;
    resZ=ax*by - ay*bx;        
    return;
}


inline void ProtonSpotPB::convertHyperionToVoxelCoordinates(double dX, double dY, double dZ,
                                int& iX, int& iY, int& iZ) const
{
 if(dX>0.)    iX = static_cast<int>( floor(dX/gridStepSize) );
 else iX = static_cast<int>( ceil(dX/gridStepSize) );
 if(dY>0.)    iY = static_cast<int>( floor(dY/gridStepSize) );
 else iY = static_cast<int>( ceil(dY/gridStepSize) );
 if(dZ>0.)    iZ = static_cast<int>( floor(dZ/gridStepSize) );
 else iZ = static_cast<int>( ceil(dZ/gridStepSize) );

 return;
}


void ProtonSpotPB::getGridEntryAndExitPoints(const DoseAtomSpec& bx,double beamX0,double beamY0,double beamZ0,
                                double& entryX,double& entryY,double& entryZ,
                                double& exitX,double& exitY,double& exitZ)
{
//    //determine distance to grid
//    double distanceToGrid;
//    getDistanceToDensGridIfHit(bx, beamX0,beamY0, beamZ0, distanceToGrid);
    
    //get start point
    double hypX, hypY, hypZ;
    int iHypX,iHypY,iHypZ;
    transformBeamCoordIntoCubeCoord(bx,beamX0,beamY0,beamZ0, hypX, hypY, hypZ);
    convertHyperionToVoxelCoordinates(hypX, hypY, hypZ,
                                iHypX,iHypY,iHypZ);
    double nRvX,nRvY,nRvZ,nxDirVecX, nxDirVecY, nxDirVecZ,nyDirVecX, nyDirVecY, nyDirVecZ;
    getNormVectorsOfBeamInHyperion(bx, nRvX, nRvY, nRvZ ,
                                     nxDirVecX, nxDirVecY, nxDirVecZ,
                                     nyDirVecX, nyDirVecY, nyDirVecZ );
    
    //entry point
    entryX=hypX;
    entryY=hypY;
    entryZ=hypZ;
    
    //get point at other side of patient
    
    //start point at other side
    //Source isocenter distance
    double sidcm = bx.getSID();//* 0.1;
    double xParticlePosCube,yParticlePosCube,zParticlePosCube;
    xParticlePosCube=entryX+2.*sidcm*nRvX;
    yParticlePosCube=entryY+2.*sidcm*nRvY;
    zParticlePosCube=entryZ+2.*sidcm*nRvZ;
    double xDir,yDir,zDir;
    xDir=-nRvX;
    yDir=-nRvY;
    zDir=-nRvZ;
    
    // trace to cube !!
	// find penetration point of central ray axis
	// transport particle to cube: rotate and trace
	//loop ends when first point in density grid was found
	//necessary, because density grid does not fill the cube coordinate system,
	//only parts of it
	double minTransX,minTransY,minTransZ;
	minTransX=minTransY=minTransZ=gridStepSize*0.49;
	double maxTransX = gridStepSize * (XDim-0.51);
  	double maxTransY = gridStepSize * (YDim-0.51);
  	double maxTransZ = gridStepSize * (ZDim-0.51);
//cout<<"DoseCompGridVoxel Size"<<m_pDoseCompGrid->getVoxelSize()<<endl;        
//cout<<"BEFORE xParticlePosCube "<<xParticlePosCube<<" yParticlePosCube "<<yParticlePosCube<<" zParticlePosCube"<<zParticlePosCube<<endl;
	bool bMaxDirNonNil = false;
	double fDirX = fabs( xDir );
	double fDirY = fabs( yDir );
	double fDirZ = fabs( zDir );

	do {  
	  double trans = 0.0;
	  if((fDirX >= fDirY) && (fDirX >= fDirZ) && (fDirX != 0.0)) {
	    if(xParticlePosCube < minTransX) 
	      trans = (xParticlePosCube + minTransX) / xDir;
	    else if ( xParticlePosCube > maxTransX )
	      trans = (xParticlePosCube - maxTransX) / xDir;
	    bMaxDirNonNil = true;
	    fDirX = 0.0;
	  }
	  else if((fDirY >= fDirZ) && (fDirY >= fDirX) && (fDirY != 0.0)) {
	    if(yParticlePosCube < minTransY) 
	      trans = (yParticlePosCube + minTransY) / yDir;
	    else if ( yParticlePosCube > maxTransY )
	      trans = (yParticlePosCube - maxTransY) / yDir;
	    bMaxDirNonNil = true;
	    fDirY = 0.0;
	  }
	  else if((fDirZ >= fDirX) && (fDirZ >= fDirY) && (fDirZ != 0.0)) {
	    if(zParticlePosCube < minTransZ) 
	      trans = (zParticlePosCube + minTransZ) / zDir;
	    else if ( zParticlePosCube > maxTransZ )
	      trans = (zParticlePosCube - maxTransZ) / zDir;
	    bMaxDirNonNil = true;
	    fDirZ = 0.0;
	  }
	  else {
	    bMaxDirNonNil = false;
	    break;
	  }
//          cout<<" xDir "<<xDir<<" yDir "<<yDir<<" zDir "<<zDir<<endl;
//          cout<<" fDirX "<<fDirX<<" fDirY "<<fDirY<<" fDirZ "<<fDirZ<<endl;
//          cout<<" trans "<<trans<<endl;
//          cout<< "xParticlePosCube "<<xParticlePosCube<<endl;
	  // if endpoint is in the cube break.
	   xParticlePosCube -= ( trans*xDir );
	   yParticlePosCube -= ( trans*yDir );
	   zParticlePosCube -= ( trans*zDir );
//cout<<" xParticlePosCube "<<xParticlePosCube<<" yParticlePosCube "<<yParticlePosCube<<" zParticlePosCube"<<zParticlePosCube<<endl;
//cout<<"ix "<<ix<<" iy "<<iy<<" iz "<<iz<<endl;
	  if( (xParticlePosCube*conversionMMtoVoxel < XDim) && (xParticlePosCube >= 0) &&
	      (yParticlePosCube*conversionMMtoVoxel < YDim) && (yParticlePosCube >= 0) &&
	      (zParticlePosCube*conversionMMtoVoxel < ZDim) && (zParticlePosCube >= 0))
	    break;
	} while(bMaxDirNonNil == true); 
        
        exitX=xParticlePosCube;
        exitY=yParticlePosCube;
        exitZ=zParticlePosCube;
        
//        double tempx=exitX-entryX;
//        double tempy=exitY-entryY;
//        double tempz=exitZ-entryZ;
//        double distanceEntryExit=sqrt( tempx*tempx + tempy*tempy + tempz*tempz);
 return;
}

//############################################################################################################

inline const Grid3D<float>& ProtonSpotPB::getDoseGrid() const
{
  return *m_pDoseGrid;
}

//converts CT number to Hounsfield units
//By default, Rescale Intercept (Offset) is -1024
//and Rescale Slope (slope) is 1
//both defined in DICOM header (not read here, just default assumed)
inline double ProtonSpotPB::convertCTtoHU(double CTnumber) const
{
   return CTnumber*1. - 1024.;
//   if (CTnumber<500.) return -1024.;
//   else return 0;
}


   
}//Namespace UKT

//------------------------------------------------------------------------------------------------
//------------------------INCLUDE FUNCTIONS AND CLASS FUNCTIONS
//------------------------------------------------------------------------------------------------
#include "includes/LUT_class.cpp"
#include "includes/beamDecomposition_class.cpp"
#include "includes/input_class.cpp"
#include "includes/nucLUT_class.cpp"
#include "includes/interpol.cpp"
