/* 
 * File:   protonbeam_writeSpotsClass.h
 * Author: hfuchs
 *
 * Created on December 17, 2012, 12:29 PM
 */

#ifndef PROTONBEAM_WRITESPOTSCLASS_H
#define	PROTONBEAM_WRITESPOTSCLASS_H

#include <vector>
#include <iostream>
#include <fstream>
#include <sys/stat.h>



class writeSpotsClass {
public:
    writeSpotsClass(const double& initCouchAngle, const double& initGantryAngle, const double& initSpotEnergy, const double& initSpotWeight,
                    const double& initSpotPosX, const double& initSpotPosY, const double& initSpotPosZ );
    writeSpotsClass();
//    writeSpotsClass(const writeSpotsClass& orig);
//    ~writeSpotsClass();

public:
double setCouchAngle(double& initCouchAngle);
double setGantryAngle(double& initGantryAngle);
double setSpotEnergy(double& initSpotEnergy);
double setSpotWeight(double& initSpotWeight);
double setPosX(double& initPosX);
double setPosY(double& initPosY);
double setPosZ(double& initPosZ);


double getCouchAngle();
double getGantryAngle();
double getSpotEnergy( );
double getSpotWeight( );
double getPosX( );
double getPosY( );
double getPosZ( );
    
friend int CompareByCouchGantryAndEnergy (const writeSpotsClass& lhs, const writeSpotsClass& rhs);
private:
double couchAngle,gantryAngle, spotEnergy, spotWeight, spotPosX, spotPosY, spotPosZ;    
 

};

int CompareByCouchGantryAndEnergy (const writeSpotsClass& lhs, const writeSpotsClass& rhs);
int countGantryAngles(std::vector<writeSpotsClass>& beamSpots);
int countCouchAngles(std::vector<writeSpotsClass>& beamSpots);
int countFields(std::vector<writeSpotsClass>& beamSpots);
double countTotalBeamWeight(std::vector<writeSpotsClass>& beamSpots);
double countFieldWeight(std::vector<writeSpotsClass>& beamSpots, double gantryAngle, double couchAngle);
double countFieldWeightPerEnergy(std::vector<writeSpotsClass>& beamSpots, double gantryAngle, double couchAngle, double energy);
int countEnergiesPerField(std::vector<writeSpotsClass>& beamSpots, double gantryAngle, double couchAngle);
int countSpotsPerEnergyPerField(std::vector<writeSpotsClass>& beamSpots, double gantryAngle, double couchAngle, double energy);
double convertRadToDeg(double rad);
bool fileExists(const std::string& filename);
//bool fileExists(const char *fileName);

#endif	/* PROTONBEAM_WRITESPOTSCLASS_H */

