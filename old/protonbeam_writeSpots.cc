
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <math.h>
#include <vector>
#include <list>
#include <string>
#include <sstream>

#include <assert.h>

//#include <protonbeam.h>
//#include <TemplateValueConst.h>
//#include <beamSUB.h>
#include <ProtonSpotGeometry.h>
#include <Grid3D.h>
//#include <BeamPatientModelInterface.h>

#include <protonbeam_writeSpotsClass.h>


using namespace std;

namespace Ukt
{


//##################################################################################################
void writeSpots(const Grid3D<int>* pIndextab, 
		const vector<const ProtonSpotGeometry *>& vPSG, 
		const vector<double>& vSpotWeights)
{
    
//    cout<<"wS pIndextab->getWidth() "<<pIndextab->getWidth()<<" pIndextab->getHeight() "<<pIndextab->getHeight()<<endl;
//    cout<<"wS pIndextab->getDepth() "<<pIndextab->getDepth()<<endl;
//    cout<<"wS total: "<<pIndextab->getWidth()*pIndextab->getHeight()*pIndextab->getDepth()<<endl;
// 
    vector<writeSpotsClass> beamSpots;

    int i=0;
  for(int nAtX = 0; nAtX< pIndextab->getWidth(); ++nAtX) {
    for(int nAtY = 0; nAtY<pIndextab->getHeight(); ++nAtY) {
      for(int nAtE = 0; nAtE<pIndextab->getDepth(); ++nAtE) { 
          
	int ndx = (*pIndextab)(nAtX, nAtY, nAtE);
	if(ndx < 0)
	  continue;
          
//        cout<<"nAtX "<<nAtX<<" nAtY "<<nAtY<<" nAtE "<<nAtE<<endl;
//	cout<<"loop nr: "<<i<<endl;
//        i++;
        
        const ProtonSpotGeometry* pTmp = vPSG[ndx];
	const double SpotWeight        = vSpotWeights[ndx];
        
        
        const double test=SpotWeight;
//	 write out stuff to taste here
  
        beamSpots.push_back (
                writeSpotsClass(  pTmp->getSpotTheta(),pTmp->getSpotPhi(),pTmp->getEnergy(),test,pTmp->getXPosition(),pTmp->getYPosition(),0.0  )     
        );


      }
    }
  }
  
//  //sort data
   sort (beamSpots.begin(), beamSpots.end(), CompareByCouchGantryAndEnergy);
   int numberOfFields=countFields(beamSpots);
   double totalBeamWeight=countTotalBeamWeight(beamSpots);

   
//  
  //write data to disk   
   
//   stringstream ss;//create a stringstream
//   ss << totalBeams;//add number to the stream
   
   string filename,name,ending;
   filename = "beamDataOutput.dat";
   name="beamDataOutput";
   ending=".dat";
   filename.assign(name);
   filename.append(ending);
   
   cout<<"orig filename: "<<filename<<endl;
   if( fileExists(filename) )
   {
       cout<<"existing filename: "<<filename<<endl;
       int number=1;
       while(fileExists(filename))
       {
           cout<<"file exists: "<<filename;
           stringstream ss;
           ss << number;
           
           filename.assign(name);
           filename.append(ss.str());
           filename.append(ending);
           number++;
           cout<<" trying: "<<filename<<endl;
       }
       
       
       
   }
//   filename.append(ss.str());
   
   
   
    ofstream datafile(filename.c_str());
   if(!datafile.good()) {
     std::cerr << "Could not open beam data output file "<<filename<<"\n";
     return ;
   }
    //Header
//    datafile<<"gantry angles: "<<countGantryAngles(beamSpots)<<endl;
//    datafile<<"couch angles: "<<countCouchAngles(beamSpots)<<endl;
//    for(int i=0;i<beamSpots.size();i++)
//    {
//        datafile<<"spot "<<i<<" gantry "<<beamSpots.at(i).getGantryAngle()<<" couch "<<beamSpots.at(i).getCouchAngle()<<endl;
//    }
    
    datafile<<"#TREATMENT_PLAN_DESCRIPTION"<<endl;
    datafile<<"#PlanName"<<endl;
    datafile<<"HyperionPBDataOutput"<<endl;
    datafile<<"#NumberOfFractions"<<endl;
    datafile<<"1"<<endl;
    
    datafile<<"##FractionID"<<endl;
    datafile<<"1"<<endl;
    
    datafile<<"#NumberOfFields"<<endl;
    datafile<<numberOfFields<<endl;
    for(i=1;i<=numberOfFields;i++)
    {
      datafile<<"###FieldsID"<<endl;  
      datafile<<i<<endl;  
    }
    datafile<<"#TotalMetersetWeightOfAllFields"<<endl;  
    datafile<<totalBeamWeight<<endl; 
    datafile<<endl;
    
//individual fields
    
    datafile<<"#FIELD-DESCRIPTION"<<endl;
    datafile<<"###FieldID"<<endl;
    datafile<<"1"<<endl;
    datafile<<"###FinalCumulativeMeterSetWeight"<<endl;
    datafile<<countFieldWeight(beamSpots, beamSpots.at(0).getGantryAngle(), beamSpots.at(0).getCouchAngle() )<<endl;
    datafile<<"###GantryAngle"<<endl;
    datafile<<convertRadToDeg( beamSpots.at(0).getGantryAngle() )<<endl;
    datafile<<"###PatientSupportAngle"<<endl;
    datafile<<convertRadToDeg( beamSpots.at(0).getCouchAngle() )<<endl;
    datafile<<"###IsocenterPosition"<<endl;
    datafile<<"0 0 0"<<endl;
 int energiesPerField=countEnergiesPerField( beamSpots, beamSpots.at(0).getGantryAngle(), beamSpots.at(0).getCouchAngle() );
    datafile<<"###NumberOfControlPoints"<<endl;
    datafile<<energiesPerField<<endl;
    datafile<<endl;
    
    datafile<<"#SPOTS_DESCRIPTION"<<endl;
    double cumulativeWeight=0.;
    int k=0;
    double energy=beamSpots.at(0).getSpotEnergy();
    for (unsigned int curSpot=0;curSpot<beamSpots.size();curSpot++)
    {
        if( energy!=beamSpots.at(curSpot).getSpotEnergy() || curSpot==0 )
        {
            datafile<<"####ControlPointIndex"<<endl;
            datafile<<k<<endl;
            datafile<<"####SpotTunnedID"<<endl;
            datafile<<"3.0"<<endl;
            datafile<<"####CummulativeMeterSetWeight"<<endl;
            datafile<<cumulativeWeight<<endl;    
            datafile<<"####Energy (MeV)"<<endl;
            datafile<<beamSpots.at(curSpot).getSpotEnergy()<<endl;
            datafile<<"####NbOfScannedSpots"<<endl;
            datafile<<countSpotsPerEnergyPerField(beamSpots, beamSpots.at(curSpot).getGantryAngle(), beamSpots.at(curSpot).getCouchAngle(), beamSpots.at(curSpot).getSpotEnergy()) <<endl;
            datafile<<"####X Y Weight"<<endl;
            k++;
        }
        datafile<<beamSpots.at(curSpot).getPosX()<<" "<<beamSpots.at(curSpot).getPosY()<<" "<<beamSpots.at(curSpot).getSpotWeight()<<endl;
        cumulativeWeight+=beamSpots.at(curSpot).getSpotWeight();
        
        energy=beamSpots.at(curSpot).getSpotEnergy();
    }
    
//    
//        datafile<<" energy weight phi theta XPos YPos Width Height Depth"<<endl;
    
//       datafile<<"test";
//       datafile<<endl;
       datafile.flush();
       datafile.close();  

  return;
}
    

   
// bool ProtonBeam::IM(const string &filename)
//   // output in MATHEMATICA readable file 
// {
//   int    k;
  
//   ofstream imfile(filename.c_str());
//   if(!imfile.good()) {
//     CERR << "Could not open IM profile file "<<filename<<"\n";
//     return false;
//   }
//   int nAtColumn, nAtRow, nAtShifter;
//   bool newformat=true;
//   if (newformat) //common output format
//   {
//       bool printenter=true; 
//       imfile << "#AtRow AtColumn AtShifter XPosition YPosition ShifterThickness Energy Weight Phi Theta" <<endl;
//       for(nAtRow=0; nAtRow<indextab->getWidth(); ++nAtRow) {
// 	  for(nAtColumn=0; nAtColumn<indextab->getHeight(); ++nAtColumn) {
// 	      printenter=false;
// 	      for(nAtShifter=0; nAtShifter < indextab->getDepth(); ++nAtShifter) { 
// 		  if((k = (*indextab)(nAtRow, nAtColumn, nAtShifter)) < 0) 
// 		  {
// 		      //  printenter=false; 
// 		  }
// 		  else {
// 		      if(bx[k]->belongs() == true) {
// 			  imfile << nAtRow <<"  "<<nAtColumn<<"  "<<nAtShifter<<"  ";
// 			  //	  auto_ptr<ProtonSpotGeometry> pRayGeo( static_cast<ProtonSpotGeometry*>(bx[k]->getRayGeometry().release()));
// 			  //	  imfile << pRayGeo->getXPosition() <<"  "<<pRayGeo->getYPosition()<<"  "<< pRayGeo->getShifterThickness()<<
// 			  //     "  "<< pRayGeo->getEnergy() << " " << bx[k]->getWeight() << "  "<< bx[k]->getPhi() << "  "<< bx[k]->getTheta()<<endl;
// 		    printenter=true; 
// 		      }
// 		      else {
// 			  //printenter=false; 
// 		      }
// 		  }
// 	      }
// 	      if(printenter) imfile <<endl;
// 	  }
// 	  imfile<<endl;
//       }
      
//       imfile.flush();
//       imfile.close();   
//   }
  
//   else //Mathematica output format
//   {
//   for(nAtRow=0; nAtRow<indextab->getWidth(); ++nAtRow) {
//     for(nAtColumn=0; nAtColumn<indextab->getHeight(); ++nAtColumn) {
//       imfile << nAtRow <<"  "<<nAtColumn<<"  ";
//       for(nAtShifter=0; nAtShifter < indextab->getDepth(); ++nAtShifter) {
// 	if((k = (*indextab)(nAtRow, nAtColumn, nAtShifter)) < 0) {
// 	  imfile << "0  ";
// 	}
// 	else {
// 	  if(bx[k]->belongs() == true) 
// 	    imfile << bx[k]->getWeight() <<"  ";
// 	  else
// 	    imfile <<"0  ";
// 	}
//       }
//       imfile <<endl;
//     }
//     imfile<<endl;
//   }
  
//   imfile.flush();
//   imfile.close();
  
//   // HJB
//   char str[256];
//   strcpy(str,filename.c_str());
//   char* pStr = strstr(str,".IMF");
//   strcpy(pStr,".SCAN");
  
//   ofstream scanfile(str);
  
//   for(nAtRow=0; nAtRow<indextab->getWidth(); ++nAtRow) {
//     for(nAtColumn=0; nAtColumn<indextab->getHeight(); ++nAtColumn) {
//       for(nAtShifter=0; nAtShifter < indextab->getDepth(); ++nAtShifter) { 
// 	scanfile << nAtRow <<"  "<<nAtColumn<<"  "<<nAtShifter<<"  ";
// 	if((k = (*indextab)(nAtRow, nAtColumn, nAtShifter)) < 0) {
// 	  scanfile << "0\n";
// 	}
// 	else {
// 	  if(bx[k]->belongs() == true) {
// 	    //	    auto_ptr<ProtonSpotGeometry> pRayGeo( static_cast<ProtonSpotGeometry*>(bx[k]->getRayGeometry().release()));
// 	    //scanfile << bx[k]->getWeight() << "  "<< bx[k]->getPhi() << "  "<< bx[k]->getTheta()<<"  "<< pRayGeo->getXPosition()
// 	    //	     <<"  "<<pRayGeo->getYPosition()<<"  "<< pRayGeo->getShifterThickness()<<
// 	    //  "  " <<bx[k]->getRayDetails()->m_mindepth<<"  "<< bx[k]->getRayDetails()->m_maxdepth<< endl;  // HJB
// 	  }
// 	  else
// 	    scanfile <<"0\n";
// 	}
//       }
//       scanfile <<endl;
//     }
//     scanfile<<endl;
//   }

//   scanfile.flush();
//   scanfile.close();
//   }


//   return true;
// }  

// void ProtonBeam::XIM(beam_display *beam)
//   // output to shell
// { 
//   unsigned int i;  
//   double maxx, maxy, maxw, minw;
  
//   maxx=maxy=0.0;
//   maxw=0.0;
//   minw=64000.0;
//   if(beam->n != static_cast<int>(bx.size())) {
//     beam->n = bx.size();
//     beam->x.resize(beam->n);
//     beam->y.resize(beam->n);
//     beam->braggX.resize(beam->n);
//     beam->braggY.resize(beam->n);
//     beam->braggZ.resize(beam->n);
//     beam->weight.resize(beam->n);
//   }
//   for(i=0; i<bx.size(); i++) {
//     beam->x[i] = bx[i]->getRayGeometry()->getXPosition();
//     beam->y[i] = bx[i]->getRayGeometry()->getYPosition(); 
//     beam->braggX[i] = bx[i]->getRayDetails()->m_braggXposition;
//     beam->braggY[i] = bx[i]->getRayDetails()->m_braggYposition; 
//     beam->braggZ[i] = bx[i]->getRayDetails()->m_braggZposition;
//     beam->weight[i] = (bx[i]->belongs()) ? bx[i]->getWeight() + bx[i]->getClusterWeight() : 0.0;
//     maxx = (fabs(beam->x[i]) > maxx) ? fabs(beam->x[i]) : maxx;
//     maxy = (fabs(beam->y[i]) > maxy) ? fabs(beam->y[i]) : maxy;
//     if(bx[i]->belongs()) {
//       maxw = (beam->weight[i] > maxw) ? beam->weight[i] : maxw;
//       minw = (beam->weight[i] < minw) ? beam->weight[i] : minw;
//     }
//   }
//   beam->max_x = maxx;
//   beam->max_y = maxy;
//   beam->max_weight = maxw;
//   beam->min_weight = minw;
//   // rectangle
//   beam->vertex_n = 4;
//   double dy = 0.5*m_DNA.getRayGeometry().second;
//   double dx = 0.5*m_DNA.getRayGeometry().first;     // 10 mm leafs!
//   beam->vertex_x[0] = -dx;  beam->vertex_y[0] = dy;
//   beam->vertex_x[1] = -dx;  beam->vertex_y[1] = -dy;
//   beam->vertex_x[2] = dx;   beam->vertex_y[2] = -dy;
//   beam->vertex_x[3] = dx;   beam->vertex_y[3] = dy;
//   beam->max_x += dx;
//   beam->max_y += dy;
//   return;
// }

}
