/* 
 * File:   ProtonSpotPB.h
 * Author: Hermann Fuchs
 *
 * Created on 07 February 2012, 14:33
 */

#ifndef ProtonSpotPB_H
#define	ProtonSpotPB_H

#define USEPROTONBEAM
//#define USEHELIUMBEAM
//USEPROTONBEAM switches to proton beam
//USEHELIUMBEAM switches to helium beam

#include <assert.h>
#include <vector>
#include <memory>

#include <Grid3D.h>
#include <SpotPB.h>

#include <BeamPatientTrafo.h>

#include <limits.h>
#include "headers/LUT_class.h"
#include "headers/beamDecomposition_class.h"
#include "headers/input_class.h"
#include "headers/nucLUT_class.h"
#include "headers/systemAndRootHeaders.h"
#include "headers/interpol.h"

namespace Ukt 
{
  
class DensityDoseCompGrid;
class DoseAtomSpec; 
class DoseMeta;
class XiPSData;

class ProtonSpotPB : public SpotPB 
{
private: 
    DensityDoseCompGrid*        m_pDoseCompGrid;
    Grid3D<float>*              m_pDoseGrid; 
    Grid3D<bool>*               m_pTargetGrid; 
    
    XiPSData*                   m_pData;
public:    
    LUT* lookUpTable;
    nucLUT* nucCorrTable;
    inputClass* myInputClass;

    int XDim;
    int YDim;
    int ZDim;
    double gridStepSize;
    //Isocenter location
    double m_IsoX;
    double m_IsoY;
    double m_IsoZ;

    double densGridWidth;
    double densGridHeight;
    double densGridDepth;
    double conversionMMtoVoxel;
        
    Double_t **outputArray;//not yet used
    Double_t **initFluenceArray;//not yet used
    Double_t **ProtonSpotPB_thisBeam_FluenceArray;
    
    Int_t ProtonSpotPB_numberOfSubBeamsOneDim;
   
    ProtonSpotPB(std::auto_ptr<DensityDoseCompGrid>, std::auto_ptr<Grid3D<bool> >, std::auto_ptr<XiPSData>); 

    ~ProtonSpotPB();
    
  DoseMeta*   calcDose(const DoseAtomSpec& bx);
  DoseMeta*   hitsTarget(const DoseAtomSpec& bx) const;
  
  //necessary due to class SpotPB
  inline const Grid3D<float>&  getDoseGrid() const;
  void                  setIsoCenter(double isoX, double isoY, double isoZ);   //in patient coordinates

    
//calculate the relativistic beta
//energy and Mass in MeV required
inline Double_t relativisticBeta(Double_t energy, Double_t mass) const;

////calculate the scattering angle
//// everything in SI units
////See Geant4 physics documentation
inline Double_t getScatteringAngleSquare(Double_t currentStepSize, Double_t energy, Double_t radiationLength);


//creates the fluence Array
//Gaussian distributed beam profile assumed
//x0 and y0 determine the position where the beam center is located
//beamInitSigma and beamInitSigma are the sigmas of the Gaussian distribution
//dimension is the dimension of the Array to be created
//stepSize size of one Step in cm
//initFluence number of Particles
//The Gauss function produces rounding errors close to sigma=0
//Therefore security checks have been implemented to avoid most of the damage
//void createFluenceArray(Double_t **fluenceArray,Double_t fluence,Double_t currentDepth,
//                                        Double_t currentBeamSigmaX, Double_t currentBeamSigmaY,
//                                        Int_t thisBeam_dimension,
//                                        Double_t thisBeam_x0, Double_t thisBeam_y0);

//loop through the scatAngleSquaredArray and quadratically sum up all contents with distance from current point
//defined through stepSize*maxIndex
//see Soukup Dissertation
inline Double_t getSummedScattAngle(Double_t *scatAngleSquaredArray, Int_t currentIndex);
inline double getSummedScattAngle(std::vector<double>& scatAngleSquaredVector,double stepSize);


//calculate the stoppingPowerRatioToWater in order to rescale the real depth to a depth in water
//gives ratio of stopping power to material versus stopping power to water
inline Double_t getStoppingPowerRatioToWater(Double_t stoppingPowerWater,Double_t stoppingPowerMaterial) const;

//Bethe-Bloch formula see Ziegler 1999
//energy, excitation energy and particle mass in MeV
//calculates the stopping power at the given energy for the given material
inline Double_t calculateStoppingPower(Double_t energy,Double_t atomicZARatio, Double_t excitationEnergy) const;


////decomposes the initial gauss distributed beam in subBeams
////spread is the maximum distance from the original beam axis in times of sigma 
Int_t beamDecomposition(const DoseAtomSpec& bx,Double_t **beamDataArray, Int_t numOfSubBeamsOneDim);


inline void transformBeamCoordIntoCubeCoord(const DoseAtomSpec& bx,double beamX,double beamY,double beamZ,double& cubeX,double& cubeY,double& cubeZ) const;

inline bool getDistanceToDensGridIfHit(const DoseAtomSpec& bx,double beamX,double beamY,double beamZ,double& distance) const;

inline bool getDistancesToDensAndTargetGridIfHit(const DoseAtomSpec& bx,double beamX,double beamY,double beamZ,
double& distanceToDensGrid,double& distanceSourceToTargetStartWEQ,double& distanceSourceToTargetStopWEQ,double& distanceSourceToContour) const;
bool calculateWEQtoTarget(const DoseAtomSpec& bx,double BeamX0,double BeamY0,double initEnergy,
        double& distanceToDensGrid, double& distanceSourceToTargetStartWEQ,double& distanceSourceToTargetStopWEQ) const;


inline Double_t getBeamShapeAtDistanceFromCenter(Double_t distanceX,Double_t distanceY,
                                                        Double_t currentBeamSigmaX, Double_t currentBeamSigmaY,
                                                        Double_t initialBeamSigmaX, Double_t initialBeamSigmaY,
                                                        Double_t GaussVoigtRelation,Double_t VoigtSigma,
                                                        Double_t VoigtLg,Double_t spacingBetweenPoints);


inline void vectorCrossProduct(double ax, double ay, double az,
                                        double bx, double by, double bz,
                                        double& resX,double& resY,double& resZ);
inline void getNormVectorsForVector(double vecX,double vecY, double vecZ,
                                        double& nv1x,double& nv1y,double& nv1z,
                                        double& nv2x,double& nv2y,double& nv2z);

inline double convertCTtoHU(double HUnit) const;

inline void convertHyperionToVoxelCoordinates(double dX, double dY, double dZ,
                                int& iX, int& iY, int& iZ) const;

inline void calcuateWEQandMSarrays(const DoseAtomSpec& bx,
                            std::vector<double>& realDepth,std::vector<double>& WEQDepth,std::vector<double>& MScurrentSquaredVariance,
                            bool& reachedBraggPeak, double& braggPeakDepthHyperion, double& maxDepthHyperion, double& distanceToContour,
                            double initEnergy,double distanceToGrid,double BeamX0, double BeamY0);
inline double getDoseAtVoxel(const DoseAtomSpec& bx,interpol* weqLUT,interpol* MSsquaredVarianceLUT, Double_t *beamDataArray, 
                                int iHypX,int iHypY, int iHypZ,
                                double distanceFromBeamX,double distanceFromBeamY,double distanceFromBeamZ);

inline void getNormVectorsOfBeamInHyperion(const DoseAtomSpec& bx,
                                    double& nRvX,double& nRvY,double& nRvZ ,
                                    double& nxDirVecX,double& nxDirVecY,double& nxDirVecZ,
                                    double& nyDirVecX,double& nyDirVecY,double& nyDirVecZ );
void doseCalcLoop(const DoseAtomSpec& bx,UktBase::BeamPatientTrafo& Trafo, 
        Double_t *beamDataArray,bool& reachedBraggPeak, double& dBPX,double& dBPY,double& dBPZ);
//inline void getDistanceFromBeamAndDepthAlongBeamAxis(const DoseAtomSpec& bx,double beamX0,double beamY0,double pointX,double pointY, double pointZ,
//                                                double& distanceFromBeamX, double& distanceFromBeamY,double& distanceFromBeamZ);

inline void getDistanceFromBeamAndDepthAlongBeamAxis(const DoseAtomSpec& bx,
        double beamX0,double beamY0,double pointX, double pointY, double pointZ,
        double startX,double startY, double startZ,
        double nRvX,double nRvY,double nRvZ ,
        double nxDirVecX,double nxDirVecY,double nxDirVecZ,
        double nyDirVecX,double nyDirVecY,double nyDirVecZ,        
        double& distanceFromBeamX, double& distanceFromBeamY,double& distanceFromBeamZ);

void getGridEntryAndExitPoints(const DoseAtomSpec& bx,double beamX0,double beamY0,double beamZ0,
                                double& entryX,double& entryY,double& entryZ,
                                double& exitX,double& exitY,double& exitZ);


  // not implemented:
//  ProtonSpotPB();
//  ProtonSpotPB(const ProtonSpotPB& );
//  ProtonSpotPB& operator=(const ProtonSpotPB& );

private:
    Double_t ProtonSpotPB_stepSize; 
    Double_t ProtonSpotPB_initFluence;
    Int_t ProtonSpotPB_lateralVoxelsToCalculate;//number of voxels in plus and minus direction to calculate


#ifdef USEPROTONBEAM
    //Proton  specific settings start           
    //mass of the particle used in the beam, given in MeV
    static const Double_t ProtonSpotPB_constParticleMass=938.272029;////in MeV;
    static const Double_t ProtonSpotPB_constParticleCharge=1;
    static const Double_t ProtonSpotPB_constParticleMassNumber=1;
#endif	/* USEPROTONBEAM */


#ifdef USEHELIUMBEAM
    //Helium Ions specific settings start           
    //mass of the particle used in the beam, given in MeV
    static const Double_t ProtonSpotPB_constParticleMass=3727.379109;////in MeV;
    static const Double_t ProtonSpotPB_constParticleCharge=2;
    static const Double_t ProtonSpotPB_constParticleMassNumber=4;
    //Helium Ions specific settings end
#endif	/* USEHELIUMBEAM */
    
    
    static const Double_t ProtonSpotPB_constMassElectron=0.510998918;//in MeV/c^2
    static const Double_t ProtonSpotPB_constExcitationEnergyWater=0.000075;//in MeV
    static const Double_t ProtonSpotPB_constRadiationLengthWater=36.08;//in cm
    static const Double_t ProtonSpotPB_constZARatioWater=0.55509;

    //dielectric coefficient
    static const Double_t ProtonSpotPB_constDiel=8.854187817e-12;//in (A*s)/(Wm)
    //speed of light
    static const Double_t ProtonSpotPB_constLightSpeed=299792458;//in m/s
    //elementary charge
    static const Double_t ProtonSpotPB_constQe=1.60217653e-19;//in Coulomb
    //PI
    static const Double_t ProtonSpotPB_constPI=3.141592653589793238;
    //Euler constant
    static const Double_t ProtonSpotPB_constEuler=2.718281828459045235;
    //1 MeV is 1.602x10^-13 Joule
    static const Double_t ProtonSpotPB_constMeVToJoule=1.602e-13;
    
};//end of class

}//namespace UKT
#endif	/* ProtonSpotPB_H */

