/* 
 * File:   testbench.cpp
 * Author: hfuchs
 *
 * Created on December 28, 2012, 8:53 AM
 */

#include "testbenchRBE.h"


#include <cstdlib>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <iomanip>

#include <math.h>
#include <string>
#include <vector>
#include <assert.h>


using namespace std;

/*
 * 
 */
int main(int argc, char** argv) 
{

   string filename;
   
#ifdef USEPROTONBEAM
   filename = "RbeParameters.dat";      
#endif	/* USEPROTONBEAM */
#ifdef USEHELIUMBEAM
   filename = "RbeParameters.dat";      
#endif	/* USEHELIUMBEAM */
 #ifdef USECARBONBEAM
   filename = "StoppingPowerCarbon.dat";      
#endif	/* USECARBONBEAM */
   
   
    LUT* lookUpTable;
    
    lookUpTable= new LUT;
    lookUpTable->setDimension(2000);//set dimension of read data file

   
   #ifdef USEHELIUMBEAM
//    lookUpTable->setFolderName((char *)"calibrationHelium/"); 
//    nucCorrTable->setFolderName((char *)"calibrationHelium/"); 
//    myInputClass->setDataFileName((char*)"calibrationHelium/myHUDataFile.dat");
    
//    lookUpTable->setFolderName((char *)"LUTHelium4MeVSmeared/");    
//    lookUpTable->setFolderName((char *)"XiOLUTHelium/");  
//
//    lookUpTable->setFolderName((char *)"calibrationHeliumEnergGaussSmeared3MeV/"); 
//    nucCorrTable->setFolderName((char *)"calibrationHeliumEnergGaussSmeared3MeV/");
//    myInputClass->setDataFileName((char*)"calibrationHeliumEnergGaussSmeared3MeV/myHUDataFile.dat");
    
        lookUpTable->setFolderName((char *)"LUTHelium_1_5MeV_10102013/"); 
//    nucCorrTable->setFolderName((char *)"LUTHelium_1_5MeV_10102013/");
//    myInputClass->setDataFileName((char*)"LUTHelium_1_5MeV_10102013/myHUDataFile.dat");
    
//    lookUpTable->setFolderName((char *)"calibrationHeliumEnergGaussSmeared3MeV/"); 
//    nucCorrTable->setFolderName((char *)"calibrationHeliumEnergGaussSmeared3MeV/");
//    myInputClass->setDataFileName((char*)"calibrationHeliumEnergGaussSmeared3MeV/myHUDataFile.dat");
    ProtonSpotPB_initFluence=100000000;//set initial beam fluence
    
#endif	/* USEHELIUMBEAM */
#ifdef USEPROTONBEAM
//    lookUpTable->setFolderName((char *)"calibrationProton/"); 
//    nucCorrTable->setFolderName((char *)"calibrationProton/");
//    myInputClass->setDataFileName((char*)"calibrationProton/myHUDataFile.dat");
    
//    lookUpTable->setFolderName((char *)"calibrationProtonEnergGaussSmeared3MeV/"); 
//    nucCorrTable->setFolderName((char *)"calibrationProtonEnergGaussSmeared3MeV/");  
//    myInputClass->setDataFileName((char*)"calibrationProtonEnergGaussSmeared3MeV/myHUDataFile.dat");


//    lookUpTable->setFolderName((char *)"LUTProton4MeVSmeared/"); 
//    lookUpTable->setFolderName((char *)"XiOLUTProton/"); 
    lookUpTable->setFolderName((char *)"LUTProton_1_5MeV_10102013/"); 
    ProtonSpotPB_initFluence=100000000;//set initial beam fluence

#endif	/* USEPROTONBEAM */
    lookUpTable->setDataFilePrefix((char *)"OneDim_Edep_");
    lookUpTable->setConfigFileName((char *)"dataAvailable.dat");

    lookUpTable->setStepSizeDataInCM(0.05);//step size of the interpolation data files in cm
    lookUpTable->setStepSizeCalculation(0.1);//in cm
    lookUpTable->setNumbOfParticlesInBeam(1);//set to one due to scaling with multiple beams only necessary with LUT
    lookUpTable->calculateTotalDepositedEnergy();
    lookUpTable->fillPenetrationDepthArray();
   
         cout<<"lut set data"<<endl;

      ofstream datafile(filename.c_str());
   if(!datafile.good()) {
     std::cerr << "Could not open beam data output file "<<filename<<"\n";
     return 0;
   }
      
            
      
    //lookUpTable->setTargetEnergy(163.526);
//lookUpTable->setTargetEnergy(150.0);

    Double_t currEnergy;  
    Double_t stoppingPowerWater=0.;
    
    datafile<<"# Energy(MeV) amplitude offset scale"<<endl;
    datafile<<"# rbe=1.1 + amplitude/ ( 1+ exp(offset- scale*depth[cm]))"<<endl;
        
     double inDepthEdepDeposition;
     double maxEdep=lookUpTable->getMaxDepositedEnergy(); ;     
//     double rbeMuliplikator=1.1;
     double depth=0.;

     
     cout<<"starting loop"<<endl;

     double energy=30;
      for(energy=30;energy<=250;energy+=10)
      {
     lookUpTable->setTargetEnergy(energy);
     
     cout<<"energy: "<<energy<<endl;
          
     optimizerRBE *optimizeRBEcurve;     
     std::vector<Double_t> outputRBEParam(2);
      optimizeRBEcurve= new optimizerRBE(lookUpTable);
      optimizeRBEcurve->setBeamEnergy(energy);
      optimizeRBEcurve->optimizeSOBP(outputRBEParam);
     delete optimizeRBEcurve; 

          
  
//        rbeMuliplikator=1.1 + 0.3/ ( 1+ exp(outputRBEParam.at(0)-outputRBEParam.at(1)*depth/10.));
        
        datafile<<setiosflags(ios::fixed) << setprecision(5)<<
               energy<<" "<<
               0.6<< " "<<
               outputRBEParam.at(0)<<" "<<
               outputRBEParam.at(1)<< 
               endl;   
        cout<<setiosflags(ios::fixed) << setprecision(5)<<
               energy<<" "<<
               0.6<< " "<<
               outputRBEParam.at(0)<<" "<<
               outputRBEParam.at(1)<< 
               endl;
              }
       
    
//    for(currEnergy=startEnergy; currEnergy>=0; currEnergy=currEnergy-energyInterval)
//    {
////      cout<<  currEnergy<<endl;
//      
//       stoppingPowerWater= calculateStoppingPower(currEnergy, ProtonSpotPB_constZARatioWater, ProtonSpotPB_constExcitationEnergyWater);
//       
//       
//       datafile<<setiosflags(ios::fixed) << setprecision(2)<<
//               currEnergy<<" "<<stoppingPowerWater<<" "<<
//               currEnergy/ProtonSpotPB_constParticleMassNumber<<" "<<stoppingPowerWater/ProtonSpotPB_constParticleMassNumber<<
//               endl;       
//    }  
    datafile.flush();
    datafile.close();    
       
    return 0;
}




//calculate the relativistic beta
//energy and Mass in MeV required
 Double_t  relativisticBeta(Double_t energy, Double_t mass) 
{
    //protonRestMass = 938.272013;////in MeV
    if (energy<0) energy=0;
    if (mass<0) mass=0;
    return sqrt(1 - pow( mass/(mass+energy) ,2) );
    //alternative version, same result
    //return  sqrt( 1 - 1/pow(1+ energy/(protonRestMass) ,2) );
}


////calculate the scattering angle
//// everything in SI units
////See Geant4 physics documentation
 Double_t  getScatteringAngleSquare(Double_t currentStepSize, Double_t energy, Double_t radiationLength)
{
        //Highland-Lynch-Dahl formula
	Double_t constC1, constC2, relatBeta, scatAngle,momentum;

	constC1=13.6; ////in MeV
	constC2=0.038;//// in MeV

        energy=energy*ProtonSpotPB_constParticleMassNumber;//passed Energy is per nucleon, formula works for whole energy
	relatBeta=relativisticBeta(energy, ProtonSpotPB_constParticleMass);
        momentum=ProtonSpotPB_constParticleMass*relatBeta/ProtonSpotPB_constLightSpeed;
       
	if (relatBeta<=0 || currentStepSize<=0) scatAngle=0;
	else scatAngle = constC1/(relatBeta*momentum*ProtonSpotPB_constLightSpeed) * ProtonSpotPB_constParticleCharge *  sqrt( currentStepSize / radiationLength ) * (  1 + constC2 * log(currentStepSize / radiationLength)   );
        ////log() computes natural logarithm in C++
	return pow(scatAngle,2);//necessary to return squared scattering angle	
}


 Double_t  getBeamShapeAtDistanceFromCenter(Double_t distanceX,Double_t distanceY,
                                                        Double_t currentBeamSigmaX, Double_t currentBeamSigmaY,
                                                        Double_t initialBeamSigmaX, Double_t initialBeamSigmaY,
                                                        Double_t GaussVoigtRelation,Double_t VoigtSigma,
                                                        Double_t VoigtLg,Double_t spacingBetweenPoints)
{
    //Gauss and Voigt function are centered around zero.
    //using +/- distance from beam center, normalized beam shape value at this position is returned
    
//    Double_t GaussVoigtRelation=nucCorrTable->getGaussVoigtRelation(currentDepth);
//    Double_t VoigtSigma=nucCorrTable->getVoigtSigma(currentDepth);
//    Double_t VoigtLg=nucCorrTable->getVoigtLg(currentDepth);
//    Double_t spacingBetweenPoints=ProtonSpotPB_stepSize;
//    //Adapting nuclear correction LUT beam diameter to current diameter
//            nucCorrTable->setBeamSigma(ProtonSpotPB_beamInitSigmaY);
    
    Double_t xg,yg,xvoigt,yvoigt,xResult,yResult,finalResult;
    xg=yg=xvoigt=yvoigt=xResult=yResult=finalResult=0.0;
    Double_t voigtArea,gaussArea; 
    
//    cout<<"distanceX "<<distanceX<<" distanceY "<<distanceY<<" currentBeamSigmaX "<<currentBeamSigmaX<<" spacingBetweenPoints "<<spacingBetweenPoints<<endl;
    
           //include correction
            //how much goes to Gauss, how much goes to Voigt
            //in total, area of both curves should be one due to normalization
            voigtArea=1/(1+GaussVoigtRelation);
            gaussArea=1-voigtArea;
//            if ( isnan(voigtArea) ||  isinf(voigtArea) || voigtArea<0. ) {cerr<<"ERROR: Aborting, voigtArea inf or nan or negative :"<< voigtArea<<endl; exit(-1);}          

//            cout<<"voigtArea "<<voigtArea;
     //Gauss distribution due to MS
            yg= TMath::Gaus(distanceY, 0.0, currentBeamSigmaY, true)*spacingBetweenPoints;//*spacingBetweenPoints to normalize to one again  
//            cout<<"currentBeamSigmaY "<<currentBeamSigmaY<<" spacingBetweenPoints "<<spacingBetweenPoints<<" distanceY "<<distanceY<<endl;
//            if ( isnan(yg) ||  isinf(yg) || yg<0. ) {cerr<<"ERROR: Aborting, yg inf or nan or negative :"<< yg<<endl; exit(-1);}
//            cout<<" yg "<<yg<<endl;
            //create Voigt curve due to nuclear correction
            //Adapting nuclear correction LUT beam diameter to current diameter
            nucCorrTable->setBeamSigma(initialBeamSigmaY);
            yvoigt=TMath::Voigt(distanceY, VoigtSigma,VoigtLg , 4)*spacingBetweenPoints;//*spacingBetweenPoints to normalize to one again  
//            if ( isnan(yvoigt) ||  isinf(yvoigt) ) {cerr<<"ERROR: Aborting, yvoigt inf or nan or negative :"<< yvoigt<<endl; exit(-1);}    

            //Add up contributions of both curves          
            yResult=yg*gaussArea+yvoigt*voigtArea;
//            cout<<" yResult "<<yResult;
     //Gauss distribution due to MS
            xg= TMath::Gaus(distanceX, 0.0, currentBeamSigmaX, true)*spacingBetweenPoints;//*spacingBetweenPoints to normalize to one again  
//            if ( isnan(xg) ||  isinf(xg) || xg<0. ) {cerr<<"ERROR: Aborting, xg inf or nan or negative :"<< xg<<endl; exit(-1);}

            //create Voigt curve due to nuclear correction
            //Adapting nuclear correction LUT beam diameter to current diameter
            nucCorrTable->setBeamSigma(initialBeamSigmaX);
            xvoigt=TMath::Voigt(distanceX, VoigtSigma,VoigtLg , 4)*spacingBetweenPoints;//*spacingBetweenPoints to normalize to one again  
//            if ( isnan(xvoigt) ||  isinf(xvoigt) || xvoigt<0. ) {cerr<<"ERROR: Aborting, xvoigt inf or nan or negative :"<< xvoigt<<endl; exit(-1);}    

            //Add up contributions of both curves          
            xResult=xg*gaussArea+xvoigt*voigtArea;            
//            cout<<" xResult "<<xResult<<endl;
            //Create overlap between curves of both dimensions -> final weight factor for this spot, does only need fluence
            finalResult=xg * yg;
            if ( isnan(finalResult) ||  isinf(finalResult) || finalResult<0.) {cerr<<"ERROR: Aborting, Weight at given spot inf or nan or negative:"<< finalResult<<endl; exit(-1);}    
 
//            cout<<" finalResult "<<finalResult<<" without spacing result: "<<finalResult/spacingBetweenPoints<<endl;
            return   finalResult;       
}



//loop through the scatAngleSquaredArray and quadratically sum up all contents with distance from current point
//defined through stepSize*maxIndex
//see Soukup Dissertation
 double  getSummedScattAngle(std::vector<double>& scatAngleSquaredVector,double stepSize)
{
	int i,currentSize;
        double depthSum=0;
	double quadrSum=0;
        
        currentSize=scatAngleSquaredVector.size();
        
        depthSum=stepSize*currentSize;
            for (i=0;i<currentSize;i++)
            {
             //first (first voxel) entry for generated angle is now farthest away.
             //quadratically sums up squared angles * depth   
             //scatAngleSquaredArray contains already squared values
             quadrSum=quadrSum + scatAngleSquaredVector.at(i)*pow(depthSum,2);
             depthSum=depthSum-stepSize;
            }
	return quadrSum;
}



//calculate the stoppingPowerRatioToWater in order to rescale the real depth to a depth in water
 Double_t  getStoppingPowerRatioToWater(Double_t stoppingPowerWater,Double_t stoppingPowerMaterial) 
{
	if (stoppingPowerMaterial<=0||stoppingPowerWater<=0) return 1.0;
	else return stoppingPowerMaterial/stoppingPowerWater;
}


//Bethe-Bloch formula
//see Ziegler 1999
//energy, excitationEnergy and particle mass in MeV
 Double_t  calculateStoppingPower(Double_t energy,Double_t atomicZARatio, Double_t excitationEnergy) 
{
	Double_t constK,relatBeta,deltaEmax,stoppingPower;

        if(energy<=0.) return 0.0; //if energy is below or zero, return 0
        relatBeta=relativisticBeta(energy,ProtonSpotPB_constParticleMass);
        
        //formula to calculate stopping power from PDG
        constK=0.307075;//in MeV * 1/g * cm^2
	deltaEmax=(2*ProtonSpotPB_constMassElectron*relatBeta*relatBeta)/(1-(relatBeta*relatBeta));
        
	stoppingPower=constK*ProtonSpotPB_constParticleCharge*ProtonSpotPB_constParticleCharge*atomicZARatio*1/(relatBeta*relatBeta) *
	(       0.5 * log ( (2*ProtonSpotPB_constMassElectron*relatBeta*relatBeta*deltaEmax)/(1-(relatBeta*relatBeta)) )
	- (relatBeta*relatBeta) - log(excitationEnergy)         );
        
        //alternative from Leo Techniques for Nuclear and Particle Physics Experiments
        //both give the same results 
//        constK=0.1535;//in MeV * 1/g * cm^2
//        Double_t gamma;
//        gamma=1 / sqrt( 1 - relatBeta*relatBeta );      
//        deltaEmax=( 2*constMassElectron*relatBeta*relatBeta*gamma*gamma*ProtonSpotPB_constLightSpeed*ProtonSpotPB_constLightSpeed )/
//                (1 + 2*constMassElectron/ProtonSpotPB_constParticleMass*sqrt( 1+relatBeta*relatBeta*gamma*gamma ) + pow(constMassElectron/ProtonSpotPB_constParticleMass,2) );
//        
//        stoppingPower=constK*1*1*atomicZARatio*1/(relatBeta*relatBeta) *
//	( log ( (2*constMassElectron*gamma*gamma*ProtonSpotPB_constLightSpeed*ProtonSpotPB_constLightSpeed*relatBeta*relatBeta*deltaEmax)/(excitationEnergy*excitationEnergy) )
//	- 2*(relatBeta*relatBeta));
if (isinf(stoppingPower)!=0 || stoppingPower<0.) stoppingPower=0.0;
	return stoppingPower;
}


////decomposes the initial gauss distributed beam in subBeams
////spread is the maximum distance from the original beam axis in times of sigma 
Int_t  beamDecomposition(Double_t **beamDataArray, Int_t numOfSubBeamsOneDim)
{

    Double_t beamInitX0=round(XDim/2.); //set initial beam coordinates in beams eye view coordinate system
    Double_t beamInitY0=round(YDim/2.);
    Double_t initEnergy=150.; //set initial beam energy   
//    if(initEnergy>80.)cout<<"WARNING: energy higher than 80 MeV/A: "<<initEnergy<<endl;
    Double_t beamInitSigmaX= 0.2;//in cm
    Double_t beamInitSigmaY= 0.2;//in cm
    Double_t beamInitSigmaXWidening = 0.;
    Double_t beamInitSigmaYWidening = 0.;
    
    assert((initEnergy>=0.));//beam energy may not be negative
    assert((beamInitSigmaX>0.)&&(beamInitSigmaY>0.));//beam sigma may not be negative or zero
    assert((beamInitSigmaXWidening>=0.)&&(beamInitSigmaYWidening>=0.));//widening may not be negative
    
    if (numOfSubBeamsOneDim<=1) //in case no splitting is required, if only one beam
    {
          beamDataArray[0][0]=beamInitX0;//PosX0
          beamDataArray[0][1]=beamInitY0;//PosY0
          
          beamDataArray[0][2]=beamInitSigmaX;//SigmaX
          beamDataArray[0][3]=beamInitSigmaY;//SigmaY

          beamDataArray[0][4]=ProtonSpotPB_initFluence;
          beamDataArray[0][5]=initEnergy;
          
          beamDataArray[0][6]=beamInitSigmaXWidening;
          beamDataArray[0][7]=beamInitSigmaYWidening;
          
//          printf("k: %i weight: %f x0: %f y0: %f sigx: %f sigy: %f fluence: %f energy: %f\n",
//                        0, (Double_t)1,beamDataArray[0][0],beamDataArray[0][1],
//                        beamDataArray[0][2], beamDataArray[0][3],
//                        beamDataArray[0][4],beamDataArray[0][5]);   
     return 1;   
    }    
    
  Int_t i,j,k;
  std::vector<Double_t> xGauss,yGauss;
  GaussFitClass *fTest;
        
  //calculate one Dim fit for x      
  fTest = new GaussFitClass();      
  fTest->numericalFit(numOfSubBeamsOneDim,beamInitSigmaX,xGauss);
  
//  cout<<"size of xGauss vector: "<<xGauss.size()<<std::endl;
  for( i=0;i<(int)xGauss.size();i=i+3)//all three dimensions in one array
  {
      //correct for initial Position x0
      xGauss.at(i)=xGauss.at(i)+beamInitX0;
//      cout<<"Mu: "<<xGauss.at(i);
//      cout<<" Sigma: "<<xGauss.at(i+1); 
//      cout<<" C: "<<xGauss.at(i+2)<<std::endl;
  }
  delete fTest;
  
  //calculate one Dim fit for y   
  fTest = new GaussFitClass();  
  fTest->numericalFit(numOfSubBeamsOneDim,beamInitSigmaY,yGauss);
  
  //cout<<"size of yGauss vector: "<<yGauss.size()<<std::endl;
  for(i=0;i<(int)yGauss.size();i=i+3)//all three dimensions in one array
  {
      //correct for initial Position y0
      yGauss.at(i)=yGauss.at(i)+beamInitY0;
//      cout<<"Mu: "<<yGauss.at(i);
//      cout<<" Sigma: "<<yGauss.at(i+1); 
//      cout<<" C: "<<yGauss.at(i+2)<<std::endl;
  }
  delete fTest;
  
  Double_t weight=0;  
  Double_t weightTotal=0;
  k=0;
  //combine dimensions and 
  for(i=0;i<numOfSubBeamsOneDim;i++)//yloop
  {
      for(j=0;j<numOfSubBeamsOneDim;j++)//xloop
      {
          //cout<<"i:"<<i<<"j:"<<j<<std::endl;
          beamDataArray[k][0]=xGauss.at(j*3);//PosX0
          beamDataArray[k][1]=yGauss.at(i*3);//PosY0
          
          beamDataArray[k][2]=xGauss.at((j*3)+1);//SigmaX
          beamDataArray[k][3]=yGauss.at((i*3)+1);//SigmaY
          
          weight=xGauss.at((j*3)+2)* yGauss.at((i*3)+2);//Re-Normalization required, done afterwards
          weightTotal=weightTotal+weight;
//          cout<<"gauss:"<<TMath::Gaus(beamInitX0, beamInitY0, initSigmaX, true)<<std::endl;
//          cout<<"cx:"<<xGauss.at((j*3)+2)<<std::endl;
//          cout<<"cy:"<<yGauss.at((i*3)+2)<<std::endl;
          beamDataArray[k][4]= weight;
          beamDataArray[k][5]=initEnergy;
          
          beamDataArray[k][6]=beamInitSigmaXWidening;
          beamDataArray[k][7]=beamInitSigmaYWidening;
          k++;
      }
  }
  
  //weight needs to be renormalized to one 
  weight=0;
//Loop over all sub beams k redistribute weight
  for (k=0;k<numOfSubBeamsOneDim*numOfSubBeamsOneDim;k++)
  {
    weight=1/weightTotal*beamDataArray[k][4];
    //cout<<"total weight: "<<weightTotal<<" old weight: "<<beamDataArray[k][4]<<" new weight: "<<weight<<" initFluence: "<<initFluence<<endl;
    beamDataArray[k][4]= weight*ProtonSpotPB_initFluence;
    
//    printf("k: %i weight: %.2f x0: %.2f y0: %.2f sigx: %.2f sigy: %.2f fluence: %.2f energy: %.2f\n",
//                        k, weight,beamDataArray[k][0],beamDataArray[k][1],
//                        beamDataArray[k][2], beamDataArray[k][3],
//                        beamDataArray[k][4],beamDataArray[k][5]);
  }
    return k;
}

 void  setIsoCenter(double isoX, double isoY, double isoZ)
{
    //isocenter is passed in voxel therefore necessary to recalculate
  m_IsoX = isoX*gridStepSize;
  m_IsoY = isoY*gridStepSize;
  m_IsoZ = isoZ*gridStepSize;

  return;
}


//bool  calculateWEQtoTarget(const DoseAtomSpec& bx,double BeamX0,double BeamY0,double initEnergy,
//        double& distanceToDensGrid, double& distanceSourceToTargetStartWEQ,double& distanceSourceToTargetStopWEQ) const
//{
//    double startX,startY,startZ;
//    double xTemp2,yTemp2,zTemp2;
//    
//    lookUpTable->setTargetEnergy(initEnergy);
//    
//    //get start point of beam
//    transformBeamCoordIntoCubeCoord(bx,BeamX0,BeamY0,0.0,startX,startY,startZ);
//    //get second point to create a direction vector
//    transformBeamCoordIntoCubeCoord(bx,BeamX0,BeamY0,1.0,xTemp2,yTemp2,zTemp2);
//
//    //calculate normalized direction vector
//    //in hyperion coordinates
//    double nRvX,nRvY,nRvZ,norm;
//    nRvX=xTemp2-startX;
//    nRvY=yTemp2-startY;
//    nRvZ=zTemp2-startZ;
//    norm=1./sqrt(nRvX*nRvX + nRvY*nRvY + nRvZ*nRvZ);
//    nRvX*=norm;
//    nRvY*=norm;
//    nRvZ*=norm;
//    
//    //calc distance to grid
//    double distanceToGrid;
//    getDistanceToDensGridIfHit(bx,BeamX0,BeamY0,0.0, distanceToGrid);
//
//    double energyProbNextStep=0.0;
//    double pointX,pointY,pointZ;
//    double steplength=gridStepSize/3.;
//    double dHUnit,densityMaterial, radiationLengthMaterial,excitationEnergyMaterial, materialAtomZARatio;
//    double stoppingPowerWater,stoppingPowerMaterial,rescaleRatio;
//    //start with offset to grid, density is air
//    double currentIntegratedDepthWEQ=distanceToGrid*1.20E-03;
//    //start at Beam entry in density grid, therefore distance source-Grid needed
//    startX+=distanceToGrid*nRvX;
//    startY+=distanceToGrid*nRvY;
//    startZ+=distanceToGrid*nRvZ;
//    
////    cout<<"WEQ XDim: "<<XDim<<" YDim "<<YDim<<" ZDim "<<ZDim<<endl;
//    int iHypX,iHypY,iHypZ;
//     bool bReachedTarget,bIsInTarget,bNotYetLeftTarget;
//     bReachedTarget=false;
//     bIsInTarget=false;
//     bNotYetLeftTarget=false;
//    int i;
//    for(i=1;;i++)
//    {
//        //start at Beam entry in density grid, therefore distance source-Grid needed
//        //proceed with scale increments and offset to density grid
//        //offset included in startX,Y and Z
//        pointX=startX + i*steplength*nRvX;
//        pointY=startY + i*steplength*nRvY;
//        pointZ=startZ + i*steplength*nRvZ;
//        convertHyperionToVoxelCoordinates(pointX, pointY, pointZ, iHypX, iHypY, iHypZ);
//       //check if coordinates are within bounds 
//       if( iHypX<0 || iHypX>=XDim || 
//           iHypY<0 || iHypY>=YDim ||
//           iHypZ<0 || iHypZ>=ZDim )
//       {
//           break;
//       }
//        
//        //if outside body, use air
////        if( m_pDoseCompGrid->isInContour(iHypX, iHypY, iHypZ)==false ) 
////            {
////              dHUnit=-1000; //Houndsfield unit of air is -1000
////              densityMaterial=1.20E-03;
////              radiationLengthMaterial=36.62;
////              excitationEnergyMaterial=85.7;
////              materialAtomZARatio=0.49919;
////              //HoundsfieldUnit density[g/cm3] radiationLength[g*cm_2] Z/A excitation energy[eV] Name
////              //-1000 1.20E-03 36.62 0.49919 85.7 Air 
////            }
////        else 
////        {
//         dHUnit = convertCTtoHU(m_pDoseCompGrid->operator()(iHypX,iHypY,iHypZ));        //query Material
//         //convert Hounsfield Unit to material properties
//         myInputClass->getOneMaterialPropertiesFromHU(dHUnit, densityMaterial, radiationLengthMaterial, 
//                                                     excitationEnergyMaterial, materialAtomZARatio);
////        }
//
//        //calculate stepSize due to different material
//         if (i<=0) energyProbNextStep=initEnergy;
//         else 
//         {
//           energyProbNextStep=lookUpTable->getInDepthEnergy( (currentIntegratedDepthWEQ+steplength)/10. );
//         }
//         //calculate rescale ratio for water equivalent path scaling       
//         stoppingPowerWater= calculateStoppingPower(energyProbNextStep, ProtonSpotPB_constZARatioWater, ProtonSpotPB_constExcitationEnergyWater);
//         stoppingPowerMaterial= calculateStoppingPower(energyProbNextStep,materialAtomZARatio, excitationEnergyMaterial);
//         rescaleRatio=  getStoppingPowerRatioToWater(stoppingPowerWater, stoppingPowerMaterial)*densityMaterial;
//         currentIntegratedDepthWEQ+=steplength*rescaleRatio;
//
//         //check whether the voxel is in the target
//           bIsInTarget=m_pTargetGrid->operator()( iHypX, iHypY , iHypZ );
//           //add up density sum from contour to target
//           if(bReachedTarget==false)
//           {
//            if (bIsInTarget==true) 
//            {
//               distanceSourceToTargetStartWEQ=currentIntegratedDepthWEQ;
//               distanceSourceToTargetStopWEQ=currentIntegratedDepthWEQ;
//                //avoid calling this statement again
//                bReachedTarget=true;
//            }
//           }
//           //add up density sum from start to end of target
//           if(bNotYetLeftTarget==false && bReachedTarget==true)
//           {
//            if (bIsInTarget==false) 
//            {
//                distanceSourceToTargetStopWEQ=currentIntegratedDepthWEQ;
//                 //avoid calling this statement again
//                bNotYetLeftTarget=true;
//            }
//           }
//    }
//
//   return bReachedTarget; 
//}

 void  calcuateWEQandMSarrays(std::vector<double>& HUArray, 
        std::vector<double>& realDepth,std::vector<double>& WEQDepth,std::vector<double>& MScurrentSquaredVariance,
        bool& reachedBraggPeak, double& braggPeakDepthHyperion,double& maxDepthHyperion, double& distanceToContour,
        double initEnergy,
        double distanceToGrid,double BeamX0, double BeamY0)
{   

//    std::vector<double> MSScatAngleSquared;  
//    MSScatAngleSquared.clear();
//    MSScatAngleSquared.reserve(1000);
    
    realDepth.clear();
    realDepth.reserve(1000);
    WEQDepth.clear();
    WEQDepth.reserve(1000);
    MScurrentSquaredVariance.clear();
    MScurrentSquaredVariance.reserve(1000);
    
    double startX,startY,startZ;
    
    double nRvX,nRvY,nRvZ;
    nRvX=0.;
    nRvY=0.;
    nRvZ=1.;
    startX=startY=startZ=0.;
    
    double scatteringAngleSquared,currSquaredVarianceDueToMS;

    double energyProbNextStep=0.0;
    double steplength=gridStepSize/3.;
    double dHUnit,densityMaterial, radiationLengthMaterial,excitationEnergyMaterial, materialAtomZARatio;
    double stoppingPowerWater,stoppingPowerMaterial,rescaleRatio;
    //start with offset to grid, density is air
    double currentIntegratedDepthWEQ=distanceToGrid*1.20E-03;
    //start at Beam entry in density grid, therefore distance source-Grid needed
    startX+=distanceToGrid*nRvX;
    startY+=distanceToGrid*nRvY;
    startZ+=distanceToGrid*nRvZ;
//    cout<<" offset to grid: "<<distanceToGrid<<endl;
//               double distanceFromBeamX;
//                double distanceFromBeamY;
//                double distanceFromBeamZ;
//    getDistanceFromBeamAndDepthAlongBeamAxis(bx,BeamX0,BeamY0, startX,  startY,  startZ, distanceFromBeamX,  distanceFromBeamY, distanceFromBeamZ);
//    cout<<"function says: startPointOffset distanceFromBeamX "<<distanceFromBeamX<<" distanceFromBeamY "<<distanceFromBeamY<<" distanceFromBeamZ "<<distanceFromBeamZ<<endl;

    rescaleRatio=1.0;
    //first depth step
    realDepth.push_back(0.);
    WEQDepth.push_back(0.);
    MScurrentSquaredVariance.push_back(0.);
    //get WEQ bragg Peak Depth
    double braggPeakDepthWaterEQ=lookUpTable->getPeakRangeOfEnergy(initEnergy)*10.; 
    braggPeakDepthHyperion=-1.0;//initialize
    maxDepthHyperion=-1.0;
    
    distanceToContour=-1.0;//initialize
    
//    cout<<"WEQ XDim: "<<XDim<<" YDim "<<YDim<<" ZDim "<<ZDim<<endl;
    int i;
    double currRealLength=0.0;
    double scattSum=0.;
    currSquaredVarianceDueToMS=0.0;
    for(i=1;i<HUArray.size();i++)
    {
        currRealLength=i*steplength+distanceToGrid;
        //start at Beam entry in density grid, therefore distance source-Grid needed
        //proceed with scale increments and offset to density grid
        //offset included in startX,Y and Z
       
        //determine distance from beam source to start of contour
        if( HUArray.size()==(i-1) )
        {
            distanceToContour=currRealLength;
           if (currentIntegratedDepthWEQ>=braggPeakDepthWaterEQ) reachedBraggPeak=true;
           else reachedBraggPeak=false;
//           cout<<"Reached end of grid."<<endl;
//               double distanceFromBeamX;
//                double distanceFromBeamY;
//                double distanceFromBeamZ;
//           cout<<"at iHypX: "<<iHypX<<" iHypY "<<iHypY<<" iHypZ "<<iHypZ<<endl;
//           cout<<"MaxDepthreal "<<(i-1)*steplength+distanceToGrid<<" max WEQ depth "<<currentIntegratedDepthWEQ<<endl;
//           getDistanceFromBeamAndDepthAlongBeamAxis(bx,BeamX0,BeamY0, pointX,  pointY,  pointZ, distanceFromBeamX,  distanceFromBeamY, distanceFromBeamZ);
//           cout<<"function says: distanceFromBeamX "<<distanceFromBeamX<<" distanceFromBeamY "<<distanceFromBeamY<<" distanceFromBeamZ "<<distanceFromBeamZ<<endl;
//           cout<<"steps: "<<i<<" steplength "<<steplength<<endl;
           
           maxDepthHyperion=currRealLength;//set maximum depth which was calculated
           
           //add additional buffer, with last settings, just to avoid rounding errors
           realDepth.push_back(currRealLength);
           WEQDepth.push_back(currentIntegratedDepthWEQ+steplength*rescaleRatio);
           realDepth.push_back(currRealLength+steplength);
           WEQDepth.push_back(currentIntegratedDepthWEQ+steplength*rescaleRatio);
           break;
       } 
        
        //query Material
        dHUnit = HUArray.at(i);

        //convert Hounsfield Unit to material properties
        myInputClass->getOneMaterialPropertiesFromHU(dHUnit, densityMaterial, radiationLengthMaterial, 
                                                     excitationEnergyMaterial, materialAtomZARatio);

        //calculate stepSize due to different material
         if (i<=0) energyProbNextStep=initEnergy;
         else 
         {
           energyProbNextStep=lookUpTable->getInDepthEnergy( (currentIntegratedDepthWEQ+steplength)/10. );
         }
         //calculate rescale ratio for water equivalent path scaling       
         stoppingPowerWater= calculateStoppingPower(energyProbNextStep, ProtonSpotPB_constZARatioWater, ProtonSpotPB_constExcitationEnergyWater);
         stoppingPowerMaterial= calculateStoppingPower(energyProbNextStep,materialAtomZARatio, excitationEnergyMaterial);
         rescaleRatio=  getStoppingPowerRatioToWater(stoppingPowerWater, stoppingPowerMaterial)*densityMaterial;
         currentIntegratedDepthWEQ+=steplength*rescaleRatio;
         
         realDepth.push_back(currRealLength);
         WEQDepth.push_back(currentIntegratedDepthWEQ);
         
         if( currentIntegratedDepthWEQ>=braggPeakDepthWaterEQ && braggPeakDepthHyperion==-1.0 ) 
         {
             braggPeakDepthHyperion=currRealLength;
//             else if ( (currentIntegratedDepthWEQ>=(braggPeakDepthWaterEQ*1.5) ) && maxDepthHyperion==-1.0)  maxDepthHyperion=currRealLength;
         }
         
         //calculate scattering angle
         
         //recalculate the scattering Angle every z step, due to multiple scattering corrections
         scatteringAngleSquared= getScatteringAngleSquare(steplength*rescaleRatio,
                                                         lookUpTable->getInDepthEnergy( currentIntegratedDepthWEQ/10.), radiationLengthMaterial);
         //store in array
         //MSScatAngleSquared.push_back(scatteringAngleSquared*(0.9*0.9));//reduce calculated angle by ten percent

         //calculate current squared variance by looping over all previous angles and steps
    
           currSquaredVarianceDueToMS=currSquaredVarianceDueToMS+ scattSum * steplength*steplength;
           scattSum=scattSum+scatteringAngleSquared*(0.9*0.9) ;//reduce calculated angle by ten percent
         
         //currSquaredVarianceDueToMS = getSummedScattAngle(MSScatAngleSquared,steplength);
         if ( isnan(currSquaredVarianceDueToMS) ||  isinf(currSquaredVarianceDueToMS) || currSquaredVarianceDueToMS<0.0) 
                                 {cerr<<"ERROR currSquaredVarianceDueToMS inf or nan or negative:"<< currSquaredVarianceDueToMS<<endl; exit(-1);}
//         MScurrentSquaredVariance.push_back(currSquaredVarianceDueToMS); 
                  MScurrentSquaredVariance.push_back(0.);  

         
//         cout<<"initEnergy: "<<initEnergy<<" currEnergy: "<<energyProbNextStep<<" HU: "<<dHUnit<<" WEQdepth: "<< currentIntegratedDepthWEQ<<" realDepth: "<<currRealLength<<endl;
    }

   return; 
}

 double  getDoseAtVoxel(double dHUnit, interpol* weqLUT,interpol* MSsquaredVarianceLUT,Double_t *beamDataArray, 
                              double distanceFromBeamX,double distanceFromBeamY,double distanceFromBeamZ)
{
//      double beamX0             = beamDataArray[0];
//      double beamY0             = beamDataArray[1];      
      double beamInitSigmaX     = beamDataArray[2];
      double beamInitSigmaY     = beamDataArray[3];
      double thisBeamFluence    = beamDataArray[4];
      double beamInitSigmaXWidening = beamDataArray[6];
      double beamInitSigmaYWidening = beamDataArray[7];

//      double initEnergy         = beamDataArray[5];
      
//    //get distances  
//    double distanceFromBeamX,distanceFromBeamY, beamDepth;
//    getDistanceFromBeamAndDepthAlongBeamAxis(bx, beamX0, beamY0,(double) iHypX*gridStepSize,(double) iHypY*gridStepSize,(double) iHypZ*gridStepSize,
//                                             distanceFromBeamX, distanceFromBeamY, beamDepth);    

    //get WEQ depth
    double weqDepth=weqLUT->getData(distanceFromBeamZ);
//    cout<<" realDepth "<<distanceFromBeamZ<<" weqDepth "<<weqDepth<<endl;
//    cout<<" beamDepth "<<distanceFromBeamZ<<" distanceFromBeamX "<<distanceFromBeamX<<" distanceFromBeamY "<<distanceFromBeamY<<endl;

    //determine material properties
    //get Hounsfield unit from Hyperion and convert to HU as Hyperion gives
    //raw scanner value, converted to Hounsfield unit
    double densityMaterial, radiationLengthMaterial, excitationEnergyMaterial, materialAtomZARatio;
    //convert Hounsfield Unit to material properties
    myInputClass->getOneMaterialPropertiesFromHU(dHUnit, densityMaterial, radiationLengthMaterial, 
                                                 excitationEnergyMaterial, materialAtomZARatio);
    
    //get rescaling
    double currEnergy=lookUpTable->getInDepthEnergy( weqDepth/10.);//LUT in cm
    double stoppingPowerWater= calculateStoppingPower(currEnergy, ProtonSpotPB_constZARatioWater, ProtonSpotPB_constExcitationEnergyWater);
    double stoppingPowerMaterial= calculateStoppingPower(currEnergy,materialAtomZARatio, excitationEnergyMaterial);
    double rescaleRatio=  getStoppingPowerRatioToWater(stoppingPowerWater, stoppingPowerMaterial)*densityMaterial;
    
    //get BeamShape
    double MSsquaredVariance=MSsquaredVarianceLUT->getData(weqDepth);//newLUTs in mm
    double currentSigmaX=sqrt(MSsquaredVariance+pow(beamInitSigmaX+ (weqDepth/10.)*beamInitSigmaXWidening,2) );//in cm
    double currentSigmaY=sqrt(MSsquaredVariance+pow(beamInitSigmaY+ (weqDepth/10.)*beamInitSigmaYWidening,2) );//in cm
//nuclear correction is calculated with function
    double GaussVoigtRelation=nucCorrTable->getGaussVoigtRelation(weqDepth/10.);
    double VoigtSigma=nucCorrTable->getVoigtSigma(weqDepth/10.);
    double VoigtLg=nucCorrTable->getVoigtLg(weqDepth/10.);
    
//    cout<<"currEnergy "<<currEnergy<<" rescaleRatio "<<rescaleRatio<<" MSsquaredVariance "<<MSsquaredVariance<<
//          " GaussVoigtRelation "<<  GaussVoigtRelation<<" VoigtSigma "<<VoigtSigma<<" VoigtLg "<<VoigtLg<<endl;
    
     if ( isnan(GaussVoigtRelation) ||  isinf(GaussVoigtRelation) || GaussVoigtRelation<0. ) {cerr<<"ERROR: Aborting, GaussVoigtRelation inf or nan or negative :"<< GaussVoigtRelation<<endl;cout<<" weqDepth "<<weqDepth<<endl; exit(-1);}
     if ( isnan(VoigtSigma) ||  isinf(VoigtSigma) || VoigtSigma<0. ) {cerr<<"ERROR: Aborting, VoigtSigma inf or nan or negative :"<< VoigtSigma<<endl;cout<<" weqDepth "<<weqDepth<<endl; exit(-1);}
     if ( isnan(VoigtLg) ||  isinf(VoigtLg) || VoigtLg<0. ) {cerr<<"ERROR: Aborting, VoigtLg inf or nan or negative :"<< VoigtLg<<endl;cout<<" weqDepth "<<weqDepth<<endl; exit(-1);}
    
    //get weightFactor
    double weightFactorNoFluence=getBeamShapeAtDistanceFromCenter(distanceFromBeamX,distanceFromBeamY,
                                                          currentSigmaX*10., currentSigmaY*10.,
                                                          beamInitSigmaX*10.,beamInitSigmaY*10.,
                                                          GaussVoigtRelation,VoigtSigma*10.,
                                                          VoigtLg*10., ProtonSpotPB_stepSize*10.);//everything in mm 
    //calc Dose
    //calculate dose deposition factor e.g. fraction of dose deposited at this depth   
     //LUT contains edep for water, should be the same as dose    
     double inDepthEdepDepositionFactor=lookUpTable->getEdep(weqDepth/10.);
     double inDepthReleasedEnergy=inDepthEdepDepositionFactor*weightFactorNoFluence*thisBeamFluence*rescaleRatio;
//   cout<<"HU: "<<dHUnit <<" dosedepFactor: "<<inDepthDoseDepositionFactor<<" weightFactor "<<weightFactorNoFluence<<" thisBeamFluence "<<thisBeamFluence<<" rescaleRatio "<<rescaleRatio<<endl;
     //------------------------------------------------------------------------------                    
     //dose calculation
                         //ATTENTION !!!
                         //inDepthReleasedDose is not correct dose, needs to be scaled according to voxelsize
                         //May not be correct
                         //1 Gy = J/kg
                         //mass = density * Volume
                         //Gray= MeVtoJoule / mass *1000 (1000 due to density in gram)
                         //=>edep*ProtonSpotPB_constMeVToJoule / rescaleRatio
    double massOfVoxelInGram=densityMaterial* pow((ProtonSpotPB_stepSize),3);  
//    double doseAtVoxel=inDepthReleasedEnergy* pow((rescaleRatio),3)*ProtonSpotPB_constMeVToJoule / (massOfVoxelInGram/1000.);
    double doseAtVoxel=inDepthReleasedEnergy* ProtonSpotPB_constMeVToJoule / (massOfVoxelInGram/1000.);
    if ( isnan(doseAtVoxel) ||  isinf(doseAtVoxel) || doseAtVoxel<0. ) {cerr<<"ERROR: Aborting, doseAtVoxel inf or nan or negative :"<< doseAtVoxel<<endl; exit(-1);}

//    if(m_pDoseCompGrid->isInContour(iHypX,iHypY,iHypZ)==false)
//    {
//    if(dHUnit>0.)
//    {
//    cout<<"distanceFromBeamSourceZ: "<< distanceFromBeamZ<<" weqDepth: "<<weqDepth<<" distanceFromBeamX: "<<distanceFromBeamX<<" distanceFromBeamY: "<<distanceFromBeamY<<endl;
//    cout<<" inDepthEdepDepositionFactor "<<inDepthEdepDepositionFactor<<" weightFactorNoFluence: "<<weightFactorNoFluence<<endl;
//    cout<<" HU: "<<dHUnit<<" densityMaterial "<<densityMaterial<<endl;
//    cout<<"  ix: "<<iHypX<<" iy: "<<iHypY<<" iz: "<<iHypZ<<endl;
////    cout<<"HU: "<<dHUnit<<" densityMaterial "<<densityMaterial<<" inDepthEdepDepositionFactor "<<inDepthEdepDepositionFactor<<endl;
////    cout<<" doseAtVoxelNoWeight "<<doseAtVoxel/weightFactorNoFluence<<" massOfVoxelInGram "<<massOfVoxelInGram<<endl;
////    }
//    }
 return doseAtVoxel; 
}

//singlePencilBeamAlgorithm(bx, Trafo, beamDataArray[k], dimensionZ,
//                                 distanceToGrid, reachedBraggPeaktemp, iPBXtemp,iPBYtemp,iPBZtemp);



 void  getNormVectorsForVector(double vecX,double vecY, double vecZ,
                                        double& nv1x,double& nv1y,double& nv1z,
                                        double& nv2x,double& nv2y,double& nv2z)
{
    
    nv1x=nv1y=nv1z=nv2x=nv2y=nv2z=0.0;
        //determine which element is the smallest
        //replace with 1 and other elements with 0
        //do crossproduct of both vectors, the result is an orthogonal vector
        if (vecX<= vecY && vecX<=vecZ)   vectorCrossProduct(vecX, vecY, vecZ,
                                            1.,0., 0.,
                                            nv1x,nv1y,nv1z);   
        else if (vecY<= vecX && vecY<=vecZ)   vectorCrossProduct(vecX, vecY, vecZ,
                                            0.,1., 0.,
                                            nv1x,nv1y,nv1z); 
        else if (vecZ<= vecX && vecZ<=vecY)   vectorCrossProduct(vecX, vecY, vecZ,
                                            0.,0., 1.,
                                            nv1x,nv1y,nv1z); 
        
        //calculate second orthogonal vector
        vectorCrossProduct(vecX, vecY, vecZ,
                            nv1x,nv1y, nv1z,
                            nv2x,nv2y,nv2z);
        
     //normalize vectors   
        double norm;
        norm=1./sqrt(nv1x*nv1x + nv1y*nv1y + nv1z*nv1z);
        nv1x*=norm;
        nv1y*=norm;
        nv1z*=norm;
        norm=1./sqrt(nv2x*nv2x + nv2y*nv2y + nv2z*nv2z);
        nv2x*=norm;
        nv2y*=norm;
        nv2z*=norm;

    return; 
}


 void  vectorCrossProduct(double ax, double ay, double az,
                                        double bx, double by, double bz,
                                        double& resX,double& resY,double& resZ)
{
    resX=ay*bz - az*by;
    resY=az*bx - ax*bz;
    resZ=ax*by - ay*bx;        
    return;
}


 void  convertHyperionToVoxelCoordinates(double dX, double dY, double dZ,
                                int& iX, int& iY, int& iZ) 
{
 if(dX>0.)    iX = static_cast<int>( floor(dX/gridStepSize) );
 else iX = static_cast<int>( ceil(dX/gridStepSize) );
 if(dY>0.)    iY = static_cast<int>( floor(dY/gridStepSize) );
 else iY = static_cast<int>( ceil(dY/gridStepSize) );
 if(dZ>0.)    iZ = static_cast<int>( floor(dZ/gridStepSize) );
 else iZ = static_cast<int>( ceil(dZ/gridStepSize) );

 return;
}


//############################################################################################################

//converts CT number to Hounsfield units
//By default, Rescale Intercept (Offset) is -1024
//and Rescale Slope (slope) is 1
//both defined in DICOM header (not read here, just default assumed)
 double convertCTtoHU(double CTnumber) 
{
   return CTnumber*1. - 1024.;
//   if (CTnumber<500.) return -1024.;
//   else return 0;
}

//------------------------------------------------------------------------------------------------
//------------------------INCLUDE FUNCTIONS AND CLASS FUNCTIONS
//------------------------------------------------------------------------------------------------
#include "includes/LUT_class.cpp"
#include "includes/beamDecomposition_class.cpp"
#include "includes/input_class.cpp"
#include "includes/nucLUT_class.cpp"
#include "includes/interpol.cpp"
#include "includes/optimizerRBE_class.cpp"

