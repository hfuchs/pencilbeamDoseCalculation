/* 
 * File:   testbench.h
 * Author: hfuchs
 *
 * Created on December 28, 2012, 8:53 AM
 */

#ifndef TESTBENCH_H
#define	TESTBENCH_H


//#define USEPROTONBEAM
//#define USEHELIUMBEAM
//USEPROTONBEAM switches to proton beam
//USEHELIUMBEAM switches to helium beam

#include <assert.h>
#include <vector>
#include <memory>

//#include <Grid3D.h>
//#include <SpotPB.h>
//
//#include <BeamPatientTrafo.h>

#include <limits.h>
#include "headers/LUT_class.h"
#include "headers/beamDecomposition_class.h"
#include "headers/input_class.h"
#include "headers/nucLUT_class.h"
#include "headers/systemAndRootHeaders.h"
#include "headers/interpol.h"




class voigtFkt: public ROOT::Math::IBaseFunctionOneDim{
public:

    double vMu;
    double vSig;
    double vLg;
    
    void setParameters(double mu, double Vsig, double VLg)
    {
        vMu=mu;
        vSig=Vsig;
        vLg=VLg;
        return;
    }
    
    double DoEval(double x) const
   {
      return TMath::Voigt(x-vMu, vSig, vLg, 4);
   }
    
    ROOT::Math::IBaseFunctionOneDim* Clone() const
   {
      return new voigtFkt();
   }

};





   
    LUT* lookUpTable;
    nucLUT* nucCorrTable;
    inputClass* myInputClass;

    int XDim;
    int YDim;
    int ZDim;
    double gridStepSize;
    //Isocenter location
    double m_IsoX;
    double m_IsoY;
    double m_IsoZ;

    double densGridWidth;
    double densGridHeight;
    double densGridDepth;
    double conversionMMtoVoxel;
        
    Double_t **outputArray;//not yet used
    Double_t **initFluenceArray;//not yet used
    Double_t **ProtonSpotPB_thisBeam_FluenceArray;
    
    Int_t ProtonSpotPB_numberOfSubBeamsOneDim;
   
    Int_t beamDecomposition(Double_t **beamDataArray, Int_t numOfSubBeamsOneDim);

    
    
//calculate the relativistic beta
//energy and Mass in MeV required
Double_t relativisticBeta(Double_t energy, Double_t mass);

////calculate the scattering angle
//// everything in SI units
////See Geant4 physics documentation
Double_t getScatteringAngleSquare(Double_t currentStepSize, Double_t energy, Double_t radiationLength);


//loop through the scatAngleSquaredArray and quadratically sum up all contents with distance from current point
//defined through stepSize*maxIndex
//see Soukup Dissertation
 Double_t getSummedScattAngle(Double_t *scatAngleSquaredArray, Int_t currentIndex);
 double getSummedScattAngle(std::vector<double>& scatAngleSquaredVector,double stepSize);


//calculate the stoppingPowerRatioToWater in order to rescale the real depth to a depth in water
//gives ratio of stopping power to material versus stopping power to water
 Double_t getStoppingPowerRatioToWater(Double_t stoppingPowerWater,Double_t stoppingPowerMaterial);

//Bethe-Bloch formula see Ziegler 1999
//energy, excitation energy and particle mass in MeV
//calculates the stopping power at the given energy for the given material
 Double_t calculateStoppingPower(Double_t energy,Double_t atomicZARatio, Double_t excitationEnergy);



//bool calculateWEQtoTarget(const DoseAtomSpec& bx,double BeamX0,double BeamY0,double initEnergy,
//        double& distanceToDensGrid, double& distanceSourceToTargetStartWEQ,double& distanceSourceToTargetStopWEQ) const;



 inline Double_t getBeamShapeAtDistanceFromCenter(Double_t distanceX, Double_t distanceY,
                                                        Double_t currentBeamSigmaX, Double_t currentBeamSigmaY,
                                                        Double_t initialBeamSigmaX, Double_t initialBeamSigmaY,
                                                        Double_t GaussVoigtRelation, Double_t VoigtSigma,
                                                        Double_t VoigtLg, Double_t spacingBetweenPoints, Double_t voigtCorrectionFactor);


 void vectorCrossProduct(double ax, double ay, double az,
                                        double bx, double by, double bz,
                                        double& resX,double& resY,double& resZ);
 void getNormVectorsForVector(double vecX,double vecY, double vecZ,
                                        double& nv1x,double& nv1y,double& nv1z,
                                        double& nv2x,double& nv2y,double& nv2z);

 double convertCTtoHU(double HUnit);
 void convertHyperionToVoxelCoordinates(double dX, double dY, double dZ,
                                int& iX, int& iY, int& iZ);

// void calcuateWEQandMSarrays(std::vector<double>& HUArray,
//                            std::vector<double>& realDepth,std::vector<double>& WEQDepth,std::vector<double>& MScurrentSquaredVariance,
//                            bool& reachedBraggPeak, double& braggPeakDepthHyperion, double& maxDepthHyperion, double& distanceToContour,
//                            double initEnergy,double distanceToGrid,double BeamX0, double BeamY0);
void calcuateWEQandMSarrays(std::vector<double>& HUArray,
        std::vector<double>& realDepth,std::vector<double>& WEQDepth,std::vector<double>& MScurrentSquaredVariance,
        std::vector<double>& stoppingPowerWaterVector,std::vector<double>& voigtCorrectionFactorVector,
        double& braggPeakDepthHyperion,double& maxDepthHyperion, double& distanceToContour,
        double initEnergy, double distanceToGrid,double BeamX0, double BeamY0);

 double getDoseAtVoxel(double dHUnit,interpol* weqLUT,interpol* MSsquaredVarianceLUT, interpol* stoppingPowerWaterLUT,interpol* voigtCorrectionLUT, 
                                Double_t *beamDataArray, 
                                double distanceFromBeamX,double distanceFromBeamY,double distanceFromBeamZ);
 
inline double NumericalIntegration(double mu, double Vsig, double VLg, double startInterval,double endInterval);
inline double NumericalIntegrationGaus(double mu, double sig, double startInterval,double endInterval);
void calculateVoigtCorrection(std::vector<double>& WEQDepth,std::vector<double>& voigtCorrectionFactorVector);

Double_t cauchyBivariate(Double_t x,Double_t y, Double_t g);


    Double_t ProtonSpotPB_stepSize; 
    Double_t ProtonSpotPB_initFluence;
    Int_t ProtonSpotPB_lateralVoxelsToCalculate;//number of voxels in plus and minus direction to calculate


#ifdef USEPROTONBEAM
    //Proton  specific settings start           
    //mass of the particle used in the beam, given in MeV
    static const Double_t ProtonSpotPB_constParticleMass=938.272029;////in MeV;
    static const Double_t ProtonSpotPB_constParticleCharge=1;
    static const Double_t ProtonSpotPB_constParticleMassNumber=1;
#endif	/* USEPROTONBEAM */


#ifdef USEHELIUMBEAM
    //Helium Ions specific settings start           
    //mass of the particle used in the beam, given in MeV
    static const Double_t ProtonSpotPB_constParticleMass=3727.379109;////in MeV;
    static const Double_t ProtonSpotPB_constParticleCharge=2;
    static const Double_t ProtonSpotPB_constParticleMassNumber=4;
    //Helium Ions specific settings end
#endif	/* USEHELIUMBEAM */
    
    
    static const Double_t ProtonSpotPB_constMassElectron=0.510998918;//in MeV/c^2
    static const Double_t ProtonSpotPB_constExcitationEnergyWater=0.000075;//in MeV
    static const Double_t ProtonSpotPB_constRadiationLengthWater=36.08;//in cm
    static const Double_t ProtonSpotPB_constZARatioWater=0.55509;

    //dielectric coefficient
    static const Double_t ProtonSpotPB_constDiel=8.854187817e-12;//in (A*s)/(Wm)
    //speed of light
    static const Double_t ProtonSpotPB_constLightSpeed=299792458;//in m/s
    //elementary charge
    static const Double_t ProtonSpotPB_constQe=1.60217653e-19;//in Coulomb
    //PI
    static const Double_t ProtonSpotPB_constPI=3.141592653589793238;
    //Euler constant
    static const Double_t ProtonSpotPB_constEuler=2.718281828459045235;
    //1 MeV is 1.602x10^-13 Joule
    static const Double_t ProtonSpotPB_constMeVToJoule=1.602e-13;



#endif	/* TESTBENCH_H */

