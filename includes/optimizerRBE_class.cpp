/* 
 * File:   optimizerRBE.cpp
 * Author: hfuchs
 * 
 * Created on July 1, 2013, 9:08 AM
 */

#include <optimizerRBE_class.h>

optimizerRBE::optimizerRBE() {
    lookUpTable= new LUT;
}

optimizerRBE::optimizerRBE(LUT* look) {
    lookUpTable= look;
}

optimizerRBE::~optimizerRBE() {
}

void optimizerRBE::optimizeSOBP(std::vector<Double_t>& output)
{
//SELECT THE MINIMIZER       
   ROOT::Math::Minimizer* myMiuit = 
          ROOT::Math::Factory::CreateMinimizer("Minuit", "Migrad");
//   ROOT::Math::Minimizer* myMiuit = 
//          ROOT::Math::Factory::CreateMinimizer("Minuit2", "Migrad");
//   ROOT::Math::Minimizer* myMiuit = 
//          ROOT::Math::Factory::CreateMinimizer("Minuit2", "Simplex");
//   ROOT::Math::Minimizer* myMiuit = 
//          ROOT::Math::Factory::CreateMinimizer("Minuit2", "Combined");
//   ROOT::Math::Minimizer* myMiuit = 
//          ROOT::Math::Factory::CreateMinimizer("Minuit2", "Scan");
//   ROOT::Math::Minimizer* myMiuit = 
//          ROOT::Math::Factory::CreateMinimizer("Minuit2", "Fumili");
//   ROOT::Math::Minimizer* myMiuit = 
//          ROOT::Math::Factory::CreateMinimizer("GSLMultiMin", "ConjugateFR");
//   ROOT::Math::Minimizer* myMiuit = 
//          ROOT::Math::Factory::CreateMinimizer("GSLMultiMin", "ConjugatePR");
//   ROOT::Math::Minimizer* myMiuit = 
//          ROOT::Math::Factory::CreateMinimizer("GSLMultiMin", "BFGS");
//   ROOT::Math::Minimizer* myMiuit = 
//          ROOT::Math::Factory::CreateMinimizer("GSLMultiMin", "BFGS2");
//   ROOT::Math::Minimizer* myMiuit = 
//          ROOT::Math::Factory::CreateMinimizer("GSLMultiMin", "SteepestDescent");
//   ROOT::Math::Minimizer* myMiuit = 
//          ROOT::Math::Factory::CreateMinimizer("GSLMultiFit", "");
//   ROOT::Math::Minimizer* myMiuit = 
//          ROOT::Math::Factory::CreateMinimizer("GSLSimAn", "");
   
   
   
   myMiuit->SetMaxFunctionCalls(10000000);
   myMiuit->SetMaxIterations(10000000);
   
//   myMiuit->SetMaxFunctionCalls(100000);
//   myMiuit->SetMaxIterations(100000);
   myMiuit->SetTolerance(0.0001);
   myMiuit->SetPrintLevel(1);
   
      //create minimization function
   int numberOfParameters=2;
   ROOT::Math::Functor f(this, &optimizerRBE::costFunction,numberOfParameters);
   myMiuit->SetFunction(f);
   
   //initialize and set limits
   
   cout<<"maxEdep "<<lookUpTable->findMaxEdep()<<endl;
   cout<<"position "<<lookUpTable->getPositionAtEdep(lookUpTable->findMaxEdep())<<endl;
   
   
   
   myMiuit->SetLimitedVariable(0,"offset",10, 0.00001,0.,500.0);//offset is limited to be bigger than zero
   myMiuit->SetLimitedVariable(1,"scale",0.7, 0.00001,0.,100.0);//scale is limited to be bigger than zero
//   myMiuit->SetLimitedVariable(2,"height",0.7, 0.00001,0.,1.7);//scale is limited to be bigger than zero
   
   
   //necessary for naming
   string tempVarName;
   std::stringstream convertInt;
   tempVarName.clear();
   Double_t calcEnergy=0.;
   
   //initialize target RBE values
   positionVector.clear();
   rbeTargetValuesVector.clear();
   lookUpTable->setTargetEnergy(initialBeamEnergy);
   
   double_t maxDepositedEnergyAtCurrentEnergy=lookUpTable->findMaxEdep();
   
//   cout<<"maxEdepScaled "<<lookUpTable->findMaxEdep()<<" maxEdep "<<lookUpTable->getPositionAtEdep(maxDepositedEnergyAtCurrentEnergy)<<endl;
//   
//   cout<<"edep "<<maxDepositedEnergyAtCurrentEnergy*0.3<<" pos "<<lookUpTable->getPositionAtEdep(0.3*maxDepositedEnergyAtCurrentEnergy)<<endl;
//   cout<<"edep "<<maxDepositedEnergyAtCurrentEnergy*0.4<<" pos "<<lookUpTable->getPositionAtEdep(0.4*maxDepositedEnergyAtCurrentEnergy)<<endl;
//   cout<<"edep "<<maxDepositedEnergyAtCurrentEnergy*0.6<<" pos "<<lookUpTable->getPositionAtEdep(0.6*maxDepositedEnergyAtCurrentEnergy)<<endl;

   
//   lookUpTable->returnEdepPositions();
   //set target rbe values
        //beam entrance for helium =1.1
   
   
//   //////////////////////////////////////////////////////////////////////////
//   ////Proton Biology modelled after Matsumotu, JRR,2014
//   positionVector.push_back(0.0);
//   rbeTargetValuesVector.push_back(1.0); 
//   
//   positionVector.push_back(lookUpTable->getPositionAtEdep(0.3*maxDepositedEnergyAtCurrentEnergy));
//   rbeTargetValuesVector.push_back(1.0);
//   positionVector.push_back(lookUpTable->getPositionAtEdep(0.4*maxDepositedEnergyAtCurrentEnergy));
//   rbeTargetValuesVector.push_back(1.0);
//   positionVector.push_back(lookUpTable->getPositionAtEdep(0.5*maxDepositedEnergyAtCurrentEnergy));
//   rbeTargetValuesVector.push_back(1.0);
//   
//   positionVector.push_back(lookUpTable->getPositionAtEdep(0.6*maxDepositedEnergyAtCurrentEnergy));
//   rbeTargetValuesVector.push_back(1.1);    
//   
//   positionVector.push_back(lookUpTable->getPositionAtEdep(0.7*maxDepositedEnergyAtCurrentEnergy));
//   rbeTargetValuesVector.push_back(1.1);  
//   
//   positionVector.push_back(lookUpTable->getPositionAtEdep(0.8*maxDepositedEnergyAtCurrentEnergy));
//   rbeTargetValuesVector.push_back(1.2);
//   
//   positionVector.push_back(lookUpTable->getPositionAtEdep(0.9*maxDepositedEnergyAtCurrentEnergy));
//   rbeTargetValuesVector.push_back(1.25);
//      positionVector.push_back(lookUpTable->getPositionAtEdep(0.95*maxDepositedEnergyAtCurrentEnergy));
//   rbeTargetValuesVector.push_back(1.4);
//   positionVector.push_back(lookUpTable->getPositionAtEdep(maxDepositedEnergyAtCurrentEnergy));
//   rbeTargetValuesVector.push_back(1.6);
//
//   //post maximum
//   positionVector.push_back(lookUpTable->getPositionAtEdep(maxDepositedEnergyAtCurrentEnergy)+1);
//   rbeTargetValuesVector.push_back(1.6);
//   positionVector.push_back(lookUpTable->getPositionAtEdep(maxDepositedEnergyAtCurrentEnergy)+2);
//   rbeTargetValuesVector.push_back(1.6);
//   positionVector.push_back(lookUpTable->getPositionAtEdep(maxDepositedEnergyAtCurrentEnergy)+5);
//   rbeTargetValuesVector.push_back(1.6);
//   positionVector.push_back(lookUpTable->getPositionAtEdep(maxDepositedEnergyAtCurrentEnergy)+20);
//   rbeTargetValuesVector.push_back(1.6);
//   //////////////////////////////////////////////////////////////////////////

   

   
   //////////////////////////////////////////////////////////////////////////
   //Helium biology modelled after Kase, RadRes 2006
   //////////////////////////////////////////////////////////////////////////
// //with         rbeValAtPos=1.0 + 1.8/ ( 1+ exp(par[0]-par[1]*positionVector.at(i)));

//      positionVector.push_back(0.0);
//   rbeTargetValuesVector.push_back(1.0);   
//   positionVector.push_back(lookUpTable->getPositionAtEdep(0.3*maxDepositedEnergyAtCurrentEnergy));
//   rbeTargetValuesVector.push_back(1.0);
//   positionVector.push_back(lookUpTable->getPositionAtEdep(0.4*maxDepositedEnergyAtCurrentEnergy));
//   rbeTargetValuesVector.push_back(1.0);
//   positionVector.push_back(lookUpTable->getPositionAtEdep(0.5*maxDepositedEnergyAtCurrentEnergy));
//   rbeTargetValuesVector.push_back(1.0);
//   
//   positionVector.push_back(lookUpTable->getPositionAtEdep(0.6*maxDepositedEnergyAtCurrentEnergy));
//   rbeTargetValuesVector.push_back(1.0);    
//   
//   positionVector.push_back(lookUpTable->getPositionAtEdep(0.7*maxDepositedEnergyAtCurrentEnergy));
//   rbeTargetValuesVector.push_back(1.1);  
//   
//   positionVector.push_back(lookUpTable->getPositionAtEdep(0.8*maxDepositedEnergyAtCurrentEnergy));
//   rbeTargetValuesVector.push_back(1.3);
//   
//   positionVector.push_back(lookUpTable->getPositionAtEdep(0.9*maxDepositedEnergyAtCurrentEnergy));
//   rbeTargetValuesVector.push_back(1.4);
//   positionVector.push_back(lookUpTable->getPositionAtEdep(maxDepositedEnergyAtCurrentEnergy));
//   rbeTargetValuesVector.push_back(1.7);
//
//   //post maximum
//   positionVector.push_back(lookUpTable->getPositionAtEdep(maxDepositedEnergyAtCurrentEnergy)+1);
//   rbeTargetValuesVector.push_back(5.0);
//   positionVector.push_back(lookUpTable->getPositionAtEdep(maxDepositedEnergyAtCurrentEnergy)+2);
//   rbeTargetValuesVector.push_back(5.0);
//   positionVector.push_back(lookUpTable->getPositionAtEdep(maxDepositedEnergyAtCurrentEnergy)+5);
//   rbeTargetValuesVector.push_back(5.0);
//   positionVector.push_back(lookUpTable->getPositionAtEdep(maxDepositedEnergyAtCurrentEnergy)+20);
//   rbeTargetValuesVector.push_back(5.0);

   //////////////////////////////////////////////////////////////////////////
   
   
   
   
   
   positionVector.push_back(lookUpTable->getPositionAtEdep(0.3*maxDepositedEnergyAtCurrentEnergy));
   rbeTargetValuesVector.push_back(1.0);
   positionVector.push_back(lookUpTable->getPositionAtEdep(0.4*maxDepositedEnergyAtCurrentEnergy));
   rbeTargetValuesVector.push_back(1.0);
   positionVector.push_back(lookUpTable->getPositionAtEdep(0.5*maxDepositedEnergyAtCurrentEnergy));
   rbeTargetValuesVector.push_back(1.0);
   
   positionVector.push_back(lookUpTable->getPositionAtEdep(0.6*maxDepositedEnergyAtCurrentEnergy));
   rbeTargetValuesVector.push_back(1.1);    
   
   positionVector.push_back(lookUpTable->getPositionAtEdep(0.7*maxDepositedEnergyAtCurrentEnergy));
   rbeTargetValuesVector.push_back(1.2);  
   
   positionVector.push_back(lookUpTable->getPositionAtEdep(0.8*maxDepositedEnergyAtCurrentEnergy));
   rbeTargetValuesVector.push_back(1.5);
   
   positionVector.push_back(lookUpTable->getPositionAtEdep(0.9*maxDepositedEnergyAtCurrentEnergy));
   rbeTargetValuesVector.push_back(1.5);
      positionVector.push_back(lookUpTable->getPositionAtEdep(0.95*maxDepositedEnergyAtCurrentEnergy));
   rbeTargetValuesVector.push_back(1.6);
   positionVector.push_back(lookUpTable->getPositionAtEdep(maxDepositedEnergyAtCurrentEnergy));
   rbeTargetValuesVector.push_back(1.6);

   //post maximum
   positionVector.push_back(lookUpTable->getPositionAtEdep(maxDepositedEnergyAtCurrentEnergy)+1);
   rbeTargetValuesVector.push_back(1.6);
   positionVector.push_back(lookUpTable->getPositionAtEdep(maxDepositedEnergyAtCurrentEnergy)+2);
   rbeTargetValuesVector.push_back(1.6);
   positionVector.push_back(lookUpTable->getPositionAtEdep(maxDepositedEnergyAtCurrentEnergy)+5);
   rbeTargetValuesVector.push_back(1.6);
   positionVector.push_back(lookUpTable->getPositionAtEdep(maxDepositedEnergyAtCurrentEnergy)+20);
   rbeTargetValuesVector.push_back(1.6);
   
   
   //   positionVector.push_back(0.0);
//   rbeTargetValuesVector.push_back(1.1);   
//   positionVector.push_back(lookUpTable->getPositionAtEdep(0.3*maxDepositedEnergyAtCurrentEnergy));
//   rbeTargetValuesVector.push_back(1.1);
//   positionVector.push_back(lookUpTable->getPositionAtEdep(0.4*maxDepositedEnergyAtCurrentEnergy));
//   rbeTargetValuesVector.push_back(1.1);
//   positionVector.push_back(lookUpTable->getPositionAtEdep(0.5*maxDepositedEnergyAtCurrentEnergy));
//   rbeTargetValuesVector.push_back(1.1);
//   
//   positionVector.push_back(lookUpTable->getPositionAtEdep(0.6*maxDepositedEnergyAtCurrentEnergy));
//   rbeTargetValuesVector.push_back(1.2);    
//   
//   positionVector.push_back(lookUpTable->getPositionAtEdep(0.7*maxDepositedEnergyAtCurrentEnergy));
//   rbeTargetValuesVector.push_back(1.3);  
//   
//   positionVector.push_back(lookUpTable->getPositionAtEdep(0.8*maxDepositedEnergyAtCurrentEnergy));
//   rbeTargetValuesVector.push_back(1.35);
//   
//   positionVector.push_back(lookUpTable->getPositionAtEdep(0.9*maxDepositedEnergyAtCurrentEnergy));
//   rbeTargetValuesVector.push_back(1.4);
//   positionVector.push_back(lookUpTable->getPositionAtEdep(maxDepositedEnergyAtCurrentEnergy));
//   rbeTargetValuesVector.push_back(1.4);
//
//   //post maximum
//   positionVector.push_back(lookUpTable->getPositionAtEdep(maxDepositedEnergyAtCurrentEnergy)+1);
//   rbeTargetValuesVector.push_back(1.4);
//   positionVector.push_back(lookUpTable->getPositionAtEdep(maxDepositedEnergyAtCurrentEnergy)+2);
//   rbeTargetValuesVector.push_back(1.4);
//   positionVector.push_back(lookUpTable->getPositionAtEdep(maxDepositedEnergyAtCurrentEnergy)+5);
//   rbeTargetValuesVector.push_back(1.4);
//   positionVector.push_back(lookUpTable->getPositionAtEdep(maxDepositedEnergyAtCurrentEnergy)+20);
//   rbeTargetValuesVector.push_back(1.4);
   
//   for(int i=0;i<positionVector.size();i++)
//   {
//       cout<<positionVector.at(i)<<" "<<endl;
//   }
      
   
   //Minimize
   myMiuit->Minimize(); 
        
        
  //get back optimized values
        
  output.clear();
  const double *xs = myMiuit->X();
  for(int i=0;i<numberOfParameters;i++)
   {
//      cout<<"Beam "<<i<<": "<< " Intensity: "<<xs[i]<<" Energy: "<<energiesVector.at(i)<<endl;
      output.push_back(xs[i]);
   }

}

//Returns the chisquared value of the difference between realfkt and fitted function
Double_t optimizerRBE::costFunction(const Double_t *par)
{
//   numberOfBeams 
//   par[0]..intensity
//   par[1]..intensity
//   par[2]..intensity
    Double_t totalEdepAtPoint=0.;
    Double_t position=0.;
    Double_t chisq = 0;
    Double_t delta=0;
    Int_t k=0;
    Double_t rbeValAtPos=0.;
    
    for(int i=0;i<positionVector.size();i++)
    {
        rbeValAtPos=1.0 + 0.6/ ( 1+ exp(par[0]-par[1]*positionVector.at(i)));

        delta= rbeTargetValuesVector.at(i) - rbeValAtPos;
        chisq += delta*delta;
    }
           
//     cout<<" chisq "<<chisq<<endl;

  return chisq;        
}

//sets step size
//should be factor 5 smaller than calculation step size
Double_t optimizerRBE::setBeamEnergy(Double_t beamEnergy)
{
    initialBeamEnergy=beamEnergy;
    return initialBeamEnergy;
}

