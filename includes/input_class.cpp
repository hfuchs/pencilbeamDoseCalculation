#ifndef INPUTCLASSFUNCTIONS
#define	INPUTCLASSFUNCTIONS

#include <input_class.h>

//------------------------------------------------------------------------------------------------
//------------------------CLASS FUNCTIONS
//------------------------------------------------------------------------------------------------


//------------------------------------------------------------------------------
//-------BEGIN INPUT CLASS
//------------------------------------------------------------------------------


inputClass::inputClass()
    {
        setDataFileName((char*)"calibration/myHUDataFile.dat");
        numberOfHUDataElements=1;
        densityLUT = new interpol;
    };
    
    inputClass::~inputClass()
    {
        Int_t i;
        //delete arrays        
        for (i=0;i<numberOfHUDataElements;i++)
        {
        delete[] huDataArray[i];
        }
        delete[] huDataArray;

    };
    


//copy constructor
inputClass::inputClass(const inputClass& orig)
{
    numberOfHUDataElements=orig.numberOfHUDataElements;
    strcpy ( dataFilename, orig.dataFilename);
    
    //        //create hu data array
        huDataArray = new Double_t*[numberOfHUDataElements];
            for (Int_t i=0;i<numberOfHUDataElements;i++)
            {
            huDataArray[i]=new Double_t[5];
                for (Int_t j=0;j<5;j++)
                {
                      huDataArray[i][j]=orig.huDataArray[i][j];                   
                }
            }
        
        huOnlyArray = new Double_t[numberOfHUDataElements];
        for(Int_t i=0;i<numberOfHUDataElements;i++)
        {
            huOnlyArray[i]=orig.huOnlyArray[i];
        }
        
        densityLUT = new interpol(*orig.densityLUT);
}
    
    
    //sets the dataFilename
    void inputClass::setDataFileName(char* filename)
    {
        strcpy ( dataFilename, filename);
        return;
    }

    
    //function reads in data file with two columns
    //first column is length, second deposited energy
    //function returns number of created entries
    //dataArray[2][numberOfEntries]
    //Reads only numbOfLinesToRead
    Int_t inputClass::readInDataFromFile(char* filename,Double_t** dataArray,Int_t numbOfLinesToRead)
    {
        FILE *df;
        Int_t i,hu;
        char line[1000];
        Double_t density,radLength,ZA,excitationEnergy;
        char name[1000];

        df=fopen(filename,"r");
        if(!df) 
        {
            printf("File %s could not be opened. Aborting. \n",filename);
            exit(-1);
            return i;
        }

        //read in available energies
        i=0;
        while (fgets(line,1000,df)) 
        {
            if (i>numbOfLinesToRead) break;//end read if more than requested number of lines
            //skip line and go to next line if starting with #
            if (line[0] == '#') continue;
            if (line[0] == '\n') continue;
            if (line[0] == ' ') continue;
            //#HoundsfieldUnit density[g/cm3] radiationLength[g*cm_2] Z/A excitation energy[eV] Name
            sscanf(&line[0],"%i %lf %lf %lf %lf %s",&hu,&density,&radLength,&ZA,&excitationEnergy,name);
            //Store read value in array

            //printf("%lf %lf\n",length,energy);
            dataArray[i][0]=(Double_t)hu;
            dataArray[i][1]=density;
            dataArray[i][2]=radLength;
            dataArray[i][3]=ZA;
            dataArray[i][4]=excitationEnergy;
            //cout<<"read hu "<<hu<<std::endl;
            //dataArray[5][i]=name;


    //                    if (i==0) printf("%s \n",line);
    //                    if (i>10000) printf("%s \n",line);
    //                    if (i>10000) printf("%lf %lf\n",dataArray[0][i],dataArray[1][i]);
    //                    if (i==0) printf("%lf %lf\n",dataArray[0][i],dataArray[1][i]);
            //printf("%i\n",i);
            i++;
        }
            fclose (df);
        return i;
    }           


    //read huDataElements from file filename
    //entries are sorted and stored in dataArray
    Int_t inputClass::readInDataArrayReturnSorted(char* filename,Int_t huDataElements, Double_t** dataArray)
    {
        Int_t i,j;
        //create temp hu data array
        Double_t **tempArray = new Double_t*[huDataElements];	
                for (i=0;i<huDataElements;i++)
                {
                tempArray[i]=new Double_t[5];
                        for (j=0;j<5;j++)
                        {
                              tempArray[i][j]=0;                   
                        }
                }

        //read in huDatafile
        readInDataFromFile(filename,tempArray,huDataElements);

        //sort array
        //extract hu numbers
        Double_t huToSort[huDataElements];
        Int_t indexArray[huDataElements];
        for(i=0;i<huDataElements;i++)
        {
           huToSort[i]=tempArray[i][0];
        }
        TMath::Sort(huDataElements, huToSort, indexArray,false);
        for(i=0;i<huDataElements;i++)
        {
//            for(j=0;j<5;j++)
//            {
//             dataArray[i][j]=tempArray[indexArray[i]][j];
//            }
            memcpy( dataArray[i], tempArray[indexArray[i]], 5 * sizeof(Double_t) );
        }

        //delete temp array  
        for (j=0;j<huDataElements;j++)
        {
          delete[] tempArray[j];                   
        }
        delete[] tempArray;

     return  huDataElements;
    }
    
    //returns the number of valid rows in filename
    //invalid is no #, \n or empty string
    Int_t inputClass::countRowsInFile(char* filename)
    {
        FILE *df;
        Int_t counter=0;
        char line[1000];

        df=fopen(filename,"r");
        if(!df) 
        {
            printf("File %s could not be opened. Aborting. \n",filename);
            exit(-1);
            return -1;
        }

        //read in files
        //count line numbers
        while((fgets(line,1000,df)) != NULL) 
        {
          if (line[0] == '#') continue;
          if (line[0] == '\n') continue;
          if (line[0] == ' ') continue;
          counter++;
        }
        fclose (df);
     return counter;
    }  
    
    //function used to read in data file, allocate arrays and store data from file in them
    void inputClass::readInDataFile()
    {
        Int_t i,j;
        //count number of elements in file
        numberOfHUDataElements=countRowsInFile(dataFilename);

//        //create hu data array
        huDataArray = new Double_t*[numberOfHUDataElements];	
            for (i=0;i<numberOfHUDataElements;i++)
            {
            huDataArray[i]=new Double_t[5];
                for (j=0;j<5;j++)
                {
                      huDataArray[i][j]=0.;                   
                }
            }

        //read in huDatafile
        readInDataArrayReturnSorted(dataFilename,numberOfHUDataElements, huDataArray);
         

        //create lookUpArray for binary search
        //delete previous array
        huOnlyArray = new Double_t[numberOfHUDataElements];
        Double_t* tempArray;
        tempArray = new Double_t[numberOfHUDataElements];
        for(i=0;i<numberOfHUDataElements;i++)
        {
            huOnlyArray[i]=huDataArray[i][0];
            tempArray[i]=huDataArray[i][1];
//            cout<<"toSet HU: "<<huOnlyArray[i]<<" density: "<<tempArray[i]<<endl;
        }
        
        //write density to interpolator LUT
        densityLUT->setInterpolEdepDataArray(numberOfHUDataElements,huOnlyArray, tempArray);
                
        delete[] tempArray;        
        

       return; 
    }

//returns corresponding material properties for material which matches passed Houndsfieldunit (dHUnit)
    void inputClass::getOneMaterialPropertiesFromHU(Double_t dHUnit, Double_t &densityMaterial, Double_t &radiationLengthMaterial, 
                                          Double_t &excitationEnergyMaterial, Double_t &materialZARatio)
    {
        Int_t huIndex;
        //find HU material in table and return index
        huIndex=getNearestElementIndex(numberOfHUDataElements,huOnlyArray, dHUnit);
//        
//        huIndex=TMath::BinarySearch(numberOfHUDataElements,huOnlyArray, dHUnit);
//        if (huIndex<0) huIndex=0;
//        else if (huIndex>=numberOfHUDataElements) huIndex=numberOfHUDataElements-1;
        
//        cout<<"huIndex "<<huIndex<<" numberOfHUDataElements "<<numberOfHUDataElements<<" dHUnit "<<dHUnit<<endl;
        //read out material properties at given index
        radiationLengthMaterial=huDataArray[huIndex][2];
        excitationEnergyMaterial=huDataArray[huIndex][4]/1000000.;//because Pencil Beam requires MeV
        materialZARatio=huDataArray[huIndex][3];
//        densityMaterial=huDataArray[huIndex][1];
        densityMaterial=densityLUT->getData(dHUnit);
        
        return;
    }    
    
    //returns corresponding material properties for material which matches passed Houndsfieldunit (dHUnit)
    void inputClass::getMaterialDensityFromHU(Double_t dHUnit, Double_t &densityMaterial)
    {
//        Int_t huIndex;
//        //find HU material in table and return index
//        huIndex=TMath::BinarySearch(numberOfHUDataElements,huOnlyArray, dHUnit);
//        if (huIndex<0) huIndex=0;
//        else if (huIndex>=numberOfHUDataElements) huIndex=numberOfHUDataElements-1;
//        densityMaterial=huDataArray[huIndex][1];
        
        densityMaterial=densityLUT->getData(dHUnit);
//        cout<<"searched dHUnit: "<<dHUnit<<" density: "<<densityMaterial<<endl;
        
        return;
    }    
    
    
    //returns element which matches the value, or the one immediately before
    int inputClass::getNearestElementIndex(int numberOfElements,double* dataArray, double searchValue)
    {
        Int_t index;
        //find HU material in table and return index
        index=TMath::BinarySearch(numberOfElements,dataArray, searchValue);
        if (index<0) index=0;
        else if (index>=numberOfElements) index=numberOfElements-1;
        
//        //check if element is the closest match
//        if(index+1 < numberOfElements)
//        {
//            if ( abs(dataArray[index]- searchValue) > abs(dataArray[index+1]- searchValue)  ) 
//            {index = index+1;
////            cout<<"using closer index. searched: "<<searchValue<<" found: "<<dataArray[index]<<" instead of: "<<dataArray[index-1]<<endl;
//            }
////            else cout<<"using index. searched: "<<searchValue<<" found: "<<dataArray[index]<<endl;
//        }
        
        return index;
    }
    

//------------------------------------------------------------------------------------------------
//------------------------END OF CLASS FUNCTIONS
//------------------------------------------------------------------------------------------------


#endif	/* INPUTCLASSFUNCTIONS */
