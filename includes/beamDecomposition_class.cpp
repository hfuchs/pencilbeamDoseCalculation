//------------------------------------------------------------------------------
//compile
//        g++ `root-config --cflags --glibs` -lMinuit fitmultiGaussV14.cpp
//------------------------------------------------------------------------------
#include <beamDecomposition_class.h>
    
//constructor
GaussFitClass::GaussFitClass() 
{
    constPI=3.141592653589793238;
}

//returns the value of the sum multiple Gaussians at a specific point
Double_t GaussFitClass::myMultiGauss(Double_t x,std::vector<Double_t> &mus,std::vector<Double_t> &sigmas,std::vector<Double_t> &constants)
    {
        Double_t sum=0;
        for (Int_t i=0;i<(Int_t)mus.size();i++)
        {
            //cout<<"sum:"<<sum<<std::endl;
            //cout<<"i"<<i<<"constants"<<constants.at(i)<<"mu"<<mus.at(i)<<"sigma"<<sigmas.at(i)<<endl;
            //sum=sum+constants.at(i) * exp( -0.5 * (x-mus.at(i))*(x-mus.at(i)) / (sigmas.at(i)*sigmas.at(i)) ) / (sqrt(2.*constPI)*sigmas.at(i));
            sum=sum+constants.at(i)*TMath::Gaus(x, mus.at(i), sigmas.at(i), true);
            
            //cout<<"Gauss "<<constants.at(i)*TMath::Gaus(x, mus.at(i), sigmas.at(i), true)<<" x: "<<x<<" mus.at("<<i<<"): "<<mus.at(i)<<" sigmas.at(i) " <<mus.at(i)<<" constants.at(i) "<<constants.at(i);
        }     
        //cout<<endl;
        //cout<<"sum "<<sum<<endl;
//        cout<<"constants: "<<constants.at(0)<<endl;
        return sum;
    }


//Returns the chisquared value of the difference between realfkt and fitted functions
Double_t GaussFitClass::myfcn(const Double_t *par)
{
   Int_t i,j;
   Double_t x;

   std::vector<Double_t> mus(numOfSubGaussians);
   std::vector<Double_t> sigmas(numOfSubGaussians);
   std::vector<Double_t> constants(numOfSubGaussians);
   //cout<<"1:"<<par[1]<<endl;
   //extract mus,sigmas and constants from parameter array

Double_t intensityParameter;
intensityParameter=par[0];
   j=0;
   for(i=0;i<halfBeamNum;i++)
   {   
       //only the sub Gaussians with positive mu are in the data vectors, negative mus have to be 
       //created locally

       //if there is an uneven number, the first sub beam is at zero position and therefore
       //does not need to be doubled
       if(i==0 && (numOfSubGaussians%2)!=0)
       {
           //cout<<i<<" Parameter: "<<par[i]<<" "<<par[i+1]<<" "<<par[i+2]<<std::endl;
           mus.at(j)=fixedMu.at(i);
           sigmas.at(j)=par[i+1];
           constants.at(j)=correlationCoeffForC.at(i)*intensityParameter;
//               cout<<"Durchlauf:"<<counter<<"Vector "<<mus.at(j)<<" "<<sigmas.at(j)<<" "<<constants.at(j)<<std::endl;
       }
       else
       {
           mus.at(j)=fixedMu.at(i);
           sigmas.at(j)=par[i+1];
           constants.at(j)=correlationCoeffForC.at(i)*intensityParameter;
//                cout<<"Durchlauf:"<<counter<<"Vector "<<mus.at(j)<<" "<<sigmas.at(j)<<" "<<constants.at(j)<<std::endl;
           j++;

           //create negative mu Gauss
           mus.at(j)=fixedMu.at(i)*(-1.0);
           sigmas.at(j)=par[i+1];
           constants.at(j)=correlationCoeffForC.at(i)*intensityParameter;    
//                cout<<"Durchlauf:"<<counter<<"Vector "<<mus.at(j)<<" "<<sigmas.at(j)<<" "<<constants.at(j)<<std::endl;
       }

       //cout<<"i:"<<i<<std::endl;
       //cout<<i<<" "<<par[i]<<" "<<par[i+1]<<" "<<par[i+2]<<std::endl;
       //cout<<"Durchlauf:"<<counter<<"mu "<<mus.at(j)<<" sig "<<sigmas.at(j)<<" c "<<constants.at(j)<<std::endl;
       //cout<<"Durchlauf:"<<counter<<"Input "<<par[i]<<" "<<par[i+1]<<" "<<par[i+2]<<std::endl;
       j++;
   }
   //cout<<"minimizing:"<<endl;
   //calculate chisquare
   Double_t chisq = 0;
   Double_t delta;
   for (i=0;i<numDataPoints; i++) 
   {
     x=startVal+i*stepSize;
     //x=0.0+i*stepSize;
     
     delta  = realData.at(i)-myMultiGauss(x,mus,sigmas,constants);
     //std::cout<<"x "<<x<<std::endl;
     chisq += delta*delta;
//         if(i==round(numDataPoints/2))
//         {
//         for(Int_t j=0;j<(Int_t)mus.size();j++) cout<<" sigma"<<j<<": "<<sigmas.at(j);
//         cout<<"IntensityParameters "<<intensityParameter<<" chisq :"<<chisq<<" sum: "<<myMultiGauss(x,mus,sigmas,constants)<<std::endl;
//         }
   }
   //will be returned to Minuit
   //cout<<"run: "<<counter<<" chissquare "<<chisq<<" starval: "<<startVal<<" x "<<x<<std::endl;
   counter++;
   return chisq;
}

//sets step size
//should be factor 5 smaller than calculation step size
Double_t GaussFitClass::setStepSize(Double_t stepSizeToSet)
{
    stepSize=stepSizeToSet;
    return stepSize;
}

//sets the data of initial beam shape from function to realData array
//mu of initial Gauss has to be at position 0
void GaussFitClass::setBeamShapeFromGaussFktToRealData(Double_t initSigma,Double_t stepSizeToSet)
{
//    Double_t initSigma;
   
    //convert passed FWHM to sigma
//    initSigma=FWHM/( 2*sqrt(2*log(2)) );

    stepSize=stepSizeToSet;

    Double_t range;
    //calculation grid should be 10 times sigma
    range=initSigma*10;
    numDataPoints=ceil(range/stepSize);

    Double_t x,x0;
    x0=0.0;
    startVal=x0-(Double_t)numDataPoints/2.*(Double_t)stepSize;
   realData.resize(numDataPoints); 
   for (Int_t i=0;i<numDataPoints;i++)//xdim
    {
     x=startVal+i*stepSize;  
     realData.at(i)=TMath::Gaus(x, x0, initSigma, true);
    }
    return;
}


//sets the real data array
//should be a factor of 5 higher resolution than calculation step size
//set the initial beam shape data taken from an array to realData array
//Points have to be equally spaced with step size stepSizeOfDataPoints
void GaussFitClass::setBeamShapeFromDataArrayToRealData(Double_t *initialBeamShapeDataArray,Int_t NumDataPointsInArray,Double_t stepSizeOfDataPoints)
{
    this->setStepSize(stepSizeOfDataPoints);
    numDataPoints=NumDataPointsInArray;
    Double_t x0=0.0;
    startVal=x0-(Double_t)numDataPoints/2.*(Double_t)stepSize;
    realData.resize(numDataPoints); 
    for (Int_t i=0;i<numDataPoints;i++)//xdim
    {
     realData.at(i)=initialBeamShapeDataArray[i];
    }
    return;
}

void GaussFitClass::numericalFit(Int_t numOfSubGaussiansToSet, Double_t initSigma, std::vector<Double_t>& output)
{
   Int_t i,j;
   counter=0;

    numOfSubGaussians=numOfSubGaussiansToSet;
    //Center of calculation grid is at 0.0
    //Double_t x0=0.0;//not needed, statement above still valid
    
    this->setStepSize(0.1);
    this->setBeamShapeFromGaussFktToRealData(initSigma,0.1);
    
    //check if realData array is set
    if(realData.size()<=0) 
    {
        std::cout<<"ERROR: GausFitClass: numericalFit: realData Array is empty."<<std::endl;
        exit(-1);
    }

   //we want a symmetric distribution
   //divide by 2 and round up, so if halfBeamNum is odd, inital beam Number was uneven too 
   halfBeamNum = ceil((Double_t)numOfSubGaussians/2.);

//SELECT THE MINIMIZER       
   ROOT::Math::Minimizer* myMiuit = 
          ROOT::Math::Factory::CreateMinimizer("Minuit", "Migrad");
//   ROOT::Math::Minimizer* myMiuit = 
//          ROOT::Math::Factory::CreateMinimizer("Minuit2", "Migrad");
//   ROOT::Math::Minimizer* myMiuit = 
//          ROOT::Math::Factory::CreateMinimizer("Minuit2", "Simplex");
//   ROOT::Math::Minimizer* myMiuit = 
//          ROOT::Math::Factory::CreateMinimizer("Minuit2", "Combined");
//   ROOT::Math::Minimizer* myMiuit = 
//          ROOT::Math::Factory::CreateMinimizer("Minuit2", "Scan");
//   ROOT::Math::Minimizer* myMiuit = 
//          ROOT::Math::Factory::CreateMinimizer("Minuit2", "Fumili");
//   ROOT::Math::Minimizer* myMiuit = 
//          ROOT::Math::Factory::CreateMinimizer("GSLMultiMin", "ConjugateFR");
//   ROOT::Math::Minimizer* myMiuit = 
//          ROOT::Math::Factory::CreateMinimizer("GSLMultiMin", "ConjugatePR");
//   ROOT::Math::Minimizer* myMiuit = 
//          ROOT::Math::Factory::CreateMinimizer("GSLMultiMin", "BFGS");
//   ROOT::Math::Minimizer* myMiuit = 
//          ROOT::Math::Factory::CreateMinimizer("GSLMultiMin", "BFGS2");
//   ROOT::Math::Minimizer* myMiuit = 
//          ROOT::Math::Factory::CreateMinimizer("GSLMultiMin", "SteepestDescent");
//   ROOT::Math::Minimizer* myMiuit = 
//          ROOT::Math::Factory::CreateMinimizer("GSLMultiFit", "");
//   ROOT::Math::Minimizer* myMiuit = 
//          ROOT::Math::Factory::CreateMinimizer("GSLSimAn", "");

   myMiuit->SetMaxFunctionCalls(10000000);
   myMiuit->SetMaxIterations(10000000);
   
//   myMiuit->SetMaxFunctionCalls(100000);
//   myMiuit->SetMaxIterations(100000);
   myMiuit->SetTolerance(0.001);
   myMiuit->SetPrintLevel(0);

   //create minimization function
   ROOT::Math::Functor f(this, &GaussFitClass::myfcn,halfBeamNum+1);
   myMiuit->SetFunction(f);

//      printf("initSigma: %f x0: %f\n",initSigma,x0);
   Double_t sigmaStart[halfBeamNum],intensityParameterStartValue;
// Set starting values and step sizes for parameters
   fixedMu.clear();
   correlationCoeffForC.clear();
   fixedMu.resize(halfBeamNum); 
   correlationCoeffForC.resize(halfBeamNum);

   Int_t divider,upperLimit,jIncrement;
   //Double_t intensityAtX0=TMath::Gaus(x0, x0, initSigma, true);
//       cout<<"intensity at x0:"<<intensityAtX0<<std::endl;
   j=1;
   upperLimit=4;//4
   divider=2;//2
   jIncrement=1;
   //Upper limit divided through divider,
   //results in the maximum distance in sigma from the center 
   //at which sub-beams will be positioned
   //e.G: sigma=3 cm, upperLimit=4,divider=2 => beams up to 2 sigma away -> 3cm
   //sigma=5 cm, upperLimit=4,divider=2 => beams up to 2 sigma away -> 5cm
   //sigma=3 cm, upperLimit=6,divider=2 => beams up to 3 sigma away -> 9cm

   Int_t index,centerIndex;
   for(i=0;i<halfBeamNum;i++)
   {
       if(i==0 && (numOfSubGaussians%2)!=0) fixedMu.at(i)=0.0;
       else
       {
           fixedMu.at(i)=j*initSigma/divider;

           if(j>=upperLimit) 
           {        
               j=1;
               divider=divider*2;
               upperLimit=upperLimit*2;
               jIncrement=2;
           }
           else  j=j+jIncrement;
       }

//           printf("i:%i muStart: %f \n",i,fixedMu.at(i));
       
       //Set Starting values for optimization for sigma
       sigmaStart[i] = (Double_t) initSigma / numOfSubGaussians;

       //calculate intensity proportion for each subGaussian
       //index is the position of the center of the sub beam in index coordinates 
       index= round( numDataPoints/2 )+ round( fixedMu.at(i)/stepSize );
       //center of the initial beam is in the center of the data array
       centerIndex=round( numDataPoints/2 );
       //cout<<"numDataPoints"<<numDataPoints<<" stepSize"<<stepSize<<" fixedMu.at(i))/stepSize  "<<fixedMu.at(i)/stepSize<<" numDataPoints/2 "<<numDataPoints/2<<endl;
       
       //calculate intensity proportion for each subGaussian
       //correlationCoeffForC is intensity proportion factor between initial beam shape and sub beam both at center position
       correlationCoeffForC.at(i)=realData.at(index)/realData.at(centerIndex);
       //cout<<"localIndex "<<index<<" localValue: "<<realData.at(index)<<" centerIndex: "<<centerIndex<<" centerValue "<<realData.at(centerIndex);
       
       //global intensity parameter, only this one will be changed. Ratio within sub beams is unchanged
       //intensityParameterStartValue=realData.at(index);
       intensityParameterStartValue=1;
       
       //cout<<"i: "<<i<<" fixedMu.at(i) "<<fixedMu.at(i)<<" index: "<<index<<" localValue: "<<realData.at(index)<<" correlationCoeffForC "<<correlationCoeffForC.at(i)<<" intensityParameterStartValue "<<intensityParameterStartValue<<endl;

       //cout<<"fixedMu.at(i) "<<fixedMu.at(i)<<" index: "<<index<<" correlationCoeffForC "<<correlationCoeffForC.at(i)<<" intensityParameterStartValue "<<intensityParameterStartValue<<endl;

   }
   //necessary for naming
   string tempVarName;
   std::stringstream convertInt;
   tempVarName.clear();


   //SET MINIMIZING PARAMETERS
   myMiuit->SetLimitedVariable(0,"intensity",intensityParameterStartValue, 0.001,0.0,100.0);//Intensity is limited to be above zero
   //myMiuit->SetFixedVariable(0,"intensity", intensityParameterStartValue);
   j=0;
   for(i=0;i<halfBeamNum;i++)//Sets parameters for all subGaussians
   {      

    tempVarName.clear();
    convertInt.str("");
    convertInt << j;

    tempVarName="sigma";
    tempVarName=tempVarName+convertInt.str();    
    myMiuit->SetLimitedVariable(i+1,tempVarName,sigmaStart[j], 0.001,0.01,initSigma);//sigma is limited to be smaller than initial beam sigma

    j++;
   }

// Now ready for minimization step
myMiuit->Minimize(); 

   const double *xs = myMiuit->X();

   //Create Gaussian parameters
   Double_t intensityParameter;
   Double_t mu,sigma,c;
   output.clear();

   intensityParameter=xs[0];
   //cout<<"intensityParameter"<<xs[0]<<endl;

   for(i=0;i<halfBeamNum;i++)
   {   
       //only the subgaussians with positive mu are in the vector, negative mus have to be 
       //created locally

       //if there is an uneven number, the first sub beam is at cero position and therefore
       //does not need to be doubled
       if(i==0 && (numOfSubGaussians%2)!=0)
       {
           //cout<<i<<" Parameter: "<<par[i]<<" "<<par[i+1]<<" "<<par[i+2]<<std::endl;
           mu=fixedMu.at(i);
           sigma=xs[i+1];
           c=correlationCoeffForC.at(i)*intensityParameter;
           output.push_back(mu);
           output.push_back(sigma);
           output.push_back(c);//intensity
                //cout<<"Durchlauf:"<<i<<"Vector mu "<<mu<<" sig "<<sigma<<" c "<<c<<std::endl;
       }
       else
       {
           mu=fixedMu.at(i);
           sigma=xs[i+1];
           c=correlationCoeffForC.at(i)*intensityParameter;
           output.push_back(mu);
           output.push_back(sigma);
           output.push_back(c);
//               cout<<"Durchlauf:"<<i<<"Vector mu "<<mu<<" sig "<<sigma<<" c "<<c<<std::endl;

           //create negative mu Gauss
           mu=fixedMu.at(i)*(-1.0);
           sigma=xs[i+1];
           c=correlationCoeffForC.at(i)*intensityParameter;///realData.at(centerIndex)*realData.at(index);
           output.push_back(mu);
           output.push_back(sigma);
           output.push_back(c);    
//               cout<<"Durchlauf:"<<i<<"Vector mu "<<mu<<" sig "<<sigma<<" c "<<c<<std::endl;
       }        
   }
   
   
   
//   for (Int_t k=0; k<(Int_t) output.size();k=k+3)
//   {
//           cout<<"Sub Beam "<<k/3<<": Constant: "<<output.at(k+2)<<" Mu: "<<output.at(k)<<" Sigma: "<<output.at(k+1)<<std::endl;    
//   }
  
       //Write outputFile
//       Double_t temp,x;
//       //Write Gauss parameters to shell
//       for (j=0;j<output.size();j=j+3)
//         {
//             mu=output.at(j);
//             sigma=output.at(j+1);
//             //cout<<"creating Gausscurves with:"<<i<<"mu "<<mu<<" sigma "<<sigma<<" c "<<c<<std::endl;
//         }
       
//    // open ASCII file
//    FILE *fp = fopen("dataOut.dat","w");
//    if(!fp) 
//    {
//        printf("File %s could not be opened. Aborting. \n","dataOut.dat");
//        exit(-1);
//        return ;
//    }
//       
//        for (i=0;i<numDataPoints;i++)//xdim
//        {
//         x=startVal+i*stepSize;  
//         temp=realData.at(i);
//         
//         fprintf(fp,"%f %f ",x,temp); 
//         
//             for (j=0;j<(Int_t) output.size();j=j+3)
//             {
//                 mu=output.at(j);
//                 sigma=output.at(j+1);
//                 c=output.at(j+2);
//                 temp=c*TMath::Gaus(x, mu, sigma, true);
//                 //cout<<"starval: "<<startVal<<" xo: "<<x0<<" initSigma: "<<initSigma<<" x: "<<x<<" val: "<<temp<<" mu "<<mu<<endl;               
//                 //cout<<"x "<<x<<endl;
//                 fprintf(fp,"%f ",temp); 
//             }
//
//            fprintf(fp,"\n"); 
//       }
//       fclose(fp);
}
    
//end of Class GaussFitClass



//int main()
//{
//    //ClassfitmultiGauss2DV1();
//    Double_t initSigmaX,initSigmaY;
//    Double_t initFluence=100000;
//    Double_t x0, y0;
//    Int_t numOfSubBeamsOneDim=2;
//    Double_t initEnergy=150.0;
//    x0=y0=0;
//    initSigmaX=initSigmaY=1.0;
//    
//    Double_t **beamDataArray = new Double_t*[numOfSubBeamsOneDim*numOfSubBeamsOneDim];
//    for (Int_t i=0;i<numOfSubBeamsOneDim*numOfSubBeamsOneDim;i++)//xdim
//    {
//            beamDataArray[i]=new Double_t[6];
//    }
//
//    beamDecomposition(beamDataArray, initSigmaX,initSigmaY,initFluence,
//        x0, y0, numOfSubBeamsOneDim, initEnergy);
//}