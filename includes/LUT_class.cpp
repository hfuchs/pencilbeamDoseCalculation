#ifndef LUTCLASS_CPP
#define	LUTCLASS_CPP
#include <LUT_class.h>


LUT::LUT() //default constructor
{
    //set the interpolator
//    interpolEdep=new ROOT::Math::Interpolator(0,ROOT::Math::Interpolation::kCSPLINE);
    interpolEdep=new ROOT::Math::Interpolator(0,ROOT::Math::Interpolation::kLINEAR);
    setParticleType(0);

//    setConfigFileName((char *)"dataAvailable.dat");
//    setFolderName((char *)"calibration/");
//    setDataFilePrefix((char *)"energydeposit_");
    setCorrectionFactorToCM(0.1);
    setStepSizeCalculation(0.05);//in cm    
    iDimensionOfArray=0;
    setDimension(10000);    
    setStepSizeDataInCM(0.01);//data is in mm //set step Size automatically corrects to cm
//    initializeLutArray();
    initializePenetrationDepthArray();
//    initializeRemainingEnergyArray();
//    initializeRawDataArray();
    totalDepositedEnergy=0;
    setNumbOfParticlesInSource(10000000);
    setNumbOfParticlesInBeam(1);
    targetEnergy=0.0;
}

LUT::LUT(Double_t targetEnergyToSet,Double_t stepSizeToSet,Int_t numbOfBeamParticlesToSet) //constructor
{
    //set the interpolator
    interpolEdep=new ROOT::Math::Interpolator(0,ROOT::Math::Interpolation::kLINEAR); 
    setParticleType(0);

    setFolderName((char *)"calibrationHelium/");
    setConfigFileName((char *)"dataAvailable.dat");
    setDataFilePrefix((char *)"energydeposit_");
    readInEnergies();//read in available energies
    setCorrectionFactorToCM(0.1);
    iDimensionOfArray=0;
    setDimension(10000);    
    setStepSizeDataInCM(stepSizeToSet);
    setStepSizeCalculation(0.05);//in cm
//    initializeLutArray();
    initializePenetrationDepthArray();
//    initializeRemainingEnergyArray();
//    initializeRawDataArray();
    totalDepositedEnergy=0;
    setNumbOfParticlesInSource(10000000);
    setNumbOfParticlesInBeam(numbOfBeamParticlesToSet);

    setTargetEnergy(targetEnergyToSet);
    calculateTotalDepositedEnergy();
    fillPenetrationDepthArray();
}

//destructor, deletes lutArray
LUT::~LUT()
{
    for(Int_t j=0; j<2; j++) delete[] lutArray[j];
    delete[] lutArray;
    
    for(Int_t j=0; j<2; j++) delete[] penetrationDepthArray[j];
    delete[] penetrationDepthArray;
    
    for(Int_t j=0; j<2; j++) delete[] remainingEnergyArray[j];
    delete[] remainingEnergyArray;
    
    for(Int_t j=0; j<energyArraySize; j++) 
    {
        delete[] rawDataArray[j][1];
        delete[] rawDataArray[j][0];
        delete[] rawDataArray[j];
    }
    delete[] rawDataArray;
        
    delete interpolEdep;
}

//copy constructor
LUT::LUT(const LUT& orig)
{
    numberOfAvailableEnergies=orig.numberOfAvailableEnergies;
    targetEnergy=orig.targetEnergy;
    stepSize=orig.stepSize;
    stepSizeCalculation=orig.stepSizeCalculation;            
    totalDepositedEnergy=orig.totalDepositedEnergy;
    maxDepositedEnergyAtCurrentEnergy=orig.maxDepositedEnergyAtCurrentEnergy;        
    correctionFactorToCM=orig.correctionFactorToCM; 
    rangeOfEnergy=orig.rangeOfEnergy;
    numbOfParticlesInSource=orig.numbOfParticlesInSource;
    numbOfBeamParticles=orig.numbOfBeamParticles;         
    percent40EdepPosition=orig.percent40EdepPosition;
    percent50EdepPosition=orig.percent50EdepPosition;
    percent60EdepPosition=orig.percent60EdepPosition;
    percent70EdepPosition=orig.percent70EdepPosition;
    percent100EdepPosition=orig.percent100EdepPosition;
    diff40to50PercentEdep=orig.diff40to50PercentEdep;
    diff50to60PercentEdep=orig.diff50to60PercentEdep;
    diff60to70PercentEdep=orig.diff60to70PercentEdep;
    edepAt40Pos=orig.edepAt40Pos;
    edepAt50Pos=orig.edepAt50Pos;
    edepAt60Pos=orig.edepAt60Pos;
    whichParticle=orig.whichParticle;
    strcpy ( folder, orig.folder);
    strcpy ( configFileName, orig.configFileName);
    strcpy ( dataFilePrefix, orig.configFileName);   
    
    //set the interpolator
    interpolEdep=new ROOT::Math::Interpolator(0,ROOT::Math::Interpolation::kLINEAR);
    setDimension(orig.iDimensionOfArray);  
    iDimensionOfArray=orig.iDimensionOfArray; 

    initializePenetrationDepthArray();
    
    //copy data
    //    Double_t energyAvailArray[energyArraySize];
    for(int i=0; i<energyArraySize;i++)
    {
        energyAvailArray[i]=orig.energyAvailArray[i];
    }
    
    //Double_t **lutArray;//contains depth and energy deposition in water
    for(int i=0; i<iDimensionOfArray;i++)
    {
        lutArray[0][i]=orig.lutArray[0][i];
        lutArray[1][i]=orig.lutArray[1][i];
    }
    
    //Double_t **remainingEnergyArray;//contains depth and remaining energy of particle
    for(int i=0; i<iDimensionOfArray;i++)
    {
        remainingEnergyArray[0][i]=orig.remainingEnergyArray[0][i];
        remainingEnergyArray[1][i]=orig.remainingEnergyArray[1][i];
    }
    
    //Double_t **penetrationDepthArray;//contains energy and penetration depth in water    
    for(int i=0; i<energyArraySize;i++)
    {
        penetrationDepthArray[0][i]=orig.penetrationDepthArray[0][i];
        penetrationDepthArray[1][i]=orig.penetrationDepthArray[1][i];
    }

//    Double_t energyAvailArray[energyArraySize];
    for(int i=0; i<energyArraySize;i++) energyAvailArray[i]=orig.energyAvailArray[i];
    
    
//    Double_t ***rawDataArray;
    for(int i=0;i<energyArraySize;i++)
    {
       for(int j=0;j<2;j++)
       {
        for(int k=0;k<iDimensionOfArray;k++)
        {
           rawDataArray[i][j][k]=orig.rawDataArray[i][j][k];
        }
       }
    }
    
}

//
//LUT& LUT::operator= (const LUT &orig)
//{
//    //set the interpolator
//    interpolEdep=new ROOT::Math::Interpolator(0,ROOT::Math::Interpolation::kLINEAR);
//    setDimension(orig.iDimensionOfArray);    
//    initializePenetrationDepthArray();
//    
//    //copy data
//    //Double_t **lutArray;//contains depth and energy deposition in water
//    for(int i=0; i<iDimensionOfArray;i++)
//    {
//        lutArray[0][i]=orig.lutArray[0][i];
//        lutArray[1][i]=orig.lutArray[1][i];
//    }
//    
//    //Double_t **remainingEnergyArray;//contains depth and remaining energy of particle
//    for(int i=0; i<iDimensionOfArray;i++)
//    {
//        remainingEnergyArray[0][i]=orig.remainingEnergyArray[0][i];
//        remainingEnergyArray[1][i]=orig.remainingEnergyArray[1][i];
//    }
//    
//    //Double_t **penetrationDepthArray;//contains energy and penetration depth in water    
//    for(int i=0; i<energyArraySize;i++)
//    {
//        remainingEnergyArray[0][i]=orig.remainingEnergyArray[0][i];
//        remainingEnergyArray[1][i]=orig.remainingEnergyArray[1][i];
//    }
//
////    Double_t energyAvailArray[energyArraySize];
//    for(int i=0; i<energyArraySize;i++) energyAvailArray[i]=orig.energyAvailArray[i];
//    
//    
////    Double_t ***rawDataArray;
//    for(int i=0;i<energyArraySize;i++)
//    {
//       for(int j=0;j<2;j++)
//       {
//        for(int k=0;k<iDimensionOfArray;k++)
//        {
//           rawDataArray[i][j][k]=orig.rawDataArray[i][j][k];
//        }
//       }
//    }
//    
//    return *this;
//}


//initialize the lut array
//now 8 entries
//0     Depth 
//1     Deposited Energy in MeV
//2     GausAmplidude
//3     GausMu
//4     GausSigma
//5     VoigtAmplitude
//6     VoigtSigma 
//7     VoigtLg
void LUT::initializeLutArray()
{
    lutArray = new Double_t*[2];
    lutArray[0] = new Double_t[iDimensionOfArray] ;
    lutArray[1] = new Double_t[iDimensionOfArray] ;
//    for(Int_t i=0; i<iDimensionOfArray; ++i) 
//    {
//        for(Int_t j=0; j<8; j++) lutArray[j][i] = 0;
//    }
    return;
}

void LUT::deleteLutArray()
{
    if (iDimensionOfArray<=0) return;
    delete[] lutArray[1];
    delete[] lutArray[0];

    delete[] lutArray;
    return;
}


void LUT::initializeRawDataArray()
{
    rawDataArray = new Double_t**[energyArraySize];
    for(int i=0;i<energyArraySize;i++)
    {
       rawDataArray[i] = new Double_t*[2];
       for(int j=0;j<2;j++)
       {
        rawDataArray[i][j] = new Double_t[iDimensionOfArray] ;
        for(int k=0;k<iDimensionOfArray;k++)
        {
           rawDataArray[i][j][k]=0.0;
        }
       }
    }
    return;
}

void LUT::deleteRawDataArray()
{
    if (energyArraySize<=0) return;
    if (iDimensionOfArray<=0) return;
    
    for(Int_t j=0; j<energyArraySize; j++) 
    {
        delete[] rawDataArray[j][1];
        delete[] rawDataArray[j][0];
        delete[] rawDataArray[j];
    }
    delete[] rawDataArray;
    return;
}

void LUT::initializeRemainingEnergyArray()
{
    remainingEnergyArray = new Double_t*[2];
    remainingEnergyArray[0] = new Double_t[iDimensionOfArray] ;
    remainingEnergyArray[1] = new Double_t[iDimensionOfArray] ;
    for(Int_t i=0; i<iDimensionOfArray; ++i) 
    {
        remainingEnergyArray[0][i] = 0.;
        remainingEnergyArray[1][i] = 0.;
    }
    return;
}

void LUT::deleteRemainingEnergyArray()
{   
   if (iDimensionOfArray<=0) return;

    delete[] remainingEnergyArray[1];
    delete[] remainingEnergyArray[0];

    delete[] remainingEnergyArray;
    return;
}

void LUT::initializePenetrationDepthArray()
{
    penetrationDepthArray = new Double_t*[2];
    penetrationDepthArray[0] = new Double_t[energyArraySize] ;
    penetrationDepthArray[1] = new Double_t[energyArraySize] ;
    for(Int_t i=0; i<energyArraySize; ++i) 
    {
      penetrationDepthArray[0][i] = 0.0;//contains energy
      penetrationDepthArray[1][i] = 0.0;//contains penetration depth in water
    }
    return;
}

void LUT::deletePenetrationDepthArray()
{   
    if (iDimensionOfArray<=0) return;

    delete[] penetrationDepthArray[1];
    delete[] penetrationDepthArray[0];
    
    delete[] penetrationDepthArray;
    return;
}

void LUT::readInAllDataFilesIntoRawDataArray()
{
    char fileNameLowerEnergy[filenameTotalSize];

    //read in data files
    //and store data in rawDataArray
    //--------------------------------------------------------------------------

    for(Int_t lowerEnergyIdx=0;lowerEnergyIdx<energyArraySize;lowerEnergyIdx++)
    {
        if (energyAvailArray[lowerEnergyIdx]<=0.) continue;//skip if energy does not exist
        
        //create file name
        sprintf(fileNameLowerEnergy,"%s%s%03.0f%s",folder,dataFilePrefix,energyAvailArray[lowerEnergyIdx],".dat");
        //read in Lower energy data                 
        readInDataFromFile(fileNameLowerEnergy,rawDataArray[lowerEnergyIdx]);
//        readOutLUT(fileNameLowerEnergy,rawDataArray[lowerEnergyIdx]);
    }
   return;
}

void LUT::retrieveData(Int_t targetEnergyIdx,Double_t** dataArray)
{
//    cout<<"retrieving data for energy "<< energyAvailArray[targetEnergyIdx]<<" at index "<<targetEnergyIdx<<endl;
//    int idx=0;
//        idx=TMath::BinarySearch(energyArraySize,energyAvailArray, targetEnergy);
        if ( targetEnergyIdx>=energyArraySize || targetEnergyIdx<0 ) //check whether value exists, abort if not
        {
            cerr<<"ERROR: Requested energy of "<<targetEnergy<<" MeV was not found. Aborting."<<endl;
            exit(-1);
        }

        memcpy( dataArray[0], rawDataArray[targetEnergyIdx][0], iDimensionOfArray * sizeof(Double_t) );
        memcpy( dataArray[1], rawDataArray[targetEnergyIdx][1], iDimensionOfArray * sizeof(Double_t) );
//        for(int i=0;i<iDimensionOfArray;i++)
//        {
//          dataArray[0][i]= rawDataArray[targetEnergyIdx][0][i];
//          dataArray[1][i]= rawDataArray[targetEnergyIdx][1][i];
//        }
//        memcpy (dest,source,strlen(str1)+1);
//        dataArray=rawDataArray[targetEnergyIdx];
  return;
}


Int_t LUT::setNumbOfParticlesInSource(Int_t particles)
{
    numbOfParticlesInSource=particles;
    return numbOfParticlesInSource;
}

Int_t LUT::setNumbOfParticlesInBeam(Int_t particles)
{
    numbOfBeamParticles=particles;
    return numbOfBeamParticles;
}

Double_t LUT::setCorrectionFactorToCM(Double_t correction)
{
    correctionFactorToCM=correction;
    return correctionFactorToCM;
}

Int_t LUT::setDimension(Int_t dimension)
{
        deleteRemainingEnergyArray();
        deleteRawDataArray();
        deleteLutArray();
    iDimensionOfArray=dimension;
        initializeLutArray();
        initializeRemainingEnergyArray();
        initializeRawDataArray();
    
    return iDimensionOfArray;
}

//take sets to Step Size in increments of data (if mm) then usually mm
Double_t LUT::setStepSizeData(Double_t step)
{
    stepSize=step;// /correctionFactorToCM;
    return stepSize;
}

//takes cm and converts to correct step size
Double_t LUT::setStepSizeDataInCM(Double_t step)
{
    stepSize=step/correctionFactorToCM;
    return stepSize;
}

Double_t LUT::setStepSizeCalculation(Double_t step)
{
    stepSizeCalculation=step;// /correctionFactorToCM;
    return stepSizeCalculation;
}

void LUT::setConfigFileName(char* filename)
{
    strcpy ( configFileName, filename);
    readInEnergies();//read in available energies
    return;
}
//sets Folder Name name must include backslash
void LUT::setFolderName(char* foldername)
{
    strcpy ( folder, foldername);
    return;
}

void LUT::setDataFilePrefix(char* filePrefix)
{
    strcpy ( dataFilePrefix, filePrefix);
    return;
}

//set data to interpolation function
void LUT::setInterpolEdepData()
{
//    cout<<"Setting Interpol Edep data"<<endl;
//    for (Int_t i=0;i<iDimensionOfArray;i++)
//    {
//        //cout<<lutArray[0][i]<<" data: "<<lutArray[1][i]<<endl;
//    }
    interpolEdep->SetData(iDimensionOfArray, lutArray[0], lutArray[1]);
//    cout<<"Finished setting Interpol Edep data"<<endl;
    return;
}


//returns current target energy
Double_t LUT::getTargetEnergy()  
{
 return targetEnergy;   
}

//returns maxDepositedEnergyAtCurrentEnergy in current LUT
Double_t LUT::getMaxDepositedEnergy()  
{
 return maxDepositedEnergyAtCurrentEnergy;   
}


//sets target energy and recalculates Look Up Table
void LUT::setTargetEnergy(Double_t energyToSet)  
{
//check if energy is already set, if yes return
if (targetEnergy==energyToSet) return;
//    readInEnergies();//read in available energies
	Int_t idx;
	//find next energy
	idx=TMath::BinarySearch(energyArraySize,energyAvailArray, energyToSet);
//        if (idx>=energyArraySize) idx=energyArraySize-1;//ensure index is within bounds of array
//        if (idx<0) idx=0;//ensure index is within bounds of array
	//if energy found is lower than available energy, print error and use lowest available
	if(energyAvailArray[idx]<=0)
	{
	 std::cerr<<"WARNING: LUT class Requested energy: "<<energyToSet<<" too low, using "<< energyAvailArray[idx+1]<<" instead!"<<endl;
	 targetEnergy=energyAvailArray[idx+1];
		//check if energy is already set, if yes return
	 if (targetEnergy==energyAvailArray[idx+1]) return;
	}
        else if(idx>=energyArraySize)
        {
         std::cerr<<"WARNING: LUT class  Requested energy: "<<energyToSet<<" too high, using "<< energyAvailArray[energyArraySize-1]<<" instead!"<<endl;
         targetEnergy=energyAvailArray[energyArraySize-1];
        }
	else targetEnergy=energyToSet;
    calculateLUT();
    setInterpolEdepData();
    findMaxEdep();
    calcPeakRangeOfEnergy();
    determinePositionsOnCurve();
    calculateTotalDepositedEnergy();
    calculateRemainingEnergyArray();
 return;   
}

//returns interpolated energy deposition
Double_t LUT::getEdep(Double_t position)
{
    //cout << stepSizeCalculation<<" "<<stepSize<<endl;
    Double_t edep=0.0;
    
    if (position < 0.0 || position > lutArray[0][iDimensionOfArray-1]) 
    {
        std::cerr<<"WARING: LUT class passed position "<<position<<" out of bounds. Returning 0."<<endl;
        return 0.;
    }
    
    edep=interpolEdep->Eval(position)*numbOfBeamParticles/numbOfParticlesInSource*stepSizeCalculation/stepSize/correctionFactorToCM;
    if (edep<0.) return 0.0;
    else return edep;
}

//returns energy of particle in current depth
Double_t LUT::getInDepthEnergy(Double_t position)
{
    Int_t currentIdx;
    //find current depth index
    currentIdx=TMath::BinarySearch(iDimensionOfArray,remainingEnergyArray[0], position);
//    if (currentIdx>=iDimensionOfArray) currentIdx=iDimensionOfArray-1;//ensure index is within bounds of array
//    else if (currentIdx<0) currentIdx=0;//ensure index is within bounds of array

    return remainingEnergyArray[1][currentIdx];
}

void LUT::calculateRemainingEnergyArray()
{
   Double_t untilNowDepositedEnergy=0.0;
   Double_t inDepthEnergy=0.0;
   for(Int_t i=0; i<iDimensionOfArray; i++) 
   {   
       inDepthEnergy=targetEnergy*(1.- (untilNowDepositedEnergy/totalDepositedEnergy) );
       remainingEnergyArray[0][i] = lutArray[0][i];
       remainingEnergyArray[1][i] = inDepthEnergy;

       untilNowDepositedEnergy=untilNowDepositedEnergy+lutArray[1][i];
    }
   return;
}

//void LUT::calculateRemainingEnergyArray()
//{
//   Double_t untilNowDepositedEnergy=0.0;
//   Double_t inDepthEnergy=0.0;
//    for(Int_t i=0; i<iDimensionOfArray; ++i) 
//    {
//        untilNowDepositedEnergy=0.0;
//        for(Int_t j=0; j<i; j++)
//        {
//          untilNowDepositedEnergy=untilNowDepositedEnergy+lutArray[1][j];  
//        }
//        inDepthEnergy=targetEnergy*(1-untilNowDepositedEnergy/totalDepositedEnergy);
//        remainingEnergyArray[0][i] = lutArray[0][i];
//        remainingEnergyArray[1][i] = inDepthEnergy;
//    }
//   return;
//}


////sum over the lutArray to get the total deposited Energy
Double_t LUT::findMaxEdep()
{
    Double_t max=0;
    for(Int_t i=0;i<iDimensionOfArray;i++)
    {
        if( lutArray[1][i]>max ) max=lutArray[1][i];
    }
    maxDepositedEnergyAtCurrentEnergy=max*numbOfBeamParticles/numbOfParticlesInSource*stepSizeCalculation/stepSize/correctionFactorToCM;
    //return max;

    return maxDepositedEnergyAtCurrentEnergy;
}





////sum over the lutArray to get the total deposited Energy
Double_t LUT::calculateTotalDepositedEnergy()
{
    Int_t i;
    totalDepositedEnergy=0.0;
    
    for(i=0;i<iDimensionOfArray;i++)
    {
        totalDepositedEnergy=totalDepositedEnergy+lutArray[1][i]; 
    }
//    cout<<"Totaldeposited energy determined to "<<totalDepositedEnergy<<endl;
    return totalDepositedEnergy;
}
       
//reads out lut array from file and returns only value part
void LUT::getLUTarraySplittedUp(Int_t targetEnergyIdx, Double_t *positionArray,Double_t *valueArray)
{
    
    if ( targetEnergyIdx>=energyArraySize || targetEnergyIdx<0 ) //check whether value exists, abort if not
        {
            std::cerr<<"ERROR: Requested energy of "<<targetEnergy<<" MeV was not found. Aborting."<<endl;
            exit(-1);
        }

    memcpy( positionArray, rawDataArray[targetEnergyIdx][0], iDimensionOfArray * sizeof(Double_t) );
    memcpy( valueArray, rawDataArray[targetEnergyIdx][1], iDimensionOfArray * sizeof(Double_t) );

 return;   
}

//functions reads in the available energies
//returns number of energies available, -1 on error
Int_t LUT::readInEnergies()
{
   char filename[filenameTotalSize];
   sprintf(filename,"%s%s",folder,configFileName);          
   Int_t i;     
   Int_t indexArray[energyArraySize];
   Char_t line[maxNumberOfCharsToRead];
   Double_t temlutArray[energyArraySize],energy;

   // open ASCII file
    FILE *fp = fopen(filename,"r");
    if(!fp) 
    {
        printf("File %s could not be opened. Aborting. \n",filename);
        exit(-1);
        return -1;
    }

    //initialize Arrays
    for(i=0;i<energyArraySize;i++)
    {
       energyAvailArray[i]=0;
       temlutArray[i]=0;
       indexArray[i]=0;
    }

    //read in available energies
    i=0;
    while (fgets(line,maxNumberOfCharsToRead,fp)) 
    {
        //skip line and go to next line if starting with #
        if (line[0] == '#') continue;
        //if (line[0] == '0') continue;
        sscanf(&line[0],"%lf",&energy);
        //Store read value in array
        //printf("%f \n",energy);
        temlutArray[i]=energy;
        i++;
    }

    //sort array
    TMath::Sort(energyArraySize, temlutArray, indexArray,false);
    for(i=0;i<energyArraySize;i++)
    {
       energyAvailArray[i]=temlutArray[indexArray[i]];
    }

   readInAllDataFilesIntoRawDataArray();

    return i;//number of energies set
}            



//Returns the number of rows in the file
//empty rows and rows starting with # are ignored
//-1 on error
Int_t LUT::countRowsInFile(char* filename)
{
    FILE *df;
    Int_t counter=0;
    char line[maxNumberOfCharsToRead];

    df=fopen(filename,"r");
    if(!df) 
    {
        printf("File %s could not be opened. Aborting. \n",filename);
        exit(-1);
        return -1;
    }

    //read in files
    //count line numbers
    while((fgets(line,maxNumberOfCharsToRead,df)) != NULL) 
    {
      if (line[0] == '#') continue;
      if (line[0] == '\n') continue;
      counter++;
    }
    fclose (df);
    //printf("%i lines found in %s\n",counter,filename);
 return counter;
}

//function reads in data file with two columns
//first column is length, second deposited energy
//function returns number of created entries
//dataArray[2][numberOfEntries]
//ATTENTION: Does only read iDimensionArray values from file, if more, data will be ignored
Int_t LUT::readInDataFromFile(char* filename,Double_t** dataArray)
{
//cout<<"readInDataFromFile"<<endl;
    FILE *df;
    Int_t i;
    char line[maxNumberOfCharsToRead];
    Double_t length,energy;

    df=fopen(filename,"r");
    //cout<<"filename "<<filename<<" df "<<df<<endl;
    if(!df) 
    {
        printf("File %s could not be opened. Aborting. \n",filename);
        exit(-1);
        return i;
    }

    //read in available energies
    i=0;
    //cout<<"starting data read in: "<<filename<<endl;
    while (fgets(line,maxNumberOfCharsToRead,df)) 
    {
        //0     Depth 
        //1     Deposited Energy in MeV
        
        //skip line and go to next line if starting with #
        if (line[0] == '#') continue;
        if (line[0] == '\n') continue;
        if (line[0] == ' ') continue;
        sscanf(&line[0],"%lf %lf",&length,&energy );
        //Store read value in array

        //printf("%lf %lf\n",length,energy);
        dataArray[0][i]=length*correctionFactorToCM;//to convert mm in cm
        dataArray[1][i]=energy;
//                    if (i==0) printf("%s \n",line);
//                    if (i>10000) printf("%s \n",line);
//                    if (i>10000) printf("%lf %lf\n",dataArray[0][i],dataArray[1][i]);
//                    if (i==0) printf("%lf %lf\n",dataArray[0][i],dataArray[1][i]);
        //printf("%i\n",i);
        //cout<<"i: "<<i<<" length: "<<dataArray[0][i]<<" edep "<<dataArray[1][i]<<endl;
        i++;
        if(i>=iDimensionOfArray) break;//if file is longer than iDimension abort read in
    }
        fclose (df);
        if (i<iDimensionOfArray) cout<<"LUT_class: "<<i<<" lines read from "<<filename<<" instead of maximum allowed "<<iDimensionOfArray<<"."<<endl;
    return i;
}       

//function reads in data file with multiple columns
//number of columns to read has to be passed to function
//function returns number of created entries
//dataArray[entriesPerLine][numberOfEntries]
//ATTENTION: Does only read iDimensionArray values from file, if more, data will be ignored
Int_t LUT::readInDataColFromFile(char* filename,Double_t** dataArray, Int_t numColumns, Int_t numLinesToStore)
{
    std::stringstream parseBuf;//Stringstream Buffer which will be parsed
    string inBuf;//String Buffer for reading
    fstream ifp;
    ifp.open(filename,ios::in);//open file
    
    if (!ifp) //if file can not be opened print error and exit
    {
        std::cerr<<"ERROR: Unable to open file: "<<filename<<endl;
        exit(-1);
        return 0;
    }
    
    Int_t linesRead=0;
    while (getline(ifp,inBuf) && linesRead<(numLinesToStore-1) )
    {
        if (inBuf[0]=='#') continue; //skip #lines
        if (inBuf[0]=='\n') continue; //skip empty lines

        parseBuf.str(inBuf);
        for (Int_t i=0;i<numColumns;i++) 
        {
          //cout<<"i "<<i<<endl;  
          parseBuf>>dataArray[i][linesRead];
        }
        ++linesRead;
        //cout<<"linesread: "<<linesRead<<endl;
        
    }
    ifp.close();
    return linesRead;
}

//reads in a data file,does not interpolate values to the correct spacing and dimension
//ATTENTION: correct spacing and dimension within data file has to be assured
void LUT::readOutLUT(char* filename,Double_t** dataArray)
{   
    //take data from raw data file
    readInDataFromFile(filename,dataArray);
    
return;    
}      

//takes two energy data files and interpolate the correct energy
//ATTENTION: requires all array to have the same dimensions
void LUT::interpolateEnergy(Double_t** interpolDataArray, Double_t energy)
{
    Int_t i,k,valueArrayDimension=0;
//    char filename[filenameTotalSize];
        //create position and value arrays
    //positions are always the same, therefore only one needed
    Double_t* positionArray = new Double_t[iDimensionOfArray];
    Double_t** valueArray = new Double_t*[energyArraySize];
    for (i=0;i<energyArraySize;i++)
    {
      valueArray[i] = new Double_t[iDimensionOfArray];
    }

    for (i=0;i<energyArraySize;i++)
    {
      if(energyAvailArray[i]<=0) {continue;}
      valueArrayDimension++;
    }
    //read in value arrays for all energies, skip if energy 0 or below
    //create data array with positions of BraggPeaks, sorted by energy.
    Double_t* braggPeakLocations = new Double_t[valueArrayDimension];
    Double_t* energiesForBraggPeakLocation = new Double_t[valueArrayDimension];
    Double_t* braggPeakEdeps = new Double_t[valueArrayDimension];
    
    k=0;
    for (i=0;i<energyArraySize;i++)
    { 
      if(energyAvailArray[i]<=0) { continue;}
      //valueArray[k] = new Double_t[iDimensionOfArray];
//      sprintf(filename,"%s%s%03.0f%s",folder,dataFilePrefix,energyAvailArray[i],".dat");
      
      getLUTarraySplittedUp(i,positionArray,valueArray[k]);
      //cout<<"read energy: "<<energyAvailArray[i]<<endl;
            
    // calculate max value and position (position of Bragg Peak)
      int idx=TMath::LocMax(iDimensionOfArray, valueArray[k]);
      if (idx>=iDimensionOfArray) idx=iDimensionOfArray-1;//ensure index is within bounds of array
      if (idx<0) idx=0;//ensure index is within bounds of array
     
      braggPeakLocations[k]=positionArray[idx];
      braggPeakEdeps[k]=valueArray[k][idx];//maxEdep
      energiesForBraggPeakLocation[k]=energyAvailArray[i];
      
      k++;
    }
    
    //valueArray[k] now contains all edep values of energy with index k
    
    //loop through all energies and calculate max value and position (position of Bragg Peak)

    //interpolate Bragg Peak location for target energy

    //initialize interpolation
    Int_t lowerEnergyIdx,higherEnergyIdx;
    
    lowerEnergyIdx=TMath::BinarySearch(valueArrayDimension,energiesForBraggPeakLocation, energy);
//    if (lowerEnergyIdx>=valueArrayDimension) lowerEnergyIdx=valueArrayDimension-1;//ensure index is within bounds of array
//    if (lowerEnergyIdx<0) lowerEnergyIdx=0;//ensure index is within bounds of array
    
    higherEnergyIdx=lowerEnergyIdx+1;
    
    Double_t energyDifference,interpolateDifference, interpolationFactor;
    energyDifference=energiesForBraggPeakLocation[higherEnergyIdx]-energiesForBraggPeakLocation[lowerEnergyIdx];
    interpolateDifference=energy-energiesForBraggPeakLocation[lowerEnergyIdx];
    
    interpolationFactor=interpolateDifference/energyDifference;
    
    //interpolate Bragg Peak location for target energy
    
    Double_t braggPeakLocationDifference;
    braggPeakLocationDifference=braggPeakLocations[higherEnergyIdx]-braggPeakLocations[lowerEnergyIdx];
    Double_t interpolatedBraggPeakPosition;
    interpolatedBraggPeakPosition=braggPeakLocations[lowerEnergyIdx]+braggPeakLocationDifference*interpolationFactor;
    
   //interpolate Bragg Peak edep for target energy
    Double_t braggPeakEdepDifference;
    braggPeakEdepDifference=braggPeakEdeps[higherEnergyIdx]-braggPeakEdeps[lowerEnergyIdx];
    Double_t interpolatedBraggPeakEdep;
    interpolatedBraggPeakEdep=braggPeakEdeps[lowerEnergyIdx]+braggPeakEdepDifference*interpolationFactor;
    
    Double_t lowerBraggPeakEdep,lowerBraggPeakPosition;
    lowerBraggPeakPosition=braggPeakLocations[lowerEnergyIdx];
    lowerBraggPeakEdep=braggPeakEdeps[lowerEnergyIdx];
    
    //calculate scaling factor to scale lower energy to higher interpolated energy
    //and scaling of edep values
    Double_t scalingFactorForLowerEnergyPosition,scalingFactorForLowerEnergyEdep;
    scalingFactorForLowerEnergyPosition=lowerBraggPeakPosition/interpolatedBraggPeakPosition;
    scalingFactorForLowerEnergyEdep=interpolatedBraggPeakEdep/lowerBraggPeakEdep;
    
    //in case rescaled positions do not exist, we need an interpolator
    //tempInterpol=new ROOT::Math::Interpolator((unsigned int)iDimensionOfArray, ROOT::Math::Interpolation::kCSPLINE);
    ROOT::Math::Interpolator *tempInterpol;

    tempInterpol=new ROOT::Math::Interpolator((unsigned int)iDimensionOfArray, ROOT::Math::Interpolation::kLINEAR);

    //write data to interpolator
    tempInterpol->SetData(iDimensionOfArray, positionArray, valueArray[lowerEnergyIdx]);
    
    Double_t scaledPosition;
    //create data array
    for (i=0;i<iDimensionOfArray;i++)
    {
        scaledPosition=scalingFactorForLowerEnergyPosition*positionArray[i];
        interpolDataArray[1][i]=tempInterpol->Eval(scaledPosition)*scalingFactorForLowerEnergyEdep;
        if (interpolDataArray[1][i]<0.) interpolDataArray[1][i]=0;
    }
    memcpy( interpolDataArray[0], positionArray, iDimensionOfArray * sizeof(Double_t) );
    
    //delete interpolator
    delete tempInterpol;
    
    //delete arrays
    delete[] braggPeakLocations;
    delete[] energiesForBraggPeakLocation;
    delete[] braggPeakEdeps;
    
    delete[] positionArray;
    for (i=0;i<energyArraySize;i++)
    {
       delete[] valueArray[i]; 
    }    
    delete[] valueArray;

    return;
}      



////takes two energy data files and interpolate the correct energy
////ATTENTION: requires all array to have the same dimensions
//void LUT::interpolateEnergy(Double_t** interpolDataArray, Double_t energy)
//{
//    Int_t i,k,valueArrayDimension=0;
////    char filename[filenameTotalSize];
//        //create position and value arrays
//    //positions are always the same, therefore only one needed
//    Double_t* positionArray = new Double_t[iDimensionOfArray];
//    Double_t** valueArray = new Double_t*[energyArraySize];
//    for (i=0;i<energyArraySize;i++)
//    {
//      valueArray[i] = new Double_t[iDimensionOfArray];
//    }
//    //count number of used energies
//    for (i=0;i<energyArraySize;i++)
//    {
//      if(energyAvailArray[i]<=0) {continue;}
//      valueArrayDimension++;
//    }
//
//    //read in value arrays for all energies, skip if energy 0 or below
////    Int_t indexOfValue;
//    //create data array with positions of BraggPeaks, sorted by energy.
//    Double_t* braggPeakLocations = new Double_t[valueArrayDimension];
//    Double_t* energiesForBraggPeakLocation = new Double_t[valueArrayDimension];
//    Double_t* braggPeakEdeps = new Double_t[valueArrayDimension];
//    
//    k=0;
//    for (i=0;i<energyArraySize;i++)
//    { 
//      if(energyAvailArray[i]<=0) { continue;}
//      //valueArray[k] = new Double_t[iDimensionOfArray];
////      sprintf(filename,"%s%s%03.0f%s",folder,dataFilePrefix,energyAvailArray[i],".dat");
//      
//      getLUTarraySplittedUp(i,positionArray,valueArray[k]);
//      //cout<<"read energy: "<<energyAvailArray[i]<<endl;
//      
//      
//    // calculate max value and position (position of Bragg Peak)
//
//      int idx=TMath::LocMax(iDimensionOfArray, valueArray[k]);
//      if (idx>=iDimensionOfArray) idx=iDimensionOfArray-1;//ensure index is within bounds of array
//      if (idx<0) idx=0;//ensure index is within bounds of array
//      double maxEdep=valueArray[k][idx];
//     
//      braggPeakLocations[k]=positionArray[idx];
//      braggPeakEdeps[k]=maxEdep;
//      
//      //braggPeakEdeps[k]=valueArray[i][indexOfValue];
//      energiesForBraggPeakLocation[k]=energyAvailArray[i];
//      //cout<<energiesForBraggPeakLocation[k]<<endl;
//      //cout<<"read energy: "<<energyAvailArray[i]<<" indexOfValue "<<indexOfValue<<  " braggPeakLocations[k] "<<braggPeakLocations[k]<<" braggPeakEdeps[k] "<<braggPeakEdeps[k]<<endl;
//      k++;
//    }
//    //valueArray[k] now contains all edep values of energy with index k
//  
//
//    
//    //loop through all energies and calculate max value and position (position of Bragg Peak)
//
//    //interpolate Bragg Peak location for target energy
//    //initialize interpolator
//    ROOT::Math::Interpolator *tempInterpol;
//    //interpolEdep=new ROOT::Math::Interpolator(0,ROOT::Math::Interpolation::kLINEAR);
//    
//    //tempInterpol=new ROOT::Math::Interpolator((unsigned int)valueArrayDimension, ROOT::Math::Interpolation::kCSPLINE);
//    tempInterpol=new ROOT::Math::Interpolator((unsigned int)valueArrayDimension, ROOT::Math::Interpolation::kLINEAR);
//
//    Double_t interpolatedBraggPeakPosition,lowerBraggPeakPosition;
//    Int_t lowerEnergyIdx;
//    //write data to interpolator
//    tempInterpol->SetData(valueArrayDimension, energiesForBraggPeakLocation, braggPeakLocations);
//    interpolatedBraggPeakPosition=tempInterpol->Eval(energy);
//    //delete interpolator
//    delete tempInterpol;
//    
//   //cout<<"done first interpol"<<endl;
//    //find index of next lower energy
//    lowerEnergyIdx=TMath::BinarySearch(valueArrayDimension,energiesForBraggPeakLocation, targetEnergy);
//        if (lowerEnergyIdx>=valueArrayDimension) lowerEnergyIdx=valueArrayDimension-1;//ensure index is within bounds of array
//    if (lowerEnergyIdx<0) lowerEnergyIdx=0;//ensure index is within bounds of array
//
//    lowerBraggPeakPosition=braggPeakLocations[lowerEnergyIdx];
//    
//    //interpolate Bragg Peak edep for target energy
//    //tempInterpol=new ROOT::Math::Interpolator((unsigned int)valueArrayDimension, ROOT::Math::Interpolation::kCSPLINE);
//    tempInterpol=new ROOT::Math::Interpolator((unsigned int)valueArrayDimension, ROOT::Math::Interpolation::kLINEAR);
//
//    Double_t interpolatedBraggPeakEdep,lowerBraggPeakEdep;
//
//    //write data to interpolator
//    tempInterpol->SetData(valueArrayDimension, energiesForBraggPeakLocation, braggPeakEdeps);
//    interpolatedBraggPeakEdep=tempInterpol->Eval(energy);
//    //delete interpolator
//    delete tempInterpol;
//  //cout<<"done second interpol"<<endl;
//
//    lowerBraggPeakEdep=braggPeakEdeps[lowerEnergyIdx];
//    
//    //calculate scaling factor to scale lower energy to higher interpolated energy
//    //and scaling of edep values
//    Double_t scalingFactorForLowerEnergyPosition,scalingFactorForLowerEnergyEdep;
//    scalingFactorForLowerEnergyPosition=lowerBraggPeakPosition/interpolatedBraggPeakPosition;
//    scalingFactorForLowerEnergyEdep=lowerBraggPeakEdep/interpolatedBraggPeakEdep;
////    cout<<"lowlowBraggPeakPosition "<< braggPeakEdeps[lowerEnergyIdx-1]<<endl;
////    cout<<"higherBraggPeakPosition "<< braggPeakEdeps[lowerEnergyIdx+1]<<endl;
////    cout<<"lowerBraggPeakPosition "<<lowerBraggPeakPosition<<" interpolatedBraggPeakPosition "<<interpolatedBraggPeakPosition<<endl;
////    cout<<"lowerBraggPeakEdep "<<lowerBraggPeakEdep<<" interpolatedBraggPeakEdep "<<interpolatedBraggPeakEdep<<endl;
////    cout<<"scalingFactorForLowerEnergyEdep "<<scalingFactorForLowerEnergyEdep<<endl;
//    //create new interpolated data array
//    
////    for(i=0;i<iDimensionOfArray;i++)
////    {
////        //cout<<"pos "<<positionArray[i]<<" edep "<<valueArray[lowerEnergyIdx][i]<<endl;
////    }
//    
//    //in case rescaled positions do not exist, we need an interpolator
//    //tempInterpol=new ROOT::Math::Interpolator((unsigned int)iDimensionOfArray, ROOT::Math::Interpolation::kCSPLINE);
//    tempInterpol=new ROOT::Math::Interpolator((unsigned int)iDimensionOfArray, ROOT::Math::Interpolation::kLINEAR);
//
//    //write data to interpolator
//    tempInterpol->SetData(iDimensionOfArray, positionArray, valueArray[lowerEnergyIdx]);
//    
//    Double_t scaledPosition;
//    //create data array
//    for (i=0;i<iDimensionOfArray;i++)
//    {
//        scaledPosition=scalingFactorForLowerEnergyPosition*positionArray[i];
////        interpolDataArray[0][i]=positionArray[i];
//     
//        //cout<<"scaledPosition "<<scaledPosition<<" scalingFactorForLowerEnergyPosition "<<scalingFactorForLowerEnergyPosition<<" positionArray[i] "<<positionArray[i]<<endl;
//        //cout<<"scalingFactorForLowerEnergyEdep "<<scalingFactorForLowerEnergyEdep<<endl;
//        interpolDataArray[1][i]=tempInterpol->Eval(scaledPosition)*scalingFactorForLowerEnergyEdep;
//        if (interpolDataArray[1][i]<0.) interpolDataArray[1][i]=0;
//    }
//    memcpy( interpolDataArray[0], positionArray, iDimensionOfArray * sizeof(Double_t) );
//    
//    //delete interpolator
//    delete tempInterpol;
//  // cout<<"done third interpol"<<endl;
//
//    
//    //delete arrays
//    delete[] braggPeakLocations;
//    delete[] energiesForBraggPeakLocation;
//    delete[] braggPeakEdeps;
//    
//    delete[] positionArray;
//    for (i=0;i<energyArraySize;i++)
//    {
//       delete[] valueArray[i]; 
//    }    
//    delete[] valueArray;
//
//    return;
//}       


 //function takes measured data array and creates an evenly spaced lookup array with values 
//from 0 to 100 cm
void LUT::interpolateArray(Double_t ** dataArray,Int_t arraySize, Double_t** interpolDataArray)
{//tempArray,row,data

    Double_t currStep;
    Int_t i;
    //printf("stepSize:%f dimension:%i array size: %i\n",stepSize,iDimensionOfArray,arraySize);
//                for(i=0;i<iDimensionOfArray;i++)
//                {
//                    printf("%f ipolvalue:%f \n",lutArray[0][i],lutArray[1][i]);
//                }
//                for(i=0;i<arraySize;i++)
//                {
//                printf("dataArra 0: %f dataArray 1:%f\n",dataArray[0][i], dataArray[1][i]);
//                }

    //printf("before interpolate\n");
    ROOT::Math::Interpolator intpol(arraySize, ROOT::Math::Interpolation::kLINEAR);
    intpol.SetData(arraySize, dataArray[0], dataArray[1]);
    for(i=0;i<iDimensionOfArray;i++)
    {
        currStep=i*stepSize;
        interpolDataArray[0][i]=currStep*correctionFactorToCM;//to convert mm in cm
        interpolDataArray[1][i]=intpol.Eval(currStep);    
        //if (!(i%100)) printf("%f value:%f \n",interpolDataArray[0][i],interpolDataArray[1][i]);
    }
    //printf("after interpolate\n");
    return;
}           


//function recalulates the Look Up Table
//necessary parameters read from class variables
void LUT::calculateLUT()
{
    Int_t lowerEnergyIdx;
//    char fileNameLowerEnergy[filenameTotalSize];

    //find if energy is already available
    lowerEnergyIdx=TMath::BinarySearch(energyArraySize,energyAvailArray, targetEnergy);
    if (lowerEnergyIdx>=energyArraySize) lowerEnergyIdx=energyArraySize-1;//ensure index is within bounds of array
    if (lowerEnergyIdx<0) lowerEnergyIdx=0;//ensure index is within bounds of array

    //read in data files
    //and create LookUpTable
    //--------------------------------------------------------------------------

    //if energy is already in a file use it
    if(targetEnergy==energyAvailArray[lowerEnergyIdx]&&energyAvailArray[lowerEnergyIdx]>0.)
    {
        //create file name
//        sprintf(fileNameLowerEnergy,"%s%s%03.0f%s",folder,dataFilePrefix,energyAvailArray[lowerEnergyIdx],".dat");
        //read in Lower energy data     
        retrieveData(lowerEnergyIdx,lutArray);
//        readOutLUT(fileNameLowerEnergy,lutArray);
    }    
    else //if energy is not yet on file interpolate
    {
    interpolateEnergy(lutArray, targetEnergy);
    }

    return;
}

//function returns the energy required to penetrate passed dWaterEqDepth
//penetration is defined as maximum value of EDEP in LUT is smaller or equal dWaterEqDepth
//if energy required is beyond dStopEnergy, -1 is returned
Double_t LUT::getEnergyRequiredToPenetrate(Double_t dWaterEqDepth)
{
    ROOT::Math::Interpolator tempInterpol((unsigned int)energyArraySize,ROOT::Math::Interpolation::kLINEAR);
    Double_t interpolatedEnergyRequiredToPenetrate;
    
//    for(Int_t i=0;i<numberOfAvailableEnergies;++i)
//    {
//        cout<<"getEnergyRequiredToPenetrate idx: "<<i<<" energy "<<penetrationDepthArray[0][i]<<" depth "<<penetrationDepthArray[1][i]<<endl;
//    }    
    //write data to interpolator
    tempInterpol.SetData(numberOfAvailableEnergies, penetrationDepthArray[1], penetrationDepthArray[0]);
    interpolatedEnergyRequiredToPenetrate=tempInterpol.Eval(dWaterEqDepth);
//cout<<"getEnergyRequiredToPenetrate looking for: "<<dWaterEqDepth<<" energy: "<<interpolatedEnergyRequiredToPenetrate<<endl; 
    return interpolatedEnergyRequiredToPenetrate;
}

//returns the depth in water-equivalent units in mm
//until the maximum Edep found in LUT files
//LUT has to be measured in water!
Double_t LUT::getPeakRangeOfEnergy(Double_t dEnergy)
{
//cout<<"getPeakRangeOfEnergy "<<dEnergy<<endl;
    //find maximum
    Double_t dDepthMax;
    dDepthMax=0.0;
    
//sets target energy and recalculates Look Up Table
setTargetEnergy(dEnergy);  

if (dEnergy<=0) return 0.0;//in case of zero energy return 0

//find max edep
Int_t idx;
idx=TMath::LocMax(iDimensionOfArray, lutArray[1]);
dDepthMax=lutArray[0][idx];
    return dDepthMax;
}

//returns rangeOfEnergy in current LUT
Double_t LUT::getRangeOfEnergy()  
{
 return rangeOfEnergy;   
}

//returns the depth in water-equivalent units in mm
//until the maximum Edep found in LUT files
//LUT has to be measured in water!
void LUT::calcPeakRangeOfEnergy()
{
//find max edep
Int_t idx;
idx=TMath::LocMax(iDimensionOfArray, lutArray[1]);
rangeOfEnergy = lutArray[0][idx];
    return;
}

//fill penetration array, used as cache for getEnergyRequiredToPenetrate
void LUT::fillPenetrationDepthArray()
{
    //store previously set energy
    Double_t currentEnergy;
    currentEnergy=getTargetEnergy(); 
    
    //count numberOfAvailableEnergies
    numberOfAvailableEnergies=0;
            
      for(Int_t i=0; i<energyArraySize; ++i) 
    {
       if(energyAvailArray[i]<=0.0) continue;
       numberOfAvailableEnergies++;
    }      
    //delete old penetrationDepthArray
    delete[] penetrationDepthArray[0];
    delete[] penetrationDepthArray[1];
    
    //allocate new penetrationDepthArray
    penetrationDepthArray[0] = new Double_t[numberOfAvailableEnergies] ;
    penetrationDepthArray[1] = new Double_t[numberOfAvailableEnergies] ;
    
    Int_t k=0;
    //insert 0 entry for interpolation
    numberOfAvailableEnergies++;
    penetrationDepthArray[0][k] = 0.0;
    penetrationDepthArray[1][k] = 0.0;
    k=1;
    for(Int_t i=0; i<energyArraySize; ++i) 
    {
      if(energyAvailArray[i]<=0.0) continue; //skip zero or below energies

      penetrationDepthArray[0][k] = energyAvailArray[i];
      penetrationDepthArray[1][k] = getPeakRangeOfEnergy(energyAvailArray[i]);
//      cout<<"fillPenetrationDepthArray idx: "<< i<<" energyAvailArray: "<< energyAvailArray[i]<<" set Energy: "<<penetrationDepthArray[0][k]<<" peakRange: "<<penetrationDepthArray[1][k]<<endl;
      k++;
    }
    
    //reset energy to previously set energy
    setTargetEnergy(currentEnergy); 
    return;
}

Double_t* LUT::getEnergyAvailArray(Int_t& sizeOfArray)
{
    Double_t* pointer;
    sizeOfArray=energyArraySize;
    pointer=energyAvailArray;
 return pointer;
}


//returns interpolated energy deposition
Double_t LUT::getPositionAtEdep(Double_t edep)
{
    //cout << stepSizeCalculation<<" "<<stepSize<<endl;
    Double_t position=0.0;
    Double_t scaleFactor= 1./(Double_t(numbOfBeamParticles)/Double_t(numbOfParticlesInSource)*stepSizeCalculation/stepSize/correctionFactorToCM);

    if (edep < 0.0 || edep > maxDepositedEnergyAtCurrentEnergy*scaleFactor+1.) 
    {
        std::cerr<<"WARING: LUT class passed edep "<<edep<<" out of bounds. Returning 0."<<endl;
        return 0.;
    }
    
    //find max index
    Int_t maxEdep=TMath::LocMax(iDimensionOfArray, lutArray[1]);
    //cout<<"maxEdepid "<<maxEdep<<" edep "<<lutArray[1][maxEdep]<<" pos "<<lutArray[0][maxEdep]<<endl;
    Int_t currentIdx=TMath::BinarySearch(maxEdep,lutArray[1],edep*scaleFactor);

    if (currentIdx>=(int)iDimensionOfArray) currentIdx=iDimensionOfArray-1;//ensure index is within bounds of array
    else if (currentIdx<0) currentIdx=0;//ensure index is within bounds of array
    
    position=lutArray[0][currentIdx];
    if (position<0.) return 0.0;
    else return position;
}


//determine and set positions for different edeps deposited
void LUT::determinePositionsOnCurve()
{    
//    percent40EdepPosition=getPositionAtEdep(0.4*maxDepositedEnergyAtCurrentEnergy);
    percent50EdepPosition=getPositionAtEdep(0.5*maxDepositedEnergyAtCurrentEnergy);
    percent60EdepPosition=getPositionAtEdep(0.6*maxDepositedEnergyAtCurrentEnergy);
    percent70EdepPosition=getPositionAtEdep(0.7*maxDepositedEnergyAtCurrentEnergy);
    percent100EdepPosition=getPositionAtEdep(1.*maxDepositedEnergyAtCurrentEnergy);
    diff40to50PercentEdep=getEdep(percent50EdepPosition)-getEdep(percent40EdepPosition);
    diff50to60PercentEdep=getEdep(percent60EdepPosition)-getEdep(percent50EdepPosition);
    diff60to70PercentEdep=getEdep(percent70EdepPosition)-getEdep(percent60EdepPosition);
    edepAt40Pos=getEdep(percent40EdepPosition);
    edepAt50Pos=getEdep(percent50EdepPosition);   
    edepAt60Pos=getEdep(percent60EdepPosition); 
}

void LUT::returnEdepPositions()
{
    cout<<"percent50EdepPosition "<<percent50EdepPosition<<
           "percent60EdepPosition "<<percent60EdepPosition<<
           "percent70EdepPosition "<<percent70EdepPosition<<
           "percent100EdepPosition "<<percent100EdepPosition<<endl;
}

//proton=0, helium=1
Int_t LUT::setParticleType(Int_t particle)
{
    if(particle<0) {cerr<<"LUT_class: ERROR: particle type wrong. Aborting."<<endl;exit(-1);}
    whichParticle=particle;
    return whichParticle;
}

//returns interpolated energy deposition
//returned value is RBE weighted according to step function
Double_t LUT::getRBEEdep(Double_t position)
{
    //cout << stepSizeCalculation<<" "<<stepSize<<endl;
    Double_t edep=0.0;
    
    if (position < 0.0 || position > lutArray[0][iDimensionOfArray-1]) 
    {
        std::cerr<<"WARING: LUT class passed position "<<position<<" out of bounds. Returning 0."<<std::endl;
        return 0.;
    }
    
    edep=interpolEdep->Eval(position)*numbOfBeamParticles/numbOfParticlesInSource*stepSizeCalculation/stepSize/correctionFactorToCM;
    if (edep<0.) edep=0.0;
    
    //RBE calculation starts here
    Double_t rbeMuliplikator=1.1;
    Double_t percent=0.;
    
    //for Protons edep * 1.1 is enough
    if(whichParticle==0)
    {
        rbeMuliplikator=1.1;
    }
    //for Helium Ions
    
    if(whichParticle==1)
    {
     if( position < percent50EdepPosition) rbeMuliplikator=1.1;
         
     else if( position < percent60EdepPosition)
     {
         percent=(edep-edepAt50Pos) /diff50to60PercentEdep;
         rbeMuliplikator=   percent*0.1 + 1.1;
     }
     else if( position < percent70EdepPosition)
     {
         percent=(edep-edepAt60Pos) /diff60to70PercentEdep;
         rbeMuliplikator=   percent*0.1 + 1.2;
     }
     else rbeMuliplikator=1.3;   
    }
    
//    cout<<" rbeMuliplikator "<<rbeMuliplikator<<endl;
//    cout<<"pos "<<position<<" 40depth "<<percent40EdepPosition<<" 50depth "<<percent50EdepPosition<<" 60depth "<<percent60EdepPosition<<endl;
    edep*= rbeMuliplikator;   
    
    return edep;
}

#endif /*LUTCLASS_CPP */
