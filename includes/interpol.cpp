/* 
 * File:   interpol.cpp
 * Author: hfuchs
 * 
 * Created on August 28, 2012, 12:25 PM
 */

//#include "../headers/interpol.h"
#include <interpol.h>


interpol::interpol() 
{
    interpolator=new ROOT::Math::Interpolator(0,ROOT::Math::Interpolation::kLINEAR);
    
}

interpol::~interpol() {
    delete interpolator;
}

//copy constructor
interpol::interpol(const interpol& orig)
{
    cout<<"copyconstr interpol"<<endl;
    xVector=orig.xVector;
    yVector=orig.yVector;
    
    interpolator=new ROOT::Math::Interpolator(0,ROOT::Math::Interpolation::kLINEAR);
    interpolator->SetData(xVector, yVector);
}



//set data to interpolation function using vector
void interpol::setInterpolEdepData(const std::vector< double > &x, const std::vector< double > &y)
{
    if(x.size()!=y.size()) 
    {
        cout<<"ERROR interpol: Passed vector sizes do not match. Aborting."<<endl;
        exit(-1);
    }
    xVector=x;
    yVector=y;
    interpolator->SetData(x, y);
    return;
}

//set data to interpolation function using array
void interpol::setInterpolEdepDataArray(int iDimensionOfArray,const double *x, const double *y)
{
    interpolator->SetData(iDimensionOfArray, x, y);
    
//    xVector.clear();
//    yVector.clear();
//    for(int i=0;i<iDimensionOfArray;i++)
//    {
//        xVector.push_back(x[i]);
//        yVector.push_back(y[i]);
//    }
    
    xVector.assign(x,x+iDimensionOfArray);
    yVector.assign(y,y+iDimensionOfArray);
    
    return;
}



//returns interpolated data
double interpol::getData(double x)
{
    double temp=0.0;
    
//    unsigned int index;
//    std::vector<double>::iterator iterator;
//    iterator=TMath::BinarySearch(xVector.begin(), xVector.end(), x);
//    index=iterator - xVector.begin();
//    if ( index<=0 || index>=yVector.size() ) temp=yVector[0];
//    else temp=yVector[index];
    
    
    if( x>xVector.back() ) 
    { 
        std::cerr<<"interpol class: WARNING: looked for value: "<<x<<" too high, returning maximum value for: "<< xVector.back()<<" instead."<<std::endl;
        return yVector.back();
    }
    if( x<xVector.front()) 
    { 
        std::cerr<<"interpol class: WARNING: looked for value: "<<x<<" too low, returning minmum value for: "<< xVector.front()<<" instead."<<std::endl;
        return yVector.front();
    }
    else temp=interpolator->Eval(x);
    
    return temp;
}


//returns interpolated data
double interpol::getDataClosestMatch(double x)
{
    if ( xVector.empty() == true) {std::cerr<<"interpol class: ERROR: lookup data array is empty!"<<endl;
    exit(-1);}
    Int_t currentIdx;
    //find current depth index
    currentIdx=TMath::BinarySearch(xVector.size(),&xVector[0], x);
//    if (currentIdx>=(int)xVector.size()) currentIdx=xVector.size()-1;//ensure index is within bounds of array
//    else if (currentIdx<0) currentIdx=0;//ensure index is within bounds of array

    return yVector[currentIdx];
}
