#ifndef PENCILBEAMFUNCTIONS
#define	PENCILBEAMFUNCTIONS

#include <heliumBeamFunctions.h>

//one pencil beam step
void inline singlePencilBeamAlgorithm(LUT &lookUpTable,nucLUT &nucCorrTable, inputClass &myInputClass, Double_t **outputArray,Double_t **initFluenceArray,Double_t &integratedDepth,
        Double_t *correctedStepSizeArray, Double_t *scatAngleSquaredArray,Double_t densityMaterial,Double_t radiationLengthMaterial,
        Double_t excitationEnergyMaterial, Double_t materialAtomZARatio, Double_t initEnergy, Double_t stepSize, 
        Double_t beamInitSigmaX,Double_t beamInitSigmaY,Double_t initFluence,Int_t currDepthStep, 
        Double_t x0, Double_t y0, Int_t dimensionX, Int_t dimensionY,Int_t offsetX,Int_t offsetY)
{
    Int_t i,j;
    //Double_t x,y,z;
    Double_t inDepthReleasedEnergy,energyProbNextStep,rescaleRatio;
    Double_t stoppingPowerWater,stoppingPowerMaterial;
    Double_t currSquaredVarianceDueToMS,scatteringAngleSquared,inDepthDoseDepositionFactor;

    //printf("Energy: %f Proton Range: %f beta: %f \n",initEnergy, relativisticBeta(initEnergy,constParticleMass));

    //integratedDepth contains recalculated(to water equivalent) depth up until current step now

    //calculate Dose, loop over x,y for each depth step

    Double_t currentEnergy;
    
   if (currDepthStep<=0) 
            {
       currentEnergy=initEnergy;
       integratedDepth=0.0;
       correctedStepSizeArray[currDepthStep]=0;
            }
   else currentEnergy=lookUpTable.getInDepthEnergy( integratedDepth );

 
            //calculate stepSize due to different material
//            stoppingPowerWater=calculateStoppingPower(energyProbNextStep,constZARatioWater, constExcitationEnergyWater, constParticleMass);
//            stoppingPowerMaterial=calculateStoppingPower(energyProbNextStep,materialAtomZARatio, excitationEnergyMaterial, constParticleMass);
//            rescaleRatio= getStoppingPowerRatioToWater(stoppingPowerWater, stoppingPowerMaterial)*densityMaterial;
            stoppingPowerWater=calculateStoppingPower(currentEnergy,constZARatioWater, constExcitationEnergyWater, constParticleMass);
            stoppingPowerMaterial=calculateStoppingPower(currentEnergy,materialAtomZARatio, excitationEnergyMaterial, constParticleMass);
            rescaleRatio= getStoppingPowerRatioToWater(stoppingPowerWater, stoppingPowerMaterial)*densityMaterial;

            //Double_t z=currDepthStep*stepSize;//z is real depth not corrected one

//            if (currDepthStep>0) 
//            {
//                //calculate current depth
//            integratedDepth=integratedDepth+stepSize*rescaleRatio;
//            //store correctedStepSize in Array for use in getSummedScattAngle
//            //integratedDepthArray[currDepthStep]=integratedDepth;
//            correctedStepSizeArray[currDepthStep]=stepSize*rescaleRatio;
//            }
//            else 
//            {
//            integratedDepth=0.0;
//            //integratedDepthArray[currDepthStep]=
//            correctedStepSizeArray[currDepthStep]=0;
//            }
            
            //integratedDepth=integratedDepth+stepSize*rescaleRatio;
            
            correctedStepSizeArray[currDepthStep]=stepSize*rescaleRatio;
            
      //energyProbNextStep=lookUpTable.getInDepthEnergy( integratedDepth );         

            //recalculate the scattering Angle every z step, due to multiple scattering corrections
//                scatteringAngleSquared= getScatteringAngleSquare(correctedStepSizeArray[currDepthStep], lookUpTable.getInDepthEnergy(integratedDepth+stepSize), 
//                                                                lookUpTable.getInDepthEnergy(integratedDepth), radiationLengthMaterial);
//                scatteringAngleSquared= getScatteringAngleSquare(stepSize, energyProbNextStep, 
//                                                                currentEnergy, radiationLengthMaterial);
                
             
                
      scatteringAngleSquared=0.0;
      Int_t numIntegralSteps;//should be 200 per 1 mm step
      
      numIntegralSteps= 1000*stepSize; //2000 was choosen to ensure above rule
      //2000*0.05=100
      for (Int_t k=0;k<numIntegralSteps;k++)
            {
          Double_t localStepSize;
          localStepSize=stepSize*rescaleRatio/numIntegralSteps;
          
           scatteringAngleSquared=scatteringAngleSquared+getScatteringAngleSquare(localStepSize, lookUpTable.getInDepthEnergy( integratedDepth+(k+1)*localStepSize ), 
                                                                                 lookUpTable.getInDepthEnergy( integratedDepth+k*localStepSize ), radiationLengthMaterial) ;
            }			
          //next depth step
          integratedDepth=integratedDepth+stepSize*rescaleRatio;      

            //add scatteringAngleSquared to Array
            scatAngleSquaredArray[currDepthStep]=scatteringAngleSquared;

            currSquaredVarianceDueToMS = getSummedScattAngle(scatAngleSquaredArray,stepSize,currDepthStep);
//         cout<<"depth: "<< currDepthStep*stepSize<<" sigma: "<<sqrt(beamInitSigmaX*beamInitSigmaX+currSquaredVarianceDueToMS)<<" scatteringAngleSquared: "<<scatteringAngleSquared<<" currSquaredVarianceDueToMS "<<currSquaredVarianceDueToMS<<endl;

            //influence of sigma in both dimensions the same, only initial sigma different
            //Double_t sigmaNew=sqrt(beamInitSigmaX*beamInitSigmaX+currSquaredVarianceDueToMS);


        //recalculate fluence distribution	
            //recalculate the fluenceArray every z step, due to multiple scattering corrections
        createFluenceArray(nucCorrTable,initFluenceArray, x0, y0, 
                sqrt(beamInitSigmaX*beamInitSigmaX+currSquaredVarianceDueToMS),
                sqrt(beamInitSigmaY*beamInitSigmaY+currSquaredVarianceDueToMS),
                dimensionX,dimensionY,offsetX,offsetY,stepSize, initFluence,
                beamInitSigmaX, beamInitSigmaY,integratedDepth-stepSize*rescaleRatio);
        
        //if(currDepthStep*stepSize==0) cout<<"sigx: "<<sqrt(beamInitSigmaX*beamInitSigmaX+currSquaredVarianceDueToMS)<<endl;
        
        
//         if(currDepthStep*stepSize==0)
//         {
//             FILE *fpOut = fopen("FluenceArray0.dat","w");
//             for (j=0;j<dimensionY;j++)
//             {
//                 Double_t fluenceSum=0;
//                for (i=0;i<dimensionX;i++) 
//                {
//                    fluenceSum=fluenceSum+initFluenceArray[j][i];
//                }
//                fprintf(fpOut,"%.2f %f \n",stepSize*(j+offsetX),fluenceSum);
//             }
//             fclose(fpOut);
//         }
        
       
//    Int_t inpX,inpY,inpZ;
//    Double_t pbX,pbY,pbZ;
//    Double_t density;
//    pbX=x0;
//    pbY=y0;
//    pbZ=integratedDepth;
//    myInputClass.PBtoInputCoordSystem(pbX,pbY,pbZ, inpX, inpY, inpZ);
//    
//    density=myInputClass.getDensityAtPbCoordinate(pbX,pbY,pbZ);
//    
//       
        
        
//    inDepthDoseDepositionFactor=lookUpTable.getEdep(integratedDepth)*1/densityMaterial;        
    inDepthDoseDepositionFactor=lookUpTable.getEdep(integratedDepth);
    
//    cout<<"-----"<<endl;
//    cout<<"x0: "<<x0<<"y0: "<<y0<<endl;
//    cout<< "depth: "<<integratedDepth<<" density "<<densityMaterial<<" newDensity "<<density<<endl;
//    cout<< " PBX: "<<pbX<<" inpX: "<<inpX  <<" PBY: "<<pbY<<" inpY: "<<inpY<<" PBZ: "<<pbZ<<" inpZ: "<<inpZ<<endl;
//    cout<<"-----"<<endl;
    ////more detailed output for step 200            

//    if (!(currDepthStep%200)) printf("curstep: %i curDepth: %.2f currStepSize: %.2f enDep: %.2f \n", 
//                                                currDepthStep,integratedDepth,
//                                                correctedStepSizeArray[currDepthStep],
//                                                inDepthDoseDepositionFactor*initFluence*densityMaterial*stepSize*rescaleRatio );
    if (!(currDepthStep%200)) printf("curstep: %i curDepth: %.2f currStepSize: %.2f enDep: %.2f \n", 
                                                currDepthStep,integratedDepth,
                                                correctedStepSizeArray[currDepthStep],
                                                inDepthDoseDepositionFactor*initFluence*densityMaterial*stepSize*rescaleRatio );

      //if (!(currDepthStep%50)) printf("curstep: %i curDepth: %f enDep: %f\n", currDepthStep,integratedDepth,inDepthDoseDepositionFactor*initFluence*densityMaterial*stepSize*stoppPowerRatio);
       // printf("depth:%f curEn:%f\n",integratedDepth,lookUpTable.getInDepthEnergy(integratedDepth));
     Double_t rescaleRatioN, oldhu; //hu is hounsfields unit
     Double_t *materialPropertiesArray;
     oldhu=-1.0;//use an unrealistic old value
     rescaleRatioN=rescaleRatio;
     materialPropertiesArray=new Double_t[5];
     for(Int_t m=0;m<5;m++)materialPropertiesArray[m]=0;
//            materialPropertiesArray[0]=(Double_t)hu;
//            materialPropertiesArray[1]=density;
//            materialPropertiesArray[2]=radLength;
//            materialPropertiesArray[3]=ZA;
//            materialPropertiesArray[4]=excitationEnergy;
     
    for (j=0;j<dimensionY;j++)//yloop
    {
            //Double_t y=(offsetY+j)*stepSize;//in cm
            //sum=0;

            //i=dimensionX/2;
            for (i=0;i<dimensionX;i++)//xloop
                    {
                     //get local Material Properties        
                    myInputClass.getMaterialPropAtPbCoordinate((offsetX+i)*stepSize,(offsetY+j)*stepSize,currDepthStep*stepSize, materialPropertiesArray);                
                    //cout<<"dens: "<<materialPropertiesArray[1]<<" radlen: "<<materialPropertiesArray[2]<<" za: "<<materialPropertiesArray[3]<<" exc: "<<materialPropertiesArray[4]<<endl;
                    
                    //if material has changed, calculate new stopping power. Else use previous one
                    if (oldhu!=materialPropertiesArray[0])
                    {
//                        cout<<"new ratio"<<" old hu: "<<oldhu<<" new hu: "<<materialPropertiesArray[0]<<" i "<<i<<" j "<<j<<endl;
                        oldhu=materialPropertiesArray[0];
                         //recalculate local stopping power
                    stoppingPowerMaterial=calculateStoppingPower(currentEnergy,materialPropertiesArray[3], materialPropertiesArray[4]/1000000, constParticleMass);// /1000000 because Pencil Beam requires MeV
                    //recalculate rescale Ratio
                    rescaleRatioN= getStoppingPowerRatioToWater(stoppingPowerWater, stoppingPowerMaterial)*materialPropertiesArray[1];    
                
                    }

 //inDepthDoseDepositionFactor=lookUpTable.getEdep(integratedDepth)*1/densityMaterial;               
                //Double_t x=(offsetX+i)*stepSize;

                    //Calculate in Depth Deposited Energy (Function returns PseudoDose, to get Energy, multiply by density	
                    //correct for voxel size changes due to material with rescaleRatio
                    //inDepthReleasedEnergy is nearly a value in MeV, but MeV per Voxel. It is in MeV and needs to be converted to J/kg, 
                    //that is to relate it with the irradiated voxel to get dose values
                    inDepthReleasedEnergy=inDepthDoseDepositionFactor*initFluenceArray[j][i]*rescaleRatioN;
                    
                    
#if CALCULATEDOSE!=0 
                    //ATTENTION !!!
                    //inDepthReleasedEnergy divided by density is not correct dose, needs to be scaled according to voxelsize
                    Double_t massOfVoxelInGram;
                    massOfVoxelInGram=densityMaterial* pow((stepSize*rescaleRatio),3);
                    //May not be correct
                    //1 Gy = J/kg
                    //mass = density * Volume
                    //Gray= MeVtoJoule / mass *1000 (1000 due to density in gram)
                    //=>edep*constMeVToJoule / rescaleRatio
                    inDepthReleasedEnergy=inDepthReleasedEnergy*1/materialPropertiesArray[1];//Dose is Energy/kg, materialPropertiesArray[1] is local density
                    outputArray[j][i]=inDepthReleasedEnergy*constMeVToJoule*1000/ massOfVoxelInGram;
#endif  
#if CALCULATEDOSE==0 
                    outputArray[j][i]=inDepthReleasedEnergy;
#endif                    
                    
//if (!(currDepthStep%200)&&j==0) cout<<"outputarray: "<<outputArray[j][i]<<endl;
                    //sum=sum+inDepthReleasedEnergy;
                    //fprintf(fp, "%.2f %.2f %.2f %f\n",x,y,z,inDepthReleasedEnergy);
                    }
    //if(inDepthReleasedEnergy>0) printf("%f %f %f \n",y,z,inDepthReleasedEnergy);

    }
//if (!(currDepthStep%500)) printf("sigmaNew: %e scatAngleSquared: %e depth:%f integDepth %f releasedEnergy:%e stoppPowerRatio: %f \n",sigmaNew, scatteringAngleSquared,z,integratedDepth,sumTwo,stoppPowerRatio);
}





//---------------------------------------------------------------------------------
//Functions 
//---------------------------------------------------------------------------------

//personalized power function
Double_t inline myPower(Double_t x, Double_t y)
{
    if (x!=0 && y!=0) return pow(x,y);
    if (x!=0 && y==0) return 1;
    else return 0;
}


//calculate the relativistic beta
//energy and Mass in MeV requried
Double_t inline relativisticBeta(Double_t energy, Double_t mass)
{
    //protonRestMass = 938.272013;////in MeV
    if (energy<0) energy=0;
    if (mass<0) mass=0;
    return sqrt(1 - pow( mass/(mass+energy) ,2) );
    //alternative version, same result
    //return  sqrt( 1 - 1/pow(1+ energy/(protonRestMass) ,2) );
}


////calculate the scattering angle
//// everything in SI units
////See Geant4 physics documentation
//Double_t getScatteringAngleSquare(Double_t stepSize, Double_t energy2, Double_t energy, Double_t radiationLength)
//{
//	//Highland-Lynch formula
//	Double_t constC1, constC2, relatBeta, scatAngle,momentum;
//
//	constC1=13.6; ////in MeV
//	constC2=0.038;//// in MeV
//
//        energy=energy*constParticleMassNumber;//passed Energy is per nucleon, formula works for whole energy
//        
//	relatBeta=relativisticBeta(energy, constParticleMass);
//        momentum=constParticleMass*relatBeta/constC;
//       
//	if (relatBeta<=0 || stepSize<=0) scatAngle=0;
//	else scatAngle = constC1/(relatBeta*momentum*constC) * constParticleCharge *  sqrt( stepSize / radiationLength ) * (  1 + constC2 * log(stepSize / radiationLength)   );
//
//        //cout<<" scatAngleSquared "<<myPower(scatAngle,2)<<endl;
//	return (myPower(scatAngle,2) );
//	////log() computes natural logarithm in C++
//}

////calculate the scattering angle
//// everything in SI units
////See Gottschalk 2010, nonlocal scattering formula Tdm 
////not so good as the other two
Double_t inline getScatteringAngleSquare(Double_t stepSize, Double_t energy2, Double_t energy, Double_t radiationLength)
{
	//Highland-Lynch formula
	Double_t relatBeta,relatBeta2,scatAngleSquared,momentum,momentum2;

      energy=energy*constParticleMassNumber;//passed Energy is per nucleon, formula needs whole energy per particle
      energy2=energy2*constParticleMassNumber;//passed Energy is per nucleon, formula needs whole energy per particle

	relatBeta=relativisticBeta(energy, constParticleMass);
        momentum=constParticleMass*relatBeta/constC;
	relatBeta2=relativisticBeta(energy2, constParticleMass);
      momentum2=constParticleMass*relatBeta2/constC;
       
      Double_t PV,PV2;      
      PV=relatBeta*constC*momentum;
      PV2=relatBeta2*constC*momentum2;

      Double_t FdM,Tfr;
      
      if(PV2==PV)//in case energy should be the same, use only Rossi formula. This may happen if LUT show fluctuations
      {
                Tfr= pow( (15.0 * constParticleCharge / PV),2) /radiationLength;
      }
      else
      {
          FdM= 0.5244 + 0.1975*log10(1.0 - pow( (PV2/PV),2 ) ) + 0.2320*log10(PV2) - 0.0098*log10(PV2)*log10(1.0- pow( (PV2/PV),2 ) );

          Tfr= pow( (15.0 * constParticleCharge / PV),2) / (radiationLength*1.299)  * FdM;
          //instead of radiation length, scattering length has to be used.
          //For water Xs/X0 is 1.299
          //as other materials in human body are similar, this should be appropriate for all other materials
      }
      //According to PDG Yplanar=theta0*z*1/sqrt(3)
        Tfr=Tfr*1/3;
                //1/sqrt(3);

	if (relatBeta2<=0 || relatBeta<=0 || stepSize<=0) scatAngleSquared=0;
	else scatAngleSquared = Tfr * stepSize;
      
//      cout<<"FdM "<<FdM<<" Tfr "<<Tfr<<" stepSize "<<stepSize<<" scatAngleSquared "<<scatAngleSquared<<endl;
      //cout<<"energy: "<<energy<<" PV "<<PV<<" PV2 "<<PV2<<" scatAngleSquared "<<scatAngleSquared<<endl;
  
	return scatAngleSquared;
	////log() computes natural logarithm in C++
        ////log10() computes the base 10 logarithm in C++
}


//// everything in SI units
////See Gottschalk 1993
////Using Gottschalk approximation with sqrt(PV*PV2) instead of PV
//Double_t getScatteringAngleSquare(Double_t stepSize, Double_t energy2, Double_t energy, Double_t radiationLength)
//{
//	//Highland-Lynch formula
//	Double_t constC1, constC2, scatAngle;
//        Double_t relatBeta,relatBeta2,scatAngleSquared,momentum,momentum2;
//      
//        energy=energy*constParticleMassNumber;//passed Energy is per nucleon, formula needs whole energy per particle
//      energy2=energy2*constParticleMassNumber;//passed Energy is per nucleon, formula needs whole energy per particle
//      
//	relatBeta=relativisticBeta(energy, constParticleMass);
//      momentum=constParticleMass*relatBeta/constC;
//	relatBeta2=relativisticBeta(energy2, constParticleMass);
//      momentum2=constParticleMass*relatBeta2/constC;
//
//        Double_t PV,PV2;      
//        PV=relatBeta*constC*momentum;
//        PV2=relatBeta2*constC*momentum2;
//
//        //Gottschalk uses 14.1 and 1/9 while PDG2010 states 13.6 and 0.038
//	constC1=13.6;//14.1;//13.6; ////in MeV
//	constC2=0.038;//1/9;//0.038;//// in MeV
//       
//	if (relatBeta<=0 || stepSize<=0) scatAngle=0;
//	else scatAngle = constC1/( sqrt(PV*PV2) ) * constParticleCharge *  sqrt( stepSize / radiationLength ) * (  1 + constC2 * log(stepSize / radiationLength)   );
//
//        //cout<<" scatAngleSquared "<<myPower(scatAngle,2)<<endl;
//	return myPower(scatAngle,2);
//	////log() computes natural logarithm in C++
//}



//calculate the scattering angle
// everything in SI units
//according to PDG Highland Lynch formula chapter 27 multiple scattering
//Double_t inline getScatteringAngleSquare(Double_t stepSize, Double_t energy2, Double_t energy, Double_t radiationLength)
//{
//	Double_t constEnergyFactor;
//	Double_t scattAngleSquare;
//        Double_t factor,beta,momentum;
//        
//        constEnergyFactor=13.6;//14.85;//12.2;//in MeV
//        energy=energy*constParticleMassNumber;//passed Energy is per nucleon, formula works for whole energy
//	beta=relativisticBeta(energy, constParticleMass);//beta is relativistic beta
//
//        momentum=constParticleMass*beta/constC;
//        
//	//factor = beta * energy;
//        factor = beta * momentum*constC;
//
//        //printf("stepSize: %f radLength: %f beta: %f energy: %f\n",stepSize,radiationLength,beta,energy);
//        if (factor != 0 && stepSize>0) scattAngleSquare= constEnergyFactor/factor*constParticleCharge* sqrt(stepSize/radiationLength)*(1+0.038*log(stepSize/radiationLength));
//
//        else scattAngleSquare=0;
//        
//       // scattAngleSquare=scattAngleSquare;//*1/10;//*3.1415/180;
//        //printf("\n scatAngleSquare: %e \n",scattAngleSquare);
//        //scattAngleSquare=scattAngleSquare*1/10;// 1/(4*sqrt(3));
//        scattAngleSquare=pow(scattAngleSquare,2);//*stepSize;
//        return scattAngleSquare;
//}

//calculate the scattering angle
// everything in SI units
//according to Soukup Disertation "Accurate methods of dose computation in proton therapy"
//Double_t inline getScatteringAngleSquare(Double_t stepSize, Double_t energy2, Double_t energy, Double_t radiationLength)
//{
//	Double_t constEnergyFactor;
//	Double_t scattAngleSquare;
//        Double_t factor,beta;
//        
//        constEnergyFactor=12.2;//14.85;//12.2;//in MeV
//
//      energy=energy*constParticleMassNumber;//passed Energy is per nucleon, formula works for whole energy
//	beta=relativisticBeta(energy, constParticleMass);//relativistic beta(Lorentz)	
//
//	//factor = beta * energy;
//        factor = beta *beta* constParticleMass;
//
//        //printf("stepSize: %f radLength: %f beta: %f energy: %f\n",stepSize,radiationLength,beta,energy);
//        if (factor != 0) scattAngleSquare=myPower( (constEnergyFactor*constParticleCharge) / factor ,2) * (stepSize/ (radiationLength) );
//
//        else scattAngleSquare=0;
//        
//        return scattAngleSquare;
//}


//creates the fluence Array
//gaussian distributed beam profile assumed
//x0 and y0 determine the position where the beam center is located
//beamInitSigma and beamInitSigma are the sigmas of the gaussian distribution
//dimension is the dimension of the Array to be created
//stepSize size of one Step in cm
//initFluence number of Particles
//The Gauss function produces rounding errors close to sigma=0
//Therefore security checks have been implemented to avoid most of the damage
void inline createFluenceArray(nucLUT &nucCorrTable, Double_t **fluenceArray, Double_t x0, Double_t y0, 
                               Double_t beamSigmaX,Double_t beamSigmaY, 
                               Int_t dimensionX,Int_t dimensionY,Int_t offsetX,Int_t offsetY,
                               Double_t stepSize, Double_t initFluence, Double_t initbeamSigmaX, Double_t initbeamSigmaY, Double_t currentDepth)
{
    //printf("y0: %f x0: %f\n",y0,x0);    
Double_t yg,y;
Double_t xg,x;
//Double_t counter=0;
Int_t i,j;
//beamSigmaX=beamSigmaY=nucCorrTable.getGaussSigma(currentDepth);
        
 //cout<<"depth: " <<currentDepth<<" Relation: "<<nucCorrTable.getGaussVoigtRelation(currentDepth)<<endl;


	//first coordinate y, second x
	//gaussian distributed beam profile assumed
        for (i=0;i<dimensionY;i++)//y
        {
            y=(i+offsetY)*stepSize;

            if ( beamSigmaY<=0.0001)
            {    
                if ( round(y*(1/stepSize)) == round(y0*(1/stepSize))  ) yg=1;
                //if (TMath::Floor(  TMath::Nint(y*(1/stepSize)) ) == TMath::Floor(  TMath::Nint(y0*(1/stepSize)) ) ) yg=1;
                else yg=0;
            }
            else 
                //Gauss distribution due to MS
                yg= TMath::Gaus(y, y0, beamSigmaY, true)*stepSize;//*stepSize to normalize to one again  
            
            //if(currentDepth==0.05) cout<<"beamSigmaArray: "<<beamSigmaY<<endl;
            //include correction
            //how much goes to gauss, how much goes to voigt
            //in total, area of both curves should be one due to normalization
            Double_t voigtArea,gaussArea;
            
            voigtArea=1/(1+nucCorrTable.getGaussVoigtRelation(currentDepth));
            gaussArea=1-voigtArea;

            //create voigt curve
            Double_t yvoigt;    
            //Adapting nuclear correction LUT beam diameter to current diameter
            nucCorrTable.setBeamSigma(initbeamSigmaY);
            
            yvoigt=TMath::Voigt(y-y0, nucCorrTable.getVoigtSigma(currentDepth), nucCorrTable.getVoigtLg(currentDepth), 4)*stepSize;//*stepSize to normalize to one again  

            //Add up contributions of both curves          
            yg=yg*gaussArea+yvoigt*voigtArea;

//printf("fluence y: %.2f Fluence: %f initFluence: %f \n",i,fluenceArray[i],initFluence);
          for (j=0;j<dimensionX;j++)//x
                {
                x=(j+offsetX)*stepSize;

                if ( beamSigmaX<=0.0001)
                {    
                    if ( round(x*(1/stepSize)) == round(x0*(1/stepSize))  ) xg=1;
                    //if (TMath::Floor(  TMath::Nint(x*(1/stepSize)) ) == TMath::Floor(  TMath::Nint(x0*(1/stepSize)) ) ) xg=1;    
                    else xg=0;
                }
                else 
                    xg= TMath::Gaus(x, x0, beamSigmaX, true)*stepSize;//*stepSize to normalize to one again 
                
                //include correction
            //how much goes to gauss, how much goes to voigt
            //in total, area of both curves should be one due to normalization
            //create voigt curve
            Double_t xvoigt;    
            
            //Adapting nuclear correction LUT beam diameter to current diameter
            nucCorrTable.setBeamSigma(initbeamSigmaX);
            xvoigt=TMath::Voigt(x-x0, nucCorrTable.getVoigtSigma(currentDepth), nucCorrTable.getVoigtLg(currentDepth), 4)*stepSize;//*stepSize to normalize to one again  

            //Add up contributions of both curves          
            xg=xg*gaussArea+xvoigt*voigtArea;


                fluenceArray[i][j] = initFluence * xg * yg;
              //if (xg==1 && yg==1) printf("flArray: %e y0: %f x0: %f\n",fluenceArray[i][j],y0,x0);    

                    //security check, if due to rounding errors fluence becomes bigger than initFluence
                  if (fluenceArray[i][j]>initFluence) fluenceArray[i][j] =initFluence;    

                
                //printf("fluence x: %.2f y: %.2f Fluence: %f initFluence: %f \n",j,i,fluenceArray[i][j],initFluence);
                //fprintf(fp, "%i %i %f\n",i,j,fluenceArray[i][j]);
//                counter=counter+fluenceArray[i][j];
                } 
        }

//	cout<<"initFluence "<<initFluence<<" sum Of Fluence: "<<counter<<std::endl;
                
        return;
}



//loop through the scatAngleSquaredArray and quadratically sum up all contents with distance from current point
//defined through stepSize*maxIndex
//see Soukup Dissertation
Double_t inline getSummedScattAngle(Double_t *scatAngleSquaredArray, Double_t stepSize, Int_t maxIndex)
{
	Int_t i;
        Double_t depthSum=0;
	Double_t quadrSum=0;
        
        depthSum=stepSize*maxIndex;
            for (i=0;i<maxIndex;i++)
            {
                //first entry (first voxel) generated angle is now farthest away.
            //scatAngleSquaredArray contains already squared values
             quadrSum=quadrSum + scatAngleSquaredArray[i]*myPower(depthSum,2);
             depthSum=depthSum-stepSize;
            }

	return quadrSum;
}



//calculate the stoppingPowerRatioToWater in order to rescale the real depth to a depth in water
Double_t inline getStoppingPowerRatioToWater(Double_t stoppingPowerWater,Double_t stoppingPowerMaterial)
{
	if (stoppingPowerMaterial==0) return 1;
	//else return stoppingPowerWater/stoppingPowerMaterial;
	else return stoppingPowerMaterial/stoppingPowerWater;
}


//Bethe-Bloch formula
//see Ziegler 1999
//energy, excitationenergy and particle mass in MeV
Double_t inline calculateStoppingPower(Double_t energy,Double_t atomicZARatio, Double_t excitationEnergy, Double_t particleMass)
{
	Double_t constK,relatBeta,deltaEmax,stoppingPower;

        relatBeta=relativisticBeta(energy,particleMass);
        
//from PDG
        constK=0.307075;//in MeV * 1/g * cm^2
	deltaEmax=(2*constMassElectron*relatBeta*relatBeta)/(1-(relatBeta*relatBeta));
        
	stoppingPower=constK*constParticleCharge*constParticleCharge*atomicZARatio*1/(relatBeta*relatBeta) *
	(
	0.5 * log ( (2*constMassElectron*relatBeta*relatBeta*deltaEmax)/(1-(relatBeta*relatBeta)) )
	- (relatBeta*relatBeta) - log(excitationEnergy));
        
        //from Leo Techniques for Nuclear and Particle Physics Experiments
        //both give the same results 
//        constK=0.1535;//in MeV * 1/g * cm^2
//        Double_t gamma;
//        
//        gamma=1 / sqrt( 1 - relatBeta*relatBeta );
//        
//        deltaEmax=( 2*constMassElectron*relatBeta*relatBeta*gamma*gamma*constC*constC )/
//                (1 + 2*constMassElectron/particleMass*sqrt( 1+relatBeta*relatBeta*gamma*gamma ) + pow(constMassElectron/particleMass,2) );
//        
//        stoppingPower=constK*1*1*atomicZARatio*1/(relatBeta*relatBeta) *
//	(
//	log ( (2*constMassElectron*gamma*gamma*constC*constC*relatBeta*relatBeta*deltaEmax)/(excitationEnergy*excitationEnergy) )
//	- 2*(relatBeta*relatBeta));

	if (stoppingPower<0 || energy<=0) stoppingPower=0;
	return stoppingPower;
}




////decomposes the initial gauss distributed beam in subBeams
////spread is the maximum distance from the original beam axis in times of sigma 
Int_t beamDecomposition(Double_t **beamDataArray, Double_t **beamShapeDataArray, Int_t NumDataPointsInArray, Double_t stepSizeOfDataPoints, Double_t initFWHMX,Double_t initFWHMY,Double_t initFluence,
        Double_t x0, Double_t y0, Int_t numOfSubBeamsOneDim,Double_t initEnergy)
{
      Int_t i,j,k;
      Int_t numbOfSubBeams;
      std::vector<Double_t> xGauss,yGauss;
      GaussFitClass *fTest;
      
      numbOfSubBeams=numOfSubBeamsOneDim;
        
  //calculate one Dim fit for x      
  fTest = new GaussFitClass();      
  fTest->setBeamShapeFromDataArrayToRealData(beamShapeDataArray[0], NumDataPointsInArray, stepSizeOfDataPoints);
  fTest->numericalFit(numbOfSubBeams,initFWHMX,xGauss);
//  cout<<"size of xGauss vector: "<<xGauss.size()<<std::endl;
  for( i=0;i<(int)xGauss.size();i=i+3)
  {
      //correct for x0
      xGauss.at(i)=xGauss.at(i)+x0;
      cout<<"Mu: "<<xGauss.at(i);
      cout<<" Sigma: "<<xGauss.at(i+1); 
      cout<<" C: "<<xGauss.at(i+2)<<std::endl;
  }
  delete fTest;
  
  //calculate one Dim fit for y   
  fTest = new GaussFitClass();  
  fTest->setBeamShapeFromDataArrayToRealData(beamShapeDataArray[1], NumDataPointsInArray, stepSizeOfDataPoints);
  fTest->numericalFit(numbOfSubBeams,initFWHMY,yGauss);
  
  //cout<<"size of yGauss vector: "<<yGauss.size()<<std::endl;
  for(i=0;i<(int)yGauss.size();i=i+3)
  {
      //correct for y0
      yGauss.at(i)=yGauss.at(i)+y0;
//      cout<<"Mu: "<<yGauss.at(i);
//      cout<<" Sigma: "<<yGauss.at(i+1); 
//      cout<<" C: "<<yGauss.at(i+2)<<std::endl;
  }
  delete fTest;
  
  Double_t weight;  
  Double_t weightTotal=0;
  k=0;
  //combine dimensions and 
  for(i=0;i<numbOfSubBeams;i++)//yloop
  {
      for(j=0;j<numbOfSubBeams;j++)//xloop
      {
          //cout<<"i:"<<i<<"j:"<<j<<std::endl;
          beamDataArray[k][0]=xGauss.at(j*3);//PosX0
          beamDataArray[k][1]=yGauss.at(i*3);//PosY0
          
          beamDataArray[k][2]=xGauss.at((j*3)+1);//SigmaX
          beamDataArray[k][3]=yGauss.at((i*3)+1);//SigmaY
          
          weight=xGauss.at((j*3)+2)* yGauss.at((i*3)+2);
          weightTotal=weightTotal+weight;
//          cout<<"gauss:"<<TMath::Gaus(x0, x0, initSigmaX, true)<<std::endl;
//          cout<<"cx:"<<xGauss.at((j*3)+2)<<std::endl;
//          cout<<"cy:"<<yGauss.at((i*3)+2)<<std::endl;
          //beamDataArray[k][4]= weight*initFluence; //old not normalized version, lead to to much fluence
          beamDataArray[k][4]= weight;
          beamDataArray[k][5]=initEnergy;
          k++;
      }
  }
  
  //weight needs to be renormalized to one 
  weight=0;
//Loop over all sub beams k redistribute weight
  Double_t sum=0;
  for (k=0;k<numbOfSubBeams*numbOfSubBeams;k++)
  {
    weight=1/weightTotal*beamDataArray[k][4];
    //cout<<"total weight: "<<weightTotal<<" old weight: "<<beamDataArray[k][4]<<" new weight: "<<weight<<" initFluence: "<<initFluence<<endl;
  
    beamDataArray[k][4]= weight*initFluence;
    
    printf("k: %i weight: %.2f x0: %.2f y0: %.2f sigx: %.2f sigy: %.2f fluence: %.2f energy: %.2f\n",
                        k, weight,beamDataArray[k][0],beamDataArray[k][1],
                        beamDataArray[k][2], beamDataArray[k][3],
                        beamDataArray[k][4],beamDataArray[k][5]);
    sum=sum+beamDataArray[k][4];
    cout<<"total fluence:"<<sum<<endl;
  }
  
    return k;
}
   
//////decomposes the initial gauss distributed beam in subBeams
//////spread is the maximum distance from the original beam axis in times of sigma 
//Int_t beamDecomposition(Double_t **beamDataArray, Double_t initSigmaX,Double_t initSigmaY,Double_t initFluence,
//        Double_t x0, Double_t y0, Int_t numOfSubBeamsOneDim,Double_t initEnergy)
//{
//    
//    
//    if (numOfSubBeamsOneDim==1)
//    {
//          beamDataArray[0][0]=x0;//PosX0
//          beamDataArray[0][1]=y0;//PosY0
//          
//          beamDataArray[0][2]=initSigmaX;//SigmaX
//          beamDataArray[0][3]=initSigmaY;//SigmaY
//
//          beamDataArray[0][4]=initFluence;
//          beamDataArray[0][5]=initEnergy;
//          
//          printf("k: %i weight: %f x0: %f y0: %f sigx: %f sigy: %f fluence: %f energy: %f\n",
//                        0, (Double_t)1,beamDataArray[0][0],beamDataArray[0][1],
//                        beamDataArray[0][2], beamDataArray[0][3],
//                        beamDataArray[0][4],beamDataArray[0][5]);   
//        
//     return 1;   
//    }    
//      Int_t i,j,k;
//      Int_t numbOfSubBeams;
//      std::vector<Double_t> xGauss,yGauss;
//      GaussFitClass *fTest;
//      
//      numbOfSubBeams=numOfSubBeamsOneDim;
//        
//  //calculate one Dim fit for x      
//  fTest = new GaussFitClass();      
//  Double_t fwhm;
//  fwhm=2*sqrt(2*log(2))/initSigmaX;
//  
//  fTest->setBeamShapeFromGaussFktToRealData(fwhm,0.1);
//  fTest->numericalFit(numbOfSubBeams,fwhm,xGauss);
//  
////  cout<<"size of xGauss vector: "<<xGauss.size()<<std::endl;
//  for( i=0;i<(int)xGauss.size();i=i+3)
//  {
//      //correct for x0
//      xGauss.at(i)=xGauss.at(i)+x0;
////      cout<<"Mu: "<<xGauss.at(i);
////      cout<<" Sigma: "<<xGauss.at(i+1); 
////      cout<<" C: "<<xGauss.at(i+2)<<std::endl;
//  }
//  delete fTest;
//  
//  //calculate one Dim fit for y   
//  fTest = new GaussFitClass();  
//  fwhm=2*sqrt(2*log(2))/initSigmaY;
//  
//  fTest->setBeamShapeFromGaussFktToRealData(fwhm,0.1);
//  //fTest->setBeamShapeFromDataArrayToRealData(Double_t *initialBeamShapeDataArray,Int_t NumDataPointsInArray,Double_t stepSizeOfDataPoints);
//  
//  fTest->numericalFit(numbOfSubBeams,fwhm,yGauss);
//  
//  //cout<<"size of yGauss vector: "<<yGauss.size()<<std::endl;
//  for(i=0;i<(int)yGauss.size();i=i+3)
//  {
//      //correct for y0
//      yGauss.at(i)=yGauss.at(i)+y0;
////      cout<<"Mu: "<<yGauss.at(i);
////      cout<<" Sigma: "<<yGauss.at(i+1); 
////      cout<<" C: "<<yGauss.at(i+2)<<std::endl;
//  }
//  delete fTest;
//  
//  Double_t weight;  
//  Double_t weightTotal=0;
//  k=0;
//  //combine dimensions and 
//  for(i=0;i<numbOfSubBeams;i++)//yloop
//  {
//      for(j=0;j<numbOfSubBeams;j++)//xloop
//      {
//          //cout<<"i:"<<i<<"j:"<<j<<std::endl;
//          beamDataArray[k][0]=xGauss.at(j*3);//PosX0
//          beamDataArray[k][1]=yGauss.at(i*3);//PosY0
//          
//          beamDataArray[k][2]=xGauss.at((j*3)+1);//SigmaX
//          beamDataArray[k][3]=yGauss.at((i*3)+1);//SigmaY
//          
//          weight=xGauss.at((j*3)+2)* yGauss.at((i*3)+2);
//          weightTotal=weightTotal+weight;
////          cout<<"gauss:"<<TMath::Gaus(x0, x0, initSigmaX, true)<<std::endl;
////          cout<<"cx:"<<xGauss.at((j*3)+2)<<std::endl;
////          cout<<"cy:"<<yGauss.at((i*3)+2)<<std::endl;
//          //beamDataArray[k][4]= weight*initFluence; //old not normalized version, lead to to much fluence
//          beamDataArray[k][4]= weight;
//          beamDataArray[k][5]=initEnergy;
//
//          k++;
//      }
//  }
//  
//  //weight needs to be renormalized to one 
//  weight=0;
////Loop over all sub beams k redistribute weight
//  for (k=0;k<numbOfSubBeams*numbOfSubBeams;k++)
//  {
//    weight=1/weightTotal*beamDataArray[k][4];
//    //cout<<"total weight: "<<weightTotal<<" old weight: "<<beamDataArray[k][4]<<" new weight: "<<weight<<" initFluence: "<<initFluence<<endl;
//  
//    beamDataArray[k][4]= weight*initFluence;
//    
//    printf("k: %i weight: %.2f x0: %.2f y0: %.2f sigx: %.2f sigy: %.2f fluence: %.2f energy: %.2f\n",
//                        k, weight,beamDataArray[k][0],beamDataArray[k][1],
//                        beamDataArray[k][2], beamDataArray[k][3],
//                        beamDataArray[k][4],beamDataArray[k][5]);
//  }
//  
//    return k;
//}
    
//////normalization is not important
void createBeamShapeArray(Double_t* initialBeamShapeDataArray,Int_t NumDataPointsInArray,Double_t stepSizeOfDataPoints,Double_t FWHM, Double_t Center)
{
    //Create Data Array

    //here we will use a Gaussian Fkt

   //convert passed FWHM to sigma
   Double_t initSigma;
   initSigma=FWHM/( 2*sqrt(2*log(2)) );
   //cout<<"initSigma calculated:"<<initSigma<<endl;
   
//   Double_t range;
//   range=NumDataPointsInArray*stepSizeOfDataPoints;

    Double_t x,startVal;
    startVal=Center-(Double_t)NumDataPointsInArray/2.*(Double_t)stepSizeOfDataPoints;
   for (Int_t i=0;i<NumDataPointsInArray;i++)//xdim
    {
     x=startVal+i*stepSizeOfDataPoints;  
     initialBeamShapeDataArray[i]=TMath::Gaus(x, Center, initSigma, true);
     //cout<<"x:"<<x<<" val: "<<initialBeamShapeDataArray[i]<<" centerval "<<TMath::Gaus(0.0, 0.0, 1, false)<<" initsigma "<<initSigma<<endl;
    }
    return;
}


//////Rectangular Function
//void createBeamShapeArray(Double_t* initialBeamShapeDataArray,Int_t NumDataPointsInArray,Double_t stepSizeOfDataPoints,Double_t FWHM, Double_t Center)
//{
//    //Create Data Array
//    
//    //here we will use a rectangular Fkt
//
//   //convert passed FWHM to sigma
//   Double_t initSigma;
//   initSigma=FWHM/( 2*sqrt(2*log(2)));
//   initSigma=initSigma*2;
//   //cout<<"initSigma calculated:"<<initSigma<<endl;
//   Double_t range;
//    range=NumDataPointsInArray*stepSizeOfDataPoints;
//
//    Double_t x,startVal;
//    startVal=Center-(Double_t)NumDataPointsInArray/2.*(Double_t)stepSizeOfDataPoints;   
//    
//    for (Int_t i=0;i<NumDataPointsInArray;i++)//xdim
//    {
//     x=startVal+i*stepSizeOfDataPoints;  
//     if( ( (x-Center) >(-1*initSigma) )&& ( (x-Center) <(initSigma) ) )    initialBeamShapeDataArray[i]= 1;
//     else initialBeamShapeDataArray[i]= 0;
//     //cout<<"x:"<<x<<" val: "<<initialBeamShapeDataArray[i]<<endl;
//    }
//    return;
//}



//////Trapezoid geht nicht
//void createBeamShapeArray(Double_t* initialBeamShapeDataArray,Int_t NumDataPointsInArray,Double_t stepSizeOfDataPoints,Double_t FWHM, Double_t Center)
//{
//    //Create Data Array
//    
//    //here we will use a rectangular Fkt
//
//   //convert passed FWHM to sigma
//   Double_t initSigma;
//   initSigma=FWHM/( 2*sqrt(2*log(2)));
//   initSigma=initSigma*2;
//   //cout<<"initSigma calculated:"<<initSigma<<endl;
//   Double_t range;
//    range=NumDataPointsInArray*stepSizeOfDataPoints;
//
//    Double_t x,startVal;
//    startVal=Center-(Double_t)NumDataPointsInArray/2.*(Double_t)stepSizeOfDataPoints;
//    
//    
//    Double_t startPlateau;
//    startPlateau=0.5*initSigma;
//
//    
//    for (Int_t i=0;i<(Int_t) NumDataPointsInArray/2;i++)//xdim
//    {
//     x=startVal+i*stepSizeOfDataPoints;  
//     if( ( (x-Center) >(-1*initSigma) )&& ( (x-Center) <(initSigma) ) )    
//     {
//         if( (x-Center) >(-1*startPlateau) && ( (x-Center) <(startPlateau) )  )   initialBeamShapeDataArray[i]= 1;
//         else initialBeamShapeDataArray[i]= x*3 - 39;
//     }
//     
//     else initialBeamShapeDataArray[i]= 0;
//     //cout<<"x:"<<x<<" val: "<<initialBeamShapeDataArray[i]<<endl;
//    }
//    return;
//}
    

#endif	/* PENCILBEAMFUNCTIONS */