/* 
 * File:   nucLUT.cpp
 * Author: main
 * 
 * Created on 29 November 2011, 12:59
 */

#include <nucLUT_class.h>

nucLUT::nucLUT() //default constructor
{
//    lutArrayEntries=5;//Number of elements in lutArray
//    setConfigFileName((char *)"dataAvailable.dat");
//    setFolderName((char *)"calibrationHelium/");
//    setDataFilePrefix((char *)"LatFit_GATE_Water_2mm_");
    iDimensionOfArray=0;

    setCorrectionFactorToCM(0.1);
    setStepSizeCalculation(0.05);//in cm
    setDimension(10000);    
    setStepSizeDataInCM(0.01);//data is in mm //set step Size automatically corrects to cm
    initializeLocalInterpolArray();
//    initializeLutArray();
//    initializeRawDataArray();

//    setTargetSigma(0.2);//in cm

}

nucLUT::nucLUT(Double_t targetEnergyToSet,Double_t targetSigmaToSet,Double_t stepSizeToSet) //constructor
{
//    lutArrayEntries=5;//Number of elements in lutArray
    setCorrectionFactorToCM(0.1);
    iDimensionOfArray=0;
    setDimension(10000);    
    setStepSizeDataInCM(stepSizeToSet);
    setStepSizeCalculation(0.05);//in cm
    initializeLocalInterpolArray();
//    initializeLutArray();
//    initializeRawDataArray();
    //interpolSigma=new ROOT::Math::Interpolator(0,ROOT::Math::Interpolation::kCSPLINE);
    setConfigFileName((char *)"dataAvailable.dat");
    setFolderName((char *)"calibrationHelium/");
    setDataFilePrefix((char *)"LatFit_GATE_Water_2mm_");
    
    readInEnergies();
    setTargetSigma(targetSigmaToSet);//in cm
    setTargetEnergy(targetEnergyToSet);
}

//destructor, deletes lutArray
nucLUT::~nucLUT()
{
    for(Int_t j=0; j<lutArrayEntries; j++) delete[] lutArray[j];
    delete[] lutArray;
    
    for(Int_t j=0; j<lutArrayEntries-1; j++) delete[] interpolLocalValues[j];
    delete[] interpolLocalValues;
    
    for(Int_t j=0; j<energyArraySize; j++) 
    {
        for(Int_t k=0; k<lutArrayEntries; k++) 
        {
         delete[] rawDataArray[j][k];
        }       

        delete[] rawDataArray[j];
    }
    delete[] rawDataArray;
}

//copy constructor
nucLUT::nucLUT(const nucLUT& orig)
{
    targetSigma=orig.targetSigma;
    lutSigma=orig.lutSigma;
    beamSigma=orig.beamSigma;    
    targetEnergy=orig.targetEnergy;
    stepSize=orig.stepSize;
    stepSizeCalculation=orig.stepSizeCalculation;
    correctionFactorToCM=orig.correctionFactorToCM;    
    numbOfParticlesInSource=orig.numbOfParticlesInSource;
    strcpy ( folder, orig.folder);
    strcpy ( configFileName, orig.configFileName);
    strcpy ( dataFilePrefix, orig.configFileName);  
    
    initializeLocalInterpolArray();   
    
    
    setDimension(orig.iDimensionOfArray); 
    iDimensionOfArray=orig.iDimensionOfArray;
      
    //copy data
    //    Double_t energyAvailArray[energyArraySize];
    for(int i=0; i<energyArraySize;i++)
    {
        energyAvailArray[i]=orig.energyAvailArray[i];
    }
    
    //Double_t **lutArray;
    for(Int_t j=0; j<lutArrayEntries; ++j) 
    {
        for(Int_t i=0; i<iDimensionOfArray; ++i) 
        {
          lutArray[j][i] = orig.lutArray[j][i];
        }
    }
    
    //Double_t ***rawDataArray;
    for(int i=0;i<energyArraySize;i++)
    {
       for(int j=0;j<lutArrayEntries;j++)
       {
        for(int k=0;k<iDimensionOfArray;k++)
        {
           rawDataArray[i][j][k]=orig.rawDataArray[i][j][k];
        }
       }
    }
    
    
}




//initialize the lut array
//now 5 entries
//0     Depth 
//1     RelationGauss/Voigt
//2     GausSigma
//3     VoigtSigma 
//4     VoigtLg
void nucLUT::initializeLutArray()
{
    lutArray = new Double_t*[lutArrayEntries];
    for(Int_t j=0; j<lutArrayEntries; ++j) 
    {
        lutArray[j] = new Double_t[iDimensionOfArray];
        for(Int_t i=0; i<iDimensionOfArray; ++i) 
        {
          lutArray[j][i] = 0.;
        }
    }
    return;
}

void nucLUT::deleteLutArray()
{
    if (iDimensionOfArray<=0) return;
    if (lutArrayEntries<=0) return;
    
    for(Int_t j=0; j<lutArrayEntries; j++) delete[] lutArray[j];
    delete[] lutArray;
    
    return;
}



void nucLUT::initializeRawDataArray()
{
    rawDataArray = new Double_t**[energyArraySize];
    for(int i=0;i<energyArraySize;i++)
    {
       rawDataArray[i] = new Double_t*[lutArrayEntries];
       for(int j=0;j<lutArrayEntries;j++)
       {
        rawDataArray[i][j] = new Double_t[iDimensionOfArray] ;
        for(int k=0;k<iDimensionOfArray;k++)
        {
           rawDataArray[i][j][k]=0.;
        }
       }
    }
    return;
}

void nucLUT::deleteRawDataArray()
{
    if (iDimensionOfArray<=0) return;
    if (lutArrayEntries<=0) return;
    if (energyArraySize<=0) return;
    
    for(Int_t j=0; j<energyArraySize; j++) 
    {
        for(Int_t k=0; k<lutArrayEntries; k++) 
        {
         delete[] rawDataArray[j][k];
        }       
        delete[] rawDataArray[j];
    }
    delete[] rawDataArray;
    
    return;
}

//initialize interpolator array for local interpolation
void nucLUT::initializeLocalInterpolArray()
{
    //interpolLocalValues=new ROOT::Math::Interpolator*[lutArrayEntries-1];
    interpolLocalValues=new ROOT::Math::Interpolator*[lutArrayEntries];
    
    for(Int_t i=0;i<lutArrayEntries;i++)
    {
      //interpolLocalValues[i]=new ROOT::Math::Interpolator(0,ROOT::Math::Interpolation::kCSPLINE);
       interpolLocalValues[i]=new ROOT::Math::Interpolator(0,ROOT::Math::Interpolation::kLINEAR); 
    }
//    cout<<"initalized local interpol array"<<endl;
//    ROOT::Math::Interpolator *interpolEdep;
//    interpolEdep=new ROOT::Math::Interpolator(0,ROOT::Math::Interpolation::kCSPLINE);
}

void nucLUT::deleteLocalInterpolArray()
{
    if (lutArrayEntries<=0) return;
    for(Int_t j=0; j<lutArrayEntries-1; j++) delete[] interpolLocalValues[j];
    delete[] interpolLocalValues;
    return;
}


void nucLUT::readInAllDataFilesIntoRawDataArray()
{
    char fileNameLowerEnergy[filenameTotalSize];

    //read in data files
    //and store data in rawDataArray
    //--------------------------------------------------------------------------
//    cout<<"starting to read in all data files into rawDataArray"<<endl;

    for(Int_t lowerEnergyIdx=0;lowerEnergyIdx<energyArraySize;lowerEnergyIdx++)
    {
//        cout<<"at energy idx "<<lowerEnergyIdx<<" energyAvailArray[lowerEnergyIdx] "<<energyAvailArray[lowerEnergyIdx]<<endl;
        if (energyAvailArray[lowerEnergyIdx]<=0.) continue;//skip if energy does not exist
        
        //create file name
        sprintf(fileNameLowerEnergy,"%s%s%03.0f%s",folder,dataFilePrefix,energyAvailArray[lowerEnergyIdx],".dat");
//        cout<<"fileName "<<fileNameLowerEnergy<<" idx "<<lowerEnergyIdx<<endl;
        //read in Lower energy data                 
        readInDataFromFileToLutArrayFormat(fileNameLowerEnergy,rawDataArray[lowerEnergyIdx]);
    }
//    cout<<"finished reading in all data files into rawDataArray"<<endl;
   return;
}


Double_t nucLUT::setCorrectionFactorToCM(Double_t correction)
{
    correctionFactorToCM=correction;
    return correctionFactorToCM;
}

Int_t nucLUT::setDimension(Int_t dimension)
{
        deleteRawDataArray();
        deleteLutArray();
    iDimensionOfArray=dimension;
        initializeLutArray();
        initializeRawDataArray();
    return iDimensionOfArray;
}

//take sets to Step Size in increments of data (if mm) then usually mm
Double_t nucLUT::setStepSizeData(Double_t step)
{
    stepSize=step;// /correctionFactorToCM;
    return stepSize;
}

//takes cm and converts to correct step size
Double_t nucLUT::setStepSizeDataInCM(Double_t step)
{
    stepSize=step/correctionFactorToCM;
    return stepSize;
}

Double_t nucLUT::setStepSizeCalculation(Double_t step)
{
    stepSizeCalculation=step;// /correctionFactorToCM;
    return stepSizeCalculation;
}

//sets the target sigma of the Beam
Double_t nucLUT::setTargetSigma(Int_t targetSigmaToSet)
{
    targetSigma=targetSigmaToSet;
    return targetSigma;
}

//sets the initial sigma of the look-up table in cm
//set initial beam sigma of LUT data in cm
Double_t nucLUT::setLutSigma(Double_t lutSigmaToSet)
{
    lutSigma=lutSigmaToSet;
    return lutSigma;
}

//sets the sigma of the current beam in cm
Double_t nucLUT::setBeamSigma(Double_t beamSigmaToSet)
{
    beamSigma=beamSigmaToSet;
    return beamSigma;
}

void nucLUT::setConfigFileName(char* filename)
{
    strcpy ( configFileName, filename);
//    cout<<"setConfigFileName: starting to read in energies"<<endl;
//    readInEnergies();//read in available energies
    return;
}
//sets Folder Name name must include backslash
void nucLUT::setFolderName(char* foldername)
{
    strcpy ( folder, foldername);
    return;
}

void nucLUT::setDataFilePrefix(char* filePrefix)
{
    strcpy ( dataFilePrefix, filePrefix);
    return;
}

//set data to interpolation functions to enable interpolation of local values
void nucLUT::setinterpolLocalValuesData()
{
    //cout<<"setting local interpol array"<<endl;
    for(Int_t i=1;i<lutArrayEntries;i++)
    {
        //create one data set containing of position and value
        interpolLocalValues[i]->SetData(iDimensionOfArray, lutArray[0], lutArray[i]);
//        for(Int_t j=0;j<iDimensionOfArray;j++)
//        {
//            //if (i==1) cout<<"xValues: "<<lutArray[0][j]<<" yValue: "<<lutArray[i][j]<<endl;
//        }
    }
    //cout<<"setted local interpol array"<<endl;
    return;
}

//sets target energy and recalculates Look Up Table
void nucLUT::setTargetEnergy(Double_t energyToSet)  
{
    if (targetEnergy==energyToSet) return;
    //find next energy
    	Int_t idx;
	idx=TMath::BinarySearch(energyArraySize,energyAvailArray, energyToSet);
//        if (idx>=energyArraySize) idx=energyArraySize-1;//ensure index is within bounds of array
//        if (idx<0) idx=0;//ensure index is within bounds of array
	//if energy found is lower than available energy, print error and use lowest available
	if(energyAvailArray[idx]<=0)
	{
	 std::cerr<<"WARNING: nucLUT class  Requested energy: "<<energyToSet<<" too low, using "<< energyAvailArray[0]<<" instead!"<<endl;
	 targetEnergy=energyAvailArray[idx+1];
	}
        else if(idx>=energyArraySize)
        {
         std::cerr<<"WARNING: nucLUT class  Requested energy: "<<energyToSet<<" too high, using "<< energyAvailArray[energyArraySize-1]<<" instead!"<<endl;
         targetEnergy=energyAvailArray[energyArraySize-1];
        }
	else targetEnergy=energyToSet;
       
//    cout<<"nucLUT calculating LUT"<<endl;
    calculateLUT();//create look up table for this energy
//    cout<<"nucLUT calculated"<<endl;
    setinterpolLocalValuesData();//setup interpolation of local values
 return;   
}

//returns current initial target energy
Double_t nucLUT::getTargetEnergy()  
{
 return targetEnergy;   
}

//interpolLocalValues
//now 4 entries
//1     RelationGauss/Voigt
//2     GausSigma
//3     VoigtSigma 
//4     VoigtLg
//returns relation of GaussAndVoigtAmplitude
Double_t nucLUT::getGaussVoigtRelation(Double_t position)
{
    //cout << stepSizeCalculation<<" "<<stepSize<<endl;
    if (position < 0.0 || position > lutArray[0][iDimensionOfArray-1]) 
    {
        std::cerr<<"WARING: nucLUT class passed position "<<position<<" out of bounds. Returning 0."<<endl;        
        return 0.;
    }
    return interpolLocalValues[1]->Eval(position);
}

//returns interpolated Gauss sigma at specified position
Double_t nucLUT::getGaussSigma(Double_t position)
{
    //cout << stepSizeCalculation<<" "<<stepSize<<endl;
    if (position < 0.0 || position > lutArray[0][iDimensionOfArray-1]) 
    {
        std::cerr<<"WARING: nucLUT class passed position "<<position<<" out of bounds. Returning 0."<<endl;
        return 0.;
    }
    return interpolLocalValues[2]->Eval(position)*correctionFactorToCM;
}

//returns interpolated voigt sigma at specified position
Double_t nucLUT::getVoigtSigma(Double_t position)
{
    //cout << stepSizeCalculation<<" "<<stepSize<<endl;
    if (position < 0.0 || position > lutArray[0][iDimensionOfArray-1]) 
    {
        std::cerr<<"WARING: nucLUT class passed position "<<position<<" out of bounds. Returning 0."<<endl;
        return 0.;
    }
    return interpolLocalValues[3]->Eval(position)*correctionFactorToCM;
}

//returns interpolated voigt lagrangian at specified position
Double_t nucLUT::getVoigtLg(Double_t position)
{
    //cout << stepSizeCalculation<<" "<<stepSize<<endl;
    if (position < 0.0 || position > lutArray[0][iDimensionOfArray-1]) 
    {
        std::cerr<<"WARING: nucLUT class passed position "<<position<<" out of bounds. Returning 0."<<endl;
        return 0.;
    }
    return interpolLocalValues[4]->Eval(position)*correctionFactorToCM;
}

//returns interpolated values for all values
void nucLUT::getAllParameters(Double_t position, Double_t& voigtGaussRelation, Double_t& gaussSigma, Double_t& voigtSigma, Double_t& voigtLg)
{
// lut array
//now 5 entries
//0     Depth 
//1     RelationGauss/Voigt
//2     GausSigma
//3     VoigtSigma 
//4     VoigtLg
    
  //find position in gauss
    int idx=TMath::BinarySearch(iDimensionOfArray,lutArray[0],position);
                //check that index is within bounds 
                if (idx<0 ||idx>=iDimensionOfArray) {
                    cerr<<"ERROR: nuclLUT_class: index out of bounds. Aborting."<<endl;
                    exit(-1);
                }
    
        double diffTot=lutArray[0][idx+1]-lutArray[0][idx];
        double diffVal=position-lutArray[0][idx];
        double interpolPolFactor=diffVal/diffTot;
        double diffDataVal;
        
        //1     RelationGauss/Voigt
        diffDataVal=lutArray[1][idx+1]-lutArray[1][idx];            
        voigtGaussRelation=lutArray[1][idx]+interpolPolFactor*diffDataVal;
    
        //2     GausSigma
        diffDataVal=lutArray[2][idx+1]-lutArray[2][idx];            
        gaussSigma=(lutArray[2][idx]+interpolPolFactor*diffDataVal)*correctionFactorToCM;
        
        //3     VoigtSigma 
        diffDataVal=lutArray[3][idx+1]-lutArray[3][idx];            
        voigtSigma=(lutArray[3][idx]+interpolPolFactor*diffDataVal)*correctionFactorToCM;
        
        //4     VoigtLg 
        diffDataVal=lutArray[4][idx+1]-lutArray[4][idx];            
        voigtLg=(lutArray[4][idx]+interpolPolFactor*diffDataVal)*correctionFactorToCM;
    return;
}


////reads out lut array from file and returns separate position array and full value array
////necessary for interpolation between two different energies
//void nucLUT::getLUTarraySplittedUp(char* filename,Double_t *positionArray,Double_t **valueArray)
//{
//   
//    //printf("reading: %s\n",filename);       
//    readInDataFromFileToLutArrayFormat(filename,valueArray);
//    //printf("splitting\n"); 
//    for(Int_t i=0;i<iDimensionOfArray;i++)
//    {
//        positionArray[i]=valueArray[0][i];
//    }    
//
// return;   
//}

//reads out lut array from file and returns only value part
void nucLUT::getLUTarraySplittedUp(Int_t targetEnergyIdx, Double_t *positionArray,Double_t **valueArray)
{
    
    if ( targetEnergyIdx>=energyArraySize || targetEnergyIdx<0 ) //check whether value exists, abort if not
        {
            cerr<<"ERROR: Requested energy of "<<targetEnergy<<" MeV was not found. Aborting."<<endl;
            exit(-1);
        }

//    cout<<"retrieving data for energy "<< energyAvailArray[targetEnergyIdx]<<" at index "<<targetEnergyIdx<<endl;
    
    memcpy( positionArray, rawDataArray[targetEnergyIdx][0], iDimensionOfArray * sizeof(Double_t) );
    
//    for(int i=0;i<iDimensionOfArray;i++)
//    {
//        cout<<"position array: i "<<i<<" value "<<rawDataArray[targetEnergyIdx][0][i]<<endl;
//    }
    for(int i=0;i<lutArrayEntries;i++)
    {        
    memcpy( valueArray[i], rawDataArray[targetEnergyIdx][i], iDimensionOfArray * sizeof(Double_t) );
    }
//        for(int i=0;i<iDimensionOfArray;i++)
//    {
//        cout<<"0 "<<lutArray[0][i]<<" 1 "<<lutArray[1][i]<<
//                " 2 "<<lutArray[2][i]<<
//                " 3 "<<lutArray[3][i]<<
//                " 4 "<<lutArray[4][i]<<endl;
//    }
//    

 return;   
}


//functions reads in the available energies
//returns number of energies available, -1 on error
Int_t nucLUT::readInEnergies()
{
   char filename[filenameTotalSize];
   sprintf(filename,"%s%s",folder,configFileName);          
   Int_t i;     
   Int_t indexArray[energyArraySize];
   Char_t line[maxNumberOfCharsToRead];
   Double_t temlutArray[energyArraySize],energy;

   // open ASCII file
    FILE *fp = fopen(filename,"r");
    if(!fp) 
    {
        printf("File %s could not be opened. Aborting. \n",filename);
        exit(-1);
        return -1;
    }

    //initialize Arrays
    for(i=0;i<energyArraySize;i++)
    {
       energyAvailArray[i]=0;
       temlutArray[i]=0;
       indexArray[i]=0;
    }

    //read in available energies
    i=0;
    while (fgets(line,maxNumberOfCharsToRead,fp)) 
    {
        //skip line and go to next line if starting with #
        if (line[0] == '#') continue;
        //if (line[0] == '0') continue;
        sscanf(&line[0],"%lf",&energy);
        //Store read value in array
        //printf("%f \n",energy);
        temlutArray[i]=energy;
        i++;
    }
    //sort array
    TMath::Sort(energyArraySize, temlutArray, indexArray,false);
    for(i=0;i<energyArraySize;i++)
    {
       energyAvailArray[i]=temlutArray[indexArray[i]];
    }
    readInAllDataFilesIntoRawDataArray();
    return i;//number of energies set
}            



//Returns the number of rows in the file
//empty rows and rows starting with # are ignored
//-1 on error
Int_t nucLUT::countRowsInFile(char* filename)
{
    FILE *df;
    Int_t counter=0;
    char line[maxNumberOfCharsToRead];

    df=fopen(filename,"r");
    if(!df) 
    {
        printf("File %s could not be opened. Aborting. \n",filename);
        exit(-1);
        return -1;
    }

    //read in files
    //count line numbers
    while((fgets(line,maxNumberOfCharsToRead,df)) != NULL) 
    {
      if (line[0] == '#') continue;
      if (line[0] == '\n') continue;
      counter++;
    }
    fclose (df);
    //printf("%i lines found in %s\n",counter,filename);
 return counter;
}

//function reads in data file with two columns
//first column is length, second deposited energy
//function returns number of created entries
//dataArray[2][numberOfEntries]
//ATTENTION: Does only read iDimensionArray values from file, if more, data will be ignored
Int_t nucLUT::readInDataFromFileToLutArrayFormat(char* filename,Double_t** dataArray)
{
    Int_t colToRead=13;
    Int_t readLines;
    //initialize temporary array
    Double_t **tempArray;  
    tempArray = new Double_t*[colToRead];
    for(Int_t j=0; j<colToRead; ++j) 
    {
        tempArray[j] = new Double_t[iDimensionOfArray];
        for(Int_t i=0; i<iDimensionOfArray; ++i) 
        {
          tempArray[j][i] = 0.;
        }
    }
    //read in look-up table   
    readLines=readInDataColFromFile(filename,tempArray,colToRead, iDimensionOfArray);
//cout<<"iDim: "<<    iDimensionOfArray<<" readLines: "<<readLines<<endl;
//    tempArrayStrukture:
//    0   Depth
//    1   GaussAmp        2       GaussAmpError
//    3   Mu              4       MuError
//    5   GaussSigma      6       GaussSigmaError
//    7   VoigtAmp        8       VoigtAmpError
//    9   VoigtSigma      10      VoigtSigmaError
//    11   VoigtLg        12      VoigtLgError
    
//        luArrayStrukture:
    //now 5 entries
    //0     Depth 
    //1     RelationGauss/Voigt
    //2     GausSigma
    //3     VoigtSigma 
    //4     VoigtLg
    //convert look-up table to lut array
    for (Int_t i=0;i<iDimensionOfArray;i++)
    {
//        cout<<"readInDataFromFileToLutArrayFormat index: "<<i<<endl;
        //0     Depth 
        dataArray[0][i]=tempArray[0][i]*correctionFactorToCM;//to convert mm in cm
        //1     Relation GaussAmp/VoigtAmp
        //If Gauss Amplitude is not normalized
        //dataArray[1][i]=(tempArray[5][i]*sqrt(2*constPI)*tempArray[1][i])/tempArray[7][i];
        //if Gauss Amplitude is normalized
        //dataArray[1][i]=(tempArray[5][i]*tempArray[1][i])/tempArray[7][i];
        if (tempArray[7][i]<=0.)
        {
        dataArray[1][i]=DBL_MAX;
        }
        else if (tempArray[1][i]<=0.)
        {
        dataArray[1][i]=DBL_MIN;
        }
        else dataArray[1][i]=(tempArray[1][i])/tempArray[7][i];
            
//        if ( isnan(dataArray[1][i]) ||  isinf(dataArray[1][i]) ) 
//        {
//                cout<<"ERRORintLUT dataArray[1][i] inf or nan :"<< dataArray[1][i]<<endl;
//                cout<<"gaussAmp: "<< tempArray[1][i]<<"voigtAmp: "<<tempArray[7][i] <<endl;
//                cout<<"divided through dbl_min: "<< DBL_MIN<<" result " <<(tempArray[1][i])/DBL_MIN<<endl;
//                cout<<"dbl_max: "<<DBL_MAX<<endl;
//        }
        //2     GausSigma
        dataArray[2][i]=tempArray[5][i];
        //3     VoigtSigma 
        dataArray[3][i]=tempArray[9][i];
        //4     VoigtLg
        dataArray[4][i]=tempArray[11][i];            
    }

for(Int_t j=0; j<colToRead; ++j) 
    {
    delete[] tempArray[j];
    }
delete[] tempArray;

    return readLines;
}       

//function reads in data file with multiple columns
//number of columns to read has to be passed to function
//function returns number of created entries
//dataArray[entriesPerLine][numberOfEntries]
//ATTENTION: Does only read iDimensionArray values from file, if more, data will be ignored
Int_t nucLUT::readInDataColFromFile(char* filename,Double_t** dataArray, Int_t numColumns, Int_t numLinesToStore)
{
    std::stringstream parseBuf;//Stringstream Buffer which will be parsed
    string inBuf;//String Buffer for reading
    fstream ifp;
    ifp.open(filename,ios::in);//open file
    
    if (!ifp) //if file can not be opened print error and exit
    {
        std::cerr<<"ERROR: Unable to open file: "<<filename<<endl;
        exit(-1);
        return 0;
    }
    
    Int_t linesRead=0;
    while (getline(ifp,inBuf) && linesRead<(numLinesToStore) )
    {
        if (inBuf[0]=='#') continue; //skip #lines
        if (inBuf[0]=='\n') continue; //skip empty lines

        parseBuf.str(inBuf);
        for (Int_t i=0;i<numColumns;i++) 
        {
          //cout<<"i "<<i<<endl;  
          parseBuf>>dataArray[i][linesRead];
        }
        ++linesRead;
        //cout<<"linesread: "<<linesRead<<endl;
        
    }
    ifp.close();
    
    if (linesRead<numLinesToStore) 
    {
        cerr<<"WARNING: Only "<<linesRead<<" lines read from "<<filename<<" instead of expected "<<numLinesToStore<<" !!!"<<endl;
//        exit(-1);
    }

    return linesRead;
}

////takes passed energy, if energy is not found in lut tables
////takes all existing lut data files and interpolate the correct energy
////ATTENTION: requires all array to have the same dimensions
//void nucLUT::interpolateLUTArray(Double_t energy,Double_t** interpolDataArray)
//{
//    Int_t i,j,k,l,valueArrayDimension=0;
////    char filename[filenameTotalSize];
//
//    //create position and value arrays
//    //positions are always the same, therefore only one needed
//    Double_t* positionArray = new Double_t[iDimensionOfArray];
//    
//    Double_t*** valueArray = new Double_t**[energyArraySize];
//    for (i=0;i<energyArraySize;i++)
//    {
//        valueArray[i] = new Double_t*[lutArrayEntries];
//        for (j=0;j<lutArrayEntries;j++)
//        {
//          valueArray[i][j] = new Double_t[iDimensionOfArray];
//        }
//        //count number of used energies
//        if(energyAvailArray[i]>0) valueArrayDimension++;
//    }
//    
////    //count number of used energies
////    for (i=0;i<energyArraySize;i++)
////    {
////      if(energyAvailArray[i]<=0) {continue;}
////      valueArrayDimension++;
////    }
//    
//    //printf("found %i energies\n",valueArrayDimension);
////    //initialize interpolator
////    ROOT::Math::Interpolator interpol((unsigned int)valueArrayDimension, ROOT::Math::Interpolation::kLINEAR);
//    //create interpolation arrays
//    Double_t *vectorX = new Double_t[valueArrayDimension];  
////    Double_t *vectorY = new Double_t[valueArrayDimension];
//
//    //read in value arrays for all energies, skip if energy 0 or below
//    
//    //retrieve position part, the same for all energies
//    bool gotPositionArray=false;
//    k=0;
//    for (i=0;i<energyArraySize;i++)
//    { 
//      if(energyAvailArray[i]<=0) { continue;}
//      if (gotPositionArray==false )    
//        { memcpy( positionArray, rawDataArray[i][0], iDimensionOfArray * sizeof(Double_t) );gotPositionArray=true;}
//
//      //valueArray[k] = new Double_t[iDimensionOfArray];
//      vectorX[k]=energyAvailArray[i];
//      //create filename
////      sprintf(filename,"%s%s%03.0f%s",folder,dataFilePrefix,energyAvailArray[i],".dat");
//      //read in lut
////      getLUTarraySplittedUp(i,positionArray,valueArray[k]);
//      
//      for(int j=1;j<lutArrayEntries;j++)        memcpy( valueArray[k][j], rawDataArray[i][j], iDimensionOfArray * sizeof(Double_t) );
//      //readInDataFromFileToLutArrayFormat(char* filename,Double_t** dataArray)
//      //printf("read in energy: %f\n",energyAvailArray[i]);
//      k++;
//    }
//    valueArrayDimension=k;
//  
//    
//    #pragma omp parallel private(i,l,j,k) shared(interpolDataArray,valueArrayDimension,energy)
//  {
//
//  #pragma omp for schedule(static) nowait
////#pragma omp for schedule(runtime) nowait
////  #pragma omp for schedule(guided) nowait
//    
//    for (i=0;i<iDimensionOfArray;i++)
//    {
//        //initialize interpolator
//        ROOT::Math::Interpolator interpol((unsigned int)valueArrayDimension, ROOT::Math::Interpolation::kLINEAR);
//        Double_t *vectorY = new Double_t[valueArrayDimension];
//        for(l=1;l<lutArrayEntries;l++)
//        {
//            k=0;
//            for (j=0;j<energyArraySize;j++)//add values to interpolation array
//            {
//                if(energyAvailArray[j]<=0) {continue;}
//                vectorY[k]=valueArray[k][l][i]; 
//                k++;
//            }
//            
////            if(l==1)
////            {
//            interpol.SetData(valueArrayDimension, vectorX, vectorY);
//            //interpolDataArray[0][i]=positionArray[i];
//            if (l>0) interpolDataArray[l][i]=interpol.Eval(energy);
//            
//            int idx=TMath::BinarySearch(valueArrayDimension,vectorX,energy);
//            //check that index is within bounds 
//            if (idx<0 ||idx>=valueArrayDimension) {
//                cerr<<"ERROR: nuclLUT_class: index out of bounds. Aborting."<<endl;
//                exit(-1);
//            }
//            //determine multiplication value
//            cout<<"vecEnergy1 "<<vectorX[idx]<<" vecnextEnergy "<<vectorX[idx+1]<<" targetEnergy "<< energy<<endl;
//            cout<<"befValue: "<< vectorY[idx-1]<<" origValue "<<vectorY[idx]<<" nextValue "<<vectorY[idx+1]<<endl;
//            
//            
//            double diffTot=vectorX[idx+1]-vectorX[idx];
//            double diffIntVal=energy-vectorX[idx];
//            double intPolFactor=diffIntVal/diffTot;
//            
//            double diffDataVal=vectorY[idx+1]-vectorY[idx];
//            
//            double newVal=vectorY[idx]+intPolFactor*diffDataVal;
//            
//            cout<<"interpolValue: "<<interpolDataArray[l][i]<<" guessedValue "<<newVal<<" intpolFactor "<<intPolFactor<<endl;
//            
//            
////            }
////            else
////            {
////                
////            }
//            
//            
//            
//            
//        }
//        delete[] vectorY;
//    }
//    
//  }
//    memcpy( interpolDataArray[0], positionArray, iDimensionOfArray * sizeof(Double_t) );
//
//
//    //delete array
//    delete[] positionArray;
//    for (i=0;i<energyArraySize;i++)
//    {
//        for (j=0;j<lutArrayEntries;j++)
//        {
//           delete[] valueArray[i][j]; 
//        }
//        delete[] valueArray[i];
//    }    
//    delete[] valueArray;
//    delete[] vectorX;
////    delete[] vectorY;
//
//    return;
//}     






//takes passed energy, if energy is not found in lut tables
//takes all existing lut data files and interpolate the correct energy
//ATTENTION: requires all array to have the same dimensions
void nucLUT::interpolateLUTArray(Double_t energy,Double_t** interpolDataArray)
{
    Int_t i,j,k,l;  
    bool gotPositionArray=false;
    int idx;
    double diffTot,diffIntVal,intPolFactor;
    double diffDataVal,newVal;
    
//#if PREPROCESSOR_PRECISION == 1    
//    #pragma omp parallel private(i,l,j,k) shared(interpolDataArray,energy)
//  {
////  #pragma omp for schedule(static) nowait
//// #pragma omp for schedule(runtime) nowait
//  #pragma omp for schedule(guided) nowait
//#endif    
    for (i=0;i<iDimensionOfArray;i++)
    {

        for(l=1;l<lutArrayEntries;l++)
        {

            if(l==1)
            {
                 idx=TMath::BinarySearch(energyArraySize,energyAvailArray,energy);
                //check that index is within bounds 
                if (idx<0 ||idx>=energyArraySize) {
                    cerr<<"ERROR: nuclLUT_class: index out of bounds. Aborting."<<endl;
                    exit(-1);
                }
                 //energyArraySize

                 diffTot=energyAvailArray[idx+1]-energyAvailArray[idx];
                 diffIntVal=energy-energyAvailArray[idx];
                 intPolFactor=diffIntVal/diffTot;
            }
            
            diffDataVal=rawDataArray[idx+1][l][i]-rawDataArray[idx][l][i];
            
            newVal=rawDataArray[idx][l][i]+intPolFactor*diffDataVal;
            interpolDataArray[l][i]=newVal;
            if (gotPositionArray==false )    
                 { memcpy( interpolDataArray[0], rawDataArray[idx][0], iDimensionOfArray * sizeof(Double_t) );gotPositionArray=true;}
   
        }
    }
//#if PREPROCESSOR_PRECISION == 1    
//  }
//#endif

    return;
}   


















////takes passed energy, if energy is not found in lut tables
////takes all existing lut data files and interpolate the correct energy
////ATTENTION: requires all array to have the same dimensions
//void nucLUT::interpolateLUTArray(Double_t energy,Double_t** interpolDataArray)
//{
//    Int_t i,j,l;
////    char filename[filenameTotalSize];
//
//    //create position and value arrays
//    //positions are always the same, therefore only one needed
//    Double_t* positionArray = new Double_t[iDimensionOfArray];
//    
//    Double_t*** valueArray = new Double_t**[energyArraySize];
//    for (i=0;i<energyArraySize;i++)
//    {
//        valueArray[i] = new Double_t*[lutArrayEntries];
//        for (j=0;j<lutArrayEntries;j++)
//        {
//          valueArray[i][j] = new Double_t[iDimensionOfArray];
//        }
//    }
//
//    //printf("found %i energies\n",valueArrayDimension);
//    //initialize interpolator
//    ROOT::Math::Interpolator interpol((unsigned int)energyArraySize, ROOT::Math::Interpolation::kLINEAR);
//    //create interpolation arrays
//    Double_t *vectorY = new Double_t[energyArraySize];
//
//    //read in value arrays for all energies, skip if energy 0 or below
//    for (i=0;i<energyArraySize;i++)
//    {  
//      getLUTarraySplittedUp(i,positionArray,valueArray[i]);
//    }
//
//    for (i=0;i<iDimensionOfArray;i++)
//    {
//        for(l=1;l<lutArrayEntries;l++)
//        {
//            for (j=0;j<energyArraySize;j++)//add values to interpolation array
//            {
//                vectorY[j]=valueArray[j][l][i]; 
//            }
//            interpol.SetData(energyArraySize, energyAvailArray, vectorY);
//            //interpolDataArray[0][i]=positionArray[i];
//            interpolDataArray[l][i]=interpol.Eval(energy);
//        }
//    }
//    memcpy( interpolDataArray[0], positionArray, iDimensionOfArray * sizeof(Double_t) );
//
//
//    //delete array
//    delete[] positionArray;
//    for (i=0;i<energyArraySize;i++)
//    {
//        for (j=0;j<lutArrayEntries;j++)
//        {
//           delete[] valueArray[i][j]; 
//        }
//        delete[] valueArray[i];
//    }    
//    delete[] valueArray;
//    delete[] vectorY;
//
//    return;
//}
   


//function recalculates the Look Up Table
//necessary parameters read from class variables
void nucLUT::calculateLUT()
{
    Int_t lowerEnergyIdx;
//    char fileNameLowerEnergy[filenameTotalSize];

    //find if energy is already available
    lowerEnergyIdx=TMath::BinarySearch(energyArraySize,energyAvailArray, targetEnergy);
//            if (lowerEnergyIdx>=energyArraySize) lowerEnergyIdx=energyArraySize-1;//ensure index is within bounds of array
//        if (lowerEnergyIdx<0) lowerEnergyIdx=0;//ensure index is within bounds of array

    //read in data files
    //and create LookUpTable
    //--------------------------------------------------------------------------

    //if energy is already in a file use it
    if(targetEnergy==energyAvailArray[lowerEnergyIdx])
    {
//        //create file name
//        sprintf(fileNameLowerEnergy,"%s%s%03.0f%s",folder,dataFilePrefix,energyAvailArray[lowerEnergyIdx],".dat");
//        //read in Lower energy data                 
//        readInDataFromFileToLutArrayFormat(fileNameLowerEnergy,lutArray);
        retrieveData(lowerEnergyIdx,lutArray);
        
    }    
    else //if energy is not yet on file interpolate
    {
    interpolateLUTArray(targetEnergy,lutArray);
    }

//        for (i=0;i<iDimensionOfArray;i++)
//        {
//        printf("%f value:%f \n",interpolLUTArray[0][i],interpolLUTArray[1][i]);
//        }
    return;
}

void nucLUT::retrieveData(Int_t targetEnergyIdx,Double_t** dataArray)
{
    cout<<"retrieving data for energy "<< energyAvailArray[targetEnergyIdx]<<" at index "<<targetEnergyIdx<<endl;
//    int idx=0;
//        idx=TMath::BinarySearch(energyArraySize,energyAvailArray, targetEnergy);
        if ( targetEnergyIdx>=energyArraySize || targetEnergyIdx<0 ) //check whether value exists, abort if not
        {
            cerr<<"ERROR: Requested index of energy of "<<targetEnergyIdx<<" was not found. Aborting."<<endl;
            exit(-1);
        }

        for(int i=0;i<lutArrayEntries;i++)
        {        
        memcpy( dataArray[i], rawDataArray[targetEnergyIdx][i], iDimensionOfArray * sizeof(Double_t) );
        }
//        for(int i=0;i<iDimensionOfArray;i++)
//        {
//          dataArray[0][i]= rawDataArray[targetEnergyIdx][0][i];
//          dataArray[1][i]= rawDataArray[targetEnergyIdx][1][i];
//        }
//        memcpy (dest,source,strlen(str1)+1);
//        dataArray=rawDataArray[targetEnergyIdx];
  return;
}