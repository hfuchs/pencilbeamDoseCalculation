#set xrange [0:200]
#plot './beamDataOutput130.dat' using 1:($2) w l,\
'./beamDataOutput132_3.dat' using 1:($2) w l,\
'./beamDataOutput134_6.dat' using 1:($2) w l,\
'./beamDataOutput140.dat' using 1:($2) w l,\
'./beamDataOutput145.dat' using 1:($2) w l,\
'./beamDataOutput.dat' using 1:($2/5) w p linecolor rgb "black"


set key top left
set xrange [0:200]

set style line 1 lt 1 lw 3 pt 3 linecolor rgb "blue"
set style line 2 lt 2 lw 3 pt 3 linecolor rgb "red"
set style line 3 lt 3 lw 3 pt 3 linecolor rgb "black"
set style line 4 lt 4 lw 3 pt 3 linecolor rgb "orange"
set style line 5 lt 5 lw 3 pt 3 linecolor rgb "brown"

plot './EdepHelium.dat' using 1:2 w l ls 1 title 'Physical',\
 './EdepHelium.dat' using 1:3 w l ls 2 title 'RBE interpol V1',\
'./EdepHelium.dat' using 1:4 w l ls 3 title 'RBE interpol new',\
'./EdepHeliumOld.dat' using 1:3 w l ls 4 title 'RBE steps'

set term pngcairo color dashed size 1400,1028
set output 'newRBE.png'
replot
